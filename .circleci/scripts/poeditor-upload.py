import sys
from optparse import OptionParser
from os import path
import os

parser = OptionParser()
parser.add_option("-f", "--file",       dest="file",        help="The file to upload.")
parser.add_option("-t", "--api-token",  dest="api_token",   help="poeditor api token.")
parser.add_option("-i", "--project-id", dest="project_id",  help="poeditor project id.")
(options, args) = parser.parse_args()

if path.exists(options.file) == False:
    print("File {0} not found.".format(options.file))
    sys.exit()

poe_command_pieces = []
poe_command_pieces.append("curl -s -X POST https://api.poeditor.com/v2/projects/upload")
poe_command_pieces.append("-F api_token={0}".format(options.api_token))
poe_command_pieces.append("-F id={0}".format(options.project_id))
poe_command_pieces.append("-F file=@'{0}'".format(path.abspath(options.file)))
poe_command_pieces.append("-F sync_terms=1")
poe_command_pieces.append("-F read_from_source=1")
poe_command_pieces.append("-F updating=terms_translations")
poe_command_pieces.append("-F language=en")

print os.popen(' '.join(poe_command_pieces)).read() + "\n"

