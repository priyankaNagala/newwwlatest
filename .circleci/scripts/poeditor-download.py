import sys
import subprocess
import os
from optparse import OptionParser
import json

parser = OptionParser()
parser.add_option("-l", "--language",   dest="language",    help="The language file to download.")
parser.add_option("-t", "--api-token",  dest="api_token",   help="poeditor api token.")
parser.add_option("-i", "--project-id", dest="project_id",  help="poeditor project id.")
parser.add_option("-f", "--file",       dest="local_file",  help="full path/filename for strings file (res directory).")
(options, args) = parser.parse_args()

if not options.language:
    print("Language not specified.")
    sys.exit()

poe_command_pieces = []
poe_command_pieces.append("curl -s -X POST https://api.poeditor.com/v2/projects/export")
poe_command_pieces.append("-F api_token={0}".format(options.api_token))
poe_command_pieces.append("-F id={0}".format(options.project_id))
poe_command_pieces.append("-F language={0}".format(options.language))
poe_command_pieces.append("-F filters=translated")
poe_command_pieces.append("-F type=xliff")

poe_command = ' '.join(map(str, poe_command_pieces)).split(' ')
output = json.loads(subprocess.check_output(poe_command))

# Download the file
download_url = output['result']['url']
download_file = "{0}.xml".format(options.language)
print("Download url for \"{0}\" language is: {1}".format(options.language, download_url))
output = subprocess.check_output("curl -s {0} -o {1}.xml".format(download_url, options.language).split(' '))

file_size = os.path.getsize(download_file)
if file_size > 0:
    os.rename(download_file, options.local_file)
else:
    print ("Download file was zero-length. Ignoring.")
