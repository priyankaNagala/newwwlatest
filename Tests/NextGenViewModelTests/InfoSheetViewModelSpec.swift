//
//  InfoSheetViewModelSpec.swift
//  nextgenTests
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Quick
import Nimble
import RxSwift

@testable import nextgen

class InfoSheetViewModelSpec: QuickSpec {
    let disposeBag = DisposeBag()

    /*
    // MARK: Stub
    class StubBikeModel: BikeModeling {
        var connected: Observable<Bool> { return _connected.asObservable()}
        var name: Observable<String> { return _name.asObservable()}
        var stateOfCharge: Observable<String> { return _stateOfCharge.asObservable()}
        var plugStatus: Observable<String> { return _plugStatus.asObservable()}
        var chargeActivity: Observable<String> { return _chargeActivity.asObservable()}
        
        private let _connected = PublishSubject<Bool>()
        private let _name = PublishSubject<String>()
        private let _stateOfCharge = PublishSubject<String>()
        private let _plugStatus = PublishSubject<String>()
        private let _chargeActivity = PublishSubject<String>()
        
        init() {
        }
        
        func connect() {
            _connected.onNext(true)
            _name.onNext("My Bike")
            _stateOfCharge.onNext("90%")
            _plugStatus.onNext("Unplugged")
            _chargeActivity.onNext("Not Charging")
        }
    }

    override func spec() {
        var viewModel: InfoSheetViewModel!
        beforeEach {
            viewModel = InfoSheetViewModel(bikeModel: StubBikeModel())
        }

        it("eventually sets cellModels property after loadDat.") {
            var cellModels: [TitledValueTableViewCellModeling]? = nil
            viewModel.cellModels.subscribe(onNext: {(modeling) in
                    cellModels = modeling
                }).disposed(by: self.disposeBag)
            viewModel.loadData()

            expect(cellModels).toEventuallyNot(beNil())
            expect(cellModels?.count).toEventually(equal(3))
            expect(cellModels?[0].title).toEventually(equal("State Of Charge"))
            expect(cellModels?[0].value).toEventually(equal("90%"))
        }
    }
 */
}
