//
//  DateUtilSpec.swift
//  nextgenTests
//
//  Created by Scott Wang on 2/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Quick
import Nimble
import RxSwift

@testable import nextgen

class DateUtilSpec: QuickSpec {

    override func spec() {
        
        it("Future times") {
            let fromDate = Date.init(timeIntervalSinceNow: 10)
            var str = DateUtil.timeAgo(date: fromDate)
            
            expect(str).to(beNil())
            
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str).to(beNil())
        }
        
        it("Justnow") {
            let fromDate = Date.init(timeIntervalSinceNow: -10)
            
            var str = DateUtil.timeAgo(date: fromDate)
            expect(str) == "Just now"

            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "10s"
        }

        it("Minutes ago") {
            var fromDate = Date.init(timeIntervalSinceNow: -10 * 60)
            
            var str = DateUtil.timeAgo(date: fromDate)
            expect(str) == "10 minutes ago"

            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "10m"

            fromDate = Date.init(timeIntervalSinceNow: -1 * 60)
            str = DateUtil.timeAgo(date: fromDate)
            expect(str) == "A minute ago"

            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "1m"
        }

        it ("Hours ago") {
            var fromDate = Date.init(timeIntervalSinceNow: -5 * 60 * 60)
            var str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "5 hours ago"

            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "5h"

            fromDate = Date.init(timeIntervalSinceNow: -1 * 60 * 60)
            str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "An hour ago"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "1h"
        }

        it ("Days ago") {
            var fromDate = Date.init(timeIntervalSinceNow: -4 * 60 * 60 * 24)
            var str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "4 days ago"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "4d"

            fromDate = Date.init(timeIntervalSinceNow: -1 * 60 * 60 * 24)
            str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "Yesterday"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "1d"
        }

        it ("Months ago") {
            var fromDate = Date.init(timeIntervalSinceNow: -7 * 60 * 60 * 24 * 30)
            var str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "7 months ago"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "7M"

            fromDate = Date.init(timeIntervalSinceNow: -1 * 60 * 60 * 24 * 30)
            str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "Last month"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "1M"
        }

        it ("Years ago") {
            var fromDate = Date.init(timeIntervalSinceNow: -3 * 60 * 60 * 24 * 365)
            var str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "3 years ago"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "3y"

            fromDate = Date.init(timeIntervalSinceNow: -1 * 60 * 60 * 24 * 365)
            str = DateUtil.timeAgo(date: fromDate)
            
            expect(str) == "Last year"
            str = DateUtil.timeAgoShort(date: fromDate)
            expect(str) == "1y"
        }
    }


}
