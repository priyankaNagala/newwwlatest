//
//  ScheduledChargingSpec.swift
//  nextgen
//
//  Created by Scott Wang on 9/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//


import Quick
import Nimble
@testable import nextgen

class ScheduledChargingSpec: QuickSpec {
    
    override func spec() {
        it ("getEndTimeAsString") {
            var endTime = ScheduledCharging.getEndTimeAsString(time: 1000, duration: 30, using24HrClock: true)
            expect(endTime) == "10:30"
            
            endTime = ScheduledCharging.getEndTimeAsString(time: 1000, duration: 60, using24HrClock: true)
            expect(endTime) == "11:00"

            endTime = ScheduledCharging.getEndTimeAsString(time: 900, duration: 180, using24HrClock: true)
            expect(endTime) == "12:00"

            endTime = ScheduledCharging.getEndTimeAsString(time: 1300, duration: 60, using24HrClock: true)
            expect(endTime) == "14:00"

            // Test wrap around midnight
            endTime = ScheduledCharging.getEndTimeAsString(time: 2000, duration: 600, using24HrClock: true)
            expect(endTime) == "6:00"

            endTime = ScheduledCharging.getEndTimeAsString(time: 1000, duration: 60, using24HrClock: false)
            expect(endTime) == "11:00AM"
            
            endTime = ScheduledCharging.getEndTimeAsString(time: 900, duration: 180, using24HrClock: false)
            expect(endTime) == "12:00PM"
            
            endTime = ScheduledCharging.getEndTimeAsString(time: 1300, duration: 60, using24HrClock: false)
            expect(endTime) == "2:00PM"
            
            // Test wrap around midnight
            endTime = ScheduledCharging.getEndTimeAsString(time: 2000, duration: 600, using24HrClock: false)
            expect(endTime) == "6:00AM"
            
            endTime = ScheduledCharging.getEndTimeAsString(time: 9999, duration: 60, using24HrClock: false)
            expect(endTime) == ""
            
            endTime = ScheduledCharging.getEndTimeAsString(time: 1400, duration: 24*60, using24HrClock: true)
            expect(endTime) == "14:00"

            endTime = ScheduledCharging.getEndTimeAsString(time: 1500, duration: 24*60, using24HrClock: false)
            expect(endTime) == "3:00PM"
        }
        
        it ("getDurationAsInt") {
            var duration = ScheduledCharging.getDurationAsInt(duration: "1h")
            expect(duration) == 60
            
            duration = ScheduledCharging.getDurationAsInt(duration: ".5h")
            expect(duration) == 30

            duration = ScheduledCharging.getDurationAsInt(duration: "2.5h")
            expect(duration) == 150
            
            duration = ScheduledCharging.getDurationAsInt(duration: "24h")
            expect(duration) == 1440
            
            duration = ScheduledCharging.getDurationAsInt(duration: "3h")
            expect(duration) == 180
            
            duration = ScheduledCharging.getDurationAsInt(duration: "abcd")
            expect(duration).to(beNil())
        }
        
        it ("getDurationAsString") {
            var duration = ScheduledCharging.getDurationAsString(60)
            expect(duration) == "1.0h"
            
            duration = ScheduledCharging.getDurationAsString(30)
            expect(duration) == "0.5h"

            duration = ScheduledCharging.getDurationAsString(180)
            expect(duration) == "3.0h"

            duration = ScheduledCharging.getDurationAsString(210)
            expect(duration) == "3.5h"

            duration = ScheduledCharging.getDurationAsString(1440)
            expect(duration) == "24.0h"

            duration = ScheduledCharging.getDurationAsString(420)
            expect(duration) == "7.0h"
        }
        
        it ("getTimeAsInt") {
            var time = ScheduledCharging.getTimeAsInt(time: "10:00", using24HrClock: true)
            expect(time) == 1000
            
            time = ScheduledCharging.getTimeAsInt(time: "22:00", using24HrClock: true)
            expect(time) == 2200

            time = ScheduledCharging.getTimeAsInt(time: "13:30", using24HrClock: true)
            expect(time) == 1330

            time = ScheduledCharging.getTimeAsInt(time: "3:00", using24HrClock: true)
            expect(time) == 300

            time = ScheduledCharging.getTimeAsInt(time: "0:00", using24HrClock: true)
            expect(time) == 0

            time = ScheduledCharging.getTimeAsInt(time: "10:00AM", using24HrClock: false)
            expect(time) == 1000

            time = ScheduledCharging.getTimeAsInt(time: "4:00PM", using24HrClock: false)
            expect(time) == 1600

            time = ScheduledCharging.getTimeAsInt(time: "3:30PM", using24HrClock: false)
            expect(time) == 1530

            time = ScheduledCharging.getTimeAsInt(time: "12:00AM", using24HrClock: false)
            expect(time) == 0
            
            time = ScheduledCharging.getTimeAsInt(time: "abc", using24HrClock: false)
            expect(time).to(beNil())
        }
        
        it ("getTimeAsString") {
            var time = ScheduledCharging.getTimeAsString(time: 100, using24HrClock: false)
            expect(time) == "1:00AM"
            
            time = ScheduledCharging.getTimeAsString(time: 100, using24HrClock: true)
            expect(time) == "1:00"
            
            time = ScheduledCharging.getTimeAsString(time: 1330, using24HrClock: false)
            expect(time) == "1:30PM"
            
            time = ScheduledCharging.getTimeAsString(time: 1330, using24HrClock: true)
            expect(time) == "13:30"

            time = ScheduledCharging.getTimeAsString(time: 030, using24HrClock: true)
            expect(time) == "0:30"

            time = ScheduledCharging.getTimeAsString(time: 030, using24HrClock: false)
            expect(time) == "12:30AM"
            
            time = ScheduledCharging.getTimeAsString(time: 3400, using24HrClock: true)
            expect(time) == ""
        }
        
        it ("enforceActiveCharging") {
            let scheduledCharging = ScheduledCharging.init(enabled: true, plugInStartTime: 100, plugtInDuration: 120, activeChargingStartTime: 200, activeChargingDuration: 120, daysOfWeek: [.mon], maxStateOfCharge: 0.75)
            
            scheduledCharging.activeChargingDuration = 24 * 60 // Past 24 hr of plug-in start
            expect(scheduledCharging.activeChargingDuration) == 23 * 60
            
            scheduledCharging.activeChargingStartTime = 600 // Beyond plug-in range
            expect(scheduledCharging.activeChargingStartTime) == 300
            
            scheduledCharging.activeChargingStartTime = 30
            expect(scheduledCharging.activeChargingStartTime) == 100
            
            // Test wrap around midnight
            scheduledCharging.plugInDuration = 300
            scheduledCharging.plugInStartTime = 2200
            scheduledCharging.activeChargingStartTime = 200
            expect(scheduledCharging.activeChargingStartTime) == 200
            
            scheduledCharging.activeChargingStartTime = 600
            expect(scheduledCharging.activeChargingStartTime) == 300
            
            // Test 24hr window
            scheduledCharging.activeChargingStartTime = 100
            scheduledCharging.plugInDuration = 60 * 24
            expect(scheduledCharging.activeChargingStartTime) == 100
            
            scheduledCharging.activeChargingStartTime = 1700
            expect(scheduledCharging.activeChargingStartTime) == 1700
        }
    }
}
