//
//  ScheduledChargingPacketSpec.swift
//  nextgen
//
//  Created by Scott Wang on 1/14/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//


import Quick
import Nimble
@testable import nextgen

class ScheduledChargingPacketSpec: QuickSpec {
    
    override func spec() {
        it ("getTime/LengthAsInt") {
            let packet = ScheduledChargingPacket()
            
            packet.activeChargeStartTime = 1
            packet.activeChargeLength = 1
            packet.plugInStartTime = 2
            packet.plugInLength = 2
            
            var time = packet.getActiveStartTimeAsInt()
            expect(time) == 30
            
            time = packet.getPlugInStartTimeAsInt()
            expect(time) == 100
            
            var length = packet.getActiveLengthAsInt()
            expect(length) == 30
            
            length = packet.getPlugInLengthAsInt()
            expect(length) == 60
            
            packet.activeChargeStartTime = 13
            time = packet.getActiveStartTimeAsInt()
            expect(time) == 630
            
            packet.activeChargeStartTime = 47
            time = packet.getActiveStartTimeAsInt()
            expect(time) == 2330

            packet.plugInLength = 22
            length = packet.getPlugInLengthAsInt()
            expect(length) == 660
        }
        
        it ("setTime/LengthWithInt") {
            let packet = ScheduledChargingPacket()
            
            packet.setActiveStartTimeWithInt(0)
            packet.setActiveLengthWithInt(0)
            packet.setPlugInStartTimeWithInt(30)
            packet.setPlugInLengthWithInt(30)
            
            expect(packet.activeChargeStartTime) == 0
            expect(packet.activeChargeLength) == 0
            expect(packet.plugInStartTime) == 1
            expect(packet.plugInLength) == 1
            
            packet.setActiveStartTimeWithInt(1000)
            expect(packet.activeChargeStartTime) == 20
            
            packet.setPlugInStartTimeWithInt(1330)
            expect(packet.plugInStartTime) == 27
            
            packet.setActiveLengthWithInt(210)
            expect(packet.activeChargeLength) == 7
            
            packet.setPlugInLengthWithInt(420)
            expect(packet.plugInLength) == 14
            
            packet.setActiveStartTimeWithInt(9999)
            expect(packet.activeChargeStartTime) == 0
            
            packet.setActiveLengthWithInt(9999)
            expect(packet.activeChargeLength) == 0
        }
    }
}
