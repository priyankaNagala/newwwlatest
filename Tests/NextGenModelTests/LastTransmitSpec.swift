//
//  LastTransmitSpec.swift
//  nextgenTests
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Quick
import Nimble
import Himotoki
@testable import nextgen

class LastTransmitSpec: QuickSpec {

    override func spec() {
        
        
        it("parses JSON data to create a new instance.") {
            let lastTransmit = try? LastTransmit.decodeValue(sampleStarcomJson)
            
            expect(lastTransmit).notTo(beNil())
            expect(lastTransmit?.name) == "496714 The 'ME' Bike"
            expect(lastTransmit?.unitNumber) == "496714"
            expect(lastTransmit?.dateTimeUtc).to(beNil())
            expect(lastTransmit?.lastSeen).notTo(beNil())
            expect(lastTransmit?.locationCoordinate).notTo(beNil())
            expect(lastTransmit?.pluggedin) == 0
            expect(lastTransmit?.charging) == false
            expect(lastTransmit?.chargingTimeLeft) == 20
            expect(lastTransmit?.stateOfCharge) == 80
            
        }
    }
}
