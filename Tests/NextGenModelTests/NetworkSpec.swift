//
//  NetworkSpec.swift
//  NextGenModelTests
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

import Quick
import Nimble
import RxAlamofire
import RxSwift

@testable import nextgen

class NetworkSpec: QuickSpec {
    override func spec() {
        var network: Network!
        let disposeBag = DisposeBag()

        beforeEach {
            network = Network()
        }

        describe("JSON") {
            it("eventually gets JSON data as specified with parameters.") {
                var json: [String: AnyObject]? = nil
                let url = "https://httpbin.org/get"
                network.requestJSON(url: url,
                                    parameters: ["a": "b" as AnyObject, "x": "y" as AnyObject],
                                    headers: nil)
                    .debug()
                    .subscribe(onNext: { json = $1 as? [String: AnyObject] })
                    .disposed(by: disposeBag)

                expect(json).toEventuallyNot(beNil(), timeout: 5)
                expect((json?["args"] as? [String: AnyObject])?["a"] as? String)
                    .toEventually(equal("b"), timeout: 5)
                expect((json?["args"] as? [String: AnyObject])?["x"] as? String)
                    .toEventually(equal("y"), timeout: 5)
            }
        }

    }
}
