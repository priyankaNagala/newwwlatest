//
//  DataService.swift
//  nextgen
//
//  Created by David Crawford on 6/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Nimble
import Quick
@testable import nextgen

class DataServiceSpec: QuickSpec {
    
    override func spec() {
        beforeEach {
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        }
        
        it("can store a value for multiple vins") {
            var currentMotorcycle = DataService.shared.currentMotorcycle
            //expect(currentMotorcycle).to(beNil())
            
            let vin = "12345";
            currentMotorcycle = DataService.shared.getMotorcycleData(vin: vin)
            expect(currentMotorcycle).notTo(beNil())
            expect(currentMotorcycle!.vin).to(equal(vin))
            expect(currentMotorcycle!.hasConnectedOnce).to(equal(false))
            
            let newVin = "23456";
            let newMotorcycle = DataService.shared.getMotorcycleData(vin: newVin)
            expect(newMotorcycle).notTo(beNil())
            expect(newMotorcycle!.vin).to(equal(newVin))
            expect(newMotorcycle!.hasConnectedOnce).to(equal(false))
            
            newMotorcycle?.hasConnectedOnce = true
            expect(currentMotorcycle!.hasConnectedOnce).to(equal(false))
            expect(newMotorcycle!.hasConnectedOnce).to(equal(true))
        }
        
        it("can store enumeration values") {
            let vin = "34567"
            let newMotorcycle = DataService.shared.getMotorcycleData(vin: vin)
            newMotorcycle?.dashboardTheme = DashboardTheme.DARK_GREEN
            
            expect(newMotorcycle?.dashboardTheme).to(equal(DashboardTheme.DARK_GREEN))
        }
        
        it("can store dashboard gauges") {
            let vin = "34567"
            if let newMotorcycle = DataService.shared.getMotorcycleData(vin: vin) {
                var dashboardGauges = newMotorcycle.dashboardGauges
                dashboardGauges.gauges[DashboardGaugeQuadrant.A] = DashboardGauge.STATE_OF_CHARGE
                dashboardGauges.gauges[DashboardGaugeQuadrant.B] = DashboardGauge.RPM
                dashboardGauges.gauges[DashboardGaugeQuadrant.C] = DashboardGauge.EFFICIENCY_INSTANT
                dashboardGauges.gauges[DashboardGaugeQuadrant.D] = DashboardGauge.MOTOR_TEMPERATURE

                newMotorcycle.dashboardGauges = dashboardGauges
            }
            
            let motorcycle = DataService.shared.getMotorcycleData(vin: vin)
            expect(motorcycle).notTo(beNil())
            expect(motorcycle?.dashboardGauges.gauges[DashboardGaugeQuadrant.A]).to(equal(DashboardGauge.STATE_OF_CHARGE))
            expect(motorcycle?.dashboardGauges.gauges[DashboardGaugeQuadrant.B]).to(equal(DashboardGauge.RPM))
            expect(motorcycle?.dashboardGauges.gauges[DashboardGaugeQuadrant.C]).to(equal(DashboardGauge.EFFICIENCY_INSTANT))
            expect(motorcycle?.dashboardGauges.gauges[DashboardGaugeQuadrant.D]).to(equal(DashboardGauge.MOTOR_TEMPERATURE))
        }
    }
}
