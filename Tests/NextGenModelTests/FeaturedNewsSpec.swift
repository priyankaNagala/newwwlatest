//
//  FeaturedNewsSpec.swift
//  nextgen
//
//  Created by Scott Wang on 3/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Quick
import Nimble
import Himotoki
@testable import nextgen

class FeaturedNewsSpec : QuickSpec {
    
    override func spec() {
        it("parse JSON data to create FeaturedNews") {
            let featuredNews = try? FeaturedNews.decodeValue(sampleNewsFeedJson)
            
            expect(featuredNews).notTo(beNil())
            expect(featuredNews?.links).notTo(beNil())

            let link1 = featuredNews?.links[0]
            expect(link1).notTo(beNil())
            expect(link1?.title) == "The Verge - Riding through the rain on the 'Model 3' of motorcycles"
            expect(link1?.url) == "https://www.theverge.com/2018/2/25/17047918/zero-ds-zf6-5-electric-motorcylce-ride"

            let link2 = featuredNews?.links[1]
            expect(link2).notTo(beNil())
            expect(link2?.title) == "LA Times - Top 10: The Best of the Best 2017 cars and trucks"
            expect(link2?.url) == "http://www.latimes.com/business/autos/la-fi-hy-2017-top-ten-20171228-story.html"
        }
    }
}
