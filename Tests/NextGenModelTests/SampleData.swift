//
//  SampleData.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

let sampleStarcomJson : [String: Any] = [
        "name":"496714 The 'ME' Bike",
        "datetime_actual":"20180213163601",
        "longitude": "-122.0119",
        "latitude": "37.0498",
        "unitnumber" : "496714",
        "soc" : 80,
        "charging" : false,
        "tipover" : 1,
        "chargingtimeleft" : 20,
        "pluggedin" : 0]

let sampleNewsFeedJson = ["links" :
[
    ["url": "https://www.theverge.com/2018/2/25/17047918/zero-ds-zf6-5-electric-motorcylce-ride",
    "title_no_source": "The Verge - Riding through the rain on the 'Model 3' of motorcycles"],
    ["url": "http://www.latimes.com/business/autos/la-fi-hy-2017-top-ten-20171228-story.html",
    "title_no_source": "LA Times - Top 10: The Best of the Best 2017 cars and trucks"]
]]
