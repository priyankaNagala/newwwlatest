//
//  AppDelegateSpec.swift
//  nextgen
//
//  Created by Scott Wang on 10/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Quick
import Nimble
import Swinject

@testable import nextgen

class AppDelegateSpec: QuickSpec {
    
    static let simpleMessage =
        ["aps":
            ["alert":
                ["title" : "A Simple Title",
                "body": "A Simple Body"]
            ]
        ]
    
    static let simpleMessage2 =
        ["aps":
            ["alert":
                ["body": "A Simple Body"]
            ]
    ]

    static let emptyMessage = [ "garbage" : "can" ]

    static let locMessage =
        ["aps":
            ["alert":
                ["title-loc-key" : "A Simple Title",
                 "loc-key": "A Simple Body"]
            ]
    ]

    static let locMessage1 =
        ["aps":
            ["alert":
                ["title-loc-key" : "A Simple Title",
                 "loc-key": "A Simple Body - %@ - %@",
                 "loc-args" : ["First", "Second"]]
            ]
    ]

    static let locMessage2 =
        ["aps":
            ["alert":
                ["title-loc-key" : "A Simple Title",
                 "loc-key": "A Simple Body - %@ - %@",
                 "loc-args" : ["First"]]
            ]
    ]

    static let locMessage3 =
        ["aps":
            ["alert":
                ["title-loc-key" : "A Simple Title",
                 "loc-key": "A Simple Body - %@ - %@",
                 "loc-args" : ["First", 2]]
            ]
    ]

    static let locMessage4 =
        ["aps":
            ["alert":
                ["title-loc-key" : "PUSH_NOTIFICATION_INTERRUPTED_CHARGING_TITLE",
                 "loc-key": "PUSH_NOTIFICATION_INTERRUPTED_CHARGING_BODY"]
            ]
    ]

    static let locMessage5 =
        ["aps":
            ["alert":
                ["title-loc-key" : "PUSH_NOTIFICATION_FIRMWARE_UPDATE_TITLE",
                 "loc-key": "PUSH_NOTIFICATION_FIRMWARE_UPDATE_BODY",
                 "loc-args" : ["45"]]
            ]
    ]

    override func spec() {
        
        describe("Push Notification") {
            testCase(caseName: "Simple Message",
                     userInfo: AppDelegateSpec.simpleMessage,
                     expectedTitle: "A Simple Title",
                     expectedBody: "A Simple Body")
            
            testCase(caseName: "No title",
                     userInfo: AppDelegateSpec.simpleMessage2,
                     expectedTitle: "",
                     expectedBody: "A Simple Body")
            
            testCase(caseName: "Empty Data",
                     userInfo: AppDelegateSpec.emptyMessage,
                     expectedTitle: "",
                     expectedBody: "")

            testCase(caseName: "Localized Simple Message",
                     userInfo: AppDelegateSpec.locMessage,
                     expectedTitle: "A Simple Title",
                     expectedBody: "A Simple Body")

            testCase(caseName: "Body Args Matching",
                     userInfo: AppDelegateSpec.locMessage1,
                     expectedTitle: "A Simple Title",
                     expectedBody: "A Simple Body - First - Second")

            testCase(caseName: "Body Args Missing",
                     userInfo: AppDelegateSpec.locMessage2,
                     expectedTitle: "A Simple Title",
                     expectedBody: "A Simple Body - First - (null)")

            testCase(caseName: "Body Args non strings",
                     userInfo: AppDelegateSpec.locMessage3,
                     expectedTitle: "A Simple Title",
                     expectedBody: "A Simple Body - %@ - %@")

            testCase(caseName: "Charge Interrupted",
                     userInfo: AppDelegateSpec.locMessage4,
                     expectedTitle: "",
                     expectedBody: "We've detected that the charging of your motorcycle has been interrupted.")

            testCase(caseName: "Firmware Update",
                     userInfo: AppDelegateSpec.locMessage5,
                     expectedTitle: "",
                     expectedBody: "Firmware version (45) is now available for your motorcycle.")
        }
    }
    
    func testCase(caseName: String, userInfo: [AnyHashable: Any], expectedTitle: String, expectedBody: String) {
        it(caseName) {
            let title = AppDelegate.parseTitle(userInfo: userInfo)
            let body = AppDelegate.parsseBody(userInfo: userInfo)
            
            expect(title) == expectedTitle
            expect(body) == expectedBody
        }
    }
}
