//
//  SwinjectUtil.swift
//  nextgenTests
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import Quick
import Nimble
import Swinject
import SwinjectStoryboard

@testable import nextgen

class SwinjectUtilSpec: QuickSpec {
    override func spec() {

        var container: Container!
        beforeEach {
            container = SwinjectUtil.sharedInstance.container
        }


        describe("Container") {

            it("resolves every service type.") {
                // Utils
                expect(container.resolve(ZeroAnalyticsService.self)).notTo(beNil())
                
                // Models
                expect(container.resolve(Networking.self)).notTo(beNil())
                expect(container.resolve(StarcomApiService.self)).notTo(beNil())
                expect(container.resolve(DataService.self)).notTo(beNil())
                
                // ViewModels
                let dashboardViewModel = container.resolve(DashboardViewModel.self)
                expect(dashboardViewModel)
                    .notTo(beNil())
                expect(dashboardViewModel?.featuredNewsApiService).notTo(beNil())
                
                let remoteConnectViewModel = container.resolve(RemoteConnectViewModel.self)
                expect(remoteConnectViewModel)
                    .notTo(beNil())
                expect(remoteConnectViewModel?.starcomApiService).notTo(beNil())
                
                let infoSheetViewModel = container.resolve(InfoSheetViewModel.self)
                expect(infoSheetViewModel)
                    .notTo(beNil())
                
                expect(container.resolve(AppHelpViewModel.self))
                    .notTo(beNil())

            }


            it("injects view models to views (Main Storyboard).") {
                let bundle = Bundle(for: DashboardViewController.self)
                let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: container)

                let dashboardViewController = storyboard
                    .instantiateViewController(withIdentifier: "DashboardViewController")
                    as! DashboardViewController
                expect(dashboardViewController.dashboardViewModel).notTo(beNil())
                expect(dashboardViewController.analyticsService).notTo(beNil())
                
            }

            it("injects view models to views (RemoteConnect Storyboard).") {
                let bundle = Bundle(for: InfoSheetViewController.self)
                let storyboard = SwinjectStoryboard.create(name: "RemoteConnect", bundle: bundle, container: container)

                let remoteConnectViewController = storyboard
                    .instantiateViewController(withIdentifier: "RemoteConnectViewController")
                    as! RemoteConnectViewController
                expect(remoteConnectViewController.remoteConnectViewModel).notTo(beNil())
                expect(remoteConnectViewController.remoteConnectViewModel?.dataService).notTo(beNil())
                expect(remoteConnectViewController.remoteConnectViewModel?.starcomApiService).notTo(beNil())
                expect(remoteConnectViewController.analyticsService).notTo(beNil())

                
                let infosheetViewController = storyboard.instantiateViewController(withIdentifier: "InfoSheetViewController") as! InfoSheetViewController
                expect(infosheetViewController.infoSheetViewModel).notTo(beNil())

            }

            it("injects view models to views (AppHelp Storyboard).") {
                let bundle = Bundle(for: AppHelpViewController.self)
                let storyboard = SwinjectStoryboard.create(name: "AppHelp", bundle: bundle, container: container)
                
                let appHelpViewController = storyboard
                    .instantiateViewController(withIdentifier: "AppHelpViewController")
                    as! AppHelpViewController
                expect(appHelpViewController.appHelpViewModel).notTo(beNil())
                expect(appHelpViewController.analyticsService).notTo(beNil())
            }
        }
    }
}
