# nextgen-ios

## Internationalization/String Translation

We will be using external tooling (POEditor) for managing translations. Builds for this app will export all strings from this workspace and upload those to POEditor. Subsequently, the same builds will download any strings translated in POEditor and import them into this workspace.

We are using a single Localizable.strings file per language. Strings that are specified in storyboards and xib files can also be translated. We use the mechanism described [here](https://medium.com/@mario.negro.martin/easy-xib-and-storyboard-localization-b2794c69c9db). Essentially, we have defined a new runtime attribute for labels and buttons that will contain the key to be used to pull the string from the Localizable.strings file. Please note that this mechanism still allows you to specify a string in Interface Builder so that you can visually QA the layouts inside of IB. This mechanism also allows you to still set the text value of a label/button at runtime if you need that type of functionality.

In order to facilitate testing of language translations, we have a created a separate Scheme for each language. These schemes have the language hardcoded and makes it easier during development such that you don't have to go into the emulators settings and pick which language you are interested in. Just pick the language Scheme you want and run the app.

https://slytrunk.atlassian.net/wiki/spaces/Zero/pages/317292565/Translations+Strategy
https://slytrunk.atlassian.net/wiki/spaces/Zero/pages/329744403/String+Translations
