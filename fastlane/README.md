fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios create_temporary_keychain
```
fastlane ios create_temporary_keychain
```

### ios buildnumber
```
fastlane ios buildnumber
```

### ios new_version
```
fastlane ios new_version
```

### ios beta
```
fastlane ios beta
```

### ios poeditor_upload
```
fastlane ios poeditor_upload
```
Upload strings to POEditor
### ios poeditor_download_english
```
fastlane ios poeditor_download_english
```
Download English strings from POEditor
### ios poeditor_download
```
fastlane ios poeditor_download
```
Download strings from POEditor

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
