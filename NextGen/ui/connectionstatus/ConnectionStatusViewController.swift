//
//  ConnectionStatusViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/3/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ConnectionStatusViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SCHEDULED_CHARGING_INTRO}

    var dataService: DataService?

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var bluetoothIconImageView: UIImageView!
    @IBOutlet weak var bluetoothStatusLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var connectionHelpButton: UIButton!
    @IBOutlet weak var connectionHelpContainerView: UIView!
    @IBOutlet weak var plugContainerView: UIView!
    @IBOutlet weak var plugIconImageView: UIImageView!
    @IBOutlet weak var plugDetailContainerView: UIView!
    @IBOutlet weak var plugStatusLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var chargingLabel: UILabel!
    @IBOutlet weak var lastConnectionContainerView: UIView!
    @IBOutlet weak var lastConnectionTimeLabel: UILabel!
    @IBOutlet weak var discardChangesButton: ZeroUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViews()
    }
    
    func updateViews() {
        if (isConnected) {
            bluetoothIconImageView.image = UIImage.init(named: "ic_tablerowdecorator_bluetooth_connected")
            bluetoothStatusLabel.text = "connection_status_connected".localized
            connectionHelpContainerView.isHidden = true
            lastConnectionContainerView.isHidden = true
            plugContainerView.isHidden = false
            plugDetailContainerView.isHidden = false
            
            if (isPluggedIn) {
                chargingLabel.isHidden = !isCharging
                if (isCharging) {
                    plugIconImageView.image = UIImage.init(named: "ic_tablerowdecorator_charge_active")
                } else {
                    plugIconImageView.image = UIImage.init(named: "ic_tablerowdecorator_charge_inactive_plugged")
                }
                plugStatusLabel.text = "remote_connect_plug_status_plugged".localized
            } else {
                plugIconImageView.image = UIImage.init(named: "ic_tablerowdecorator_charge_inactive")
                plugStatusLabel.text = "remote_connect_plug_status_unplugged".localized
                chargingLabel.isHidden = true
            }
            discardChangesButton.isHidden = true
            descriptionLabel.isHidden = true
        } else {
            bluetoothIconImageView.image = UIImage.init(named: "ic_tablerowdecorator_bluetooth_notconnected")
            bluetoothStatusLabel.text = "connection_status_not_connected".localized
            connectionHelpContainerView.isHidden = false
            lastConnectionTimeLabel.text = dataService?.lastConnectedDateFormatted
            lastConnectionContainerView.isHidden = false
            plugDetailContainerView.isHidden = true
            plugContainerView.isHidden = true
            
            if let zeroMotorcycleService = self.zeroMotorcycleService {
                discardChangesButton.isHidden = zeroMotorcycleService.offlineQueueIsEmpty().value
                descriptionLabel.isHidden = discardChangesButton.isHidden
            } else {
                discardChangesButton.isHidden = true
                descriptionLabel.isHidden = true
            }
        }
    }
    
    override func onMotorcycleConnect() {
        super.onMotorcycleConnect()
        updateViews()
    }
    
    override func onMotorcycleDisconnect() {
        super.onMotorcycleDisconnect()
        updateViews()
    }
    
    override func onMotorcycleChargingChange(isCharging: Bool) {
        super.onMotorcycleChargingChange(isCharging: isCharging)
        updateViews()
    }
    
    override func onMotorcyclePluggedChange(isPluggedIn: Bool) {
        super.onMotorcyclePluggedChange(isPluggedIn: isPluggedIn)
        updateViews()
    }
    
    // MARK: - IBAction
    @IBAction func connectionHelpPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .help)
    }
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func discardChanges(_ sender: Any) {
        zeroMotorcycleService?.clearOfflineQueue()
        descriptionLabel.isHidden = true
        discardChangesButton.isEnabled = false
    }
}
