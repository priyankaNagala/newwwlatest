//
//  ConnectViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ConnectViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_CONNECT }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var takeTourButton: ZeroUIButton!
    @IBOutlet weak var getHelpButton: ZeroSecondaryUIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func animateActivityIndicator(animate: Bool) {
        animate ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        self.activityIndicatorLabel.isHidden = !animate
    }
    
    // MARK: - IBAction
    
    @IBAction func takeTourPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .tour)
    }
    
    @IBAction func getHelpPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .help)
    }
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
