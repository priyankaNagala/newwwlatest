//
//  ConnectImproveViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ConnectImproveViewModel {
    var dataService: DataService
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func onAutomaticLogsSend() {
        dataService.trackMotorcycleUsageLogs = true
    }
    
    func onDontSendLogs() {
        dataService.trackMotorcycleUsageLogs = false
    }
}
