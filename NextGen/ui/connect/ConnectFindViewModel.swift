//
//  ConnectFindViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import RxCocoa
import RxSwift

class ConnectFindViewModel {

    var onNotFindingMotorycles = PublishRelay<Bool>()
    var onScannedMotorcycles = PublishRelay<[TIOPeripheral]>()
    var foundMotorcycles = [TIOPeripheral]()
    //var disposeBag = CompositeDisposable()
    var disposeBag = DisposeBag()

    var tioService: TioService
    fileprivate var scanStopped = false
    
    init(tioService: TioService) {
        self.tioService = tioService
    }
    
    func startScanner() {
        if (!tioService.bluetoothAvailable.value) {
            onNotFindingMotorycles.accept(true)
            return
        }
        
        // Clear out anything that may have been here already
        disposeBag = DisposeBag()
        
        // Listen for a certain amount of time and at the end, let the observer know
        // how many motorocycles we found.
        Observable<Int>.timer(Constant.BLUETOOTH_SCAN_SECONDS, scheduler: MainScheduler.instance)
            .subscribe { [weak self] in
                guard let strongSelf = self, strongSelf.scanStopped == false else { return }
                strongSelf.stopScanner()
                strongSelf.onScannedMotorcycles.accept(strongSelf.foundMotorcycles)
            }.disposed(by: disposeBag)
        
        // If we reach half the scan time and we haven't found any motorcycles, let the observer know
        // so they can show the help UI
        Observable<Int>.timer(Constant.BLUETOOTH_SCAN_SECONDS / 2, scheduler: MainScheduler.instance)
            .subscribe { [weak self] in
                guard let strongSelf = self, strongSelf.scanStopped == false else { return }
                if strongSelf.foundMotorcycles.count == 0 {
                    strongSelf.onNotFindingMotorycles.accept(true)
                }
            }.disposed(by: disposeBag)
        
        // Start the tioService scan and accrue the found motorcycles locally
        tioService.startScan()
            .subscribe({ [weak self] foundPeripheral in
                guard let strongSelf = self, strongSelf.scanStopped == false else { return }
                if let peripheral = foundPeripheral.element {
                    DDLogDebug("FOUND PERIPHERAL")
                    var found = false
                    strongSelf.foundMotorcycles.forEach { existingPeripheral in
                        if existingPeripheral.name == peripheral.name {
                            found = true
                            DDLogDebug("FOUND PERIPHERAL ALREADY IN LIST " + existingPeripheral.name)
                            return
                        }
                    }
                    if !found {
                        DDLogDebug("FOUND PERIPHERAL ADDING " +  peripheral.name)
                        strongSelf.foundMotorcycles.append(peripheral)
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    func stopScanner() {
        guard scanStopped == false else { return }
        scanStopped = true
        tioService.stopScan()
    }
}
