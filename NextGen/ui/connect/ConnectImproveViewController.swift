//
//  ConnectImproveViewController.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ConnectImproveViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_IMPROVE }

    @IBOutlet weak var closeButton: ZeroUIBarButtonItem!
    @IBOutlet weak var automaticSendButton: ZeroUIButton!
    @IBOutlet weak var dontSendButton: ZeroSecondaryUIButton!
    
    var connectImproveViewModel: ConnectImproveViewModel?
    var dataService: DataService?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let dataService = self.dataService else { return }
        
        // Remove close button during onboarding
        if !dataService.hasSeenOnboarding {
            navigationItem.leftBarButtonItems = []
            navigationItem.hidesBackButton = true
        }

        disposeBag.insertAll(all:
            automaticSendButton.rx.tap.subscribe({ _ in
                self.connectImproveViewModel?.onAutomaticLogsSend()
                UILauncher.launchConnectComplete(self)
            }),
            
            dontSendButton.rx.tap.subscribe({ _ in
                self.connectImproveViewModel?.onDontSendLogs()
                UILauncher.launchConnectComplete(self)
            })
        )
    }

    // NOTE:
    // For some reason when removing the Close Button from the navigationItems causes rx to send a phanton click event.
    // So using IBAction here instead.
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
