//
//  ConnectCompleteViewController.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ConnectCompleteViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_COMPLETE }

    @IBOutlet weak var closeButton: ZeroUIBarButtonItem!
    @IBOutlet weak var finishButton: ZeroUIButton!
    
    var dataService: DataService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let dataService = self.dataService else { return }
        
        // Remove close button during onboarding
        if !dataService.hasSeenOnboarding {
            navigationItem.leftBarButtonItems = []
            navigationItem.hidesBackButton = true
        }

        disposeBag.insertAll(all:
            finishButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                
                if dataService.hasSeenOnboarding {
                    strongSelf.dismiss(animated: true, completion: nil)
                } else {
                    dataService.hasSeenOnboarding = true
                    UILauncher.launchRootTabBar()
                }
            })
        )
    }

    // NOTE:
    // For some reason when removing the Close Button from the navigationItems causes rx to send a phanton click event.
    // So using IBAction here instead.
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
