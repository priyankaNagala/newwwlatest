//  ConnectFindViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import UIKit

class ConnectFindViewController: ZeroUIViewController {
    
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_FIND }

    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activityContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var takeTourButton: ZeroUIButton!
    @IBOutlet weak var getHelpButton: ZeroSecondaryUIButton!
    @IBOutlet weak var helperContainer: UIView!
    @IBOutlet weak var retryButton: ZeroUIButton!
    @IBOutlet weak var skipButton: ZeroUIButton! // For Development
    @IBOutlet weak var closeButton: ZeroUIBarButtonItem!
    
    var connectFindViewModel: ConnectFindViewModel?
    var dataService: DataService?
    var enableBluetoothShownOnce: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = connectFindViewModel, let dataService = self.dataService else { return }

        // Remove close button during onboarding
        if !dataService.hasSeenOnboarding {
            navigationItem.leftBarButtonItems = []
            skipButton.isHidden = false // Skip thru onboarding
            navigationItem.hidesBackButton = true
        }

        disposeBag.insertAll(all:
            
            viewModel.onScannedMotorcycles.subscribe({ [weak self] motorcycles in
                guard let strongSelf = self else { return }
                if let peripherals = motorcycles.element {
                    if peripherals.count > 0 {
                        UILauncher.launchConnectConnect(strongSelf, motorcycles: peripherals)
                        return
                    }
                }
                strongSelf.showScanIndicator(show: false)
                strongSelf.retryButton.isHidden = false
            }),
            
            viewModel.onNotFindingMotorycles.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.helperContainer.isHidden = false
                if !viewModel.tioService.bluetoothAvailable.value {
                    UILauncher.launchConnectEnableBluetooth(strongSelf)
                    strongSelf.showScanIndicator(show: false)
                    strongSelf.retryButton.isHidden = false
                }
            }),
            
            retryButton.rx.tap.subscribe({ _ in
                self.helperContainer.isHidden = true
                self.showScanIndicator(show: true)
                self.retryButton.isHidden = true
                self.connectFindViewModel?.startScanner()
            }),
            
            skipButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self, strongSelf.skipButton.isHidden == false else { return }
                UILauncher.launchRegisterPrompt(strongSelf)
            }
        )
        
        connectFindViewModel?.startScanner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let viewModel = connectFindViewModel else { return }
        // We need this here since onNotFindMotorcycles' event could fire before viewDidLoad completes
        // And presenting VCs will not work at that point.
        if !viewModel.tioService.bluetoothAvailable.value && !enableBluetoothShownOnce {
            enableBluetoothShownOnce = true
            UILauncher.launchConnectEnableBluetooth(self)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        connectFindViewModel?.stopScanner()
    }
    
    func showScanIndicator(show: Bool) {
        show ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        activityIndicator.isHidden = !show
        activityIndicatorLabel.isHidden = !show
    }
    
    // MARK: - IBAction
    @IBAction func takeTourPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .tour)
    }
    
    @IBAction func getHelpPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .help)
    }
    
    // NOTE:
    // For some reason when removing the Close Button from the navigationItems causes rx to send a phanton click event.
    // So using IBAction here instead.
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
