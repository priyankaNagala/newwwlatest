//
//  ConnectMotorcycleItemViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ConnectMotorcycleItemViewModel {
    var motorcycle: TIOPeripheral
    
    init(motorcycle: TIOPeripheral) {
        self.motorcycle = motorcycle
    }
}
