//
//  ConnectConnectViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift
import CocoaLumberjack

class ConnectConnectViewModel {
    var foundMotorcycles = BehaviorRelay<[ConnectMotorcycleSection]>(value: [])
    var onMotorcycleConnect = PublishSubject<Bool>()
    var onConnectionFailed = PublishRelay<Bool>()
    var zeroMotorcycleService: ZeroMotorcycleService
    var dataService: DataService
    var disposeBag = DisposeBag()
    
    init(dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    typealias ConnectMotorcycleDataSource = RxTableViewSectionedReloadDataSource<ConnectMotorcycleSection>
    
    struct ConnectMotorcycleSection: SectionModelType  {
        typealias Item = ConnectMotorcycleItemViewModel
        var items: [Item]
        init(items: [Item]) {
            self.items = items
        }
        init(original: ConnectMotorcycleSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    func connectMotorcyclesDataSource() -> ConnectMotorcycleDataSource {
        let cellId = ConnectMotorcycleTableViewCell.ReuseIdentifier
        return ConnectMotorcycleDataSource(
            configureCell: { _, tv, ip, item  in
                let cell = tv.dequeueReusableCell(withIdentifier: cellId, for: ip) as! ConnectMotorcycleTableViewCell
                cell.configure(withViewModel: item)
                return cell
            }
        )
    }
    
    func loadModel(motorcycles: [TIOPeripheral]) {
        var models = [ConnectMotorcycleItemViewModel]()
        models = motorcycles.map { it in
            return ConnectMotorcycleItemViewModel(motorcycle: it)
        }
        foundMotorcycles.accept([ConnectMotorcycleSection(items: models)])
    }
    
    func connect(peripheral: TIOPeripheral) {
        // Connect Timeout
        
        let disposable = Observable<Int>.timer(Constant.BLUETOOTH_CONNECT_TIMEOUT, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self else { return }
                DDLogDebug("CONNECT TIMEOUT")
                strongSelf.disposeBag = DisposeBag()
                strongSelf.onConnectionFailed.accept(true)
            })
        _ = disposeBag.insert(disposable)
        
        if (zeroMotorcycleService.isConnected()) {
            // Check to see if we are connected to the one they want. If so, just move
            // on. If not, disconnect
            if let connectedPeripheral = zeroMotorcycleService.getConnectedPeripheral() {
                if (connectedPeripheral.identifier != peripheral.identifier) {
                    connectedPeripheral.cancelConnection()
                    
                    zeroMotorcycleService.getConnectionState()
                        .subscribe({ [weak self] connectionState in
                            guard let strongSelf = self else { return }
                            if let isConnected = connectionState.element {
                                if (!isConnected) {
                                    strongSelf.zeroMotorcycleService.connect(peripheral: peripheral)
                                } else {
                                    strongSelf.disposeBag = DisposeBag()
                                    let connectedPreviously = strongSelf.dataService.hasConnectedOnce
                                    strongSelf.dataService.hasConnectedOnce = true
                                    strongSelf.onMotorcycleConnect.onNext(connectedPreviously)
                                }
                            }
                        })
                        .disposed(by: disposeBag)
                    
                    return
                }
                
                // If we are here, we are already connected to the peripheral that
                // we want.
                onMotorcycleConnect.onNext(true)
            }
        } else {
            zeroMotorcycleService.getConnectionState()
                .subscribe({ [weak self] connectionState in
                    guard let strongSelf = self else { return }
                    if let isConnected = connectionState.element {
                        if (isConnected) {
                            strongSelf.disposeBag = DisposeBag()
                            let connectedPreviously = strongSelf.dataService.hasConnectedOnce
                            strongSelf.dataService.hasConnectedOnce = true
                            strongSelf.onMotorcycleConnect.onNext(connectedPreviously || strongSelf.dataService.loggedIn)
                        }
                    }
                })
                .disposed(by: disposeBag)
        
            zeroMotorcycleService.connect(peripheral: peripheral)
        }
    }
}
