//
//  ConnectMotorcycleTableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class ConnectMotorcycleTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "ConnectMotorcycleTableViewCell"
    
    @IBOutlet weak var motorcycleName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func configure(withViewModel viewModel: ConnectMotorcycleItemViewModel) {
        motorcycleName.text = viewModel.motorcycle.name
    }
}
