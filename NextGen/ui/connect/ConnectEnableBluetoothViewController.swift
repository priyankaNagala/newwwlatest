//
//  ConnectEnableBluetoothViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ConnectEnableBluetoothViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_ENABLE_BLUETOOTH }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
