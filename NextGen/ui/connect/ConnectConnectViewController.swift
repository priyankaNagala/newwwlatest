//
//  ConnectConnectViewController.swift
//  nextgen
//
//  Created by David Crawford on 8/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class ConnectConnectViewController: ZeroUIViewController {
    
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CONNECT_CONNECT }
    
    @IBOutlet weak var closeButton: ZeroUIBarButtonItem!
    @IBOutlet weak var connectMotorcyclesTableView: UITableView!
    
    var connectConnectViewModel: ConnectConnectViewModel?
    var foundMotorcycles = [TIOPeripheral]()
    var dataService: DataService?
    var isConnecting: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = connectConnectViewModel, let dataService = self.dataService else { return }
        
        // Remove close button during onboarding
        if !dataService.hasSeenOnboarding {
            navigationItem.leftBarButtonItems = []
            navigationItem.hidesBackButton = true
        }

        let cellId = ConnectMotorcycleTableViewCell.ReuseIdentifier
        connectMotorcyclesTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        
        let connectMotorcyclesDataSource = viewModel.connectMotorcyclesDataSource()
        
        disposeBag.insertAll(all:
            viewModel.foundMotorcycles
                .bind(to: connectMotorcyclesTableView.rx.items(dataSource: connectMotorcyclesDataSource)),
            
            connectMotorcyclesTableView.rx.itemSelected.subscribe { [weak self] event in
                guard let strongSelf = self, strongSelf.isConnecting == false else { return }
                if let indexPath = event.element,  let itemViewModel:ConnectMotorcycleItemViewModel = try? strongSelf.connectMotorcyclesTableView.rx.model(at: indexPath) {
                    let cell = strongSelf.connectMotorcyclesTableView.cellForRow(at: indexPath) as! ConnectMotorcycleTableViewCell
                    cell.activityIndicator.startAnimating()
                    viewModel.connect(peripheral: itemViewModel.motorcycle)
                    strongSelf.isConnecting = true
                }
            },
            
            viewModel.onMotorcycleConnect.subscribe({ [weak self] connectedPreviously in
                guard let strongSelf = self else { return }
                if let connectedPreviously = connectedPreviously.element {
                    if (connectedPreviously) {
                        UILauncher.launchConnectComplete(strongSelf)
                    } else {
                        UILauncher.launchRegisterPrompt(strongSelf)
                    }
                }
                strongSelf.isConnecting = false
            }),
            
            viewModel.onConnectionFailed.subscribe { [weak self] event in
                guard let strongSelf = self, let failed = event.element else { return }
                if failed {
                    let alert = UIAlertController(
                        title: "connect_failed".localized,
                        message: "connect_failed_description".localized,
                        preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: { action in
                        strongSelf.dismiss(animated: true, completion: nil)
                    }))
                    strongSelf.present(alert, animated: true)
                }
            }
        )
        
        viewModel.loadModel(motorcycles: foundMotorcycles)
    }

    // NOTE:
    // For some reason when removing the Close Button from the navigationItems causes rx to send a phanton click event.
    // So using IBAction here instead.
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func getHelpPressed(_ sender: Any) {
        UILauncher.launchAppHelp(self, helpType: .help)
    }
}
