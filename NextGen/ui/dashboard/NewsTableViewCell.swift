//
//  NewsTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 3/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class NewsTableViewCell: ZeroUITableViewCell {

    static let ReuseIdentifier = "NewsTableViewCell"
    
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var headlineLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = ColorUtil.color(.BackgroundBlack)
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
}
