//
//  DashboardViewModel.swift
//  NextGenViewModel
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxDataSources
import RxCocoa
import RxSwift
import CocoaLumberjack

struct SectionOfLinks {
    var header: String
    var items: [Link]
}

extension SectionOfLinks: SectionModelType {
    typealias Item = Link
    
    init(original: SectionOfLinks, items: [Link]) {
        self = original
        self.items = items
    }
}

class DashboardViewModel  {
    var featuredNewsApiService: FeaturedNewsApiService
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var starcomApiService: StarcomApiService
    var disposeBag = DisposeBag()
    var bluetoothDisposeBag = DisposeBag()
    var motorcycleImageSet = false
    var modelLoaded = false
    var appeared = false
    var numRetries = 0
    
    var sections = PublishSubject<[SectionOfLinks]>()

    var onPlugStatusTextChange = PublishRelay<String>()
    var onPlugStatusColorChange = PublishRelay<UIColor>()
    var onStateOfChargeTextChange = PublishRelay<String>()
    var onStateOfChargeColorChange = PublishRelay<UIColor>()
    var onMotorcycleImageChange = PublishRelay<String>()
    var onMotorcycleConnectionChange = BehaviorRelay<Bool>(value: false)
    var onRideModeChange = PublishRelay<String>()
    var onAutoConnecting = BehaviorRelay<Bool>(value: true)
    var lastCapturedRideName = BehaviorRelay<String>(value: "")
    var lastCapturedRideTextColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.LightGray))
    
    init(featuredNewsApiService: FeaturedNewsApiService,
         dataService: DataService,
         zeroMotorcycleService: ZeroMotorcycleService,
         starcomApiService: StarcomApiService) {
        self.featuredNewsApiService = featuredNewsApiService
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
        self.starcomApiService = starcomApiService
    }
    
    func onViewWillAppear() {
        if modelLoaded && appeared {
            getFeaturedNews()
            readRideModesFromBike()
            acceptCapturedRide()
        }
        appeared = true
    }
    
    func loadModel() {
        acceptAutoConnectState()
        readRideModesFromBike()
        subscribeToBikeInfo()
        subscribeToConnectionState()
        reauthenticate()
        acceptCapturedRide()
        getFeaturedNews()

        modelLoaded = true
    }
    
    func acceptAutoConnectState() {
        if let connectionService = SwinjectUtil.sharedInstance.container.resolve(ConnectionService.self) {
            connectionService.autoConnecting.subscribe { [weak self] event in
                guard let strongSelf = self, let connecting = event.element else { return }
                if connecting {
                    strongSelf.onAutoConnecting.accept(connecting)
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        strongSelf.onAutoConnecting.accept(connecting)
                    }
                    
                }
            }.disposed(by: disposeBag)
        }
    }
    
    func acceptCapturedRide() {
        if let ride = dataService.capturedRide {
            lastCapturedRideName.accept(ride.getName())
            lastCapturedRideTextColor.accept(ColorUtil.color(.White))
        } else {
            lastCapturedRideName.accept("data_no_ride_captured".localized)
            lastCapturedRideTextColor.accept(ColorUtil.color(.LightGray))
        }
    }
    
    func acceptCurrentRideMode() {
        if let currentRideMode = dataService.currentMotorcycle?.currentRideMode {
            onRideModeChange.accept(currentRideMode.name)
        }
    }
    
    func subscribeToConnectionState() {
        zeroMotorcycleService.getConnectionState()
            .subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                let connected = event.element!
                if (connected) {
                    strongSelf.onPlugStatusTextChange.accept("battery_charging".localized)
                    // The next statement isnt absolutely needed but adding anyway just to be safe
                    strongSelf.onStateOfChargeTextChange.accept("global_empty_data_percentage".localized)
                    strongSelf.onMotorcycleConnectionChange.accept(true)
                } else {
                    strongSelf.motorcycleImageSet = false
                    strongSelf.onMotorcycleImageChange.accept("GrayedOutMotorcycle")
                    strongSelf.onPlugStatusTextChange.accept("battery_charging".localized)
                    strongSelf.onStateOfChargeTextChange.accept("global_empty_data_percentage".localized)
                    strongSelf.onMotorcycleConnectionChange.accept(false)
                }
            }.disposed(by: disposeBag)
    }
    
    func subscribeToBikeInfo() {
        zeroMotorcycleService
            .getBikeInfo()
            .subscribe({ [weak self] obs in
                guard let strongSelf = self else { return }
                guard let bikeInfoPacket = obs.element else { return }
                if (!strongSelf.motorcycleImageSet) {
                    strongSelf.motorcycleImageSet = true
                    strongSelf.onMotorcycleImageChange.accept(bikeInfoPacket.getMotorcycleColorName())
                }
                let isPluggedIn = bikeInfoPacket.isPluggedIn()
                let isCharging = bikeInfoPacket.isCharging()
                if (isPluggedIn) {
                    if (isCharging) {
                        strongSelf.onPlugStatusTextChange.accept("battery_charging".localized)
                        strongSelf.onPlugStatusColorChange.accept(ColorUtil.color(.Green))
                    } else {
                        strongSelf.onPlugStatusTextChange.accept("battery_plugged_in".localized)
                        strongSelf.onPlugStatusColorChange.accept(ColorUtil.uiColor(.whiteText))
                    }
                } else {
                    strongSelf.onPlugStatusTextChange.accept("battery_unplugged".localized)
                    strongSelf.onPlugStatusColorChange.accept(ColorUtil.uiColor(.whiteText))
                }
                let stateOfChargePercentage = bikeInfoPacket.stateOfCharge
                if (stateOfChargePercentage < Constant.STATE_OF_CHARGE_LOW_PERCENTAGE) {
                    strongSelf.onStateOfChargeColorChange.accept(ColorUtil.color(.Orange))
                    strongSelf.onStateOfChargeTextChange.accept(
                        String(format: "dashboard_state_of_charge_low".localized, stateOfChargePercentage))
                } else {
                    strongSelf.onStateOfChargeColorChange.accept(ColorUtil.uiColor(.whiteText))
                    strongSelf.onStateOfChargeTextChange.accept(
                        String(format: "dashboard_state_of_charge".localized, stateOfChargePercentage))
                }
            })
            .disposed(by: disposeBag)
    }
    
    func getFeaturedNews() {
        featuredNewsApiService.api()
            .subscribe(onNext: { (featuredNews) in
                self.sections.onNext([SectionOfLinks(header: "news_latest_news".localized, items: featuredNews.links)])
            }, onError: { (error) in
                DDLogError(error.localizedDescription)
            }, onCompleted: {
                
            }).disposed(by: disposeBag)

    }
    
    func reauthenticate() {
        if let userName = dataService.starcomUser, let password = dataService.starcomPassword {
            Starcom.shared.login(starcomApiService: starcomApiService, user: userName, password: password).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let response = event.element {
                    if let sessionId = response.sessionId {
                        strongSelf.dataService.starcomSessionId = sessionId
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.registerForPushNotifications()
                        }
                        // Save username/password
                        strongSelf.dataService.persistLoginCredentials(user: userName, password: password)
                    } else if let error = response.error {
                        // Fail silently
                        DDLogWarn("\(error)")
                        strongSelf.dataService.starcomPassword = nil
                    }
                }
                }.disposed(by: disposeBag)
        }
    }
    
    func readRideModesFromBike() {
        let customRideModePacket = RideModeCustomSettingsPacket()
        customRideModePacket.isEmpty = true
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: customRideModePacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            
            if let packet = event.element as? RideModeCustomSettingsPacket {
                strongSelf.bluetoothDisposeBag = DisposeBag() // We got a response, no need for additional callbacks
                strongSelf.numRetries = 0
                strongSelf.setActiveRideMode(Int(packet.activeMode))
                strongSelf.acceptCurrentRideMode()
                DDLogDebug("Read Custom RideMode Response Packet " + packet.getFullPacket().toHexString())
            } else {
                DDLogWarn("Read Custom RideMode Response Unknown")
            }
            }.disposed(by: bluetoothDisposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self, let _ = event.element else { return }
            strongSelf.bluetoothDisposeBag = DisposeBag()
            DDLogDebug("Write Custom RideMode Response Resend Packet")
            guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else { return }
            strongSelf.numRetries = strongSelf.numRetries + 1
            strongSelf.readRideModesFromBike()
            }.disposed(by: bluetoothDisposeBag)
        
    }

    func setActiveRideMode(_ index: Int) {
        let activeModes = dataService.currentMotorcycle?.activeRideModes ?? []
        if activeModes.count > index {
            let currentRideMode = activeModes[index]
            dataService.currentMotorcycle?.currentRideMode = currentRideMode
        }
    }
}
