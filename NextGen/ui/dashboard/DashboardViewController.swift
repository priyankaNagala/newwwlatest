//
//  DashboardViewController.swift
//  NextGenView
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import Crashlytics
import RxCocoa
import RxDataSources
import RxSwift

final class DashboardViewController: ZeroTabViewController, UITableViewDelegate {
    static let TABLE_HEADER_HEIGHT:CGFloat = 490
    static let NOTIFICATION_VIEW_HEIGHT:CGFloat = 117
    static let MOTORCYCLE_IMAGE_TOP_PADDING:CGFloat = 18
    static let REMOTE_CONNECT_BUTTON_HEIGHT:CGFloat = 36
    static let VALUE_LABEL_HEIGHT:CGFloat = 22
    
    override var screenName: String { return Constant.ANALYTICS_SCREEN_DASHBOARD }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var motorcycleImage: UIImageView!
    @IBOutlet weak var motorcycleImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var remoteConnectButton: ZeroUIButton!
    @IBOutlet weak var batteryIconImageView: UIImageView!
    @IBOutlet weak var performanceIconImageView: UIImageView!
    @IBOutlet weak var dataIconImageView: UIImageView!
    @IBOutlet weak var batteryLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var batteryLabel2: ZeroUpperCaseUILabel!
    @IBOutlet weak var rideModeLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var lastCapturedRideLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var notificaftionContainerView: UIView!
    @IBOutlet weak var firmwareNotificationIconImageView: UIImageView!
    @IBOutlet weak var firmwareNotificationView: UIView!
    @IBOutlet weak var firmwareNotificationUpdatebutton: ZeroTertiaryUIButton!
    @IBOutlet weak var firmwareNotificationDismissButton: ZeroTertiaryUIButton!
    @IBOutlet weak var bluetoothNotificationView: UIView!
    @IBOutlet weak var bluetoothNotificationConnectButton: ZeroTertiaryUIButton!
    @IBOutlet weak var bluetoothNotificationTroubleshootButton: ZeroTertiaryUIButton!
    @IBOutlet weak var bluetoothNotificationDescriptionLabel: UILabel!
    @IBOutlet weak var bluetoothNotificationActivityIndicator: UIActivityIndicatorView!
    
    var dataService: DataService?
    var dashboardViewModel: DashboardViewModel?
    var dataSource: RxTableViewSectionedReloadDataSource<SectionOfLinks>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerCells()
        dashboardViewModel?.loadModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dashboardViewModel?.onViewWillAppear()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupView() {
        guard let dashboardViewModel = self.dashboardViewModel, let dataService = self.dataService else { return }
        
        // Tableview
        tableView.backgroundColor = ColorUtil.color(.BackgroundBlack)
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 90.0
        tableView.sectionHeaderHeight = 74.0
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        TableUtil.zeroMargins(tableView)
        showNotification(false)
        
        // TODO: Change/fix this when we add the relive and share ride work
        lastCapturedRideLabel.textColor = ColorUtil.color(.Gray)
        
        disposeBag.insertAll(all:
            dataService.onLoginChange.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let loggedIn = event.element {
                    strongSelf.remoteConnectButton.isHidden = !loggedIn || dashboardViewModel.onMotorcycleConnectionChange.value
                    strongSelf.updateHeaderHeight()
                }
            },
            
            bluetoothNotificationConnectButton.rx.tap.subscribe{ [weak self] _ in
                guard let strongSelf = self else { return }
                if !strongSelf.notificaftionContainerView.isHidden {
                    strongSelf.connectionService?.stop()
                    UILauncher.launchConnectFind(strongSelf)
                }
            },
                             
            bluetoothNotificationTroubleshootButton.rx.tap.subscribe{ [weak self] _ in
                guard let strongSelf = self else { return }
                if !strongSelf.notificaftionContainerView.isHidden {
                    UILauncher.launchAppHelp(strongSelf, helpType: .help)
                }
            },
                             
            firmwareNotificationUpdatebutton.rx.tap.subscribe{ _ in
            // TODO Add update firmware
            },
                             
            firmwareNotificationDismissButton.rx.tap.subscribe {  [weak self] _ in
                guard let strongSelf = self else { return }
                if !strongSelf.notificaftionContainerView.isHidden {
                    strongSelf.showNotification(false)
                }
            },
                             
            remoteConnectButton.rx.tap.subscribe{ _ in
                UILauncher.launchRemoteConnect(self)
            },
            
            dashboardViewModel.onRideModeChange.subscribe({ [weak self] rideModeName in
                guard let strongSelf = self else { return }
                guard let rideModeName = rideModeName.element else { return }
                strongSelf.rideModeLabel.text = rideModeName
            }),
            
            dashboardViewModel.onPlugStatusTextChange.subscribe({ [weak self] plugStatus in
                guard let strongSelf = self else { return }
                guard let plugStatusText = plugStatus.element else { return }
                strongSelf.batteryLabel.text = plugStatusText
            }),
            
            dashboardViewModel.onPlugStatusColorChange.subscribe({ [weak self] plugStatusColor in
                guard let strongSelf = self else { return }
                guard let plugStatusColor = plugStatusColor.element else { return }
                strongSelf.batteryLabel.textColor = plugStatusColor
            }),
            
            dashboardViewModel.onStateOfChargeTextChange.subscribe({ [weak self] chargeStatus in
                guard let strongSelf = self else { return }
                guard let chargeStatusText = chargeStatus.element else { return }
                strongSelf.batteryLabel2.text = chargeStatusText
            }),
            
            dashboardViewModel.onStateOfChargeColorChange.subscribe({ [weak self] chargeStatusColor in
                guard let strongSelf = self else { return }
                guard let chargeStatusColor = chargeStatusColor.element else { return }
                strongSelf.batteryLabel2.textColor = chargeStatusColor
            }),
            
            dashboardViewModel.onMotorcycleImageChange.subscribe({ [weak self] image in
                guard let strongSelf = self else { return }
                guard let namedImage = image.element else { return }
                strongSelf.motorcycleImage.image = UIImage.init(named: namedImage)
            }),
            
            dashboardViewModel.onMotorcycleConnectionChange.subscribe({ [weak self] isConnected in
                guard let strongSelf = self else { return }
                guard let isConnected = isConnected.element else { return }
                strongSelf.batteryLabel.text = "" // Just zero this out until the first packet comes back
                if (isConnected) {
                    strongSelf.batteryIconImageView.tintImageColor(color: ColorUtil.color(.Yellow))
                    strongSelf.performanceIconImageView.tintImageColor(color: ColorUtil.color(.Yellow))
                    strongSelf.dataIconImageView.tintImageColor(color: ColorUtil.color(.Yellow))
                    strongSelf.batteryLabel.isHidden = false
                    strongSelf.batteryLabel.textColor = ColorUtil.color(.White)
                    strongSelf.batteryLabel2.textColor = ColorUtil.color(.White)
                    strongSelf.rideModeLabel.textColor = ColorUtil.color(.White)
                    strongSelf.remoteConnectButton.isHidden = true
                    strongSelf.showNotification(false)
                } else {
                    guard let strongSelf = self else { return }
                    strongSelf.batteryIconImageView.tintImageColor(color: ColorUtil.color(.Gray))
                    strongSelf.performanceIconImageView.tintImageColor(color: ColorUtil.color(.Gray))
                    strongSelf.dataIconImageView.tintImageColor(color: ColorUtil.color(.Gray))
                    strongSelf.batteryLabel.isHidden = true
                    strongSelf.batteryLabel2.textColor = ColorUtil.color(.Gray)
                    strongSelf.rideModeLabel.textColor = ColorUtil.color(.Gray)
                    strongSelf.remoteConnectButton.isHidden = !dataService.onLoginChange.value
                    strongSelf.showBluetoothNotification()
                }
            }),
            
            dashboardViewModel.onAutoConnecting.subscribe { [weak self] event in
                guard let strongSelf = self, let connecting = event.element else { return }
                if connecting {
                    strongSelf.bluetoothNotificationTroubleshootButton.isHidden = true
                    strongSelf.bluetoothNotificationConnectButton.isEnabled = false
                    strongSelf.bluetoothNotificationConnectButton.setTitle("dashboard_connecting".localized, for: .normal)
                    strongSelf.bluetoothNotificationActivityIndicator.startAnimating()
                } else {
                    strongSelf.bluetoothNotificationTroubleshootButton.isHidden = false
                    strongSelf.bluetoothNotificationConnectButton.isEnabled = true
                    strongSelf.bluetoothNotificationConnectButton.setTitle("dashboard_connect".localized, for: .normal)
                    strongSelf.bluetoothNotificationActivityIndicator.stopAnimating()
                }
            },
            
            dashboardViewModel.lastCapturedRideName.bind(to: lastCapturedRideLabel.rx.text),
            dashboardViewModel.lastCapturedRideTextColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.lastCapturedRideLabel.textColor = color
            }
        )
    }
    
    func registerCells() {
        tableView.delegate = self
        tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: NewsTableViewCell.ReuseIdentifier)
        tableView.register(UINib(nibName: "SectionTitleTableViewCell", bundle: nil), forCellReuseIdentifier: SectionTitleTableViewCell.ReuseIdentifier)
        
        dataSource = RxTableViewSectionedReloadDataSource<SectionOfLinks>(configureCell: { (ds: TableViewSectionedDataSource<SectionOfLinks>, tv: UITableView, ip: IndexPath, item: Link) in
            let cell = tv.dequeueReusableCell(withIdentifier: NewsTableViewCell.ReuseIdentifier, for: ip) as! NewsTableViewCell
            cell.sourceLabel.text = item.source
            cell.headlineLabel.text = item.title
            return cell
        })
        
        disposeBag.insertAll(all:
            dashboardViewModel!.sections
                .bind(to: tableView.rx.items(dataSource: dataSource!)),
                             
            tableView.rx.modelSelected(Link.self)
                .subscribe({ event in
                    if let itemViewModel = event.element,
                        let url = URL(string: itemViewModel.url) {
                        UILauncher.launchWebView(self, url: url)
                    }
                }
            )
        )
    }
    
    func showBluetoothNotification() {
        guard let dataService = self.dataService else { return }
        
        bluetoothNotificationView.alpha = 1.0
        firmwareNotificationView.alpha = 0

        
        let dateFormat = dataService.lastConnectedDateFormatted
        bluetoothNotificationDescriptionLabel.text = String.init(format: "dashboard_bluetooth_not_connected_description".localized, dateFormat)

        showNotification(true)
    }
    
    func showFirmwareNotification() {
        firmwareNotificationIconImageView.tintImageColor(color: ColorUtil.color(.Yellow))
        bluetoothNotificationView.alpha = 0
        firmwareNotificationView.alpha = 1.0


        showNotification(true)
    }
    
    func showNotification(_ show: Bool) {
        notificaftionContainerView.isHidden = !show
        updateHeaderHeight()
    }
    
    func updateHeaderHeight() {
        tableView.beginUpdates()
        var height = DashboardViewController.TABLE_HEADER_HEIGHT
        var frame = tableView.tableHeaderView!.frame

        if !notificaftionContainerView.isHidden {
            height += DashboardViewController.NOTIFICATION_VIEW_HEIGHT
            motorcycleImageTopConstraint.constant = DashboardViewController.MOTORCYCLE_IMAGE_TOP_PADDING +
                                                    DashboardViewController.NOTIFICATION_VIEW_HEIGHT
        } else {
            motorcycleImageTopConstraint.constant = DashboardViewController.MOTORCYCLE_IMAGE_TOP_PADDING
        }
        if !remoteConnectButton.isHidden {
            height += DashboardViewController.REMOTE_CONNECT_BUTTON_HEIGHT
        }
        if batteryLabel.isHidden || batteryLabel.text == "" {
            height -= DashboardViewController.VALUE_LABEL_HEIGHT
        }
        if lastCapturedRideLabel.text == "dashboard_last_captured_ride_na".localized.uppercased() {
            height -= DashboardViewController.VALUE_LABEL_HEIGHT
        }

        frame.size.height = height
        UIView.animate(withDuration: 0.5) {
            self.tableView.tableHeaderView!.frame = frame
        }
        tableView.endUpdates()
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: SectionTitleTableViewCell.ReuseIdentifier) as! SectionTitleTableViewCell
        cell.titleLabel.text = self.dataSource?.sectionModels[section].header
        return cell
    }

    // MARK: - IBAction
    @IBAction func unwindFromWebView(segue: UIStoryboardSegue) { /* Unwind segue, do nothing */ }
}
