//
//  RelivePlaybackViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 12/24/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CocoaLumberjack
import GoogleMaps

class RelivePlaybackViewModel {
    enum PlaybackSpeed: Int {
        case single, double, quad
        
        func localizedString() -> String {
            switch self {
            case .single:
                return "1x".localized
            case .double:
                return "2x".localized
            case .quad:
                return "4x".localized
            }
        }
        
        func multiplier() -> Double {
            switch self {
            case .single:
                return 1
            case .double:
                return 2
            case .quad:
                return 4
            }
        }
    }
    
    private var dataService: DataService
    private var startingOdometer: Int = 0
    
    var speed = BehaviorRelay<String>(value: "")
    var speedUnit = BehaviorRelay<String>(value: "")
    var power = BehaviorRelay<String>(value: "")
    var distance = BehaviorRelay<String>(value: "")
    var distanceUnit = BehaviorRelay<String>(value: "")
    var soc = BehaviorRelay<String>(value: "")
    var torque = BehaviorRelay<String>(value: "")
    var regen = BehaviorRelay<String>(value: "")
    var leanLeft = BehaviorRelay<String>(value: "")
    var leanRight = BehaviorRelay<String>(value: "")
    var startTime = BehaviorRelay<String>(value: "")
    var endTime = BehaviorRelay<String>(value: "")
    var currentTime = BehaviorRelay<String>(value: "")
    var playbackSpeed = BehaviorRelay<String>(value: "1x")
    var isPlaying = BehaviorRelay<Bool>(value: false)
    var currentLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var currentPlaybackSpeed = BehaviorRelay<PlaybackSpeed>(value: .single)
    var locations = BehaviorRelay<[CLLocationCoordinate2D]>(value: [CLLocationCoordinate2D]())
    var distanceData = BehaviorRelay<[Int]>(value: [Int]())
    var socData = BehaviorRelay<[Int]>(value: [Int]())
    var speedData = BehaviorRelay<[Int]>(value: [Int]())
    var powerData = BehaviorRelay<[Int]>(value: [Int]())
    var torqRegenData = BehaviorRelay<[Int]>(value: [Int]())
    var leanData = BehaviorRelay<[Int]>(value: [Int]())
    var currentIndex = BehaviorRelay<Int>(value: 0)

    private var dateFormatterShort = DateFormatter.init()
    private var dateFormatter = DateFormatter.init()
    private var timer: Timer?
    
    init(dataService: DataService) {
        self.dataService = dataService
        if dataService.using24HrClock() {
            dateFormatter.dateFormat = "HH:mm\nM/d"
            dateFormatterShort.dateFormat = "HH:mm"
        } else {
            dateFormatter.dateFormat = "hh:mma\nM/d"
            dateFormatter.amSymbol = "am"
            dateFormatter.pmSymbol = "pm"
            dateFormatterShort.dateFormat = "hh:mm"
        }
    }
    
    func loadModel() {
        if let capturedRide = dataService.capturedRide {
            locations.accept(capturedRide.getLocations())

            if dataService.usingMetricDistance {
                let speeds = capturedRide.getSpeedData()
                var convertedSpeeds = [Int]()
                for s in speeds {
                    convertedSpeeds.append(Int(Float(s) / 100))
                }
                speedData.accept(convertedSpeeds)
                speedUnit.accept("global_kph".localized)

                let dinstances = capturedRide.getDistanceData()
                var convertedDistances = [Int]()
                for d in dinstances {
                    convertedDistances.append(Int(Float(d) / 10))
                }
                distanceData.accept(convertedDistances)
                distanceUnit.accept("global_km".localized)
            } else {
                let speeds = capturedRide.getSpeedData()
                var convertedSpeeds = [Int]()
                for s in speeds {
                    convertedSpeeds.append(Int((Float(s) / 100) / Constant.KM_IN_ONE_MILE))
                }
                speedData.accept(convertedSpeeds)
                speedUnit.accept("global_mph".localized)
                
                let dinstances = capturedRide.getDistanceData()
                var convertedDinstances = [Int]()
                for d in dinstances {
                    convertedDinstances.append(Int((Float(d) / 10) / Constant.KM_IN_ONE_MILE))
                }
                distanceData.accept(convertedDinstances)
                distanceUnit.accept("global_mi".localized)
            }
            socData.accept(capturedRide.getSocData())
            powerData.accept(capturedRide.getPowerData())
            torqRegenData.accept(capturedRide.getTorqRegenData())
            
            let leans = capturedRide.getLeanData()
            var convertedLeans = [Int]()
            for l in leans {
                convertedLeans.append(Int(Float(l) / 10))
            }
            leanData.accept(convertedLeans)
            
            if let dataPoint = capturedRide.getInitialDataPoint() {
                startingOdometer = dataPoint.odometer
                acceptDataPoint(dataPoint)
            }
            
            startTime.accept(dateFormatter.string(from: capturedRide.getStartTime()))
            endTime.accept(dateFormatter.string(from: capturedRide.getEndTime()))
            isPlaying.accept(false)
            playbackSpeed.accept(currentPlaybackSpeed.value.localizedString())
        }
    }
    
    func onSliderValueChanged(_ value: Int) {
        guard let capturedRide = dataService.capturedRide else { return }
        
        if let dataPoint = capturedRide.getRideDataPointAt(value) {
            acceptDataPoint(dataPoint)
        }
        currentIndex.accept(value)
    }
    
    func onActionPressed() {
        if isPlaying.value {
            stop()
        } else {
            start()
        }
    }
    
    func start() {
        isPlaying.accept(true)
        let interval = 1.0 / currentPlaybackSpeed.value.multiplier()
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (timer) in
            guard let capturedRide = self.dataService.capturedRide else { return }
            if self.currentIndex.value >= capturedRide.getDataPointCount() {
                self.stop()
            } else {
                self.onSliderValueChanged(self.currentIndex.value + 1)
            }
        })
    }
    
    func stop() {
        isPlaying.accept(false)
        if let timer = self.timer {
            timer.invalidate()
        }
    }
    
    func onSpeedPressed() {
        switch currentPlaybackSpeed.value {
        case .single:
            currentPlaybackSpeed.accept(.double)
        case .double:
            currentPlaybackSpeed.accept(.quad)
        case .quad:
            currentPlaybackSpeed.accept(.single)
        }
        playbackSpeed.accept(currentPlaybackSpeed.value.localizedString())
        if let timer = self.timer, isPlaying.value {
            timer.invalidate()
            start()
        }
    }
    
    func acceptDataPoint(_ dataPoint: RideDataPoint) {
        let tripDistance = dataPoint.odometer - startingOdometer
        if dataService.usingMetricDistance {
            let convertedSpeed = Int(Float(dataPoint.speed) / 100)
            speed.accept("\(convertedSpeed)")
            let convertedDistance = Int(Float(tripDistance) / 10)
            distance.accept("\(convertedDistance)")
        } else {
            let convertedSpeed = Int((Float(dataPoint.speed) / 100) / Constant.KM_IN_ONE_MILE)
            speed.accept("\(convertedSpeed)")
            let convertedDistance = Int((Float(tripDistance) / 10) / Constant.KM_IN_ONE_MILE)
            distance.accept("\(convertedDistance)")
        }
        
        power.accept("\(dataPoint.power)")
        soc.accept("\(dataPoint.soc)")
        torque.accept("\(dataPoint.torque)")
        regen.accept("\(dataPoint.brakeRegen)")
        if dataPoint.leanAngle < 0 {
            leanLeft.accept("\(abs(Int(Float(dataPoint.leanAngle) / 10)))")
            leanRight.accept("0")
        } else {
            leanRight.accept("\(abs(Int(Float(dataPoint.leanAngle) / 10)))")
            leanLeft.accept("0")
        }
        
        let location = CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude)
        currentLocation.accept(location)
        currentTime.accept(dateFormatterShort.string(from: dataPoint.date))
    }
}
