//
//  ShareStatPickerViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol ShareStatPickerViewModelDelegate {
}

class ShareStatPickerViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SHARE_RIDE_STAT_PICKER }
    
    var pickerViewModel: ShareStatPickerViewModel?
    var pickerDelegate: ShareStatPickerViewModelDelegate?

    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    
    @IBOutlet weak var durationValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var distanceValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var distanceUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxSpeedValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxSpeedUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var avgSpeedValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var avgSpeedUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxPowerValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxPowerUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxTorqueValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxTorqueUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var socUsedValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var socUsedUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var energyRegeneratedValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var energyRegeneratedUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxLeanLeftValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxLeanLeftUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxLeanRightValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var maxLeanRightUnitLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var lifetimeOdoValueLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var lifetimeOdoUnitLabel: ZeroUpperCaseUILabel!

    @IBOutlet weak var durationCheckmarkImageView: UIImageView!
    @IBOutlet weak var distanceCheckmarkImageView: UIImageView!
    @IBOutlet weak var maxSpeedCheckmarkImageView: UIImageView!
    @IBOutlet weak var avgSpeedCheckmarkImageView: UIImageView!
    @IBOutlet weak var maxPowerCheckmarkImageView: UIImageView!
    @IBOutlet weak var maxTorqueCheckmarkImageView: UIImageView!
    @IBOutlet weak var socUsedCheckmarkImageView: UIImageView!
    @IBOutlet weak var energyRegeneratedCheckmarkImageView: UIImageView!
    @IBOutlet weak var maxLeftCheckmarkImageView: UIImageView!
    @IBOutlet weak var maxRightCheckmarkImageView: UIImageView!
    @IBOutlet weak var lifetimeOdoCheckmarkImageView: UIImageView!
    
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var maxSpeedView: UIView!
    @IBOutlet weak var avgSpeedView: UIView!
    @IBOutlet weak var maxPowerView: UIView!
    @IBOutlet weak var maxTorqueView: UIView!
    @IBOutlet weak var socUsedView: UIView!
    @IBOutlet weak var energyRegeneratedView: UIView!
    @IBOutlet weak var maxLeftView: UIView!
    @IBOutlet weak var maxRightView: UIView!
    @IBOutlet weak var odoView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "relive_share_pick_data_point".localized

        bindViews()
    }
    
    fileprivate func bindViews() {
        guard let viewModel = pickerViewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.pickerTitle.bind(to: titleLabel.rx.text),
            viewModel.duration.bind(to: durationValueLabel.rx.text),
            viewModel.distance.bind(to: distanceValueLabel.rx.text),
            viewModel.distanceUnit.bind(to: distanceUnitLabel.rx.text),
            viewModel.maxSpeed.bind(to: maxSpeedValueLabel.rx.text),
            viewModel.maxSpeedUnit.bind(to: maxSpeedUnitLabel.rx.text),
            viewModel.avgSpeed.bind(to: avgSpeedValueLabel.rx.text),
            viewModel.avgSpeedUnit.bind(to: avgSpeedUnitLabel.rx.text),
            viewModel.maxPower.bind(to: maxPowerValueLabel.rx.text),
            viewModel.maxTorque.bind(to: maxTorqueValueLabel.rx.text),
            viewModel.socUsed.bind(to: socUsedValueLabel.rx.text),
            viewModel.energyRegenerated.bind(to: energyRegeneratedValueLabel.rx.text),
            viewModel.maxLeanLeft.bind(to: maxLeanLeftValueLabel.rx.text),
            viewModel.maxLeanRight.bind(to: maxLeanRightValueLabel.rx.text),
            viewModel.lifetimeOdo.bind(to: lifetimeOdoValueLabel.rx.text),
            viewModel.lifetimeOdoUnit.bind(to: lifetimeOdoUnitLabel.rx.text),
            
            viewModel.durationCheckmarkIsHidden.bind(to: durationCheckmarkImageView.rx.isHidden),
            viewModel.distanceCheckmarkIsHidden.bind(to: distanceCheckmarkImageView.rx.isHidden),
            viewModel.maxSpeedCheckmarkIsHidden.bind(to: maxSpeedCheckmarkImageView.rx.isHidden),
            viewModel.avgSpeedCheckmarkIsHidden.bind(to: avgSpeedCheckmarkImageView.rx.isHidden),
            viewModel.maxPowerCheckmarkIsHidden.bind(to: maxPowerCheckmarkImageView.rx.isHidden),
            viewModel.maxTorqueCheckmarkIsHidden.bind(to: maxTorqueCheckmarkImageView.rx.isHidden),
            viewModel.socUsedCheckmarkIsHidden.bind(to: socUsedCheckmarkImageView.rx.isHidden),
            viewModel.energyRegeneratedCheckmarkIsHidden.bind(to: energyRegeneratedCheckmarkImageView.rx.isHidden),
            viewModel.maxLeanLeftCheckmarkIsHidden.bind(to: maxLeftCheckmarkImageView.rx.isHidden),
            viewModel.maxLeanRightCheckmarkIsHidden.bind(to: maxRightCheckmarkImageView.rx.isHidden),
            viewModel.lifetimeOdoCheckmarkIsHidden.bind(to: lifetimeOdoCheckmarkImageView.rx.isHidden),
            
            viewModel.durationEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.durationValueLabel, unitLabel: nil, checkImageView: strongSelf.durationCheckmarkImageView)
            },
            viewModel.distanceEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.distanceValueLabel, unitLabel: strongSelf.distanceUnitLabel, checkImageView: strongSelf.distanceCheckmarkImageView)
            },
            viewModel.maxSpeedEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.maxSpeedValueLabel, unitLabel: strongSelf.maxSpeedUnitLabel, checkImageView: strongSelf.maxSpeedCheckmarkImageView)
            },
            viewModel.avgSpeedEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.avgSpeedValueLabel, unitLabel: strongSelf.avgSpeedUnitLabel, checkImageView: strongSelf.avgSpeedCheckmarkImageView)
            },
            viewModel.maxPowerEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.maxPowerValueLabel, unitLabel: strongSelf.maxPowerUnitLabel, checkImageView: strongSelf.maxPowerCheckmarkImageView)
            },
            viewModel.maxTorqueEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.maxTorqueValueLabel, unitLabel: strongSelf.maxTorqueUnitLabel, checkImageView: strongSelf.maxTorqueCheckmarkImageView)
            },
            viewModel.socUsedEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.socUsedValueLabel, unitLabel: strongSelf.socUsedUnitLabel, checkImageView: strongSelf.socUsedCheckmarkImageView)
            },
            viewModel.energyRegeneratedEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.energyRegeneratedValueLabel, unitLabel: strongSelf.energyRegeneratedUnitLabel, checkImageView: strongSelf.energyRegeneratedCheckmarkImageView)
            },
            viewModel.maxLeanLeftEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.maxLeanLeftValueLabel, unitLabel: strongSelf.maxLeanLeftUnitLabel, checkImageView: strongSelf.maxLeftCheckmarkImageView)
            },
            viewModel.maxLeanRightEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.maxLeanRightValueLabel, unitLabel: strongSelf.maxLeanRightUnitLabel, checkImageView: strongSelf.maxRightCheckmarkImageView)
            },
            viewModel.lifetimeOdoEnabled.subscribe { [weak self] event in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.enableStat(enabled: enabled, valueLabel: strongSelf.lifetimeOdoValueLabel, unitLabel: strongSelf.lifetimeOdoUnitLabel, checkImageView: strongSelf.lifetimeOdoCheckmarkImageView)
            },
            
            viewModel.onStatPicked.subscribe { [weak self] Event in
                guard let strongSelf = self else { return }
                if let navVC = strongSelf.navigationController {
                    navVC.popViewController(animated: true)
                } else {
                    strongSelf.dismiss(animated: true, completion: nil)
                }
            },
            
            durationView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onDurationSelected()
            },
            distanceView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onDistanceSelected()
            },
            maxSpeedView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onMaxSpeedSelected()
            },
            avgSpeedView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onAvgSpeedSelected()
            },
            maxPowerView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onMaxPowerSelected()
            },
            maxTorqueView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onMaxTorqueSelected()
            },
            socUsedView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onSocUsedSelected()
            },
            /*energyRegeneratedView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onEnergyRegeneratedSelected()
            },*/
            maxLeftView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onMaxLeanLeftSelected()
            },
            maxRightView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onMaxLeanRightSelected()
            },
            odoView.rx.tapGesture().subscribe { event in
                guard let recognizer = event.element, recognizer.state == .recognized else { return }
                viewModel.onOdometerSelected()
            }
        )
        
        viewModel.loadModel()
    }
    
    private func enableStat(enabled: Bool, valueLabel: UILabel, unitLabel: UILabel?, checkImageView: UIImageView) {
        if enabled {
            valueLabel.textColor = ColorUtil.color(.White)
            checkImageView.tintColor = ColorUtil.color(.White)
            unitLabel?.textColor = ColorUtil.color(.White)
        } else {
            valueLabel.textColor = ColorUtil.color(.LightGray)
            checkImageView.tintColor = ColorUtil.color(.LightGray)
            unitLabel?.textColor = ColorUtil.color(.LightGray)
        }
    }

    @IBAction func onBackPressed(_ sender: Any) {
        if let navVC = self.navigationController {
            navVC.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
