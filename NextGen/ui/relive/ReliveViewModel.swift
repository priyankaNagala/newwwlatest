//
//  ReliveViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 12/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GoogleMaps

class ReliveViewModel {
    var dataService: DataService

    var maxSpeed = BehaviorRelay<String>(value: "")
    var maxPower = BehaviorRelay<String>(value: "")
    var maxTorque = BehaviorRelay<String>(value: "")
    var maxRegen = BehaviorRelay<String>(value: "")
    var maxLeanLeft = BehaviorRelay<String>(value: "")
    var maxLeanRight = BehaviorRelay<String>(value: "")
    
    var maxSpeedLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var maxPowerLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var maxTorqueLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var maxRegenLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var maxLeanLeftLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    var maxLeanRightLocation = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D.init())
    
    var locations = BehaviorRelay<[CLLocationCoordinate2D]>(value: [CLLocationCoordinate2D]())

    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func loadModel() {
        if let capturedRide = dataService.capturedRide {
            locations.accept(capturedRide.getLocations())
            
            if let dataPoint = capturedRide.getMaxSpeedDataPoint() {
                if dataService.usingMetricDistance {
                    let converted = Int(Float(dataPoint.speed) / 100)
                    maxSpeed.accept("\(converted)" + "global_kph".localized)
                } else {
                    let converted = Int((Float(dataPoint.speed) / 100) / Constant.KM_IN_ONE_MILE)
                    maxSpeed.accept("\(converted)" + "global_mph".localized)
                }
                maxSpeedLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
            if let dataPoint = capturedRide.getMaxPowerDataPoint() {
                maxPower.accept("\(dataPoint.power)" + "global_kw".localized)
                maxPowerLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
            if let dataPoint = capturedRide.getMaxTorqueDataPoint() {
                maxTorque.accept("\(dataPoint.torque)" + "global_nm".localized)
                maxTorqueLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
            if let dataPoint = capturedRide.getMaxRegenDataPoint() {
                let value = dataPoint.brakeRegen < dataPoint.coastRegen ? dataPoint.brakeRegen : dataPoint.coastRegen
                maxRegen.accept("\(value)" + "global_nm".localized)
                maxRegenLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
            if let dataPoint = capturedRide.getMaxLeanLeftDataPoint() {
                let leanAngle = abs(Int(Float(dataPoint.leanAngle) / 10))
                maxLeanLeft.accept("\(leanAngle)" + "global_degree".localized)
                maxLeanLeftLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
            if let dataPoint = capturedRide.getMaxLeanRightDataPoint() {
                let leanAngle = abs(Int(Float(dataPoint.leanAngle) / 10))
                maxLeanRight.accept("\(leanAngle)" + "global_degree".localized)
                maxLeanRightLocation.accept(CLLocationCoordinate2D.init(latitude: dataPoint.latitutude, longitude: dataPoint.longitude))
            }
        } else {
            maxSpeed.accept("global_empty_data".localized + "global_mph".localized)
            maxPower.accept("global_empty_data".localized + "global_kw".localized)
            maxTorque.accept("global_empty_data".localized + "global_nm".localized)
            maxRegen.accept("global_empty_data".localized + "global_nm".localized)
            maxLeanLeft.accept("global_empty_data".localized + "global_degree".localized)
            maxLeanRight.accept("global_empty_data".localized + "global_degree".localized)
            locations.accept([CLLocationCoordinate2D]())

        }
    }
}
