//
//  ShareStatPickerViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class ShareStatPickerViewModel {
    enum StatLocation {
        case Left, Center, Right
        
        func localizedString() -> String {
            switch self {
            case .Left:
                return "relive_share_left".localized
            case .Center:
                return "relive_share_center".localized
            case .Right:
                return "relive_share_right".localized
            }
        }
    }
    
    private var statLocation:StatLocation = .Left
    
    var dataService: DataService
    
    var pickerTitle = BehaviorRelay<String>(value: "")

    var duration = BehaviorRelay<String>(value: "")
    var durationCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var durationEnabled = BehaviorRelay<Bool>(value: true)
    var distance = BehaviorRelay<String>(value: "")
    var distanceCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var distanceEnabled = BehaviorRelay<Bool>(value: true)
    var distanceUnit = BehaviorRelay<String>(value: "")
    var maxSpeed = BehaviorRelay<String>(value: "")
    var maxSpeedCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var maxSpeedEnabled = BehaviorRelay<Bool>(value: true)
    var maxSpeedUnit = BehaviorRelay<String>(value: "")
    var avgSpeed = BehaviorRelay<String>(value: "")
    var avgSpeedCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var avgSpeedEnabled = BehaviorRelay<Bool>(value: true)
    var avgSpeedUnit = BehaviorRelay<String>(value: "")
    var maxPower = BehaviorRelay<String>(value: "")
    var maxPowerCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var maxPowerEnabled = BehaviorRelay<Bool>(value: true)
    var maxTorque = BehaviorRelay<String>(value: "")
    var maxTorqueCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var maxTorqueEnabled = BehaviorRelay<Bool>(value: true)
    var socUsed = BehaviorRelay<String>(value: "")
    var socUsedCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var socUsedEnabled = BehaviorRelay<Bool>(value: true)
    var energyRegenerated = BehaviorRelay<String>(value: "")
    var energyRegeneratedCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var energyRegeneratedEnabled = BehaviorRelay<Bool>(value: true)
    var maxLeanLeft = BehaviorRelay<String>(value: "")
    var maxLeanLeftCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var maxLeanLeftEnabled = BehaviorRelay<Bool>(value: true)
    var maxLeanRight = BehaviorRelay<String>(value: "")
    var maxLeanRightCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var maxLeanRightEnabled = BehaviorRelay<Bool>(value: true)
    var lifetimeOdo = BehaviorRelay<String>(value: "")
    var lifetimeOdoCheckmarkIsHidden = BehaviorRelay<Bool>(value: true)
    var lifetimeOdoEnabled = BehaviorRelay<Bool>(value: true)
    var lifetimeOdoUnit = BehaviorRelay<String>(value: "")
    
    var onStatPicked = PublishRelay<Bool>()
    
    init(dataService: DataService) {
        self.dataService = dataService
    }

    func setStatLocation(_ statLocation: StatLocation) {
        self.statLocation = statLocation
        pickerTitle.accept(statLocation.localizedString())
    }

    func loadModel() {
        pickerTitle.accept(statLocation.localizedString())
        
        if let capturedRide = dataService.capturedRide {
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: duration, unit: nil, stat: .Duration)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: distance, unit: distanceUnit, stat: .Distance)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: maxSpeed, unit: maxSpeedUnit, stat: .MaxSpeed)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: avgSpeed, unit: avgSpeedUnit, stat: .AvgSpeed)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: maxPower, unit: nil, stat: .MaxPower)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: maxTorque, unit: nil, stat: .MaxTorque)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: socUsed, unit: nil, stat: .SocUsed)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: energyRegenerated, unit: nil, stat: .EnergyRegenerated)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: maxLeanLeft, unit: nil, stat: .MaxLeanLeft)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: maxLeanRight, unit: nil, stat: .MaxLeanRight)
            CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: lifetimeOdo, unit: lifetimeOdoUnit, stat: .LifetimeOdo)
        }
        
        let leftStat = dataService.leftShareStat
        let centerStat = dataService.centerShareStat
        let rightStat = dataService.rightShareStat
        
        acceptIsHidden(leftStat)
        acceptIsHidden(centerStat)
        acceptIsHidden(rightStat)
        
        switch statLocation {
        case .Left:
            acceptIsEnabled(rightStat)
            acceptIsEnabled(centerStat)
        case .Center:
            acceptIsEnabled(rightStat)
            acceptIsEnabled(leftStat)
        case .Right:
            acceptIsEnabled(leftStat)
            acceptIsEnabled(centerStat)
        }
    }
    
    func onDurationSelected() {
        if durationEnabled.value {
            updateSelectedStat(.Duration)
        }
    }

    func onDistanceSelected() {
        if distanceEnabled.value {
            updateSelectedStat(.Distance)
        }
    }

    func onMaxSpeedSelected() {
        if maxSpeedEnabled.value {
            updateSelectedStat(.MaxSpeed)
        }
    }

    func onAvgSpeedSelected() {
        if avgSpeedEnabled.value {
            updateSelectedStat(.AvgSpeed)
        }
    }

    func onMaxPowerSelected() {
        if maxPowerEnabled.value {
            updateSelectedStat(.MaxPower)
        }
    }

    func onMaxTorqueSelected() {
        if maxTorqueEnabled.value {
            updateSelectedStat(.MaxTorque)
        }
    }

    func onSocUsedSelected() {
        if socUsedEnabled.value {
            updateSelectedStat(.SocUsed)
        }
    }

    func onEnergyRegeneratedSelected() {
        if energyRegeneratedEnabled.value {
            updateSelectedStat(.EnergyRegenerated)
        }
    }

    func onMaxLeanLeftSelected() {
        if maxLeanLeftEnabled.value {
            updateSelectedStat(.MaxLeanLeft)
        }
    }
    
    func onMaxLeanRightSelected() {
        if maxLeanRightEnabled.value {
            updateSelectedStat(.MaxLeanRight)
        }
    }

    func onOdometerSelected() {
        if lifetimeOdoEnabled.value {
            updateSelectedStat(.LifetimeOdo)
        }
    }

    fileprivate func updateSelectedStat(_ stat: Settings.ShareStat) {
        switch statLocation {
        case .Left:
            dataService.leftShareStat = stat
        case .Center:
            dataService.centerShareStat = stat
        case .Right:
            dataService.rightShareStat = stat
        }
        onStatPicked.accept(true)
    }
    
    fileprivate func acceptIsHidden(_ stat: Settings.ShareStat) {
        switch stat {
        case .Duration:
            durationCheckmarkIsHidden.accept(false)
        case .Distance:
            distanceCheckmarkIsHidden.accept(false)
        case .MaxSpeed:
            maxSpeedCheckmarkIsHidden.accept(false)
        case .AvgSpeed:
            avgSpeedCheckmarkIsHidden.accept(false)
        case .MaxPower:
            maxPowerCheckmarkIsHidden.accept(false)
        case .MaxTorque:
            maxTorqueCheckmarkIsHidden.accept(false)
        case .SocUsed:
            socUsedCheckmarkIsHidden.accept(false)
        case .EnergyRegenerated:
            energyRegeneratedCheckmarkIsHidden.accept(false)
        case .MaxLeanLeft:
            maxLeanLeftCheckmarkIsHidden.accept(false)
        case .MaxLeanRight:
            maxLeanRightCheckmarkIsHidden.accept(false)
        case .LifetimeOdo:
            lifetimeOdoCheckmarkIsHidden.accept(false)
        }
    }

    fileprivate func acceptIsEnabled(_ stat: Settings.ShareStat) {
        switch stat {
        case .Duration:
            durationEnabled.accept(false)
        case .Distance:
            distanceEnabled.accept(false)
        case .MaxSpeed:
            maxSpeedEnabled.accept(false)
        case .AvgSpeed:
            avgSpeedEnabled.accept(false)
        case .MaxPower:
            maxPowerEnabled.accept(false)
        case .MaxTorque:
            maxTorqueEnabled.accept(false)
        case .SocUsed:
            socUsedEnabled.accept(false)
        case .EnergyRegenerated:
            energyRegeneratedEnabled.accept(false)
        case .MaxLeanLeft:
            maxLeanLeftEnabled.accept(false)
        case .MaxLeanRight:
            maxLeanRightEnabled.accept(false)
        case .LifetimeOdo:
            lifetimeOdoEnabled.accept(false)
        }
    }
}
