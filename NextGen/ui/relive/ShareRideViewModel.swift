//
//  ShareRideViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack
import GoogleMaps

class ShareRideViewModel {
    var dataService: DataService

    var locations = BehaviorRelay<[CLLocationCoordinate2D]>(value: [CLLocationCoordinate2D]())
    var leftStatTitle = BehaviorRelay<String>(value: "")
    var centerStatTitle = BehaviorRelay<String>(value: "")
    var rightStatTitle = BehaviorRelay<String>(value: "")
    
    var leftStatValue = BehaviorRelay<String>(value: "")
    var centerStatValue = BehaviorRelay<String>(value: "")
    var rightStatValue = BehaviorRelay<String>(value: "")
    
    var leftStatUnit = BehaviorRelay<String>(value: "")
    var centerStatUnit = BehaviorRelay<String>(value: "")
    var rightStatUnit = BehaviorRelay<String>(value: "")

    fileprivate var modelLoaded = false
    fileprivate var appeared = false
    
    init(dataService: DataService) {
        self.dataService = dataService
    }

    func onViewWillAppear() {
        if modelLoaded && appeared {
            acceptStats()
        }
        appeared = true
    }
    
    func loadModel() {
        if let capturedRide = dataService.capturedRide {
            locations.accept(capturedRide.getLocations())
        }
        acceptStats()
        modelLoaded = true
    }
    
    fileprivate func acceptStats() {
        let leftShareStat = dataService.leftShareStat
        let centerShareStat = dataService.centerShareStat
        let rightShareStat = dataService.rightShareStat

        leftStatTitle.accept(leftShareStat.localizedString())
        centerStatTitle.accept(centerShareStat.localizedString())
        rightStatTitle.accept(rightShareStat.localizedString())
        
        leftStatUnit.accept("")
        centerStatUnit.accept("")
        rightStatUnit.accept("")

        guard let capturedRide = dataService.capturedRide else { return }
        CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: leftStatValue, unit: leftStatUnit, stat: leftShareStat)
        CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: rightStatValue, unit: rightStatUnit, stat: rightShareStat)
        CapturedRide.acceptStatValueUnit(capturedRide: capturedRide, value: centerStatValue, unit: centerStatUnit, stat: centerShareStat)
    }
}
