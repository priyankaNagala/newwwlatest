//
//  RelivePlaybackViewController.swift
//  nextgen
//
//  Created by Scott Wang on 12/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import GoogleMaps
import CocoaLumberjack

class RelivePlaybackViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_RELIVE_RIDE }

    // Playback controls
    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var playbackSpeedButton: UIButton!
    
    @IBOutlet weak var speedValueLabel: UILabel!
    @IBOutlet weak var speedUnitLabel: UILabel!
    @IBOutlet weak var powerValueLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    @IBOutlet weak var distanceUnitLabel: UILabel!
    @IBOutlet weak var socValueLabel: UILabel!
    @IBOutlet weak var torqueValueLabel: UILabel!
    @IBOutlet weak var regenValueLabel: UILabel!
    @IBOutlet weak var leanLeftValueLabel: UILabel!
    @IBOutlet weak var leanRightValueLabel: UILabel!
    
    @IBOutlet weak var speedPowerChart: LineChart!
    @IBOutlet weak var distanceSocChart: LineChart!
    @IBOutlet weak var torqueRegenChart: LineChart!
    @IBOutlet weak var leanChart: LineChart!
    // TODO: Probably need to take into account any unit conversions in this VC
    
    var viewModel: RelivePlaybackViewModel?
    var routePolyline = MapUtil.getStyledPolyline()
    var startMarker = GMSMarker.init()
    var endMarker = GMSMarker.init()
    var currentMarker = GMSMarker.init()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupPlaybackView()
        setupMap()
        bindViews()
    }

    // MARK: -
    
    func setupPlaybackView() {
        progressSlider.setThumbImage(UIImage.init(named: "ic_map_dot_black_large_center"), for: .normal)
    }
    
    func setupMap() {
        mapContainerView.isMyLocationEnabled = true
        mapContainerView.mapStyle = MapUtil.getMapStyle()
        routePolyline.map = mapContainerView

        startMarker.icon = UIImage.init(named: "ic_map_dot_white_center")
        startMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        endMarker.icon = UIImage.init(named: "ic_map_dot_black_center")
        endMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        currentMarker.icon = UIImage.init(named: "ic_map_dot_black_large_center")
        currentMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
    }
    
    func bindViews() {
        guard let viewModel = self.viewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.speed.bind(to: speedValueLabel.rx.text),
            viewModel.power.bind(to: powerValueLabel.rx.text),
            viewModel.distance.bind(to: distanceValueLabel.rx.text),
            viewModel.soc.bind(to: socValueLabel.rx.text),
            viewModel.torque.bind(to: torqueValueLabel.rx.text),
            viewModel.regen.bind(to: regenValueLabel.rx.text),
            viewModel.leanLeft.bind(to: leanLeftValueLabel.rx.text),
            viewModel.leanRight.bind(to: leanRightValueLabel.rx.text),
            viewModel.startTime.bind(to: startTimeLabel.rx.text),
            viewModel.endTime.bind(to: endTimeLabel.rx.text),
            viewModel.currentTime.bind(to: currentTimeLabel.rx.text),
            viewModel.playbackSpeed.bind(to: playbackSpeedButton.rx.title(for: .normal)),

            viewModel.speedUnit.subscribe { [weak self] event in
                guard let strongSelf = self, let unit = event.element else { return }
                strongSelf.speedPowerChart.data1Label = unit
                strongSelf.speedUnitLabel.text = unit
            },
            viewModel.distanceUnit.subscribe { [weak self] event in
                guard let strongSelf = self, let unit = event.element else { return }
                strongSelf.distanceSocChart.data1Label = unit
                strongSelf.distanceUnitLabel.text = unit
            },

            viewModel.locations.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let locations = event.element {
                    DispatchQueue.global(qos: .background).async {
                        let path = GMSMutablePath.init()
                        for location in locations {
                            if location.latitude != 0 && location.longitude != 0 {
                                let loc = CLLocation.init(latitude: location.latitude, longitude: location.longitude)
                                path.add(loc.coordinate)
                            }
                        }
                        DispatchQueue.main.async {
                            if locations.count > 0 {
                                var startLocation = locations[0]
                                for location in locations {
                                    startLocation = location
                                    if location.latitude != 0 && location.longitude != 0 {
                                        break
                                    }
                                }
                                strongSelf.startMarker.position = startLocation
                                strongSelf.startMarker.map = strongSelf.mapContainerView
                                
                                var endLocation = locations[locations.count - 1]
                                for location in locations.reversed() {
                                    endLocation = location
                                    if location.latitude != 0 && location.longitude != 0 {
                                        break
                                    }
                                }
                                strongSelf.endMarker.position = endLocation
                                strongSelf.endMarker.map = strongSelf.mapContainerView
                                
                                MapUtil.centerMapOnLocation(map: strongSelf.mapContainerView, start: startLocation, end: endLocation)
                            }
                            
                            strongSelf.routePolyline.path = path
                        }
                    }
                    strongSelf.progressSlider.minimumValue = 0
                    strongSelf.progressSlider.maximumValue = Float(locations.count - 1)
                }
            },
            
            viewModel.currentLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.currentMarker.position = location
                    strongSelf.currentMarker.map = strongSelf.mapContainerView
                }
            },
            
            viewModel.isPlaying.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let isPlaying = event.element {
                    if isPlaying {
                        strongSelf.actionButton.setTitle("", for: .normal)
                    } else {
                        strongSelf.actionButton.setTitle("", for: .normal)
                    }
                }
            },
            
            viewModel.distanceData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let distances = event.element {
                    strongSelf.distanceSocChart.data1 = distances
                }
            },
            viewModel.socData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let socs = event.element {
                    strongSelf.distanceSocChart.data2 = socs
                }
            },
            viewModel.powerData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let powers = event.element {
                    strongSelf.speedPowerChart.data2 = powers
                }
            },
            viewModel.speedData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let speeds = event.element {
                    strongSelf.speedPowerChart.data1 = speeds
                }
            },
            viewModel.torqRegenData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let torqRegen = event.element {
                    strongSelf.torqueRegenChart.data1 = torqRegen
                }
            },
            viewModel.leanData.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let lean = event.element {
                    strongSelf.leanChart.data1 = lean
                }
            },
            viewModel.currentIndex.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let index = event.element {
                    strongSelf.distanceSocChart.currentIndex = index
                    strongSelf.speedPowerChart.currentIndex = index
                    strongSelf.torqueRegenChart.currentIndex = index
                    strongSelf.leanChart.currentIndex = index
                    strongSelf.progressSlider.value = Float(index)
                    strongSelf.positionTimeLabel(Float(index))
                }
            },

            actionButton.rx.tap.subscribe { event in
                viewModel.onActionPressed()
            },
            
            playbackSpeedButton.rx.tap.subscribe { event in
                viewModel.onSpeedPressed()
            }
        )
        viewModel.loadModel()
    }
    
    func positionTimeLabel(_ value: Float) {
        guard progressSlider.maximumValue != progressSlider.minimumValue else { return }
        // Position current time label above thumb image
        let width = progressSlider.frame.size.width
        let percent = value / (progressSlider.maximumValue - progressSlider.minimumValue)
        var centerX = progressSlider.frame.origin.x + (width * CGFloat(percent))
        
        let labelHalfWidth = (currentTimeLabel.frame.width / 2)
        
        // Fix min to align leading
        if centerX - labelHalfWidth < progressSlider.frame.origin.x {
            centerX = progressSlider.frame.origin.x + labelHalfWidth
        }
        // Fix max to align trailing
        if centerX + labelHalfWidth > progressSlider.frame.origin.x + progressSlider.frame.size.width {
            centerX = progressSlider.frame.origin.x + progressSlider.frame.size.width - labelHalfWidth
        }
        currentTimeLabel.center = CGPoint.init(x: centerX, y: currentTimeLabel.center.y)
    }

    // MARK: - IBAction
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onSliderValueChanged(Int(sender.value))
    }
}
