//
//  ReliveViewController.swift
//  nextgen
//
//  Created by Scott Wang on 12/16/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift
import CocoaLumberjack
import SwinjectStoryboard

class ReliveViewController: ZeroUIViewController, RideDataSheetDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_RELIVE_RIDE }

    static let MARKER_WIDTH = 70
    static let MARKER_HEIGHT = 35
    
    @IBOutlet weak var mapContainerView: GMSMapView!
    
    var maxSpeedMarker = GMSMarker.init()
    var maxPowerMarker = GMSMarker.init()
    var maxTorqueMarker = GMSMarker.init()
    var maxRegenMarker = GMSMarker.init()
    var maxLeanLeftMarker = GMSMarker.init()
    var maxLeanRightMarker = GMSMarker.init()
    var startMarker = GMSMarker.init()
    var endMarker = GMSMarker.init()
    
    var routePolyLine = MapUtil.getStyledPolyline()
    var sheetAdded = false
    
    var viewModel: ReliveViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupMap()
        bindViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !sheetAdded {
            sheetAdded = true
            addRideDataSheet()
        }
    }
    
    func bindViews() {
        guard let viewModel = self.viewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.maxSpeed.bind(to: (maxSpeedMarker.iconView as! MapMarkerView).label.rx.text),
            viewModel.maxPower.bind(to: (maxPowerMarker.iconView as! MapMarkerView).label.rx.text),
            viewModel.maxTorque.bind(to: (maxTorqueMarker.iconView as! MapMarkerView).label.rx.text),
            viewModel.maxRegen.bind(to: (maxRegenMarker.iconView as! MapMarkerView).label.rx.text),
            viewModel.maxLeanLeft.bind(to: (maxLeanLeftMarker.iconView as! MapMarkerView).label.rx.text),
            viewModel.maxLeanRight.bind(to: (maxLeanRightMarker.iconView as! MapMarkerView).label.rx.text),
            
            viewModel.maxSpeedLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxSpeedMarker.position = location
                }
            },
            viewModel.maxPowerLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxPowerMarker.position = location
                }
            },
            viewModel.maxTorqueLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxTorqueMarker.position = location
                }
            },
            viewModel.maxRegenLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxRegenMarker.position = location
                }
            },
            viewModel.maxLeanLeftLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxLeanLeftMarker.position = location
                }
            },
            viewModel.maxLeanRightLocation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let location = event.element {
                    strongSelf.maxLeanRightMarker.position = location
                }
            },
            
            viewModel.locations.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                DispatchQueue.global(qos: .background).async {
                    if let locations = event.element {
                        let path = GMSMutablePath.init()
                        for location in locations {
                            if location.longitude != 0 && location.latitude != 0 {
                                let loc = CLLocation.init(latitude: location.latitude, longitude: location.longitude)
                                path.add(loc.coordinate)
                            }
                        }
                        DispatchQueue.main.async {
                            if locations.count > 0 {
                                var startLocation = locations[0]
                                for location in locations {
                                    startLocation = location
                                    if location.latitude != 0 && location.longitude != 0 {
                                        break
                                    }
                                }
                                strongSelf.startMarker.position = startLocation
                                strongSelf.startMarker.map = strongSelf.mapContainerView
                                
                                var endLocation = locations[locations.count - 1]
                                for location in locations.reversed() {
                                    endLocation = location
                                    if location.latitude != 0 && location.longitude != 0 {
                                        break
                                    }
                                }
                                strongSelf.endMarker.position = endLocation
                                strongSelf.endMarker.map = strongSelf.mapContainerView
                                
                                MapUtil.centerMapOnLocation(map: strongSelf.mapContainerView, start: startLocation, end: endLocation, padding:100.0)
                            }

                            strongSelf.routePolyLine.path = path
                        }
                    }
                }
            }
        )
        
        viewModel.loadModel()
    }

    func setupMap() {
        mapContainerView.isMyLocationEnabled = true
        mapContainerView.mapStyle = MapUtil.getMapStyle()
        
        routePolyLine.map = mapContainerView
        setupMarkers()
    }
    
    func setupMarkers() {
        maxSpeedMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width: ReliveViewController.MARKER_WIDTH, height:  ReliveViewController.MARKER_HEIGHT))
        maxPowerMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width:  ReliveViewController.MARKER_WIDTH, height: ReliveViewController.MARKER_HEIGHT))
        maxTorqueMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width:  ReliveViewController.MARKER_WIDTH, height: ReliveViewController.MARKER_HEIGHT))
        maxRegenMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width:  ReliveViewController.MARKER_WIDTH, height: ReliveViewController.MARKER_HEIGHT))
        maxLeanLeftMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width:  ReliveViewController.MARKER_WIDTH, height: ReliveViewController.MARKER_HEIGHT))
        maxLeanRightMarker.iconView = MapMarkerView.init(frame: CGRect(x: 0, y: 0, width:  ReliveViewController.MARKER_WIDTH, height: ReliveViewController.MARKER_HEIGHT))
        startMarker.icon = UIImage.init(named: "ic_map_dot_white_center")
        startMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        endMarker.icon = UIImage.init(named: "ic_map_dot_black_center")
        endMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
    }
    
    func addRideDataSheet() {
        // https://github.com/AhmedElassuty/BottomSheetController
        // 1- Init RideDataSheetVC
        let bundle = Bundle(for: RideDataSheetViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Relive", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let rideDataSheetVC = storyboard.instantiateViewController(withIdentifier: "RideDataSheetViewController") as! RideDataSheetViewController
        rideDataSheetVC.delegate = self
        
        // 2- Add InfoSheetVC as a child view
        self.addChildViewController(rideDataSheetVC)
        self.view.addSubview(rideDataSheetVC.view)
        rideDataSheetVC.didMove(toParentViewController: self)
        
        // 3- Adjust InfoSheet frame and initial position.
        let height = view.frame.height
        let width  = view.frame.width
        rideDataSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    
    
    // MARK: - IBAction
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - RideDataSheetDelegate
    func enableMaxSpeed(_ enable: Bool) {
        maxSpeedMarker.map = enable ? mapContainerView : nil
    }
    
    func enableMaxPower(_ enable: Bool) {
        maxPowerMarker.map = enable ? mapContainerView : nil
    }
    
    func enableMaxTorque(_ enable: Bool) {
        maxTorqueMarker.map = enable ? mapContainerView : nil
    }
    
    func enableMaxRegen(_ enable: Bool) {
        maxRegenMarker.map = enable ? mapContainerView : nil
    }
    
    func enableMaxLeanLeft(_ enable: Bool) {
        maxLeanLeftMarker.map = enable ? mapContainerView : nil
    }
    
    func enableMaxLeanRight(_ enable: Bool) {
        maxLeanRightMarker.map = enable ? mapContainerView : nil
    }
    
    func launchRelivePlayback() {
        UILauncher.launchRelivePlayback(self, rideName: self.title ?? "")
    }
}
