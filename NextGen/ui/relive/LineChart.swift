//
//  LineChart.swift
//  nextgen
//
//  Created by Scott Wang on 1/6/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

@IBDesignable class LineChart: UIView {
    // MARK: - Settings
    
    @IBInspectable public var data1Label: String = ""
    @IBInspectable public var data2Label: String = ""
    @IBInspectable public var data1Color: UIColor = ColorUtil.color(.Yellow)
    @IBInspectable public var data2Color: UIColor = ColorUtil.color(.Orange)
    @IBInspectable public var axisColor: UIColor = ColorUtil.color(.Gray)
    @IBInspectable public var gridColor: UIColor = ColorUtil.color(.BarShadow)
    @IBInspectable public var indicatorColor: UIColor = ColorUtil.color(.Green)
    
    @IBInspectable public var displayNegative: Bool = false
    @IBInspectable public var data1NegativeLabel: String = ""
    @IBInspectable public var data2NegativeLabel: String = ""
    
    var data1: [Int] = [Int]()
    var data2: [Int] = [Int]()
    var currentIndex: Int = 0 {
        didSet {
            drawIndicator()
        }
    }
    
    fileprivate var chartLayer: CALayer = CALayer()
    fileprivate var indicatorLayer: CALayer = CALayer()
    
    fileprivate let labelGutterWidth: CGFloat = 45.0
    fileprivate let gridWidth: CGFloat = 30.0
    fileprivate let labelHeight: CGFloat = 15.0

    // MARK: - Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    fileprivate func sharedInit() {
        layer.addSublayer(chartLayer)
        self.chartLayer.frame = bounds
        self.drawChart()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.redraw()
    }
    
    // MARK: -

    fileprivate func redraw() {
        self.chartLayer.removeFromSuperlayer()
        
        self.chartLayer = CALayer()
        layer.addSublayer(self.chartLayer)
        self.chartLayer.frame = bounds
        
        self.drawChart()
    }
    
    fileprivate func drawChart() {
        self.drawAxis()
        self.drawGrid()
        self.drawLabels()
        self.drawDataLine()
        self.drawIndicator()
    }
    
    fileprivate func drawAxis() {
        // X Axis
        var axisY = bounds.height - 1
        if displayNegative {
            axisY = bounds.height / 2
        }
        
        let xAxisLayer = CAShapeLayer()
        let xAxisPath = UIBezierPath.init()

        xAxisPath.move(to: CGPoint.init(x: labelGutterWidth, y: axisY))
        xAxisPath.addLine(to: CGPoint.init(x: bounds.width - labelGutterWidth, y: axisY))
        
        xAxisLayer.frame = bounds
        xAxisLayer.path = xAxisPath.cgPath
        xAxisLayer.strokeColor = axisColor.cgColor
        xAxisLayer.fillColor = UIColor.clear.cgColor
        xAxisLayer.lineWidth = 1.0
        chartLayer.addSublayer(xAxisLayer)
        
        // Left Y Axis
        if data1Label.count > 0 {
            let yAxisLayer = CAShapeLayer()
            let yAxisPath = UIBezierPath.init()
            yAxisPath.move(to: CGPoint.init(x: labelGutterWidth, y: 0))
            yAxisPath.addLine(to: CGPoint.init(x: labelGutterWidth, y: bounds.height - 1))
            
            yAxisLayer.frame = bounds
            yAxisLayer.path = yAxisPath.cgPath
            yAxisLayer.strokeColor = data1Color.cgColor
            yAxisLayer.fillColor = UIColor.clear.cgColor
            yAxisLayer.lineWidth = 1.0
            chartLayer.addSublayer(yAxisLayer)
        }
        // Right Y Axis
        if data2Label.count > 0 {
            let yAxisLayer = CAShapeLayer()
            let yAxisPath = UIBezierPath.init()
            yAxisPath.move(to: CGPoint.init(x: bounds.width - labelGutterWidth, y: 0))
            yAxisPath.addLine(to: CGPoint.init(x: bounds.width - labelGutterWidth, y: bounds.height - 1))
            
            yAxisLayer.frame = bounds
            yAxisLayer.path = yAxisPath.cgPath
            yAxisLayer.strokeColor = data2Color.cgColor
            yAxisLayer.fillColor = UIColor.clear.cgColor
            yAxisLayer.lineWidth = 1.0
            chartLayer.addSublayer(yAxisLayer)
        }
    }
    
    fileprivate func drawGrid() {
        var position = labelGutterWidth + gridWidth
        let end = bounds.width - labelGutterWidth
        let gridLayer = CAShapeLayer()
        let gridPath = UIBezierPath.init()
        while position < end {
            gridPath.move(to: CGPoint.init(x: position, y: 0))
            gridPath.addLine(to: CGPoint.init(x: position, y: bounds.height - 1))
            
            position = position + gridWidth
        }
        gridLayer.frame = bounds
        gridLayer.path = gridPath.cgPath
        gridLayer.strokeColor = gridColor.cgColor
        gridLayer.fillColor = UIColor.clear.cgColor
        gridLayer.lineWidth = 1.0
        chartLayer.addSublayer(gridLayer)
    }
    
    fileprivate func drawDataLine() {
        drawDataLine(data: data1, color:data1Color)
        drawDataLine(data: data2, color:data2Color)
    }
    
    fileprivate func drawDataLine(data: [Int], color: UIColor) {
        if data.count > 0 {
            let hPercentage:CGFloat = 1.0 / CGFloat(data.count - 1)
            var maxVal = data.max()!
            var minVal = 0
            if displayNegative {
                minVal = data.min()!
                if abs(maxVal) > abs(minVal) {
                    minVal = -maxVal
                } else {
                    maxVal = -minVal
                }
            }
            let range = CGFloat(maxVal - minVal)
            
            let path = UIBezierPath.init()
            for index in 0..<data.count {
                let vPercentage:CGFloat = CGFloat(data[index] - minVal) / range
                let dataPoint = CGPoint.init(x: CGFloat(index) * hPercentage * (self.bounds.width - 2*labelGutterWidth)+labelGutterWidth,
                                             y: (1.0 - vPercentage) * (self.bounds.height))
                if index == 0 {
                    path.move(to: dataPoint)
                } else {
                    path.addLine(to: dataPoint)
                }
            }
            let chartLineLayer = CAShapeLayer()
            chartLineLayer.frame = self.bounds
            chartLineLayer.path = path.cgPath
            chartLineLayer.strokeColor = color.cgColor
            chartLineLayer.fillColor = UIColor.clear.cgColor
            chartLineLayer.lineWidth = 1.0
            
            chartLayer.addSublayer(chartLineLayer)
        }
    }

    fileprivate func drawLabels() {
        if data1Label.count > 0 {
            addLabel(string: data1Label.localized, color: data1Color, point: CGPoint.init(x: 0, y: 0))
            
            if displayNegative {
                var label = data1Label.localized
                if data1NegativeLabel.count > 0 {
                    label = data1NegativeLabel.localized
                }
                addLabel(string: label, color: data1Color, point: CGPoint.init(x: 0, y: bounds.height - labelHeight))
            }
        }
        if data2Label.count > 0 {
            addLabel(string: data2Label.localized, color: data2Color, point: CGPoint.init(x: bounds.width - labelGutterWidth, y: 0))
            
            if displayNegative {
                var label = data2Label.localized
                if data2NegativeLabel.count > 0 {
                    label = data2NegativeLabel.localized
                }
                addLabel(string: label, color: data2Color, point: CGPoint.init(x: bounds.width - labelGutterWidth, y: bounds.height - labelHeight))
            }
        }
    }
    
    func addLabel(string: String, color: UIColor, point: CGPoint) {
        let labelLayer = CATextLayer()
        let frame = CGRect.init(x: point.x, y: point.y, width: labelGutterWidth, height: labelHeight)
        let attrString = attributedChartLabel(string, color: color)
        labelLayer.string = attrString
        labelLayer.contentsScale = UIScreen.main.scale
        labelLayer.backgroundColor = UIColor.clear.cgColor
        labelLayer.alignmentMode = kCAAlignmentCenter
        labelLayer.isWrapped = true
        labelLayer.frame = frame
        
        chartLayer.addSublayer(labelLayer)
    }
    
    func attributedChartLabel(_ string: String, color: UIColor) -> NSAttributedString {
        let font = FontUtil.getHelveticaNeueMed(size: 10.0)
        let textAttributes: [NSAttributedStringKey : AnyObject] = [NSAttributedStringKey.font: font,
                                                                   NSAttributedStringKey.foregroundColor: color]
        let attributedString = NSAttributedString.init(string: string, attributes: textAttributes)
        return attributedString
    }
    
    func drawIndicator() {
        guard data1.count > 0 else { return }
        
        indicatorLayer.removeFromSuperlayer()
        
        indicatorLayer = CALayer()
        
        // Line
        let indicatorLineLayer = CAShapeLayer()
        let indicatorPath = UIBezierPath.init()
        let width = bounds.width - (2*labelGutterWidth)
        let offset = labelGutterWidth + (CGFloat(currentIndex) / CGFloat(data1.count)) * width
        indicatorPath.move(to: CGPoint.init(x:offset , y: 0))
        indicatorPath.addLine(to: CGPoint.init(x: offset, y: bounds.height))
        
        indicatorLineLayer.frame = bounds
        indicatorLineLayer.path = indicatorPath.cgPath
        indicatorLineLayer.strokeColor = indicatorColor.cgColor
        indicatorLineLayer.fillColor = UIColor.clear.cgColor
        indicatorLineLayer.lineWidth = 1.0
        indicatorLayer.addSublayer(indicatorLineLayer)

        // Circle
        let indicatorPointLayer = CAShapeLayer()
        indicatorPointLayer.frame = self.bounds
        
        indicatorPointLayer.path = UIBezierPath.init(arcCenter: CGPoint.init(x: offset, y: bounds.height + 4.0), radius: 3.0, startAngle: 0, endAngle: CGFloat(2.0 * Double.pi), clockwise: true).cgPath
        indicatorPointLayer.strokeColor = UIColor.clear.cgColor
        indicatorPointLayer.fillColor = indicatorColor.cgColor
        indicatorPointLayer.lineWidth = 0
        indicatorLayer.addSublayer(indicatorPointLayer)

        // Triangle
        let indicatorTriangleLayer = CAShapeLayer()
        let indicatorTrianglePath = UIBezierPath.init()
        indicatorTrianglePath.move(to: CGPoint.init(x: offset, y: bounds.height))
        indicatorTrianglePath.addLine(to: CGPoint.init(x: offset - 1.5, y: bounds.height + 3))
        indicatorTrianglePath.addLine(to: CGPoint.init(x: offset + 1.5, y: bounds.height + 3))
        indicatorTrianglePath.close()

        indicatorTriangleLayer.frame = self.bounds
        indicatorTriangleLayer.path = indicatorTrianglePath.cgPath
        indicatorTriangleLayer.strokeColor = indicatorColor.cgColor
        indicatorTriangleLayer.fillColor = indicatorColor.cgColor
        indicatorTriangleLayer.lineWidth = 1.0
        indicatorLayer.addSublayer(indicatorTriangleLayer)

        chartLayer.addSublayer(indicatorLayer)
    }
}
