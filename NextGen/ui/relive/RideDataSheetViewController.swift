//
//  RideDataSheetViewController.swift
//  nextgen
//
//  Created by Scott Wang on 12/16/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

// https://github.com/AhmedElassuty/BottomSheetController

class RideDataSheetViewController: BottomSheetViewController {

    var viewModel: RideDataSheetViewModel?
    var delegate: RideDataSheetDelegate?
    
    @IBOutlet weak var maxSpeedValueLabel: UILabel!
    @IBOutlet weak var maxSpeedUnitLabel: UILabel!
    @IBOutlet weak var maxPowerValueLabel: UILabel!
    @IBOutlet weak var maxTorqueValueLabel: UILabel!
    @IBOutlet weak var maxRegenValueLabel: UILabel!
    @IBOutlet weak var maxLeanLeftVallueLabel: UILabel!
    @IBOutlet weak var maxLeanRightValueLabel: UILabel!
    @IBOutlet weak var socUsedValueLabel: UILabel!
    @IBOutlet weak var energyRegeneratedValueLabel: UILabel!
    @IBOutlet weak var maxSpeedCheckImageView: UIImageView!
    @IBOutlet weak var maxPowerCheckImageView: UIImageView!
    @IBOutlet weak var maxTorqueCheckImageView: UIImageView!
    @IBOutlet weak var maxRegenCheckImageView: UIImageView!
    @IBOutlet weak var maxLeanLeftCheckImageView: UIImageView!
    @IBOutlet weak var maxLeanRightCheckImageView: UIImageView!
    
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        autoOpenClose = true
        fullView = 200
        bindViews()
    }
    
    // MARK: -
    
    func bindViews() {
        guard let viewModel = self.viewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.maxSpeed.bind(to: maxSpeedValueLabel.rx.text),
            viewModel.maxSpeedUnit.bind(to: maxSpeedUnitLabel.rx.text),
            viewModel.maxPower.bind(to: maxPowerValueLabel.rx.text),
            viewModel.maxTorque.bind(to: maxTorqueValueLabel.rx.text),
            viewModel.maxRegen.bind(to: maxRegenValueLabel.rx.text),
            viewModel.maxLeanLeft.bind(to: maxLeanLeftVallueLabel.rx.text),
            viewModel.maxLeanRight.bind(to: maxLeanRightValueLabel.rx.text),
            viewModel.socUsed.bind(to: socUsedValueLabel.rx.text),
            viewModel.energyRegenerated.bind(to: energyRegeneratedValueLabel.rx.text),

            viewModel.maxSpeedEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxSpeedCheckImageView.isHidden = !enabled
                }
            },
            viewModel.maxPowerEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxPowerCheckImageView.isHidden = !enabled
                }
            },
            viewModel.maxTorqueEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxTorqueCheckImageView.isHidden = !enabled
                }
            },
            viewModel.maxRegenEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxRegenCheckImageView.isHidden = !enabled
                }
            },
            viewModel.maxLeanLeftEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxLeanLeftCheckImageView.isHidden = !enabled
                }
            },
            viewModel.maxLeanRightEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.maxLeanRightCheckImageView.isHidden = !enabled
                }
            }
        )
        
        viewModel.loadModel(delegate)
    }
    
    // MARK: - IBAction
    
    @IBAction func reliveRidePressed(_ sender: Any) {
        delegate?.launchRelivePlayback()
    }

    @IBAction func maxSpeedPressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxSpeedPressed()
    }
    @IBAction func maxPowerPressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxPowerPressed()
    }
    @IBAction func maxTorquePressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxTorquePressed()
    }
    @IBAction func maxRegenPressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxRegenPressed()
    }
    @IBAction func maxLeanLeftPressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxLeanLeftPressed()
    }
    @IBAction func maxLeanRightPressed(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.onMaxLeanRightPressed()
    }
}
