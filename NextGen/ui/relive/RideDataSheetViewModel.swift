//
//  RideDataSheetViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 12/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol RideDataSheetDelegate {
    func enableMaxSpeed(_ enable: Bool)
    func enableMaxPower(_ enable: Bool)
    func enableMaxTorque(_ enable: Bool)
    func enableMaxRegen(_ enable: Bool)
    func enableMaxLeanLeft(_ enable: Bool)
    func enableMaxLeanRight(_ enable: Bool)
    func launchRelivePlayback()
}

class RideDataSheetViewModel {
    
    var dataService: DataService
    var delegate: RideDataSheetDelegate?
    
    var maxSpeedEnabled = BehaviorRelay<Bool>(value: true)
    var maxPowerEnabled = BehaviorRelay<Bool>(value: true)
    var maxTorqueEnabled = BehaviorRelay<Bool>(value: false)
    var maxRegenEnabled = BehaviorRelay<Bool>(value: false)
    var maxLeanLeftEnabled = BehaviorRelay<Bool>(value: false)
    var maxLeanRightEnabled = BehaviorRelay<Bool>(value: false)

    var maxSpeed = BehaviorRelay<String>(value: "")
    var maxSpeedUnit = BehaviorRelay<String>(value: "")
    var maxPower = BehaviorRelay<String>(value: "")
    var maxTorque = BehaviorRelay<String>(value: "")
    var maxRegen = BehaviorRelay<String>(value: "")
    var maxLeanLeft = BehaviorRelay<String>(value: "")
    var maxLeanRight = BehaviorRelay<String>(value: "")

    var socUsed = BehaviorRelay<String>(value: "")
    var energyRegenerated = BehaviorRelay<String>(value: "")
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func loadModel(_ delegate: RideDataSheetDelegate?) {
        self.delegate = delegate
        
        maxSpeedEnabled.accept(true)
        delegate?.enableMaxSpeed(true)
        maxPowerEnabled.accept(true)
        delegate?.enableMaxPower(true)

        maxTorqueEnabled.accept(false)
        maxRegenEnabled.accept(false)
        maxLeanLeftEnabled.accept(false)
        maxLeanRightEnabled.accept(false)
        
        if let capturedRide = dataService.capturedRide {
            if let dataPoint = capturedRide.getMaxSpeedDataPoint() {
                if dataService.usingMetricDistance {
                    let converted = Int(Float(dataPoint.speed) / 100)
                    maxSpeed.accept("\(converted)")
                    maxSpeedUnit.accept("global_kph".localized)
                } else {
                    let converted = Int((Float(dataPoint.speed) / 100) / Constant.KM_IN_ONE_MILE)
                    maxSpeed.accept("\(converted)")
                    maxSpeedUnit.accept("global_mph".localized)
                }
            }
            if let dataPoint = capturedRide.getMaxPowerDataPoint() {
                maxPower.accept("\(dataPoint.power)")
            }
            if let dataPoint = capturedRide.getMaxTorqueDataPoint() {
                maxTorque.accept("\(dataPoint.torque)")
            }
            if let dataPoint = capturedRide.getMaxRegenDataPoint() {
                let value = dataPoint.brakeRegen < dataPoint.coastRegen ? dataPoint.brakeRegen : dataPoint.coastRegen
                maxRegen.accept("\(value)")
            }
            if let dataPoint = capturedRide.getMaxLeanLeftDataPoint() {
                let leanAngle = abs(Int(Float(dataPoint.leanAngle) / 10))
                maxLeanLeft.accept("\(leanAngle)")
            }
            if let dataPoint = capturedRide.getMaxLeanRightDataPoint() {
                let leanAngle = abs(Int(Float(dataPoint.leanAngle) / 10))
                maxLeanRight.accept("\(leanAngle)")
            }
            socUsed.accept("\(capturedRide.getSocUsed())")
            energyRegenerated.accept("\(capturedRide.getEnergyRegenerated())")
            
        } else {
            maxSpeed.accept("global_empty_data".localized)
            maxPower.accept("global_empty_data".localized)
            maxTorque.accept("global_empty_data".localized)
            maxRegen.accept("global_empty_data".localized)
            maxLeanLeft.accept("global_empty_data".localized)
            maxLeanRight.accept("global_empty_data".localized)

            socUsed.accept("global_empty_data_percentage".localized)
            energyRegenerated.accept("global_empty_data".localized)
        }
    }
    
    func onMaxSpeedPressed() {
        maxSpeedEnabled.accept(!maxSpeedEnabled.value)
        delegate?.enableMaxSpeed(maxSpeedEnabled.value)
    }
    func onMaxPowerPressed() {
        maxPowerEnabled.accept(!maxPowerEnabled.value)
        delegate?.enableMaxPower(maxPowerEnabled.value)
    }
    func onMaxTorquePressed() {
        maxTorqueEnabled.accept(!maxTorqueEnabled.value)
        delegate?.enableMaxTorque(maxTorqueEnabled.value)
    }
    func onMaxRegenPressed() {
        maxRegenEnabled.accept(!maxRegenEnabled.value)
        delegate?.enableMaxRegen(maxRegenEnabled.value)
    }
    func onMaxLeanLeftPressed() {
        maxLeanLeftEnabled.accept(!maxLeanLeftEnabled.value)
        delegate?.enableMaxLeanLeft(maxLeanLeftEnabled.value)
    }
    func onMaxLeanRightPressed() {
        maxLeanRightEnabled.accept(!maxLeanRightEnabled.value)
        delegate?.enableMaxLeanRight(maxLeanRightEnabled.value)
    }
}
