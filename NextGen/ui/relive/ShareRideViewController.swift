//
//  ShareRideViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/18/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit
import GoogleMaps
import CocoaLumberjack

class ShareRideViewController: ZeroUIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_RELIVE_RIDE }

    var shareRideViewModel: ShareRideViewModel?
    
    @IBOutlet weak var shareImageContainerView: UIView!
    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var photoContainerView: UIImageView!
    @IBOutlet weak var shareButton: ZeroUIButton!
    @IBOutlet weak var leftStatTitleLabel: UILabel!
    @IBOutlet weak var leftStatValueLabel: UILabel!
    @IBOutlet weak var leftStatUnitLabel: UILabel!
    @IBOutlet weak var centerStatTitleLabel: UILabel!
    @IBOutlet weak var centerStatValueLabel: UILabel!
    @IBOutlet weak var centerStatUnitLabel: UILabel!
    @IBOutlet weak var rightStatTitleLabel: UILabel!
    @IBOutlet weak var rightStatValueLabel: UILabel!
    @IBOutlet weak var rightStatUnitLabel: UILabel!
    @IBOutlet weak var leftRideDataLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var centerRideDataLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var rightRideDataLabel: ZeroUpperCaseUILabel!
    
    @IBOutlet weak var mediaSegmentedControl: UISegmentedControl! {
        didSet {
            mediaSegmentedControl.setTitle("relive_share_map".localized, forSegmentAt: 0)
            mediaSegmentedControl.setTitle("relive_share_photo".localized, forSegmentAt: 1)
        }
    }
    
    var startMarker = GMSMarker.init()
    var endMarker = GMSMarker.init()
    var routePolyLine = MapUtil.getStyledPolyline()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "relive_share_title".localized
        setupMap()
        bindViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        shareRideViewModel?.onViewWillAppear()
    }
    
    fileprivate func setupMap() {
        mapContainerView.isMyLocationEnabled = true
        mapContainerView.mapStyle = MapUtil.getMapStyle()
        routePolyLine.map = mapContainerView
        
        startMarker.icon = UIImage.init(named: "ic_map_dot_white_center")
        startMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        endMarker.icon = UIImage.init(named: "ic_map_dot_black_center")
        endMarker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
    }
    
    fileprivate func bindViews() {
        guard let viewModel = shareRideViewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.leftStatTitle.bind(to: leftStatTitleLabel.rx.text),
            viewModel.centerStatTitle.bind(to: centerStatTitleLabel.rx.text),
            viewModel.rightStatTitle.bind(to: rightStatTitleLabel.rx.text),
            viewModel.leftStatTitle.bind(to: leftRideDataLabel.rx.text),
            viewModel.centerStatTitle.bind(to: centerRideDataLabel.rx.text),
            viewModel.rightStatTitle.bind(to: rightRideDataLabel.rx.text),
            viewModel.leftStatValue.bind(to: leftStatValueLabel.rx.text),
            viewModel.centerStatValue.bind(to: centerStatValueLabel.rx.text),
            viewModel.rightStatValue.bind(to: rightStatValueLabel.rx.text),
            viewModel.leftStatUnit.bind(to: leftStatUnitLabel.rx.text),
            viewModel.centerStatUnit.bind(to: centerStatUnitLabel.rx.text),
            viewModel.rightStatUnit.bind(to: rightStatUnitLabel.rx.text),

            viewModel.locations.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                DispatchQueue.global(qos: .background).async {
                    if let locations = event.element {
                        let path = GMSMutablePath.init()
                        for location in locations {
                            let loc = CLLocation.init(latitude: location.latitude, longitude: location.longitude)
                            path.add(loc.coordinate)
                        }
                        DispatchQueue.main.async {
                            if locations.count > 0 {
                                let startLocation = locations[0]
                                strongSelf.startMarker.position = startLocation
                                strongSelf.startMarker.map = strongSelf.mapContainerView
                                
                                let endLocation = locations[locations.count - 1]
                                strongSelf.endMarker.position = endLocation
                                strongSelf.endMarker.map = strongSelf.mapContainerView

                                MapUtil.centerMapOnLocation(map: strongSelf.mapContainerView, start: startLocation, end: endLocation, padding: 75.0)
                            }
                            
                            strongSelf.routePolyLine.path = path
                        }
                    }
                }
            },
                             
            shareButton.rx.controlEvent([.touchUpInside]).subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                let image = strongSelf.shareImageContainerView.asImage()
                let shareSheet = UIActivityViewController.init(activityItems: [image], applicationActivities: nil)
                shareSheet.completionWithItemsHandler = { _, completed, _, _ in
                    if completed {
                        if let navVC = strongSelf.navigationController {
                            navVC.popViewController(animated: true)
                        } else {
                            strongSelf.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                strongSelf.present(shareSheet, animated: true, completion: nil)
            },
            
            mediaSegmentedControl.rx.selectedSegmentIndex.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let selected = event.element else { return }
                if selected == 0 {
                    strongSelf.mapContainerView.isHidden = false
                    strongSelf.photoContainerView.isHidden = true
                } else {
                    strongSelf.mapContainerView.isHidden = true
                    strongSelf.photoContainerView.isHidden = false
                    let vc = UIImagePickerController()
                    vc.allowsEditing = true
                    vc.sourceType = .photoLibrary
                    vc.delegate = strongSelf
                    strongSelf.present(vc, animated: true, completion: nil)
                }
            }
        )
            
        viewModel.loadModel()
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        if let navVC = self.navigationController {
            navVC.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ShareStatPickerViewController {
            switch segue.identifier {
            case "left":
                destination.pickerViewModel?.setStatLocation(.Left)
            case "right":
                destination.pickerViewModel?.setStatLocation(.Right)
            case "center":
                destination.pickerViewModel?.setStatLocation(.Center)
            default:
                break
            }
        }
    }
    
    // MARK: - UImagePickerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            photoContainerView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        mediaSegmentedControl.selectedSegmentIndex = 0
        mapContainerView.isHidden = false
        photoContainerView.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
}
