//
//  RootTabBarController.swift
//  NextGenView
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class RootTabBarController: UITabBarController {

    var armedVCShown = false
    var syncVCShown = false
    var armedVC: UIViewController?
    var syncVC: UIViewController?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let zeroMotorcycleService = SwinjectUtil.sharedInstance.container.resolve(ZeroMotorcycleService.self) else { return }
        
        zeroMotorcycleService.getBikeInfo().subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let packet = event.element {
                if packet.isArmed() {
                    if !strongSelf.armedVCShown {
                        if strongSelf.syncVCShown { // Dismiss existing sync
                            DispatchQueue.main.async {
                                strongSelf.syncVC?.dismiss(animated: false, completion: {
                                    strongSelf.launchArmed()
                                })
                                strongSelf.syncVCShown = false
                            }
                        } else {
                            strongSelf.launchArmed()
                        }
                    }
                } else {
                    if strongSelf.armedVCShown {
                        DispatchQueue.main.async {
                            strongSelf.armedVC?.dismiss(animated: true, completion: {
                                // Check if we need to pop up sync again
                                if !zeroMotorcycleService.offlineQueueIsEmpty().value {
                                    strongSelf.launchSyncChanges()
                                }
                            })
                            strongSelf.armedVCShown = false
                        }
                    }
                }
            }
        }.disposed(by: disposeBag)
        
        zeroMotorcycleService
            .getConnectionState()
            .subscribe({ [weak self] event in
                guard let strongSelf = self, let connected = event.element else { return }
                if !connected { // Dismiss if disconnected
                    if strongSelf.armedVCShown {
                        DispatchQueue.main.async {
                            strongSelf.armedVC?.dismiss(animated: true, completion: nil)
                            strongSelf.armedVCShown = false
                        }
                    }
                
                    if strongSelf.syncVCShown {
                        DispatchQueue.main.async {
                            strongSelf.syncVC?.dismiss(animated: true, completion: nil)
                            strongSelf.syncVCShown = false
                        }
                    }
                } else {
                    // Connected and there are offline packets
                    if !zeroMotorcycleService.offlineQueueIsEmpty().value && !strongSelf.syncVCShown {
                        if let presentedVC = strongSelf.presentedViewController {
                            DispatchQueue.main.async {
                                presentedVC.dismiss(animated: false, completion: {
                                    strongSelf.launchSyncChanges()
                                })
                            }
                        } else {
                            strongSelf.launchSyncChanges()
                        }
                    }
                }
            })
        .disposed(by: disposeBag)
    }
    
    // MARK: -
    fileprivate func launchSyncChanges() {
        DispatchQueue.main.async {
            self.syncVC = UILauncher.launchSyncChanges(self)
            self.syncVCShown = true
        }
    }
    
    fileprivate func launchArmed() {
        DispatchQueue.main.async {
            self.armedVC = UILauncher.launchArmedViewController(self)
            self.armedVCShown = true
        }
    }
    
    // MARK: - IBAction
    @IBAction func unwindFromSuccessfulLogin(segue: UIStoryboardSegue) { /* Unwind segue, do nothing */ }
    @IBAction func unwindFromSuccessfulRegister(segue: UIStoryboardSegue) { /* Unwind segue, do nothing */ }

    @IBAction func unwindFromSyncChanges(segue: UIStoryboardSegue) {
        syncVCShown = false
    }

}
