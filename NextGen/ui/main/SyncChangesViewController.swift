//
//  SyncChangesViewController.swift
//  nextgen
//
//  Created by Scott Wang on 6/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class SyncChangesViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SYNC_CHANGES}
    
    var syncChangesViewModel: SyncChangesViewModel?
    
    @IBOutlet weak var syncAppChangesButton: ZeroUIButton!
    @IBOutlet weak var discardAppChangesButton: ZeroUIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViews()
    }
    
    func bindViews() {
        guard let viewModel = syncChangesViewModel else { return }
        disposeBag.insertAll(all:
            viewModel.dismiss.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "unwindFromSyncChanges", sender: self)
            },
                             
            viewModel.showActivityIndicator.subscribe { [weak self] event in
                guard let strongSelf = self, let show = event.element else { return }
                if show {
                    strongSelf.activityIndicator.startAnimating()
                } else {
                    strongSelf.activityIndicator.stopAnimating()
                }
            },
            
            discardAppChangesButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.discardAppChangesButton.isEnabled = false
                strongSelf.syncAppChangesButton.isEnabled = false
                
                viewModel.onDiscardAppChanges()
            },
            
            syncAppChangesButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncAppChangesButton.isEnabled = false
                strongSelf.discardAppChangesButton.isEnabled = false
                
                viewModel.onSyncAppChanges()
            }
        )
        
        viewModel.loadModel()
    }
}
