//
//  SyncChangesViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 6/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import RxSwift
import RxCocoa
import CocoaLumberjack

class SyncChangesViewModel {
    var zeroMotorcycleService: ZeroMotorcycleService
    
    var showActivityIndicator = BehaviorRelay<Bool>(value: false)
    var dismiss = PublishRelay<Bool>()
    
    init(zeroMotorcycleService: ZeroMotorcycleService) {
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    func loadModel() {
        showActivityIndicator.accept(false)
    }
    
    func onSyncAppChanges() {
        showActivityIndicator.accept(true)
        zeroMotorcycleService.sendOfflinePackets()
        dismiss.accept(true)
    }
    
    func onDiscardAppChanges() {
        showActivityIndicator.accept(true)
        zeroMotorcycleService.clearOfflineQueue()
        dismiss.accept(true)
    }
}
