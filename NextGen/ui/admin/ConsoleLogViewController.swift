//
//  ConsoleLogViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/21/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class ConsoleLogViewController: ZeroUIViewController {

    @IBOutlet weak var logTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fileLogger = appDelegate.fileLogger
        
        let file = fileLogger.logFileManager.sortedLogFilePaths[0]
        
        if let log = try? String.init(contentsOfFile: file, encoding: .utf8) {
            logTextView.text = log
        }
    }
    
    @IBAction func sharePressed(_ sender: Any) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fileLogger = appDelegate.fileLogger
        
        var files = [Any]()
        for file in fileLogger.logFileManager.sortedLogFilePaths {
            let fileUrl = URL.init(fileURLWithPath: file)
            files.append(fileUrl)
            if files.count >= 5 {
                break
            }
        }
            
        let shareSheet = UIActivityViewController.init(activityItems: files, applicationActivities: nil)
        shareSheet.completionWithItemsHandler = { _, completed, _, _ in
            if completed {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.present(shareSheet, animated: true, completion: nil)
    }
}
