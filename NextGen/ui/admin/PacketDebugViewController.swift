//
//  PacketDebugViewController.swift
//  nextgen
//
//  Created by David Crawford on 9/5/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import RxCocoa
import UIKit
import RxSwift

class PacketDebugViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_PACKET_DEBUG }
    
    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    @IBOutlet weak var packetBytesTextView: UITextView!
    @IBOutlet weak var bikeInfoButton: ZeroUIButton!
    @IBOutlet weak var dashGaugesButton: ZeroUIButton!
    @IBOutlet weak var rawBytesTextView: UITextView!
    @IBOutlet weak var sendRawBytesButton: ZeroUIButton!
    @IBOutlet weak var commandChannelButton: ZeroUIButton!
    @IBOutlet weak var rideShareButton: ZeroUIButton!
    @IBOutlet weak var logInitButton: ZeroUIButton!
    @IBOutlet weak var errorsButton: ZeroUIButton!
    
    private var runningBikeInfoContinuous = false
    private var bikeInfoReceived = false
    private var bluetoothDisposeBag = DisposeBag()
    
    private var rawBytesResponses = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        disposeBag.insertAll(all:
            backButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            }),
                             
            bikeInfoButton.rx.tap.subscribe{ [weak self] _ in
                guard let strongSelf = self else { return }
                if strongSelf.runningBikeInfoContinuous {
                    return
                }
                
                if let motorcycleService = strongSelf.zeroMotorcycleService {
                    strongSelf.runningBikeInfoContinuous = true
                    
                    if motorcycleService.isConnected() {
                        let bikeInfoPacket = BikeInfoPacket()
                        bikeInfoPacket.isEmpty = true
                        motorcycleService.sendSingleZeroPacket(packet: bikeInfoPacket, priority: true).subscribe { event in
                            strongSelf.bluetoothDisposeBag = DisposeBag()
                            if let bikeInfo = event.element as? BikeInfoPacket {
                                DDLogDebug("Received BikeInfo Packet - " + bikeInfo.getFullPacket().toHexString())
                                strongSelf.appendToOutput(bikeInfo.getFullPacket().toHexString())
                            }
                            }.disposed(by: strongSelf.bluetoothDisposeBag)
                    }
                }
            },

            dashGaugesButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                if motorcycleService.isConnected() {
                    let dashGaugesPacket = DashGaugesPacket()
                    dashGaugesPacket.isEmpty = true
                    motorcycleService.sendSingleZeroPacket(packet: dashGaugesPacket, priority: true).subscribe { event in
                        strongSelf.bluetoothDisposeBag = DisposeBag()
                        if let dashGauges = event.element as? DashGaugesPacket {
                            DDLogDebug("Received DashGauges Packet - " + dashGauges.getFullPacket().toHexString())
                            strongSelf.appendToOutput(dashGauges.getFullPacket().toHexString())
                        }
                    }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
            },
            
            packetBytesTextView.rx.tapGesture()
                .when(.recognized)
                .subscribe({ [weak self] _ in
                    guard let strongSelf = self else { return }
                    UIPasteboard.general.string = strongSelf.packetBytesTextView?.text
                }),
            
            sendRawBytesButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                strongSelf.rawBytesTextView.resignFirstResponder()
                if motorcycleService.isConnected() {
                    let bytes = strongSelf.rawBytesTextView.text.replacingOccurrences(of: "0x", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")
                    
                    motorcycleService.sendRawBytes(Data(hex: bytes)).subscribe { event in
                        if strongSelf.rawBytesResponses > 2 { // Show the next few packet responses
                            strongSelf.bluetoothDisposeBag = DisposeBag()
                            strongSelf.rawBytesResponses = strongSelf.rawBytesResponses + 1
                        }
                        if let response = event.element {
                            strongSelf.appendToOutput(response)
                        }
                    }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
            },
            
            commandChannelButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                if motorcycleService.isConnected() {
                    let commandPacket = CommandChannelPacket.getCommandEcuIndexPacket(readWrite: .CMD_CH_READ, ecuIndex: 0)
                    motorcycleService.sendSingleZeroPacket(packet: commandPacket, priority: true).subscribe { event in
                        strongSelf.bluetoothDisposeBag = DisposeBag()
                        if let packet = event.element as? CommandChannelPacket {
                            DDLogDebug("Received Command Channel Response - " + packet.getFullPacket().toHexString())
                            strongSelf.appendToOutput(packet.getFullPacket().toHexString())
                        }
                    }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
                
            },
            
            rideShareButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                if motorcycleService.isConnected() {
                    let rideSharePacket = RideSharePacket()
                    motorcycleService.sendSingleZeroPacket(packet: rideSharePacket, priority: true).subscribe { event in
                        strongSelf.bluetoothDisposeBag = DisposeBag()
                        if let packet = event.element as? RideSharePacket {
                            DDLogDebug("Received Ride Share Response - " + packet.getFullPacket().toHexString())
                            strongSelf.appendToOutput(packet.getFullPacket().toHexString())
                        }
                        }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
            },
            
            logInitButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                if motorcycleService.isConnected() {
                    let logInitPacket = LogInitPacket()
                    motorcycleService.sendSingleZeroPacket(packet: logInitPacket, priority: true).subscribe { event in
                        strongSelf.bluetoothDisposeBag = DisposeBag()
                        if let packet = event.element as? LogInitPacket {
                            DDLogDebug("Received LogInit Response - " + packet.getFullPacket().toHexString())
                            strongSelf.appendToOutput(packet.getFullPacket().toHexString())
                        }
                        }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
            },
            
            errorsButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self, let motorcycleService = strongSelf.zeroMotorcycleService else { return }
                if motorcycleService.isConnected() {
                    let errorCodesPacket = ErrorCodesPacket()
                    motorcycleService.sendSingleZeroPacket(packet: errorCodesPacket, priority: true).subscribe { event in
                        strongSelf.bluetoothDisposeBag = DisposeBag()
                        if let packet = event.element as? ErrorCodesPacket {
                            DDLogDebug("Received Error Codes Response - " + packet.getFullPacket().toHexString())
                            strongSelf.appendToOutput(packet.getFullPacket().toHexString())
                        }
                        }.disposed(by: strongSelf.bluetoothDisposeBag)
                }
            }

        )
        
        if let zeroMotorcycleService = self.zeroMotorcycleService {
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let resendPacket = event.element {
                    DDLogDebug("REceived Resend Response - " + resendPacket.getFullPacket().toHexString())
                    strongSelf.appendToOutput(resendPacket.getFullPacket().toHexString())
                }
            }.disposed(by: disposeBag)
        }
    }
    
    private func appendToOutput(_ output: String) {
        packetBytesTextView.text = "\(output)\n\n\(packetBytesTextView.text ?? "")"
    }
}
