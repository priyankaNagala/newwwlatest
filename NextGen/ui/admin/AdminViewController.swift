//
//  AdminViewController.swift
//  nextgen
//
//  Created by David Crawford on 9/5/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import UIKit
import Firebase
import GoogleSignIn

class AdminViewController: ZeroUIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ADMIN }
    
    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    @IBOutlet weak var notificationToken: UIButton!
    @IBOutlet weak var packetDebugButton: UIButton!
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var signOutButton: ZeroSecondaryUIButton!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var consolLogButton: UIButton!
    @IBOutlet weak var capturedRideTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        setupView()
        readCapturedRide()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateLoginState()
    }
    
    private func setupView() {
        disposeBag.insertAll(all:
            backButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            }),
                
            packetDebugButton.rx.tapGesture()
                .when(.recognized)
                .subscribe({ [weak self] _ in
                    guard let strongSelf = self else { return }
                    UILauncher.launchPacketDebug(strongSelf)
                })
        )
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.notificationToken.setTitle(result.token, for: .normal)
            }
        }
        
    }
    
    func readCapturedRide() {
        let dataService = DataService.shared
        if let capturedRide = dataService.capturedRide {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            
            if let data = try? encoder.encode(capturedRide) {
                capturedRideTextView.text = String(data: data, encoding: .utf8)!
            }
        } else {
            capturedRideTextView.text = "No Captured Ride"
        }
    }
    
    func updateLoginState() {
        if let currentUser = GIDSignIn.sharedInstance()?.currentUser { // Logged In
            userEmailLabel.text = "\(currentUser.profile.name ?? "")\n\(currentUser.profile.email ?? "")"
            userEmailLabel.isHidden = false
            signInButton.isHidden = true
            signOutButton.isHidden = false
        } else {
            userEmailLabel.isHidden = true
            signInButton.isHidden = false
            signOutButton.isHidden = true
        }
    }
    
    @IBAction func copyNotificationToken(_ sender: Any) {
        UIPasteboard.general.string = self.notificationToken.titleLabel?.text
    }
    @IBAction func signOutPressed(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signOut()
        updateLoginState()
    }
    
    // MARK: GIDSSignInDelegate
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            updateLoginState()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
    }
}
