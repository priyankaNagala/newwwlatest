//
//  LoginViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 10/12/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class LoginViewModel {

    var onEmailError = PublishRelay<String?>()
    var onPasswordError = PublishRelay<String?>()
    var onSubmitError = PublishRelay<String?>()
    var onSubmitSuccess = PublishRelay<Bool>()
    
    var savedUsername = BehaviorRelay<String?>(value: nil)
    var savedPassword = BehaviorRelay<String?>(value: nil)
    
    var disposeBag = DisposeBag()
    var starcomApiService : StarcomApiService
    var dataService: DataService
    
    init(starcomApiService: StarcomApiService, dataService: DataService) {
        self.starcomApiService = starcomApiService
        self.dataService = dataService
        if let starcomUser = dataService.starcomUser {
            self.savedUsername.accept(starcomUser)
        }
        if let starcomPassword = dataService.starcomPassword {
            self.savedPassword.accept(starcomPassword)
        }
    }
    
    func submitForm(email: String?, password: String?) {
        var errors = false
        if email == nil || email!.count == 0 {
            onEmailError.accept("starcom_email_empty_error".localized)
            errors = true
        } else {
            onEmailError.accept(nil)
        }
        if password == nil || password!.count == 0 {
            onPasswordError.accept("starcom_password_empty_error".localized)
            errors = true
        } else {
            onPasswordError.accept(nil)
        }
        
        if !errors {
            Starcom.shared.login(starcomApiService: starcomApiService, user: email!, password: password!).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let response = event.element {
                    if let sessionId = response.sessionId {
                        strongSelf.dataService.starcomSessionId = sessionId
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            appDelegate.registerForPushNotifications()
                        }
                        // Save username/password
                        strongSelf.dataService.persistLoginCredentials(user: email!, password: password!)
                        strongSelf.onSubmitSuccess.accept(true)
                    } else if let error = response.error {
                        strongSelf.onSubmitError.accept(error)
                    }
                }
            }.disposed(by: disposeBag)
        }
    }
}
