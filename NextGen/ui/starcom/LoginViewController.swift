//
//  LoginViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa

class LoginViewController: ZeroUIViewController, UITextFieldDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_LOGIN }

    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var registerButton: UIBarButtonItem!
    @IBOutlet weak var loginButton: ZeroUIButton!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailErrorIndicatorView: UIView!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorIndicatorView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!

    var viewModel: LoginViewModel?
    
    override func viewDidLoad() {
        let logo = UIImage(named: "Branding")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        super.viewDidLoad()
        guard let viewModel = self.viewModel else { return }
        resetErrorLabels()
        emailTextField.delegate = self
        passwordTextField.delegate = self

        disposeBag.insertAll(all:
            closeButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.dismiss(animated: true, completion: nil)
            },
                             
            registerButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                UILauncher.switchToRegister(strongSelf)
            },
            
            loginButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                viewModel.submitForm(email: strongSelf.emailTextField.text, password: strongSelf.passwordTextField.text)
            },
            
            viewModel.onEmailError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                
                if let error = event.element {
                    if error != nil {
                        strongSelf.emailErrorLabel.text = error
                        strongSelf.emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Orange)
                    } else {
                        strongSelf.emailErrorLabel.text = ""
                        strongSelf.emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
                    }
                }
            },
            
            viewModel.onPasswordError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let error = event.element {
                    if error != nil {
                        strongSelf.passwordErrorLabel.text = error
                        strongSelf.passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Orange)
                    } else {
                        strongSelf.passwordErrorLabel.text = ""
                        strongSelf.passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
                    }
                }
            },
            
            viewModel.onSubmitError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let error = event.element {
                    strongSelf.errorLabel.text = error
                } else {
                    strongSelf.errorLabel.text = ""
                }
            },
            
            viewModel.onSubmitSuccess.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "unwindFromSuccessfulLogin", sender: self)
            },
            
            viewModel.savedUsername.bind(to: self.emailTextField.rx.text),
            
            view.rx.tapGesture().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            }
        )
    }
    
    func resetErrorLabels() {
        emailErrorLabel.text = ""
        passwordErrorLabel.text = ""
        passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
        emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
        errorLabel.text = ""
    }

    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let viewModel = self.viewModel else { return false }
        textField.resignFirstResponder()
        if registerButton.isEnabled {
            viewModel.submitForm(email: emailTextField.text, password: passwordTextField.text)
        }
        return true
    }
}
