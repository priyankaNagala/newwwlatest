//
//  AccountViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 10/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class AccountViewModel {
    
    fileprivate var dataService: DataService
    fileprivate var starcomApiService: StarcomApiService
    fileprivate var zeroMotorcycleService: ZeroMotorcycleService
    fileprivate var numRetries = 0
    fileprivate var disposeBag = DisposeBag()

    var onLogout = PublishRelay<Bool>()
    var onDeregistered = PublishRelay<Bool>()
    var onDeregisterError = PublishRelay<String>()

    init(starcomApiService: StarcomApiService, dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.starcomApiService = starcomApiService
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }

    func logout() {
        if let sessionId = dataService.starcomSessionId, let regToken = dataService.registrationToken {
            // Unregister notificcations first
            Starcom.shared.deregisterForNotifications(starcomApiService: starcomApiService, sessionId: sessionId, registrationId: regToken).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                DDLogDebug("Starcom notify_unset")
                Starcom.shared.logout(starcomApiService: strongSelf.starcomApiService, sessionId: sessionId).subscribe { [weak self] _ in
                    guard let strongSelf = self else { return }
                    strongSelf.dataService.clearCredentials()
                    strongSelf.dataService.starcomSessionId = nil
                    strongSelf.onLogout.accept(true)
                }.disposed(by: strongSelf.disposeBag)
            }.disposed(by: self.disposeBag)
        }
    }
    
    func deregister() {
        if !zeroMotorcycleService.isConnected() {
            onDeregisterError.accept("starcom_deregister_unconnected".localized)
            return
        }
        if let vin = dataService.currentVin, let sessionId = dataService.starcomSessionId, let user = dataService.starcomUser {
            Starcom.shared.deregister(starcomApiService: starcomApiService, sessionId: sessionId, vin: vin).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let response = event.element {
                    if let _ = response.error {
                        strongSelf.onDeregistered.accept(false)
                    } else {
                        strongSelf.deleteUser(user: user, sessionId: sessionId)
                    }
                }
            }.disposed(by: disposeBag)
        } else {
            onDeregistered.accept(false)
        }
 
    }
    
    func deleteUser(user: String, sessionId: String) {
        Starcom.shared.deleteUser(starcomApiService: starcomApiService, user: user, sessionId: sessionId).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let response = event.element {
                if let _ = response.error {
                    strongSelf.onDeregistered.accept(false)
                } else {
                    strongSelf.dataService.clearCredentials()
                    strongSelf.dataService.starcomSessionId = nil
                    strongSelf.sendDeregistrationPacket()
                }
            }
        }.disposed(by:disposeBag)
    }

    func sendDeregistrationPacket() {
        let packet = StarcomRegistrationPacket()
        packet.successfulDeregister()
        zeroMotorcycleService.sendSingleZeroPacket(packet: packet, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let _ = event.element as? AckPacket {
                strongSelf.disposeBag = DisposeBag()
                strongSelf.onDeregistered.accept(true)
                DDLogDebug("Starcom DeRegistration Packet Response ACK")
            }
            }.disposed(by: disposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self else {return }
            strongSelf.disposeBag = DisposeBag()
            DDLogDebug("Starcom DeRegistration Packet Response RESEND")
            guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                strongSelf.onDeregistered.accept(false)
                return
            }
            strongSelf.numRetries = strongSelf.numRetries + 1
            strongSelf.sendDeregistrationPacket()
            }.disposed(by: disposeBag)
    }

}
