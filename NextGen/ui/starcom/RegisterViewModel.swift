//
//  RegisterViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 10/12/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CocoaLumberjack

class RegisterViewModel {
    
    var onEmailError = PublishRelay<String?>()
    var onPasswordError = PublishRelay<String?>()
    var onSubmitError = PublishRelay<String?>()
    var onSubmitSuccess = PublishRelay<Bool>()
    
    fileprivate var disposeBag = DisposeBag()
    fileprivate var starcomApiService : StarcomApiService
    fileprivate var dataService: DataService
    fileprivate var zeroMotorcycleService: ZeroMotorcycleService
    fileprivate var numRetries = 0
    
    init(starcomApiService: StarcomApiService, dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.starcomApiService = starcomApiService
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }

    func submitForm(email: String?, password: String?) {
        var errors = false
        if email == nil || email!.count == 0 {
            onEmailError.accept("starcom_email_empty_error".localized)
            errors = true
        } else {
            onEmailError.accept(nil)
        }
        if password == nil || password!.count == 0 {
            onPasswordError.accept("starcom_password_empty_error".localized)
            errors = true
        } else {
            if password!.count < Constant.MIN_PASSWORD_LENGTH {
                onPasswordError.accept(String.init(format: "starcom_password_length_error".localized, Constant.MIN_PASSWORD_LENGTH))
                errors = true
            } else {
                onPasswordError.accept(nil)
            }
        }
        
        if !zeroMotorcycleService.isConnected() {
            onSubmitError.accept("starcom_register_unconnected".localized)
            return
        }
        
        if !errors {
            if let vin = dataService.currentVin, let passcode = dataService.passcode {
                Starcom.shared.register(starcomApiService: starcomApiService, user: email!, password: password!, vin: vin, passcode: passcode).subscribe { [weak self] event in
                    guard let strongSelf = self else { return }
                    if let response = event.element {
                        if let sessionId = response.sessionId {
                            strongSelf.dataService.starcomSessionId = sessionId
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                appDelegate.registerForPushNotifications()
                            }
                            strongSelf.dataService.persistLoginCredentials(user: email!, password: password!)
                            strongSelf.sendRegistrationPacket()
                        } else if let error = response.error {
                            strongSelf.onSubmitError.accept(error)
                        }
                    }
                }.disposed(by: disposeBag)
            } else {
                DDLogError("VIN/Passcode not found for register")
            }
        }
    }
    
    func sendRegistrationPacket() {
        let packet = StarcomRegistrationPacket()
        packet.successfulRegister()
        zeroMotorcycleService.sendSingleZeroPacket(packet: packet, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let _ = event.element as? AckPacket {
                strongSelf.disposeBag = DisposeBag()
                strongSelf.onSubmitSuccess.accept(true)
                DDLogDebug("Starcom Registration Packet Response ACK")
            }
        }.disposed(by: disposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self else {return }
            strongSelf.disposeBag = DisposeBag()
            DDLogDebug("Starcom Registration Packet Response RESEND")
            guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                strongSelf.onSubmitError.accept("starcom_login_error".localized)
                return
            }
            strongSelf.numRetries = strongSelf.numRetries + 1
            strongSelf.sendRegistrationPacket()
        }.disposed(by: disposeBag)
    }
}
