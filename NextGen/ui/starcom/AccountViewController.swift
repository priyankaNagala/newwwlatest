//
//  AccountViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa

class AccountViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ACCOUNT }


    @IBOutlet weak var logoutButton: ZeroTertiaryUIButton!
    @IBOutlet weak var statusLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var deregisterButton: ZeroTertiaryUIButton!
    
    var viewModel: AccountViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = self.viewModel else { return }

        disposeBag.insertAll(all:
            closeButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.dismiss(animated: true, completion: nil)
            },

            logoutButton.rx.tap.subscribe { _ in
                viewModel.logout()
            },
            
            viewModel.onLogout.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.dismiss(animated: true, completion: nil)
            },
            
            deregisterButton.rx.tap.subscribe { [weak self] event in
                guard let _ = self else { return }
                
                viewModel.deregister()
            },
            
            viewModel.onDeregistered.subscribe { [weak self] event in
                guard let strongSelf = self, let success = event.element else { return }

                var message = "starcom_deregister_confirmation".localized
                if !success {
                    message = "starcom_deregister_error".localized
                }
                let alert = UIAlertController(
                    title: "starcom_deregister".localized,
                    message: message,
                    preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: { (acction) in
                    strongSelf.dismiss(animated: true, completion: nil)
                }))
                
                strongSelf.present(alert, animated: true, completion: nil)
            },
            
            viewModel.onDeregisterError.subscribe { [weak self] event in
                guard let strongSelf = self, let message = event.element else { return }
                
                let alert = UIAlertController(
                    title: "starcom_deregister".localized,
                    message:  message,
                    preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: nil))
                
                strongSelf.present(alert, animated: true, completion: nil)
                
            }
        )
    }
}
