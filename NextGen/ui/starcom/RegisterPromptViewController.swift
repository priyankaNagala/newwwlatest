//
//  RegisterPromptViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class RegisterPromptViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_REGISTER_PROMPT }

    @IBOutlet weak var registerLoginButton: ZeroUIButton!
    @IBOutlet weak var skipButton: ZeroSecondaryUIButton!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    var dataService: DataService?
    var skipToNext: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let dataService = self.dataService else { return }

        // Remove close button during onboarding
        if !dataService.hasSeenOnboarding {
            navigationItem.leftBarButtonItems = []
            navigationItem.hidesBackButton = true
        }

        disposeBag.insertAll(all:
            registerLoginButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                UILauncher.launchRegister(strongSelf)
            },
                             
            skipButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                UILauncher.launchConnectImprove(strongSelf)
            }
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if skipToNext {
            UILauncher.launchConnectImprove(self)
        }
    }
    
    // MARK: IBAction
    @IBAction func unwindFromSuccessfulLogin(segue: UIStoryboardSegue) {
        skipToNext = true
    }
    
    @IBAction func unwindFromSuccessfulRegister(segue: UIStoryboardSegue) {
        skipToNext = true
    }

}
