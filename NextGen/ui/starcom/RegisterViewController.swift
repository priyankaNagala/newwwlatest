//
//  RegisterViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa

class RegisterViewController: ZeroUIViewController, UITextViewDelegate, UITextFieldDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_REGISTER}

    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var loginButton: UIBarButtonItem!
    @IBOutlet weak var registerButton: ZeroUIButton!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailErrorIndicatorView: UIView!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorIndicatorView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var agreeCheckbox: UIButton!
    @IBOutlet weak var termsTextView: UITextView!
    
    var viewModel: RegisterViewModel?
    
    override func viewDidLoad() {
        let logo = UIImage(named: "Branding")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        super.viewDidLoad()
        guard let viewModel = self.viewModel else { return }
        resetErrorLabels()
        setupTermsTextView()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        disposeBag.insertAll(all:
            closeButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.dismiss(animated: true, completion: nil)
            },
                             
            loginButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }

                UILauncher.switchToLogin(strongSelf)
            },
            
            registerButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                viewModel.submitForm(email: strongSelf.emailTextField.text, password: strongSelf.passwordTextField.text)
            },
            
            viewModel.onEmailError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let error = event.element {
                    if error != nil {
                        strongSelf.emailErrorLabel.text = error
                        strongSelf.emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Orange)
                    } else {
                        strongSelf.emailErrorLabel.text = ""
                        strongSelf.emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
                    }
                }
            },
            
            viewModel.onPasswordError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let error = event.element {
                    if error != nil {
                        strongSelf.passwordErrorLabel.text = error
                        strongSelf.passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Orange)
                    } else {
                        strongSelf.passwordErrorLabel.text = ""
                        strongSelf.passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
                    }
                }
            },
            
            viewModel.onSubmitError.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let error = event.element {
                    strongSelf.errorLabel.text = error
                } else {
                    strongSelf.errorLabel.text = ""
                }
            },
            
            viewModel.onSubmitSuccess.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "unwindFromSuccessfulRegister", sender: self)
            },
            
            agreeCheckbox.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }

                strongSelf.agreeCheckbox.isSelected = !strongSelf.agreeCheckbox.isSelected
                strongSelf.registerButton.isEnabled = strongSelf.agreeCheckbox.isSelected
            },
            
            view.rx.tapGesture().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            }
        )
    }
    
    func setupTermsTextView() {
        let terms = "starcom_terms_of_service".localized
        let privacy = "starcom_privacy_policy".localized
        let agreement = "starcom_register_agree_to_terms".localized
        let formattedString = String(format: agreement, terms, privacy)
        let attributes: [NSAttributedStringKey : Any] =
            [NSAttributedStringKey.foregroundColor: ColorUtil.color(.LightGray),
             NSAttributedStringKey.font : FontUtil.getHelveticaNeue(size: 13.0)]
        let attribString = NSMutableAttributedString(string: formattedString, attributes: attributes)
        let privacyAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.link: Constant.PRIVACY_URL]
        let termsAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.link: Constant.TERMS_URL]
        
        let termsRange = attribString.mutableString.range(of: terms)
        let privacyRange = attribString.mutableString.range(of: privacy)

        attribString.addAttributes(termsAttributes, range: termsRange)
        attribString.addAttributes(privacyAttributes, range: privacyRange)
        termsTextView.attributedText = attribString
        termsTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : ColorUtil.color(.Yellow)]
        termsTextView.delegate = self
    }
    
    func resetErrorLabels() {
        emailErrorLabel.text = ""
        passwordErrorLabel.text = ""
        passwordErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
        emailErrorIndicatorView.backgroundColor = ColorUtil.color(.Yellow)
        errorLabel.text = ""
    }
    
    // MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == Constant.TERMS_URL {
            UILauncher.launchWebViewModal(self, url: URL)
        } else if URL.absoluteString == Constant.PRIVACY_URL {
            UILauncher.launchWebViewModal(self, url: URL)
        }
        return false
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let viewModel = self.viewModel else { return false }
        textField.resignFirstResponder()
        if registerButton.isEnabled {
            viewModel.submitForm(email: emailTextField.text, password: passwordTextField.text)
        }
        return true
    }
    
    // MARK: - IBAction
    @IBAction func unwindFromWebView(segue: UIStoryboardSegue) { /* Unwind segue, do nothing */ }

}
