//
//  FirmwareManagerViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 11/6/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class FirmwareManagerViewModel {
    static let MAX_DOWNLOAD_RETRY = 3
    
    var currentBMSVersion = BehaviorRelay<String>(value:"")
    var currentMBBVersion = BehaviorRelay<String>(value:"")
    
    var firmwareService: FirmwareService?
    var dataService: DataService?
    private var disposeBag = DisposeBag()
    private var downloadCount = 0
    
    init(firmwareService: FirmwareService, dataService: DataService?) {
        self.dataService = dataService
        self.firmwareService = firmwareService
    }
    
    func loadModel() {
        currentBMSVersion.accept("Version 24")
        currentMBBVersion.accept("Version 35")

        getCurrentBuild()
    }
    
    func getCurrentBuild() {
        guard let dataService = self.dataService, let firmwareService = self.firmwareService, let currentMotorcycle = dataService.currentMotorcycle else { return }
        firmwareService.makeCurrentBuildApiRequest().subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let response = event.element, let fwVersions = response.fwVersions {
                for (_,fwVersion) in fwVersions {
                    if let partNumber = fwVersion.partNumber, currentMotorcycle.isMbbPartNumber(partNumber), let bom = fwVersion.bom {
                        strongSelf.checkForBuildPackage(bom)
                    }
                }
            }
        }.disposed(by: disposeBag)
    }   
    
    func checkForBuildPackage(_ version: String) {
        guard let firmwareService = self.firmwareService, self.downloadCount < FirmwareManagerViewModel.MAX_DOWNLOAD_RETRY else { return }
        self.downloadCount = self.downloadCount + 1
        if !firmwareService.buildFileDownloaded(version) {
            firmwareService.makeBuildDownloadApiRequest(version).subscribe { [weak self] event in
                guard let strongSelf = self, let result = event.element else { return }
                if let error = result.error {
                    DDLogError(error)
                    strongSelf.retryDownload(version)
                    return
                }
                if let fup = firmwareService.getFupForBuildPackage(version) {
                    if firmwareService.fupPackageIsComplete(fup) {
                        strongSelf.checkNeedForFirmwareUpdateEcusPacket(fup)
                    } else {
                        DDLogError("Fup package incomplete")
                        strongSelf.retryDownload(version)
                    }
                } else {
                    DDLogError("Unable to get Fup object")
                    strongSelf.retryDownload(version)
                }
            }.disposed(by: disposeBag)
        }
    }
    
    func checkNeedForFirmwareUpdateEcusPacket(_ fup: Fup) {
        // TODO ECUs
    }
    
    func retryDownload(_ version: String) {
        guard let firmwareService = self.firmwareService else { return }
        firmwareService.cleanBuildDirectory(version) // Clean download directory first
        checkForBuildPackage(version) // Retry
    }
}
