//
//  FirmwareManagerViewController.swift
//  nextgen
//
//  Created by Scott Wang on 11/6/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class FirmwareManagerViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_FIRMWARE_MANAGER }
    
    @IBOutlet weak var firmwareStatusLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var currentMBBVersionLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var currentBMSVersionLabel: ZeroUpperCaseUILabel!
    
    var viewModel:FirmwareManagerViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViews()
    }
    
    func bindViews() {
        guard let viewModel = self.viewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.currentMBBVersion.bind(to: currentMBBVersionLabel.rx.text),
            viewModel.currentBMSVersion.bind(to: currentBMSVersionLabel.rx.text)
        )
        
        viewModel.loadModel()
        
    }

    // MARK: - IBAction
    @IBAction func closePressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
