//
//  WebViewViewController.swift
//  nextgen
//
//  Created by Scott Wang on 3/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit


class WebViewViewController: ZeroUIViewController, UIWebViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_WEB_VIEW }

    @IBOutlet weak var webView: UIWebView!
    var url:URL?
    
    @IBAction func onShareClick(_ sender: Any) {
        if let url = url {
            let activityViewController = UIActivityViewController(activityItems: [NSURL(string: url.absoluteString) as Any], applicationActivities: nil)
            present(activityViewController, animated: true, completion: {})
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let url = url {
            webView.scalesPageToFit = true
            webView.loadRequest(URLRequest(url: url))
            webView.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UIWebViewDelegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.title = webView.stringByEvaluatingJavaScript(from: "document.title")
    }
}
