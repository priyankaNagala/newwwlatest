//
//  LogSelectViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class LogSelectViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var logPullService: LogPullService
    
    var motorcycleLogButtonText = BehaviorRelay<NSAttributedString>(value: NSAttributedString.init())
    var motorcycleBatteryLogButtonText = BehaviorRelay<NSAttributedString>(value: NSAttributedString.init())
    var motorcycleLogEnabled = BehaviorRelay<Bool>(value: false)
    var motorcycleBatteryLogEnabled = BehaviorRelay<Bool>(value: false)
    
    var bluetoothDisposeBag = DisposeBag()
    var numRetries = 0
    
    init(dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService, logPullService: LogPullService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
        self.logPullService = logPullService
        logPullService.clear()
    }
    
    func loadModel() {
        acceptButtonText()
        
        
        readLogInitFromBike() // Get size of logs
    }
    
    fileprivate func readLogInitFromBike() {
        zeroMotorcycleService.sendSingleZeroPacket(packet: LogInitPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            if let logPacket = event.element as? LogInitPacket {
                strongSelf.bluetoothDisposeBag = DisposeBag()
                strongSelf.logPullService.setLogInit(logPacket)
                strongSelf.acceptButtonText()
            } else {
                DDLogDebug("Read LogInit Response Unknown Packet")
            }
            
        }.disposed(by: bluetoothDisposeBag)
        
        if numRetries < Constant.BLUETOOTH_RESEND_RETRIES {
            numRetries = numRetries + 1
            
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self, let _ = event.element else { return }
                strongSelf.bluetoothDisposeBag = DisposeBag()
                DDLogDebug("Read LogInit Response Resend Packet")
                strongSelf.readLogInitFromBike()
            }.disposed(by: bluetoothDisposeBag)
        } else { // Skip handler and reset retry counter
            numRetries = 0
        }
    }
    
    fileprivate func acceptButtonText() {
        if logPullService.mbbHasLogs() {
            if logPullService.minutesForMbb() > 1 {
                motorcycleLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_log_type".localized, subtitle: String(format: "logs_download_time".localized, logPullService.minutesForMbb())))
            } else {
                motorcycleLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_log_type".localized, subtitle: "logs_download_time_one_minute".localized))
            }
            motorcycleLogEnabled.accept(true)
            
            if logPullService.bmsHasLogs() {
                if logPullService.minutesForBms() > 1 {
                    motorcycleBatteryLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_battery_log_type".localized, subtitle: String(format: "logs_download_time".localized, logPullService.minutesForBms())))
                } else {
                    motorcycleBatteryLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_battery_log_type".localized, subtitle: "logs_download_time_one_minute".localized))
                }
                motorcycleBatteryLogEnabled.accept(true)
            }
        } else {
            motorcycleLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_log_type".localized, subtitle: " "))
            motorcycleBatteryLogButtonText.accept(FontUtil.getTwoLineAttributedStringForButton(title: "logs_motorcycle_battery_log_type".localized, subtitle: " "))
        }
    }
}
