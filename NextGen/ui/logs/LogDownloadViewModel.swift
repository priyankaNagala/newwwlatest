//
//  LogDownloadViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CocoaLumberjack

class LogDownloadViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var logPullService: LogPullService

    var logTitle = BehaviorRelay<String>(value: "")
    var statusText = BehaviorRelay<String>(value: "logs_loading".localized)
    var progress = BehaviorRelay<Float>(value: 0)
    var progressText = BehaviorRelay<String>(value: "0%")
    var shareEnabled = BehaviorRelay<Bool>(value: false)
    
    var filesToShare = BehaviorRelay<[Any]>(value: [Any]())
    
    fileprivate var disposeBag = DisposeBag()
    
    init(dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService, logPullService: LogPullService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
        self.logPullService = logPullService
    }
    
    func loadModel(_ mbbAndBmsLogs: Bool) {
        logPullService.downloadMbbAndBms = mbbAndBmsLogs
        
        if mbbAndBmsLogs {
            logTitle.accept("logs_motorcycle_battery_log_type".localized)
        } else {
            logTitle.accept("logs_motorcycle_log_type".localized)
        }
        
        logPullService.progress.subscribe { [weak self] event in
            guard let strongSelf = self, let p = event.element else { return }
            strongSelf.progress.accept(p)
            strongSelf.progressText.accept(String(format:"%.0f%%", p*100))
        }.disposed(by: disposeBag)
        
        logPullService.complete.subscribe { [weak self] event in
            guard let strongSelf = self, let complete = event.element else { return }
            if complete {
                strongSelf.shareEnabled.accept(true)
                strongSelf.progress.accept(1.0)
                strongSelf.progressText.accept("100%")
                strongSelf.statusText.accept("logs_loading_complete".localized)
                strongSelf.filesToShare.accept(strongSelf.logPullService.getFilesToShare())
            }
        }.disposed(by: disposeBag)
        
        logPullService.failed.subscribe { [weak self] event in
            guard let strongSelf = self, let failed = event.element else { return }
            if failed {
                strongSelf.statusText.accept("logs_loading_failed".localized)
            }
        }.disposed(by: disposeBag)
        
        logPullService.start()
    }
    
    func onCancel() {
        logPullService.cancel()
    }
}
