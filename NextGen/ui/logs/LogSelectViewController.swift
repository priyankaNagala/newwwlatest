//
//  LogSelectViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class LogSelectViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_LOG_SELECT }

    var logSelectViewModel: LogSelectViewModel?
    
    @IBOutlet weak var motorcycleLogButton: ZeroUIButton!
    @IBOutlet weak var motorcycleBatteryLogButton: ZeroUIButton!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViews()
    }
    
    fileprivate func bindViews() {
        guard let viewModel = logSelectViewModel else { return }

        disposeBag.insertAll(all:
            viewModel.motorcycleLogEnabled.bind(to: motorcycleLogButton.rx.isEnabled),
            viewModel.motorcycleBatteryLogEnabled.bind(to: motorcycleBatteryLogButton.rx.isEnabled),
            viewModel.motorcycleLogButtonText.bind(to: motorcycleLogButton.rx.attributedTitle(for: .normal)),
            viewModel.motorcycleBatteryLogButtonText.bind(to: motorcycleBatteryLogButton.rx.attributedTitle(for: .normal)),

            backButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            }
        )
    
        viewModel.loadModel()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if segue.identifier == "MBB_BMS", let vc = segue.destination as? LogDownloadViewController {
            vc.mbbAndBmsLogs = true
        }
    }
}
