//
//  LogDownloadViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class LogDownloadViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_LOG_DOWNLOAD }
    
    var logDownloadViewModel: LogDownloadViewModel?
    var mbbAndBmsLogs: Bool = false
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var logLabel: UILabel!
    @IBOutlet weak var statusLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var shareButton: ZeroUIButton!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViews()
    }
    
    fileprivate func bindViews() {
        guard let viewModel = logDownloadViewModel else { return }
    
        disposeBag.insertAll(all:
            cancelButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                viewModel.onCancel()
                strongSelf.performSegue(withIdentifier: "unwindFromCancelLogs", sender: strongSelf.cancelButton)
            },
                             
            viewModel.logTitle.bind(to: logLabel.rx.text),
            viewModel.progress.bind(to: progressView.rx.progress),
            viewModel.progressText.bind(to: progressLabel.rx.text),
            viewModel.shareEnabled.bind(to: shareButton.rx.isEnabled),
            viewModel.statusText.bind(to: statusLabel.rx.text),
            
            shareButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                let shareSheet = UIActivityViewController.init(activityItems: viewModel.filesToShare.value, applicationActivities: nil)
                shareSheet.completionWithItemsHandler = { _, completed, _, _ in
                    if completed {
                        strongSelf.performSegue(withIdentifier: "unwindFromCancelLogs", sender: strongSelf.cancelButton)
                    }
                }

                strongSelf.present(shareSheet, animated: true, completion: nil)
            }
        )
        
        viewModel.loadModel(mbbAndBmsLogs)
    }
}
