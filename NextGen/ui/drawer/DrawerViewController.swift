//
//  DrawerViewController.swift
//  nextgen
//
//  Created by Scott Wang on 8/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class DrawerViewController: ZeroUIViewController, UITableViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_DRAWER}
    
    @IBOutlet weak var drawerTableView: UITableView!
    
    var drawerViewModel: DrawerViewModel?
    var drawerDataSource: DrawerViewModel.DrawerDataSource?
    
    private let tableItemCellId = DrawerItemTableViewCell.ReuseIdentifier
    private let tableHeaderCellId = DrawerHeaderTableViewCell.ReuseIdentifier
    private let tableButtonCellId = DrawerButtonTableViewCell.ReuseIdentifier
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let viewModel = drawerViewModel else { return }
        viewModel.loadModel()
    }
    
    func bindView() {
        guard let viewModel = drawerViewModel else { return }
        
        drawerTableView.delegate = self
        drawerTableView.register(UINib(nibName: tableItemCellId, bundle: nil), forCellReuseIdentifier: tableItemCellId)
        drawerTableView.register(UINib(nibName: tableHeaderCellId, bundle: nil), forCellReuseIdentifier: tableHeaderCellId)
        drawerTableView.register(UINib(nibName: tableButtonCellId, bundle: nil), forCellReuseIdentifier: tableButtonCellId)
        
        self.drawerDataSource = viewModel.drawerDataSource()

        disposeBag.insertAll(all:
            viewModel.drawerSections
                .bind(to: drawerTableView.rx.items(dataSource: drawerDataSource!)),
                             
            drawerTableView.rx.modelSelected(DrawerItemViewModel.self)
                .subscribe { [weak self] itemViewModel in
                    guard let strongSelf = self else { return }
                    if let drawerItemViewModel = itemViewModel.element {
                        strongSelf.closeDrawer()
                        strongSelf.launchDrawerItemAction(drawerItemViewModel: drawerItemViewModel)
                    }
                }
        )
    }
    
    func launchDrawerItemAction(drawerItemViewModel: DrawerItemViewModel) {
        guard let rootTabBarController = UILauncher.currentlySelectedViewController() else { return }
        // TODO: Fill all of these case statements in once we know where they ae all going
        switch drawerItemViewModel.itemType {
        case .LOGIN:
            UILauncher.launchLogin(rootTabBarController)
        case .ACCOUNT:
            UILauncher.launchAccount(rootTabBarController)
        case .SETTINGS:
            UILauncher.launchSettings(rootTabBarController)
        case .OWNERSUPPORT:
            UILauncher.launchWebViewModal(rootTabBarController, url: URL(string: "owners_resources_url".localized)!)
        case .RATE:
            guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id1354186695?action=write-review") else { return }
            UILauncher.launchWebViewModal(rootTabBarController, url: writeReviewURL)
        case .HELP:
            UILauncher.launchAppHelp(rootTabBarController, helpType: .help)
        case .PRIVACY:
            UILauncher.launchWebViewModal(rootTabBarController, url: URL(string: Constant.PRIVACY_URL)!)
        case .TERMS:
            UILauncher.launchWebViewModal(rootTabBarController, url: URL(string: Constant.TERMS_URL)!)
        case .ADMIN:
            UILauncher.launchAdmin(rootTabBarController)
        case .CHARGING_STATION:
            UILauncher.launchChargingStations(rootTabBarController)
        case .CALL_ROADSIDE_ASSIST:
            UILauncher.launchRoadsideAssist(rootTabBarController)
        case .ROADSIDE_ASSIST_SETUP:
            UILauncher.launchRoadsideAssistSetup(rootTabBarController)
        }
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerTitle = drawerDataSource?.sectionModels[section].header else { return nil }
        let cell = tableView.dequeueReusableCell(withIdentifier: tableHeaderCellId) as! DrawerHeaderTableViewCell
        cell.configure(withHeaderString: headerTitle)
        return cell
    }
}
