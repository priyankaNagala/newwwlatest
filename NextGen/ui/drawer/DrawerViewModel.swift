//
//  DrawerViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift

class DrawerViewModel {
    var drawerSections = BehaviorRelay<[DrawerSection]>(value: [])
    
    var dataService: DataService
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    typealias DrawerDataSource = RxTableViewSectionedReloadDataSource<DrawerSection>
    
    struct DrawerSection: SectionModelType  {
        typealias Item = DrawerItemViewModel
        var header: String
        var items: [Item]
        init(header: String, items: [Item]) {
            self.header = header
            self.items = items
        }
        init(original: DrawerSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    func drawerDataSource() -> DrawerDataSource {
        let cellId = DrawerItemTableViewCell.ReuseIdentifier
        let buttonCellId = DrawerButtonTableViewCell.ReuseIdentifier
        return DrawerDataSource(
            configureCell: { _, tv, ip, item  in
                if item.itemType == .CHARGING_STATION || item.itemType == .CALL_ROADSIDE_ASSIST {
                    let cell = tv.dequeueReusableCell(withIdentifier: buttonCellId, for: ip) as! DrawerButtonTableViewCell
                    cell.configure(title: item.label, subtitle: item.subLabel)
                    return cell
                } else {
                    let cell = tv.dequeueReusableCell(withIdentifier: cellId, for: ip) as! DrawerItemTableViewCell
                    cell.configure(withViewModel: item)
                    return cell
                }
            }
        )
    }
    
    func loadModel() {
        let topSection = [
            dataService.loggedIn ? DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.ACCOUNT) :
                DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.LOGIN),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.SETTINGS)
        ]
        let utilitySection = [
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.CHARGING_STATION),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.CALL_ROADSIDE_ASSIST),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.ROADSIDE_ASSIST_SETUP)]
        let supportSection = [
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.OWNERSUPPORT),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.RATE),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.HELP)
        ]
        let legalSection = [
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.PRIVACY),
            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.TERMS)
//            DrawerItemViewModel(itemType: DrawerItemViewModel.DrawerItemType.ADMIN)
        ]
        let sections = [
            DrawerSection(header:" ", items:topSection),
            DrawerSection(header:"drawer_utilities".localized, items:utilitySection),
            DrawerSection(header:"drawer_support".localized, items:supportSection),
            DrawerSection(header:"drawer_legal".localized, items:legalSection)
        ]
        drawerSections.accept(sections)
    }
}
