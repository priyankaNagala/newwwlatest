//
//  DrawerButtonTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 1/23/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class DrawerButtonTableViewCell: UITableViewCell {
    static let ReuseIdentifier = "DrawerButtonTableViewCell"

    @IBOutlet weak var button: ZeroUIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()

        button.setTitle("", for: .normal)
    }

    func configure(title: String, subtitle: String?) {
        button.titleLabel?.lineBreakMode = .byWordWrapping
        let attrib = FontUtil.getTwoLineAttributedStringForButton(title: title, subtitle: subtitle ?? " ")
        button.setAttributedTitle(attrib, for: .normal)
    }
}
