//
//  DrawerHeaderTableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class DrawerHeaderTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "DrawerHeaderTableViewCell"
        
    @IBOutlet weak var label: UILabel!
    
    func configure(withHeaderString: String) {
        label.text = withHeaderString
    }
}
