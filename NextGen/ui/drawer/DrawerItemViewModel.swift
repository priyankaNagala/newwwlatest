//
//  DrawerItemViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class DrawerItemViewModel {
    
    enum DrawerItemType {
        case SETTINGS, HELP, PRIVACY, TERMS, RATE, ADMIN, ACCOUNT, LOGIN, OWNERSUPPORT, CHARGING_STATION, CALL_ROADSIDE_ASSIST, ROADSIDE_ASSIST_SETUP
    }
    
    var itemType: DrawerItemType
    var label: String
    var subLabel: String?
    
    init(itemType: DrawerItemType) {
        self.itemType = itemType
        
        switch self.itemType {
        case .SETTINGS:
            self.label = "drawer_settings".localized
        case .HELP:
            self.label = "drawer_need_help".localized
        case .PRIVACY:
            self.label = "drawer_privacy_policy".localized
        case .TERMS:
            self.label = "drawer_terms_and_conditions".localized
        case .RATE:
            self.label = "drawer_rate_app".localized
        case .ADMIN:
            self.label = "drawer_admin".localized
        case .LOGIN :
            self.label = "drawer_login".localized
        case .ACCOUNT:
            self.label = "drawer_account".localized
        case .OWNERSUPPORT:
            self.label = "drawer_owner_support".localized
        case .CHARGING_STATION:
            self.label = "drawer_charging_station".localized
            self.subLabel = "drawer_google_maps".localized
        case .CALL_ROADSIDE_ASSIST:
            self.label = "drawer_roadside_assist".localized
            let phone = DataService.shared.roadsideAssistPhone
            let policy = DataService.shared.roadsideAssistPolicy
            
            if phone != nil && phone!.count > 0 {
                subLabel = phone!
            }
            if policy != nil && policy!.count > 0 {
                if subLabel != nil {
                    subLabel = subLabel! + " | " + policy!
                } else {
                    subLabel = policy!
                }
            }
        case .ROADSIDE_ASSIST_SETUP:
            self.label = "drawer_roadside_assist_setup".localized
        }
        
    }
}
