//
//  DrawerItemTableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift

class DrawerItemTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "DrawerItemTableViewCell"
    
    @IBOutlet weak var label: ZeroUpperCaseUILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        label.text = ""
    }
    
    func configure(withViewModel viewModel: DrawerItemViewModel) {
        label.text = viewModel.label
    }
}
