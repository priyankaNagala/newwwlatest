//
//  RoadsideAssistSetupViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/23/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

class RoadsideAssistSetupViewController: ZeroUIViewController {

    var roadsideAssistViewModel: RoadsideAssistSetupViewModel?
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var policyTextField: UITextField!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: ZeroUIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViews()
    }
    
    fileprivate func bindViews() {
        guard let viewModel = self.roadsideAssistViewModel else { return }
        disposeBag.insertAll(all:
            viewModel.phone.bind(to: phoneTextField.rx.text),
            viewModel.policy.bind(to: policyTextField.rx.text),
            
            backButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.dismiss(animated: true, completion: nil)
            },
            
            saveButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                viewModel.onSave(phone: strongSelf.phoneTextField.text, policy: strongSelf.policyTextField.text)
                strongSelf.dismiss(animated: true, completion: nil)
            },
                             
            view.rx.tapGesture().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            },
            
            phoneTextField.rx.controlEvent(.editingDidEndOnExit).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            },
            policyTextField.rx.controlEvent(.editingDidEndOnExit).subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            }
        )
        viewModel.loadModel()
    }
}
