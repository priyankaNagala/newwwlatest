//
//  RoadsideAssistSetupViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/23/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class RoadsideAssistSetupViewModel {
    
    var dataService: DataService
    
    var phone = BehaviorRelay<String>(value: "")
    var policy = BehaviorRelay<String>(value: "")
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func loadModel() {
        phone.accept(dataService.roadsideAssistPhone ?? "")
        policy.accept(dataService.roadsideAssistPolicy ?? "")
    }
    
    func onSave(phone: String?, policy: String?) {
        dataService.roadsideAssistPolicy = policy
        dataService.roadsideAssistPhone = phone
    }
}
