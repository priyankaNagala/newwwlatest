//
//  EditComparableSettingsViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/17/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class EditComparableSettingsViewModel {
    var dataService: DataService
    
    var currencySelectedIndex = BehaviorRelay<Int>(value: 0)
    var currencySegment0 = BehaviorRelay<String>(value: "comp_settings_currency_dollar".localized)
    var currencySegment1 = BehaviorRelay<String>(value: "comp_settings_currency_euro".localized)

    var fuelEfficiencyIndex = BehaviorRelay<Int>(value: 0)
    var fuelEfficiencySegment0 = BehaviorRelay<String>(value: "comp_settings_fuel_efficiency_miles".localized)
    var fuelEfficiencySegment1 = BehaviorRelay<String>(value: "comp_settings_fuel_efficiency_km".localized)
    var fuelEfficiencyMin = BehaviorRelay<Float>(value: 0)
    var fuelEfficiencyMax = BehaviorRelay<Float>(value: 0)
    
    var costOfGasLabel = BehaviorRelay<String>(value: "comp_settings_cost_per_gallon".localized)
    var costOfGas = BehaviorRelay<String>(value: "")
    var costOfElectricity = BehaviorRelay<String>(value: "")

    var comparableEfficiency = BehaviorRelay<Float>(value: 0)
    var comparableEfficiencyString = BehaviorRelay<String>(value: "")
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func loadModel() {
        acceptCurrencyValues()
        acceptFuelEfficiencyValues()
        acceptCostValues()
    }
    
    func onCurrencyChanged(_ index: Int) {
        dataService.currency = index == 0 ? .DOLLARS : .EUROS
        acceptCurrencyValues()
        acceptCostValues()
    }
    
    // Maps to .editingDidChange
    func onCostOfGasUpdated(_ value: String?) {
        updateCostOfGas(value)
    }
    
    // Maps to .editingDidEnd
    func onCostOfGasSaved(_ value: String?) {
        updateCostOfGas(value)
        acceptCostValues()
    }
    
    func updateCostOfGas(_ value: String?) {
        // Just update saved values if valid
        if let cost = value, let doubleVal = Double(cost) {
            if dataService.currency == .DOLLARS {
                dataService.costOfGas = roundCurrency(doubleVal)
            } else {
                dataService.costOfGas = roundCurrency(doubleVal / Constant.DATA_USD_TO_EURO_EXCHANGE_RATE)
            }
        }
    }
    
    // Maps to .editingDidChange
    func onCostOfElectricityUpdated(_ value: String?) {
        updateCostOfElectricity(value: value)
    }
    
    // Maps to .editingDidEnd
    func onCostOfElectricitySaved(_ value: String?) {
        updateCostOfElectricity(value: value)
        acceptCostValues()
    }
    
    func updateCostOfElectricity(value: String?) {
        // Just update saved values if valid
        if let cost = value, let doubleVal = Double(cost) {
            if dataService.currency == .DOLLARS {
                dataService.costOfElectricity = roundCurrency(doubleVal)
            } else {
                dataService.costOfElectricity = roundCurrency(doubleVal / Constant.DATA_USD_TO_EURO_EXCHANGE_RATE)
            }
        }
    }
    
    func acceptCostValues() {
        if dataService.currency == .DOLLARS {
            let formattedGas = String.init(format:"%0.2f", dataService.costOfGas)
            costOfGas.accept(formattedGas)
            let formattedElec = String.init(format:"%0.2f", dataService.costOfElectricity)
            costOfElectricity.accept(formattedElec)
        } else {
            let formattedGas = String.init(format:"%0.2f", roundCurrency(dataService.costOfGas * Constant.DATA_USD_TO_EURO_EXCHANGE_RATE))
            costOfGas.accept(formattedGas)
            let formattedElec = String.init(format:"%0.2f", roundCurrency(dataService.costOfElectricity * Constant.DATA_USD_TO_EURO_EXCHANGE_RATE))
            costOfElectricity.accept(formattedElec)
        }
    }
    
    func acceptFuelEfficiencyValues() {
        if dataService.fuelEfficiency == .MILES_PER_GALLON {
            fuelEfficiencyIndex.accept(0)
            costOfGasLabel.accept("comp_settings_cost_per_gallon".localized)
            fuelEfficiencyMax.accept(Float(dataService.fuelEfficiency.maxValue()))
            fuelEfficiencyMin.accept(Float(dataService.fuelEfficiency.minValue()))

            comparableEfficiency.accept(Float(dataService.comparableEfficiency))
        } else {
            fuelEfficiencyIndex.accept(1)
            costOfGasLabel.accept("comp_settings_cost_per_liter".localized)
            // Inverted in this case
            fuelEfficiencyMin.accept(Float(dataService.fuelEfficiency.maxValue()))
            fuelEfficiencyMax.accept(Float(dataService.fuelEfficiency.minValue()))

            let invertedValue = dataService.fuelEfficiency.minValue() - dataService.comparableEfficiency + dataService.fuelEfficiency.maxValue()
            comparableEfficiency.accept(Float(invertedValue))
        }
        
        if dataService.fuelEfficiency == .MILES_PER_GALLON {
            let str = String.init(format: "%.0f %@", dataService.comparableEfficiency, dataService.fuelEfficiency.abbreviation())
            comparableEfficiencyString.accept(str)
        } else {
            let str = String.init(format: "%.1f %@", dataService.comparableEfficiency, dataService.fuelEfficiency.abbreviation())
            comparableEfficiencyString.accept(str)
        }
    }
    
    func acceptCurrencyValues() {
        if dataService.currency == .DOLLARS {
            currencySelectedIndex.accept(0)
        } else {
            currencySelectedIndex.accept(1)
        }
    }
    
    func onFuelEfficiencyChanged(_ index: Int) {
        dataService.fuelEfficiency = index == 0 ? .MILES_PER_GALLON : .LITERS_PER_100KM
        acceptFuelEfficiencyValues()
    }
    
    func onComparableEfficiencyChanged(_ value: Float) {
        if dataService.fuelEfficiency == .MILES_PER_GALLON {
            dataService.comparableEfficiency = Double(value)
        } else {
            // Inverted
            let invertedValue = dataService.fuelEfficiency.minValue() - Double(value) + dataService.fuelEfficiency.maxValue()
            dataService.comparableEfficiency = invertedValue
        }
        acceptFuelEfficiencyValues()
    }
    
    func onResetToDefaults() {
        dataService.resetComparableSettingsToDefault()
        loadModel()
    }
    
    private func roundCurrency(_ value: Double) -> Double {
        return round(value * 100) / 100
    }
}
