//
//  EditComparableSettingViewController.swift
//  nextgen
//
//  Created by Scott Wang on 1/17/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EditComparableSettingViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_EDIT_COMPARABLE_SETTINGS }

    var settingsViewModel: EditComparableSettingsViewModel?
    
    @IBOutlet weak var currencySegmentedControl: UISegmentedControl!
    @IBOutlet weak var costOfGasLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var costOfGasTextField: UITextField!
    @IBOutlet weak var costOfElectricityTextField: UITextField!
    @IBOutlet weak var fuelEfficiencySegmentedControl: UISegmentedControl!
    @IBOutlet weak var comparableEfficiencySlider: UISlider!
    @IBOutlet weak var comparableEfficiencyLabel: UILabel!
    @IBOutlet weak var resetToDefaultsButton: ZeroTertiaryUIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindViews()
    }
    
    func bindViews() {
        guard let viewModel = settingsViewModel else { return }
        
        disposeBag.insertAll(all:
            
            viewModel.costOfGasLabel.bind(to: costOfGasLabel.rx.text),
            
            viewModel.currencySegment0.bind(to: currencySegmentedControl.rx.titleForSegment(at: 0)),
            viewModel.currencySegment1.bind(to: currencySegmentedControl.rx.titleForSegment(at: 1)),
            viewModel.currencySelectedIndex.bind(to: currencySegmentedControl.rx.selectedSegmentIndex),

            viewModel.fuelEfficiencySegment0.bind(to: fuelEfficiencySegmentedControl.rx.titleForSegment(at: 0)),
            viewModel.fuelEfficiencySegment1.bind(to: fuelEfficiencySegmentedControl.rx.titleForSegment(at: 1)),
            viewModel.fuelEfficiencyIndex.bind(to: fuelEfficiencySegmentedControl.rx.selectedSegmentIndex),
            viewModel.fuelEfficiencyMax.subscribe { [weak self] event in
                guard let strongSelf = self, let max = event.element else { return }
                strongSelf.comparableEfficiencySlider.maximumValue = max
            },
            viewModel.fuelEfficiencyMin.subscribe { [weak self] event in
                guard let strongSelf = self, let min = event.element else { return }
                strongSelf.comparableEfficiencySlider.minimumValue = min
            },

            viewModel.costOfGas.bind(to: costOfGasTextField.rx.text),
            viewModel.costOfElectricity.bind(to: costOfElectricityTextField.rx.text),
            
            viewModel.comparableEfficiency.subscribe { [weak self] event in
                guard let strongSelf = self, let comparableEfficiency = event.element else { return }
                
                strongSelf.comparableEfficiencySlider.value = comparableEfficiency
            },
            viewModel.comparableEfficiencyString.bind(to: comparableEfficiencyLabel.rx.text),

            // Updates
            currencySegmentedControl.rx.selectedSegmentIndex.changed.subscribe { event in
                guard let index = event.element else { return }
                viewModel.onCurrencyChanged(index)
            },
            
            costOfGasTextField.rx.controlEvent([.editingChanged]).subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.onCostOfGasUpdated(strongSelf.costOfGasTextField.text)
            },
            costOfGasTextField.rx.controlEvent([.editingDidEnd]).subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.onCostOfGasSaved(strongSelf.costOfGasTextField.text)
            },
            costOfElectricityTextField.rx.controlEvent([.editingChanged]).subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.onCostOfElectricityUpdated(strongSelf.costOfElectricityTextField.text)
            },
            costOfElectricityTextField.rx.controlEvent([.editingDidEnd]).subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.onCostOfElectricitySaved(strongSelf.costOfElectricityTextField.text)
            },
            
            fuelEfficiencySegmentedControl.rx.selectedSegmentIndex.changed.subscribe { event in
                guard let index = event.element else { return }
                viewModel.onFuelEfficiencyChanged(index)
            },
            
            comparableEfficiencySlider.rx.value.changed.subscribe { event in
                guard let value = event.element else { return }
                viewModel.onComparableEfficiencyChanged(value)
            },
            
            resetToDefaultsButton.rx.controlEvent([.touchUpInside]).subscribe { _ in
                viewModel.onResetToDefaults()
            },

            view.rx.tapGesture().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.view.endEditing(true)
            }
            
        )
        
        viewModel.loadModel()
    }
    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
