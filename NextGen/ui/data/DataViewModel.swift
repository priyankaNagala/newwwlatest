//
//  DataViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 12/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class DataViewModel {
    static let RIDE_SHARE_PACKET_INTERVAL: Double = 2 // Request RideShare packet every X seconds
    
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    
    var lastCapturedRideName = BehaviorRelay<String>(value:"data_no_ride_captured".localized)
    var lastCapturedRide = BehaviorRelay<CapturedRide?>(value:nil)
    var capturingRide = BehaviorRelay<Bool>(value: false)
    var capturingSince = BehaviorRelay<String>(value: "")
    var rideCaptureButtonText = BehaviorRelay<NSAttributedString>(value: NSAttributedString.init())
    var onPromptDeleteCapture = BehaviorRelay<Bool>(value: false)
    var captureRideEnabled = BehaviorRelay<Bool>(value: false)
    
    var rideSharePacketTimer: Timer?
    var capturedRide: CapturedRide?
    var bluetoothDisposeBag = DisposeBag()
    var disposeBag = DisposeBag()
    
    // Ownership data
    var odometer = BehaviorRelay<String>(value:"00000")
    var odometerTitle = BehaviorRelay<String>(value: "data_odometer".localized)
    var totalEnergyUsed = BehaviorRelay<String>(value:"0")
    var avgEfficiency = BehaviorRelay<String>(value:"0")
    var avgEfficiencyUnit = BehaviorRelay<String>(value: "global_wh_per_mi".localized)
    var moneySaved = BehaviorRelay<String>(value: "$0")
    var co2Reduced = BehaviorRelay<String>(value: "0")
    var co2ReducedUnit = BehaviorRelay<String>(value: "global_pounds".localized)
    
    private var modelLoaded = false
    private var appeared = false

    init(dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    func loadModel() {
        lastCapturedRide.accept(dataService.capturedRide)
        lastCapturedRideName.accept(rideCaptureName())
        capturingRide.accept(dataService.capturingRide)
        capturingSince.accept("")
        rideCaptureButtonText.accept(rideCaptureButtonAttribString())
        zeroMotorcycleService.getConnectionState().subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let status = event.element {
                strongSelf.captureRideEnabled.accept(status)
            }
        }.disposed(by: disposeBag)
        
        readRideShareFromBike() // To get initial odometer
        modelLoaded = true
    }
    
    func onViewWillAppear() {
        if modelLoaded && appeared {
            readRideShareFromBike()
            lastCapturedRideName.accept(rideCaptureName())
        }
        appeared = true
    }
    
    func onRideCapturePressed() {
        if dataService.capturingRide {
            endCapture()
            rideCaptureButtonText.accept(rideCaptureButtonAttribString())
            lastCapturedRideName.accept(rideCaptureName())
            capturingRide.accept(dataService.capturingRide)
        } else {
            onPromptDeleteCapture.accept(true)
        }
    }
    
    func onDeleteRideCapture() {
        dataService.capturedRide = nil
        lastCapturedRide.accept(dataService.capturedRide)
        lastCapturedRideName.accept(rideCaptureName())

        startCapture()
        rideCaptureButtonText.accept(rideCaptureButtonAttribString())
        capturingRide.accept(dataService.capturingRide)
    }
    
    // MARK: - Private
    
    private func startCapture() {
        dataService.capturingRide = true
        capturedRide = CapturedRide()
        dataService.capturedRide = capturedRide

        //captureSampleData()
        rideSharePacketTimer = Timer.scheduledTimer(timeInterval: DataViewModel.RIDE_SHARE_PACKET_INTERVAL, target: self, selector: #selector(self.readRideShareFromBike), userInfo: nil, repeats: true)
    }
    
    // Test method to capture sample data locally
    private func captureSampleData() {
        let path = Bundle.main.path(forResource: "sample", ofType: "csv")
        let csvString = try! String.init(contentsOfFile: path!)

        let rows = csvString.components(separatedBy: "\n")
        var curSpeed = 0
        var curPower = 0
        var curBrake = 0
        var curCoast = 0
        var curLean = 0
        var curTorque = 0
        var curTorqRegen = 0
        var curOdo = 1000
        var curSoc = 100
        
        var count = 0
        for row in rows {
            if row.count == 0 {
                continue
            }
            let values = row.components(separatedBy: ",")
            let rideSharePacket = RideSharePacket.init()
            rideSharePacket.latitutude = Double(values[1])!
            rideSharePacket.longitude = Double(values[0])!
            rideSharePacket.speed = UInt16(curSpeed)
            rideSharePacket.power = Int16(curPower)
            rideSharePacket.brakeRegen = Int32(curBrake)
            rideSharePacket.coastRegen = Int32(curCoast)
            rideSharePacket.leanAngle = Int32(curLean)
            rideSharePacket.torque = Int32(curTorque)
            rideSharePacket.odometer = UInt32(curOdo)
            rideSharePacket.soc = UInt8(curSoc)
            
            capturedRide?.appendRideShare(rideSharePacket)
        
            curSpeed = curSpeed + Int.random(in: -300...300)
            if curSpeed < 0 {
                curSpeed = 0
            }

            curPower = curPower + Int.random(in: -2...2)
            if curPower < 0 {
                curPower = 0
            }
            
            
            curLean = curLean + Int.random(in:-10...10)
            if curLean > 90 {
                curLean = 90
            }
            if curLean < -90 {
                curLean = -90
            }
            
            curTorqRegen = curTorqRegen + Int.random(in: -3...3)
            if curTorqRegen > 0 {
                curTorque = curTorqRegen
                curBrake = 0
            } else {
                curBrake = curTorqRegen
                curTorque = 0
            }
            curCoast = curCoast + Int.random(in: -2...2)
            if count % 1000 == 0 {
                curSoc = curSoc - 1
            }
            if count % 40 == 0 {
                curOdo = curOdo + 10
            }
            
            count = count + 1
        }
    }
    
    private func endCapture() {
        dataService.capturingRide = false
        capturedRide?.setEndTime(Date())
        dataService.capturedRide = capturedRide
        rideSharePacketTimer?.invalidate()
        lastCapturedRide.accept(dataService.capturedRide)
    }
    
    @objc private func readRideShareFromBike() {
        zeroMotorcycleService.sendSingleZeroPacketReplacingSameType(packet: RideSharePacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            if let packet = event.element as? RideSharePacket {
                strongSelf.bluetoothDisposeBag = DisposeBag()
                strongSelf.dataService.currentMotorcycle?.odometer = Int(packet.odometer)
                strongSelf.formatOdometer(Int(packet.odometer))
                strongSelf.dataService.currentMotorcycle?.totalEnergy = Int(packet.totalEnergy)
                strongSelf.totalEnergyUsed.accept("\(Int(packet.totalEnergy))")
                strongSelf.formatAvgEfficiency(odometer: Int(packet.odometer), energy: Int(packet.totalEnergy))
                strongSelf.calculateMoneySaved(totalEnergy: Int(packet.totalEnergy), odometer: Int(packet.odometer))
                strongSelf.calculateCO2(totalEnergy: Int(packet.totalEnergy), odometer: Int(packet.odometer))
                strongSelf.capturedRide?.appendRideShare(packet)
                DDLogDebug("Read RideShare Response: " + packet.getFullPacket().toHexString())
            } else {
                DDLogDebug("Read RideShare Response Unknown Packet")
            }
        }.disposed(by: bluetoothDisposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self, let resendPacket = event.element else { return }
            strongSelf.bluetoothDisposeBag = DisposeBag()
            DDLogDebug("Read RideShare Response Resend Packet: " + resendPacket.getFullPacket().toHexString())
            // We don't need to resend since we're getting another packet soon
        }.disposed(by: bluetoothDisposeBag)
        
        rideCaptureButtonText.accept(rideCaptureButtonAttribString())
    }
    
    private func rideCaptureName() -> String {
        if let ride = dataService.capturedRide {
            return ride.getName()
        } else {
            return "data_no_ride_captured".localized
        }
    }
    
    private func rideCaptureButtonAttribString() -> NSAttributedString {
        if dataService.capturingRide {
            var timeAgo: String?

            if let startedDate = dataService.capturedRide?.getStartTime() {
                let now = Date()
                let hour = Calendar.current.dateComponents([.hour], from: startedDate, to: now).hour ?? 0
                let min = (Calendar.current.dateComponents([.minute], from: startedDate, to: now).minute ?? 0) % 60
                timeAgo = String.init(format: "global_started_ago".localized, hour, min)
            }
            return FontUtil.getTwoLineAttributedStringForButton(title:"data_end_ride_capture".localized, subtitle: timeAgo)
        } else {
            let attribString = NSMutableAttributedString.init(string: "data_start_ride_capture".localized,
                                                              attributes: [NSAttributedStringKey.font : FontUtil.getHelveticaNeueMed(size: 16.0),
                                                                           NSAttributedStringKey.foregroundColor : ColorUtil.color(.Black)])
            return attribString
        }
    }
    
    // MARK: - Ownership Data
    private func formatOdometer(_ value: Int) {
        // Odometer in 0.1km
        
        var convertedValue = value
        if dataService.usingMetricDistance {
            convertedValue = Int(Float(value) / 10)
            odometerTitle.accept("data_odometer".localized + "(" + "settings_distance_units_kilometers".localized + ")")
        } else {
            convertedValue = Int((Float(value) / 10) / Constant.KM_IN_ONE_MILE)
            odometerTitle.accept("data_odometer".localized + "(" + "settings_distance_units_miles".localized + ")")
        }
        // Pad with 0s up to 5 digits
        if convertedValue < 100000 {
            odometer.accept(String.init(format: "%05d", convertedValue))
        } else {
            odometer.accept("\(convertedValue)")
        }
    }
    
    private func formatAvgEfficiency(odometer: Int, energy: Int) {
        // Odometer is 0.1km
        var convertedOdometer:Float = Float(odometer) / 10

        if !dataService.usingMetricDistance {
            convertedOdometer = convertedOdometer / Constant.KM_IN_ONE_MILE
            avgEfficiencyUnit.accept("global_wh_per_mi".localized)
        } else {
            avgEfficiencyUnit.accept("global_wh_per_km".localized)
        }
        guard convertedOdometer > 0 else { return }
        let efficiency = (Float(energy) * 1000.0) / Float(convertedOdometer)
        
        avgEfficiency.accept("\(Int(efficiency))")
    }
    
    private func calculateMoneySaved(totalEnergy: Int, odometer: Int) {
        // Odomter in 0.1km
        var savings:Double = 0
        var costOfGas:Double = 0
        var costOfElectricity:Double = 0

        if dataService.fuelEfficiency == .MILES_PER_GALLON {
            let odometerInMi = (Double(odometer) / 10) / Double(Constant.KM_IN_ONE_MILE)
            costOfGas = ((Double(odometerInMi) / dataService.comparableEfficiency) * dataService.costOfGas) // mi / mi/gal * $/gal = $
            
            costOfElectricity = Double(totalEnergy) * dataService.costOfElectricity
        } else {
            costOfGas = ((Double(odometer) / 10) * dataService.comparableEfficiency) * dataService.costOfGas / 100 // km * L/km * $/L = $
            costOfElectricity = Double(totalEnergy) * dataService.costOfElectricity
        }

        // Let's not have negative savings
        savings = costOfGas - costOfElectricity
        if savings < 0 {
            savings = 0
        }
        
        if dataService.currency == .DOLLARS {
            moneySaved.accept("$\(Int(savings))")
        } else {
            savings = savings * Constant.DATA_USD_TO_EURO_EXCHANGE_RATE
            moneySaved.accept("€\(Int(savings))")
        }
    }
    
    fileprivate func calculateCO2(totalEnergy: Int, odometer: Int) {
        /* co2 = (distance_traveled_miles * (19.4 / milesPerGallon)) - (total_power_used_kw * 0.524) */

        let distanceInMi = (Double(odometer) / 10) / Double(Constant.KM_IN_ONE_MILE)
        var mpg = dataService.comparableEfficiency
        if dataService.fuelEfficiency == .LITERS_PER_100KM {
            mpg = 1.0 / ( Double(dataService.comparableEfficiency) * Double(Constant.KM_IN_ONE_MILE ) * Double(Constant.GAL_IN_ONE_LITER))  // L/km * km/mi * gal/L = gal/mi  <- then inverse
        }
        
        var co2 = (distanceInMi * (19.4 / mpg)) - (Double(totalEnergy) * 0.524)
        
        // Let's not have negative savings
        if co2 < 0 {
            co2 = 0
        }
        
        co2Reduced.accept(String(format:"%.0f", co2))
    }
}
