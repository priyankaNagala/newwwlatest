//
//  DataViewController.swift
//  NextGenView
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class DataViewController: ZeroTabViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_DATA }

    var dataViewModel: DataViewModel?
    
    @IBOutlet weak var lastCapturedRideLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var startRideCaptureButton: ZeroUIButton!
    
    @IBOutlet weak var odometerTitleLabel: UILabel!
    @IBOutlet weak var odometerLabel: UILabel!

    @IBOutlet weak var totalEnergyUsedLabel: UILabel!
    @IBOutlet weak var avgEfficiencyLabel: UILabel!
    @IBOutlet weak var avgEfficiencyUnitLabel: UILabel!
    
    @IBOutlet weak var moneySavedLabel: UILabel!

    @IBOutlet weak var co2ReducedLabel: UILabel!
    @IBOutlet weak var co2ReduceUnitLabel: UILabel!
    
    @IBOutlet weak var editComparableSettingsButton: ZeroTertiaryUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startRideCaptureButton.titleLabel?.lineBreakMode = .byWordWrapping
        bindViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataViewModel?.onViewWillAppear()
    }

    func bindViews() {
        guard let viewModel = dataViewModel else { return }
        
        disposeBag.insertAll(all:
            viewModel.lastCapturedRideName.bind(to: lastCapturedRideLabel.rx.text),
                             
            viewModel.lastCapturedRide.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let lastCapturedRide = event.element {
                    if lastCapturedRide != nil {
                        strongSelf.lastCapturedRideLabel.textColor = ColorUtil.color(.White)
                    } else {
                        strongSelf.lastCapturedRideLabel.textColor = ColorUtil.color(.Gray)
                    }
                }
            },
            
            viewModel.rideCaptureButtonText.bind(to: startRideCaptureButton.rx.attributedTitle(for: .normal)),
            
            startRideCaptureButton.rx.tap.subscribe { event in
                viewModel.onRideCapturePressed()
            },
            
            viewModel.onPromptDeleteCapture.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let promptDelete = event.element {
                    if promptDelete {
                        let alert = UIAlertController(title: "data_delete_ride_capture_prompt".localized,
                                                      message: "data_delete_ride_capture_description".localized,
                                                      preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "global_delete".localized, style: .destructive, handler: { (action) in
                            viewModel.onDeleteRideCapture()
                        }))
                        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
                        strongSelf.present(alert, animated: true)
                    }
                }
            },
            
            viewModel.captureRideEnabled.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                if let enabled = event.element {
                    strongSelf.startRideCaptureButton.isEnabled = enabled
                }
            },
            
            // Ownership Data
            viewModel.odometer.bind(to: odometerLabel.rx.text),
            viewModel.odometerTitle.bind(to: odometerTitleLabel.rx.text),
            viewModel.totalEnergyUsed.bind(to: totalEnergyUsedLabel.rx.text),
            viewModel.avgEfficiency.bind(to: avgEfficiencyLabel.rx.text),
            viewModel.avgEfficiencyUnit.bind(to: avgEfficiencyUnitLabel.rx.text),
            viewModel.moneySaved.bind(to: moneySavedLabel.rx.text),
            viewModel.co2Reduced.bind(to: co2ReducedLabel.rx.text),
            viewModel.co2ReducedUnit.bind(to: co2ReduceUnitLabel.rx.text),
            
            editComparableSettingsButton.rx.tap.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                UILauncher.launchEditComparableSettingViewController(strongSelf)
            }
        )
        
        viewModel.loadModel()
    }
    
    // MARK: - IBAction
    @IBAction func lastCapturedRidePressed(_ sender: Any) {
        guard let viewModel = dataViewModel else { return }

        if viewModel.lastCapturedRide.value != nil {
            UILauncher.launchReliveRide(self, rideName: viewModel.lastCapturedRideName.value)
        }
    }
}
