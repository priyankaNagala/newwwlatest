//
//  RouteTypePickerViewController.swift
//  nextgen
//
//  Created by Scott Wang on 8/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class RouteTypePickerViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ROUTE_TYPE_PICKER }

    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var routeTypePickerViewModel: RouteTypePickerViewModel!
    var batteryManager: BatteryManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let cellId = SettingSelectionTableViewCell.ReuseIdentifier
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        
        disposeBag.insertAll(all:
            routeTypePickerViewModel.dataSource.bind(to: tableView.rx.items(cellIdentifier: cellId, cellType: SettingSelectionTableViewCell.self)) { (row, routeTypeViewModel, cell) in
                cell.configure(routeTypeViewModel)
            },
                             
            tableView.rx.modelSelected(RouteTypeViewModel.self).subscribe( { [weak self] event in
                guard let strongSelf = self, let viewModel = event.element else { return }

                strongSelf.routeTypePickerViewModel.onRouteTypeSelected(viewModel)
            }),
            
            routeTypePickerViewModel.onSavedRouteType.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.close()
            }
        )
        
        routeTypePickerViewModel.loadModel(model: batteryManager)
    }

    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBAction
    
    @IBAction func backPressed(_ sender: Any) {
        close()
    }
}
