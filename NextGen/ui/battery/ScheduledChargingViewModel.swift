//
//  ScheduledChargingViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 7/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class ScheduledChargingViewModel {
    var dataService: DataService
    var scheduledCharging: ScheduledCharging?
    var zeroMotorcycleService: ZeroMotorcycleService

    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
        initPlugInPickerItems()
    }

    var pickerTimeItems = BehaviorRelay<[String]>(value: [String]())
    var pickerDurationItems = BehaviorRelay<[String]>(value: [String]())
    var activeChargingTimeItems = BehaviorRelay<[String]>(value: [String]())
    var activeChargingDurationItems = BehaviorRelay<[String]>(value: [String]())

    // MARK: - Inputs
    var enabled = BehaviorRelay<Bool>(value: false)
    var plugInWindowStartTime = BehaviorRelay<String>(value: "")
    var plugInWindowEndTime = BehaviorRelay<String>(value: "")
    var plugInWindowDuration = BehaviorRelay<String>(value: "")
    var plugInWindowDescription = BehaviorRelay<String>(value: "")
    var activeChargingStartTime = BehaviorRelay<String>(value: "")
    var activeChargingEndTime = BehaviorRelay<String>(value: "")
    var activeChargingDuration = BehaviorRelay<String>(value: "")
    var activeChargingDescription = BehaviorRelay<String>(value: "")
    var daysOfTheWeek = BehaviorRelay<[ScheduledCharging.DayOfWeek]>(value:[ScheduledCharging.DayOfWeek]())
    var scheduledChargingDescription = BehaviorRelay<NSAttributedString>(value: NSAttributedString.init())
    var onUpdateComplete = PublishRelay<Bool>()
    var onQueueComplete = PublishRelay<Bool>()

    var bluetoothDisposeBag = DisposeBag()
    var numRetries = 0

    // MARK: - Outputs
    func onEnabledControlChanged(isOn: Bool) {
        guard let scheduledCharging = self.scheduledCharging else { return }
        scheduledCharging.enabled = isOn
        enabled.accept(isOn)
        scheduledChargingDescription.accept(scheduledChargingDescriptionString())
        daysOfTheWeek.accept(scheduledCharging.daysOfWeek)
    }
    
    func toggleDayOfWeek(dayOfWeek: ScheduledCharging.DayOfWeek) {
        if let scheduledCharging = self.scheduledCharging {
            scheduledCharging.toggleDayOfWeek(dayOfWeek)
            daysOfTheWeek.accept(scheduledCharging.daysOfWeek)
        }
    }
    
    func plugInWindowStartTimeSelected(time: String?, amPm: String?) {
        guard let scheduledCharging = self.scheduledCharging else { return }

        if let timeAsInt = ScheduledCharging.getTimeAsInt(time: time ?? plugInWindowStartTime.value,
                                                          using24HrClock: dataService.using24HrClock()) {
            scheduledCharging.plugInStartTime = timeAsInt
            plugInWindowStartTime.accept(scheduledCharging.plugInStartTimeFormatted)
            plugInWindowEndTime.accept(scheduledCharging.plugInEndTimeFormatted)
            plugInWindowDescription.accept(plugInWindowDescriptionString())
            acceptActiveChargingValues()
            scheduledChargingDescription.accept(scheduledChargingDescriptionString())
        }
    }
    
    func plugInWindowDurationSelected(duration: String) {
        guard let scheduledCharging = self.scheduledCharging else { return }

        if let durationAsInt = ScheduledCharging.getDurationAsInt(duration: duration) {
            scheduledCharging.plugInDuration = durationAsInt
            plugInWindowDuration.accept(scheduledCharging.plugInDurationFormatted)
            plugInWindowEndTime.accept(scheduledCharging.plugInEndTimeFormatted)
            plugInWindowDescription.accept(plugInWindowDescriptionString())
            acceptActiveChargingValues()
            scheduledChargingDescription.accept(scheduledChargingDescriptionString())
        }
    }

    func activeChargingStartTimeSelected(time: String?, amPm: String?) {
        guard let scheduledCharging = self.scheduledCharging else { return }
        
        if let timeAsInt = ScheduledCharging.getTimeAsInt(time: time ?? activeChargingStartTime.value,
                                                          using24HrClock: dataService.using24HrClock()) {
            scheduledCharging.activeChargingStartTime = timeAsInt
            acceptActiveChargingValues()
            scheduledChargingDescription.accept(scheduledChargingDescriptionString())
        }
    }
    
    func activeChargingDurationSelected(duration: String) {
        guard let scheduledCharging = self.scheduledCharging else { return }
        
        if let durationAsInt = ScheduledCharging.getDurationAsInt(duration: duration) {
            scheduledCharging.activeChargingDuration = durationAsInt
            acceptActiveChargingValues()
            scheduledChargingDescription.accept(scheduledChargingDescriptionString())
        }
    }
    
    func onUpdatePressed() {
        sendScheduledChargingToBike()
    }
    
    func acceptActiveChargingValues() {
        guard let scheduledCharging = self.scheduledCharging else { return }
        updateActiveChargingDurationPickers()
        updateActiveChargingStartTimePickers()
        activeChargingStartTime.accept(scheduledCharging.activeChargingStartTimeFormatted)
        activeChargingEndTime.accept(scheduledCharging.activeChargingEndTimeFormatted)
        activeChargingDuration.accept(scheduledCharging.activeChargingDurationFormatted)
        activeChargingDescription.accept(activeChargingDescriptionString())
    }
    
    // MARK: -
    
    func loadModel() {
        if let scheduledCharging = dataService.currentMotorcycle?.scheduledCharging {
            self.scheduledCharging = scheduledCharging
            updateActiveChargingDurationPickers()
            updateActiveChargingStartTimePickers()

            enabled.accept(scheduledCharging.enabled)
            
            plugInWindowStartTime.accept(scheduledCharging.plugInStartTimeFormatted)
            plugInWindowDuration.accept(scheduledCharging.plugInDurationFormatted)
            
            activeChargingStartTime.accept(scheduledCharging.activeChargingStartTimeFormatted)
            activeChargingDuration.accept(scheduledCharging.activeChargingDurationFormatted)
            
            daysOfTheWeek.accept(scheduledCharging.daysOfWeek)
            scheduledChargingDescription.accept(scheduledChargingDescriptionString())
            plugInWindowDescription.accept(plugInWindowDescriptionString())
            activeChargingDescription.accept(activeChargingDescriptionString())
        }
    }

    func initPlugInPickerItems() {
        var times = [String]()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm"

        if let midnight = dateFormatter.date(from: "0:00") {
            for hour in 0...23 {
                for minute in [0, 30] {
                    var timeInterval = DateComponents()
                    timeInterval.hour = hour
                    timeInterval.minute = minute

                    if let time = Calendar.current.date(byAdding: timeInterval, to: midnight) {
                        times.append(dataService.using24HrClock() ? ScheduledCharging.dateFormatter.string(from: time) :
                                                                    ScheduledCharging.amPmFormatter.string(from: time))
                    }
                }
            }
        }
        
        pickerTimeItems.accept(times)
        
        var durations = [String]()
        for halfHours in 1...48 {
            let minutes = halfHours * 30
            durations.append(ScheduledCharging.getDurationAsString(minutes))
        }
        pickerDurationItems.accept(durations)
    }
    
    func updateActiveChargingDurationPickers() {
        guard let scheduledCharging = self.scheduledCharging else { return }
        var durations = [String]()
        for halfHours in 1...48 {
            let minutes = halfHours * 30
            if minutes > scheduledCharging.activeChargingMaxDuration {
                break;
            }
            durations.append(ScheduledCharging.getDurationAsString(minutes))
        }

        activeChargingDurationItems.accept(durations)
    }
    
    func updateActiveChargingStartTimePickers() {
        guard let scheduledCharging = self.scheduledCharging else { return }
        var times = [String]()
        let plugInStartTimeStr = ScheduledCharging.getTimeAsString(time: scheduledCharging.plugInStartTime, using24HrClock: true)
        
        if let plugInStartTime = ScheduledCharging.dateFormatter.date(from: plugInStartTimeStr) {
            for minutes in stride(from: 0, through: scheduledCharging.plugInDuration, by: 30) {
                var timeInterval = DateComponents()
                timeInterval.minute = minutes
                
                if let time = Calendar.current.date(byAdding: timeInterval, to: plugInStartTime) {
                    times.append(dataService.using24HrClock() ? ScheduledCharging.dateFormatter.string(from: time) :
                                                                ScheduledCharging.amPmFormatter.string(from: time))
                }
            }
        }
        activeChargingTimeItems.accept(times)
    }

    func indexOfTimeString(_ timeStr: String) -> Int? {
        return pickerTimeItems.value.index(of: timeStr)
    }
    
    func indexOfDurationString(_ durationStr: String) -> Int? {
        return pickerDurationItems.value.index(of: durationStr)
    }
    
    func indexOfActiveChargingDurationString(_ durationStr: String) -> Int? {
        return activeChargingDurationItems.value.index(of: durationStr)
    }
    
    func indexOfActiveChargingTimeString(_ timeStr: String) -> Int? {
        return activeChargingTimeItems.value.index(of: timeStr)
    }
    
    func scheduledChargingDescriptionString() -> NSAttributedString {
        guard let scheduledCharging  = self.scheduledCharging else { return NSAttributedString.init() }
        let plugInWindowRange = plugInWindowDescriptionString()
        let activeChargingStartTimeStr = activeChargingStartTime.value
        let activeChargingEndTimeStr = ScheduledCharging.getEndTimeAsString(time: scheduledCharging.activeChargingStartTime, duration: scheduledCharging.activeChargingDuration, using24HrClock: dataService.using24HrClock())
        let maxStateOfChargeString = maxStateOfChargeDescriptionString()
        
        let formattedString = "scheduled_charging_settings_description".localized
        let resultString = String.init(format: formattedString, plugInWindowRange, activeChargingStartTimeStr, activeChargingEndTimeStr, "")
        let attribString = NSMutableAttributedString.init(string: resultString,
                                                          attributes: [NSAttributedStringKey.font : FontUtil.uiFont(.newsTitle),
                                                                       NSAttributedStringKey.foregroundColor : ColorUtil.uiColor(.grayText)])
        attribString.append(maxStateOfChargeString)
        
        // Add color for time text when enabled
        if enabled.value {
            if let range = resultString.range(of: plugInWindowRange) {
                attribString.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], range: NSRange(range, in: resultString))
            }
            if let range = resultString.range(of: activeChargingStartTimeStr) {
                attribString.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], range: NSRange(range, in: resultString))
            }
            if let range = resultString.range(of: activeChargingEndTimeStr) {
                attribString.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], range: NSRange(range, in: resultString))
            }
        }

        return attribString
    }
    
    func plugInWindowDescriptionString() -> String {
        guard let scheduledCharging  = self.scheduledCharging else { return "" }
        let plugInWindowStartTimeStr = plugInWindowStartTime.value
        let plugInWindowEndTimeStr = ScheduledCharging.getEndTimeAsString(time: scheduledCharging.plugInStartTime,
                                                                          duration: scheduledCharging.plugInDuration,
                                                                          using24HrClock: dataService.using24HrClock())
        let startTimeRangeStr = "\(plugInWindowStartTimeStr) - \(plugInWindowEndTimeStr)"
        return startTimeRangeStr
    }
    
    func activeChargingDescriptionString() -> String {
        guard let scheduledCharging  = self.scheduledCharging else { return "" }
        let activeChargingStartTimeStr = activeChargingStartTime.value
        let activeChargingEndTimeStr = ScheduledCharging.getEndTimeAsString(time: scheduledCharging.activeChargingStartTime,
                                                                            duration: scheduledCharging.activeChargingDuration,
                                                                            using24HrClock: dataService.using24HrClock())
        let rangeStr = "\(activeChargingStartTimeStr) - \(activeChargingEndTimeStr)"
        return rangeStr
    }
    
    func maxStateOfChargeDescriptionString() -> NSAttributedString {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager else { return NSAttributedString.init() }
        
        if batteryManager.chargeTargetEnabled && batteryManager.targetCharge != 1.0 {
            let formattedString = "scheduled_charging_max_state_of_charge_description".localized
            let maxSoc = Int(batteryManager.targetCharge * 100.0)
            let resultString = String.init(format: formattedString, maxSoc)
            return NSAttributedString.init(string: resultString, attributes: [NSAttributedStringKey.font : FontUtil.uiFont(.newsTitle),
                                                                              NSAttributedStringKey.foregroundColor : enabled.value ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)])
        } else {
            return NSAttributedString.init(string: "scheduled_charging_max_state_of_charge_full_description".localized,
                                           attributes: [NSAttributedStringKey.font : FontUtil.uiFont(.newsTitle),
                                                        NSAttributedStringKey.foregroundColor : enabled.value ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)])
        }
    }
    
    func sendScheduledChargingToBike() {
        guard let scheduledCharging = self.scheduledCharging else { return }

        let scheduledChargingPacket = ScheduledChargingPacket()
        scheduledChargingPacket.setPlugInStartTimeWithInt(scheduledCharging.plugInStartTime)
        scheduledChargingPacket.setPlugInLengthWithInt(scheduledCharging.plugInDuration)
        scheduledChargingPacket.setActiveStartTimeWithInt(scheduledCharging.activeChargingStartTime)
        scheduledChargingPacket.setActiveLengthWithInt(scheduledCharging.activeChargingDuration)
        scheduledChargingPacket.enabled = scheduledCharging.enabled
        scheduledChargingPacket.setDays(scheduledCharging.daysOfWeek)

        if zeroMotorcycleService.isConnected() {
            zeroMotorcycleService.sendSingleZeroPacket(packet: scheduledChargingPacket, priority: true).subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                
                if let _ = event.element as? AckPacket {
                    strongSelf.bluetoothDisposeBag = DisposeBag()
                    strongSelf.dataService.currentMotorcycle?.scheduledCharging = scheduledCharging
                    strongSelf.onUpdateComplete.accept(true)
                }
                
            }.disposed(by: bluetoothDisposeBag)
            
            zeroMotorcycleService.getScheduledChargingResend().subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                strongSelf.bluetoothDisposeBag = DisposeBag()
                DDLogDebug("Write ScheduledCharging Response Resend Packet")
                guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                    strongSelf.onUpdateComplete.accept(false)
                    return
                }
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.sendScheduledChargingToBike()
            }.disposed(by: bluetoothDisposeBag)
        } else {
            dataService.currentMotorcycle?.scheduledCharging = scheduledCharging
            zeroMotorcycleService.queueSingleZeroPacket(packet: scheduledChargingPacket)
            onQueueComplete.accept(true)
        }
    }
}
