//
//  RouteTypePickerViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 8/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

class RouteTypePickerViewModel {
    var dataService: DataService
    var batteryManager: BatteryManager?
    
    private var routeTypes: Variable<[RouteTypeViewModel]> = Variable([])
    let dataSource: Observable<[RouteTypeViewModel]>
    var onSavedRouteType = PublishRelay<Void>()

    private var selectedRouteType: BatteryManager.RouteType?
    
    init(dataService: DataService) {
        self.dataService = dataService
        self.dataSource = routeTypes.asObservable()
    }
    
    func loadModel(model: BatteryManager?) {
        self.batteryManager = model
        if let batteryManager = self.batteryManager {
            self.selectedRouteType = batteryManager.routeType
            
            routeTypes.value.removeAll()
            _ = BatteryManager.RouteType.allCases.map { routeType in
                let viewModel = RouteTypeViewModel(routeType: routeType, selected: routeType == selectedRouteType)
                
                routeTypes.value.append(viewModel)
            }
        }
    }
    
    func onRouteTypeSelected(_ viewModel: RouteTypeViewModel) {
        if let batteryManager = batteryManager {
            batteryManager.routeType = viewModel.routeType
            onSavedRouteType.accept(())
        }
    }
}
