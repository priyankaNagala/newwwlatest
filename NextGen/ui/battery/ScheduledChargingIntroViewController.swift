//
//  ScheduledChargingIntroViewController.swift
//  nextgen
//
//  Created by Scott Wang on 7/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ScheduledChargingIntroViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SCHEDULED_CHARGING_INTRO}

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataService.shared.hasSeenScheduledChargingIntro = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func continuePressed(_ sender: Any) {
        UILauncher.launchScheduledCharging(self, navController: self.navigationController)
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
