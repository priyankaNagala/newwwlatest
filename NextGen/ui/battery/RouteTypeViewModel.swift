//
//  RouteTypeViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 8/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class RouteTypeViewModel: SettingSelectionViewModel {
    var routeType: BatteryManager.RouteType
    var name = String()
    var selected = false
    
    init(routeType: BatteryManager.RouteType, selected: Bool) {
        self.routeType = routeType
        self.name = routeType.localizedString()
        self.selected = selected
    }
    
    // MARK: SettingSelectionViewModel
    func isSelected() -> Bool {
        return selected
    }
    
    func title() -> String {
        return name
    }
}
