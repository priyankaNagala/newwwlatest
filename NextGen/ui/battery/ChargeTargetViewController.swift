//
//  ChargeTargetViewController.swift
//  nextgen
//
//  Created by Scott Wang on 8/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ChargeTargetViewController: ZeroUIViewController, SyncActivityOverlayViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_CHARGE_TARGET }
    
    fileprivate var syncView: SyncActivityOverlayView = SyncActivityOverlayView()

    @IBOutlet weak var enabledSwitch: UISwitch!
    @IBOutlet weak var targetRangeChargeTitleLabel: UILabel!
    @IBOutlet weak var targetRangeChargeSlider: ZeroUISlider!
    @IBOutlet weak var targetRangeChargeValueLabel: UILabel!
    @IBOutlet weak var routeTypeLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var rideStyleTitleLabel: UILabel!
    @IBOutlet weak var enableDescription: UILabel!
    @IBOutlet weak var rideStyleSegmentedControl: ZeroUISegmentedControl! {
        didSet {
            rideStyleSegmentedControl.setTitle("charge_target_ride_style_conservative".localized, forSegmentAt: 0)
            rideStyleSegmentedControl.setTitle("charge_target_ride_style_aggressive".localized, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var terrainTitleLabel: UILabel!
    @IBOutlet weak var terrainSegmentedControl: ZeroUISegmentedControl! {
        didSet {
            terrainSegmentedControl.setTitle("charge_target_terrain_flat".localized, forSegmentAt: 0)
            terrainSegmentedControl.setTitle("charge_target_terrain_hills".localized, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var airTemperatureTitleLabel: UILabel!
    @IBOutlet weak var airTemperatureSegmentedControl: ZeroUISegmentedControl! {
        didSet {
            airTemperatureSegmentedControl.setTitle("charge_target_air_temperature_cold".localized, forSegmentAt: 0)
            airTemperatureSegmentedControl.setTitle("charge_target_air_temperature_mild".localized, forSegmentAt: 1)
            airTemperatureSegmentedControl.setTitle("charge_target_air_temperature_warm".localized, forSegmentAt: 2)
        }
    }
    @IBOutlet weak var continueChargingSwitch: UISwitch!
    @IBOutlet weak var setAsDefaultSwitch: UISwitch!
    @IBOutlet weak var updateChargeTargetButton: ZeroUIButton!
    
    var chargeTargetViewModel: ChargeTargetViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncView.delegate = self
        bindViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        syncView.frame = view.bounds
    }
    
    // MARK: - Bind Views
    func bindViews() {
        guard let viewModel = chargeTargetViewModel else { return } // Should be injected
        
        disposeBag.insertAll(all:
            // Enabled
            viewModel.enabled.subscribe { event in
                guard let isOn = event.element else { return }
                self.enabledSwitch.isOn = isOn
                self.enableSliders(isOn)
                self.enableDescription.isHidden = isOn
            },
            enabledSwitch.rx.isOn.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let isOn = event.element else { return }
                viewModel.onEnabledControlChanged(isOn)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },
            
            // Target Range
            viewModel.maxRange.subscribe { [weak self] event in
                guard let strongSelf = self, let maxRange = event.element else { return }
                strongSelf.targetRangeChargeSlider.maximumValue = maxRange
            },
            viewModel.targetRange.bind(to: targetRangeChargeSlider.rx.value),
            viewModel.targetRangeChargeFormatted.bind(to: targetRangeChargeValueLabel.rx.text),
            targetRangeChargeSlider.rx.value.changed.subscribe{ [weak self] event in
                guard let strongSelf = self, let value = event.element else { return }

                let min = Float(Constant.CHARGE_TARGET_MIN_PERCENTAGE) * strongSelf.targetRangeChargeSlider.maximumValue
                let max = Float(Constant.CHARGE_TARGET_MAX_PERCENTAGE) * strongSelf.targetRangeChargeSlider.maximumValue
                
                if value < min {
                    strongSelf.targetRangeChargeSlider.value = min
                }
                else if value > max {
                    strongSelf.targetRangeChargeSlider.value = max
                } else {
                    viewModel.onTargetRangeChanged(value)
                    strongSelf.updateChargeTargetButton.isEnabled = true
                }
            },
            
            // Route Type
            viewModel.routeType.bind(to: routeTypeLabel.rx.text),
            
            // Ride Style
            viewModel.rideStyle.bind(to: rideStyleSegmentedControl.rx.selectedSegmentIndex),
            rideStyleSegmentedControl.rx.selectedSegmentIndex.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let value = event.element else { return }
                viewModel.onRideStyleChanged(value)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },
            
            // Terrain
            viewModel.terrain.bind(to: terrainSegmentedControl.rx.selectedSegmentIndex),
            terrainSegmentedControl.rx.selectedSegmentIndex.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let value = event.element else { return }
                viewModel.onTerrainChanged(value)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },
            
            // Air Temperature
            viewModel.airTemperature.bind(to: airTemperatureSegmentedControl.rx.selectedSegmentIndex),
            airTemperatureSegmentedControl.rx.selectedSegmentIndex.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let value = event.element else { return }
                viewModel.onAirTemperatureChanged(value)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },

            // Continue Charging / Set as Default
            viewModel.continueCharging.bind(to: continueChargingSwitch.rx.isOn),
            continueChargingSwitch.rx.isOn.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let isOn = event.element else { return }
                viewModel.onContinueChargingChanged(isOn)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },
            viewModel.setAsDefault.bind(to: setAsDefaultSwitch.rx.isOn),
            setAsDefaultSwitch.rx.isOn.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let isOn = event.element else { return }
                viewModel.onSetAsDefaultChanged(isOn)
                strongSelf.updateChargeTargetButton.isEnabled = true
            },
            
            // Update Button
            updateChargeTargetButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showSyncActivity(true)
                strongSelf.syncView.showSyncUnsuccessful(false)
                strongSelf.view.addSubview(strongSelf.syncView)
                viewModel.onUpdatePressed()
            },
            viewModel.onUpdateComplete.subscribe { [weak self] event in
                guard let strongSelf = self, let completed = event.element else { return }
                if completed {
                    strongSelf.navigationController?.popViewController(animated: true)
                } else {
                    strongSelf.syncView.showSyncUnsuccessful(true)
                }
            },
            viewModel.onQueueComplete.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showQueued(true)
            }
        )
        viewModel.loadModel()
    }

    // MARK: - SyncActivityOverlayViewDelegate
    func toastDismissed() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    func enableSliders(_ enabled: Bool) {
        let enabledTextColor = ColorUtil.color(.White)
        let disabledTextColor = ColorUtil.color(.Gray)

        targetRangeChargeSlider.isEnabled = enabled
        targetRangeChargeTitleLabel.textColor = enabled ? enabledTextColor : disabledTextColor
    }
    
    // MARK: - IBAction
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func routeTypePressed(_ sender: Any) {
        guard let viewModel = chargeTargetViewModel else { return } // Should be injected
        UILauncher.launchRouteTypePicker(self, batteryManager: viewModel.batteryManager)
            .bind { [weak self] _ in
                guard let strongSelf = self else { return }
                viewModel.onRouteTypeChanged()
                strongSelf.updateChargeTargetButton.isEnabled = true }
            .disposed(by: disposeBag)
    }
}
