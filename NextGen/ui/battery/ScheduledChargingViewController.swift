//
//  ScheduledChargingViewController.swift
//  nextgen
//
//  Created by Scott Wang on 7/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ScheduledChargingViewController: ZeroUIViewController, UIPickerViewDelegate, SyncActivityOverlayViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SCHEDULED_CHARGING}
    static let PICKER_HEIGHT:CGFloat = 128.0
    
    var scheduledChargingViewModel: ScheduledChargingViewModel?
    fileprivate var syncView: SyncActivityOverlayView = SyncActivityOverlayView()

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var enabledSwitch: UISwitch!

    @IBOutlet weak var plugInWindowPlugIconImageView: UIImageView!
    @IBOutlet weak var plugInWindowTitleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var plugInWindowStartTimePickerView: UIPickerView!
    @IBOutlet weak var plugInWindowDurationPickerView: UIPickerView!
    @IBOutlet weak var plugInWindowButton: UIButton!
    @IBOutlet weak var plugInWindowDescriptionLabel: UILabel!
    
    @IBOutlet weak var activeChargingPlugIconImageView: UIImageView!
    @IBOutlet weak var activeChargingChargeIconImageView: UIImageView!
    @IBOutlet weak var activeChargingTitleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var activeChargingStartTimePickerView: UIPickerView!
    @IBOutlet weak var activeChargingDurationPickerView: UIPickerView!
    @IBOutlet weak var activeChargingButton: UIButton!
    @IBOutlet weak var activeChargingDescriptionLabel: UILabel!
    
    @IBOutlet weak var daysOfTheWeekTitleLabel: ZeroUpperCaseUILabel!
    @IBOutlet var daysOfTheWeekButtons: [UIButton]!
    @IBOutlet var daysOfTheWeekIcons: [UIImageView]!
    @IBOutlet weak var scheduledChargingDescriptionOfSettingsLabel: UILabel!
    @IBOutlet weak var updateButton: ZeroUIButton!
    
    @IBOutlet weak var plugInWindowPickerContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var activeChargingPickerContainerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncView.delegate = self
        bindViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        syncView.frame = view.bounds
    }
    
    // MARK: - Bind Views
    func bindViews() {
        guard let viewModel = scheduledChargingViewModel else { return } // Should be injected
        
        disposeBag.insertAll(all:

            // Enabled
            viewModel.enabled.subscribe { [weak self] event in
                guard let strongSelf = self, let isOn = event.element else { return }
                strongSelf.enabledSwitch.isOn = isOn
                strongSelf.enablePickers(isOn)
            },

            enabledSwitch.rx.isOn.changed.subscribe { [weak self] event in
                guard let strongSelf = self, let isOn = event.element else { return }
                viewModel.onEnabledControlChanged(isOn: isOn)
                strongSelf.updateButton.isEnabled = true
            },
            
            // Plug-In, Active Charging range description labels
            viewModel.plugInWindowDescription.bind(to: plugInWindowDescriptionLabel.rx.text),
            viewModel.activeChargingDescription.bind(to: activeChargingDescriptionLabel.rx.text),
            
            // Start/End time of pickers
            viewModel.pickerTimeItems.bind(to: plugInWindowStartTimePickerView.rx.items) { [weak self] (row, element, view) in
                guard let strongSelf = self else { return UILabel() }
                guard let label = view as? UILabel else {
                    return strongSelf.getPickerLabel(element)
                }
                label.text = element
                return label
            },
            viewModel.pickerDurationItems.bind(to: plugInWindowDurationPickerView.rx.items) { [weak self] (row, element, view) in
                guard let strongSelf = self else { return UILabel() }
                guard let label = view as? UILabel else {
                    return strongSelf.getPickerLabel(element)
                }
                label.text = element
                return label
            },
            viewModel.activeChargingTimeItems.bind(to: activeChargingStartTimePickerView.rx.items) { [weak self] (row, element, view) in
                guard let strongSelf = self else { return UILabel() }
                guard let label = view as? UILabel else {
                    return strongSelf.getPickerLabel(element)
                }
                label.text = element
                return label
            },
            viewModel.activeChargingDurationItems.bind(to: activeChargingDurationPickerView.rx.items) { [weak self] (row, element, view) in
                guard let strongSelf = self else { return UILabel() }
                guard let label = view as? UILabel else {
                    return strongSelf.getPickerLabel(element)
                }
                label.text = element
                return label
            },
            
            // Set currently selected start/end time
            viewModel.plugInWindowStartTime.subscribe { [weak self] event in
                guard let strongSelf = self, let startTime = event.element else { return }
                if let index = viewModel.indexOfTimeString(startTime) {
                    strongSelf.plugInWindowStartTimePickerView.selectRow(index, inComponent: 0, animated: true)
                }
            },
            viewModel.plugInWindowDuration.subscribe { [weak self] event in
                guard let strongSelf = self, let duration = event.element else { return }
                if let index = viewModel.indexOfDurationString(duration) {
                    strongSelf.plugInWindowDurationPickerView.selectRow(index, inComponent: 0, animated: true)
                }
            },
            viewModel.activeChargingStartTime.subscribe { [weak self] event in
                guard let strongSelf = self, let startTime = event.element else { return }
                if let index = viewModel.indexOfActiveChargingTimeString(startTime) {
                    strongSelf.activeChargingStartTimePickerView.selectRow(index, inComponent: 0, animated: true)
                }
            },
            viewModel.activeChargingDuration.subscribe { [weak self] event in
                guard let strongSelf = self, let duration = event.element else { return }
                if let index = viewModel.indexOfActiveChargingDurationString(duration) {
                    strongSelf.activeChargingDurationPickerView.selectRow(index, inComponent: 0, animated: true)
                }
            },

            // Plug-In picker updates
            plugInWindowStartTimePickerView.rx.modelSelected(String.self).subscribe({ [weak self] event in
                guard let strongSelf = self,
                    let viewModel = strongSelf.scheduledChargingViewModel,
                    let selectedTime = event.element?.first
                    else { return }

                viewModel.plugInWindowStartTimeSelected(time: selectedTime, amPm: nil)
                strongSelf.updateButton.isEnabled = true
            }),
            plugInWindowDurationPickerView.rx.modelSelected(String.self).subscribe({ [weak self] event in
                guard let strongSelf = self,
                    let viewModel = strongSelf.scheduledChargingViewModel,
                    let selectedDuration = event.element?.first
                    else { return }
                
                viewModel.plugInWindowDurationSelected(duration: selectedDuration)
                strongSelf.updateButton.isEnabled = true
            }),
            activeChargingStartTimePickerView.rx.modelSelected(String.self).subscribe({ [weak self] event in
                guard let strongSelf = self,
                    let viewModel = strongSelf.scheduledChargingViewModel,
                    let selectedTime = event.element?.first
                    else { return }
                
                viewModel.activeChargingStartTimeSelected(time: selectedTime, amPm: nil)
                strongSelf.updateButton.isEnabled = true
            }),
            activeChargingDurationPickerView.rx.modelSelected(String.self).subscribe({ [weak self] event in
                guard let strongSelf = self,
                    let viewModel = strongSelf.scheduledChargingViewModel,
                    let selectedDuration = event.element?.first
                    else { return }
                
                viewModel.activeChargingDurationSelected(duration: selectedDuration)
                strongSelf.updateButton.isEnabled = true
            }),

            // Days Of The Week
            viewModel.daysOfTheWeek.subscribe(onNext: { [weak self] (daysOfTheWeek) in
                guard let strongSelf = self else { return }
                for day in daysOfTheWeek {
                    if let button = strongSelf.dayOfWeekButton(day) {
                        let color = strongSelf.enabledSwitch.isOn ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)
                        button.setTitleColor(color, for: .normal)
                    }
                    if let icon = strongSelf.dayOfWeekIcon(day) {
                        icon.isHidden = false
                    }
                }
                for nonDays in ScheduledCharging.allDaysOfWeek.filter({!daysOfTheWeek.contains($0)}) {
                    if let button = strongSelf.dayOfWeekButton(nonDays) {
                        button.setTitleColor(ColorUtil.uiColor(.grayText), for: .normal)
                    }
                    if let icon = strongSelf.dayOfWeekIcon(nonDays) {
                        icon.isHidden = true
                    }
                }
            }),
            
            // Scheduled Setting Description
            viewModel.scheduledChargingDescription.bind(to: scheduledChargingDescriptionOfSettingsLabel.rx.attributedText),
            
            // Update Button
            updateButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showSyncActivity(true)
                strongSelf.syncView.showSyncUnsuccessful(false)
                strongSelf.view.addSubview(strongSelf.syncView)
                viewModel.onUpdatePressed()
            },
            
            viewModel.onUpdateComplete.subscribe { [weak self] event in
                guard let strongSelf = self, let completed = event.element else { return }
                if completed {
                    strongSelf.navigationController?.popViewController(animated: true)
                } else {
                    strongSelf.syncView.showSyncUnsuccessful(true)
                }
            },
            
            viewModel.onQueueComplete.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showQueued(true)
            }
        )
        
        viewModel.loadModel()
    }
    
    // MARK: - SyncActivityOverlayViewDelegate
    func toastDismissed() {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: - IBAction
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func plugInWindowButtonPressed(_ sender: Any) {
        guard enabledSwitch.isOn else { return }
        showPlugInWindowPicker(plugInWindowPickerContainerViewHeightConstraint.constant == 0)
        if plugInWindowPickerContainerViewHeightConstraint.constant != 0 && activeChargingPickerContainerViewHeightConstraint.constant != 0 {
            // Hide active charging, only one can be opened at a time
            showActiveChargingPicker(false)
        }
    }
    
    @IBAction func activeChargingButtonPressed(_ sender: Any) {
        guard enabledSwitch.isOn else { return }
        showActiveChargingPicker(activeChargingPickerContainerViewHeightConstraint.constant == 0)
        if activeChargingPickerContainerViewHeightConstraint.constant != 0 &&
            plugInWindowPickerContainerViewHeightConstraint.constant != 0 {
            // Hide Plug in window, only one can be opened at a time
            showPlugInWindowPicker(false)
        }
    }
    
    @IBAction func dayOfWeekPressed(_ sender: UIButton) {
        guard enabledSwitch.isOn else { return }
        if let viewModel = scheduledChargingViewModel, let dayOfWeek = ScheduledCharging.DayOfWeek.init(rawValue: sender.tag) {
            viewModel.toggleDayOfWeek(dayOfWeek: dayOfWeek)
            self.updateButton.isEnabled = true
        }
    }
    
    // MARK: -
    func getPickerLabel(_ text: String) -> UILabel {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.white
        pickerLabel.font = FontUtil.uiFont(.pickerItem)
        pickerLabel.textAlignment = NSTextAlignment.center
        pickerLabel.text = text
        return pickerLabel
    }
    
    func dayOfWeekButton(_ dayOfWeek: ScheduledCharging.DayOfWeek) -> UIButton? {
        for button in daysOfTheWeekButtons {
            if button.tag == dayOfWeek.rawValue {
                return button
            }
        }
        return nil
    }
    
    func dayOfWeekIcon(_ dayOfWeek: ScheduledCharging.DayOfWeek) -> UIImageView? {
        for icon in daysOfTheWeekIcons {
            if icon.tag == dayOfWeek.rawValue {
                return icon
            }
        }
        return nil
    }
    
    func showPlugInWindowPicker(_ show: Bool) {
        plugInWindowButton.isSelected = show
        plugInWindowPickerContainerViewHeightConstraint.constant = show ? ScheduledChargingViewController.PICKER_HEIGHT : 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    func showActiveChargingPicker(_ show: Bool) {
        activeChargingButton.isSelected = show
        activeChargingPickerContainerViewHeightConstraint.constant = show ? ScheduledChargingViewController.PICKER_HEIGHT : 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    func enablePickers(_ enable: Bool) {
        showPlugInWindowPicker(enable)
        showActiveChargingPicker(false)
        plugInWindowTitleLabel.textColor = enable ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)
        activeChargingTitleLabel.textColor = enable ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)
        daysOfTheWeekTitleLabel.textColor = enable ? ColorUtil.uiColor(.whiteText) : ColorUtil.uiColor(.grayText)
        plugInWindowDescriptionLabel.textColor = enable ? ColorUtil.uiColor(.greenText) : ColorUtil.uiColor(.yellowText)
        activeChargingDescriptionLabel.textColor = enable ? ColorUtil.uiColor(.greenText) : ColorUtil.uiColor(.yellowText)
        for icon in daysOfTheWeekIcons {
            icon.isHighlighted = enable
        }
        plugInWindowPlugIconImageView.image = enable ? UIImage(named: "ic_plugcharge_plug_active") : UIImage(named: "ic_plugcharge_plug_inactive")
        activeChargingPlugIconImageView.image = enable ? UIImage(named: "ic_plugcharge_plug_active") : UIImage(named: "ic_plugcharge_plug_inactive")
        activeChargingChargeIconImageView.image = enable ?  UIImage(named: "ic_plugcharge_charge_active") : UIImage(named: "ic_plugcharge_charge_inactive")
    }
}


