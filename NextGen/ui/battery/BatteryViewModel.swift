//
//  BatteryViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 7/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class BatteryViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var bluetoothDisposeBag = DisposeBag()
    var chargeTargetDisposeBag = DisposeBag()
    var scheduledChargingDisposeBag = DisposeBag()
    var writeChargeTargetDisposeBag = DisposeBag()
    var numChargeTargetRetries = 0
    var numWriteChargeTargetRetries = 0
    var numScheduledChargingRetried = 0
    
    // MARK: - INPUTS
    var targetRange = BehaviorRelay<Int>(value: 0)
    var targetRangeTitle = BehaviorRelay<String>(value: "battery_full_range".localized)
    var targetRangeTitleColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var targetRangeValueColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var targetRangeUnitColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var chargeTime = BehaviorRelay<String>(value: "0:00")
    var chargeTimeTitle = BehaviorRelay<String>(value: "battery_charge_time".localized)
    var chargeTimeTitleColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var chargeTimeValueColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var chargeRate = BehaviorRelay<String>(value: "0")
    var chargeRateTitle = BehaviorRelay<String>(value: "battery_charge_rate".localized)
    var chargeRateTitleColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var chargeRateValueColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var chargeRateUnit = BehaviorRelay<String>(value: "global_kw".localized)

    var currentCharge = BehaviorRelay<Int>(value: 0)
    var currentRange = BehaviorRelay<Int>(value: 0)
    var isCharging = BehaviorRelay<Bool>(value: false)
    var isChargingGlowColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.Green))
    var backgroundTrackColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.DarkGreen))
    var targetCharge = BehaviorRelay<Float>(value: 0.0)
    var storageMode = BehaviorRelay<Bool>(value: false)
    var distanceUnit = BehaviorRelay<String>(value: "global_mi".localized)

    var chargingState = BehaviorRelay<String>(value: "battery_unplugged".localized)
    var chargingStateColor = BehaviorRelay<UIColor>(value: ColorUtil.color(.LightGray))
    var chargingStateIcon = BehaviorRelay<UIImage?>(value: nil)

    var defaultTargetIcon = BehaviorRelay<UIImage?>(value: nil)
    var defaultTargetIconTint = BehaviorRelay<UIColor>(value: ColorUtil.color(.White))
    var defaultTargetControlEnabled = BehaviorRelay<Bool>(value: false)

    var chargeToPercentage = BehaviorRelay<Float>(value: 0.0)
    var isChargingOverridden = BehaviorRelay<Bool>(value: false)
    var isPluggedIn = BehaviorRelay<Bool>(value: false)
    
    var scheduledChargingDescription = BehaviorRelay<String>(value: "")
    var onUpdateComplete = PublishRelay<Bool>()

    
    private var modelLoaded = false
    private var appeared = false
    private var chargeToTargetIconTemplated = UIImage.init(named: "ic_batterycharge_recurring")

    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    func loadModel() {
        // Read charget target first, bike info continuously sent
        readChargeTargetFromBike()
        readScheduledCharginging()
        readBikeInfoFromBike()
        modelLoaded = true
    }
    
    func onViewWillAppear() {
        if modelLoaded && appeared {
            readChargeTargetFromBike()
            readScheduledCharginging()
        }
        appeared = true
    }
    
    func onChargeTargetChange(_ value: Int) {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager, value != Int(round(batteryManager.targetCharge * 100)) else { return }
        batteryManager.targetCharge = Float(value) / 100
        dataService.currentMotorcycle?.batteryManager = batteryManager
        sendChargeTargetToBike()
    }
    
    func onChargeOverridePressed() {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager else { return }
        let chargeTargetPacket = ChargeTargetPacket()
        chargeTargetPacket.enabled = batteryManager.chargeTargetEnabled
        chargeTargetPacket.chargeTargetSoc = UInt8(batteryManager.targetChargeRounded * 100)
        chargeTargetPacket.hardTarget = !batteryManager.continueCharging
        chargeTargetPacket.defaultTarget = batteryManager.setAsDefault
        chargeTargetPacket.chargeOverride = true
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: chargeTargetPacket, priority: false).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            if let _ = event.element as? AckPacket {
                strongSelf.writeChargeTargetDisposeBag = DisposeBag()
                DDLogDebug("Write ChargeTarget Response ACK Packet")
                strongSelf.dataService.currentMotorcycle?.batteryManager = batteryManager
                strongSelf.isChargingOverridden.accept(true)
            }
            }.disposed(by: writeChargeTargetDisposeBag)
        
        zeroMotorcycleService.getChargeTargetResend().subscribe { [weak self] event in
            guard let strongSelf = self, let _ = event.element else { return }
            strongSelf.writeChargeTargetDisposeBag = DisposeBag()
            DDLogDebug("Write ChargeTarget Response Resend Packet")
            guard strongSelf.numWriteChargeTargetRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                strongSelf.numWriteChargeTargetRetries = 0
                return
            }
            strongSelf.numWriteChargeTargetRetries = strongSelf.numWriteChargeTargetRetries + 1
            strongSelf.onChargeOverridePressed()
            }.disposed(by: writeChargeTargetDisposeBag)
    }
    
    // MARK: - Storage Mode
    
    func onActivateStorageMode() {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager else { return }
        let chargeTargetPacket = ChargeTargetPacket()
        chargeTargetPacket.enabled = batteryManager.chargeTargetEnabled
        chargeTargetPacket.chargeTargetSoc = UInt8(batteryManager.targetChargeRounded * 100)
        chargeTargetPacket.hardTarget = !batteryManager.continueCharging
        chargeTargetPacket.defaultTarget = batteryManager.setAsDefault
        chargeTargetPacket.inStorageMode = true

        zeroMotorcycleService.sendSingleZeroPacket(packet: chargeTargetPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            strongSelf.chargeTargetDisposeBag = DisposeBag()
            if let packet = event.element as? AckPacket {
                DDLogDebug("Write ChargeTarget Response ChargeTarget Packet " + packet.getFullPacket().toHexString())
                strongSelf.activateStorageMode(true)
            }
        }.disposed(by: chargeTargetDisposeBag)
        
        // Add resend handler
        if numChargeTargetRetries < Constant.BLUETOOTH_RESEND_RETRIES {
            numChargeTargetRetries = numChargeTargetRetries + 1
            
            zeroMotorcycleService.getChargeTargetResend().subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                strongSelf.chargeTargetDisposeBag = DisposeBag()
                DDLogDebug("Write ChargeTarget Response Resend Packet")
                strongSelf.onActivateStorageMode()
                }.disposed(by: chargeTargetDisposeBag)
        } else { // Skip handler and reset retries count
            numChargeTargetRetries = 0
        }
    }

    func onDisableStorageMode() {
        // Only key-cycling motorcycle is supposed to disable storage mode
    }
    
    private func activateStorageMode(_ activate: Bool) {
        storageMode.accept(activate)
    }
    
    // MARK: -  Bluetooth
    
    func readChargeTargetFromBike() {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager else { return }
        let readPacket = ChargeTargetPacket()
        readPacket.isEmpty = true
        
        // Read from battery manager
        if !zeroMotorcycleService.isConnected() {
            targetCharge.accept(batteryManager.targetCharge)
            return
        }
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: readPacket, priority: false).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }

            if let chargeTargetPacket = event.element as? ChargeTargetPacket {
                strongSelf.chargeTargetDisposeBag = DisposeBag()
                // Only read enabled, this could possibly be changed by MBB
                batteryManager.chargeTargetEnabled = chargeTargetPacket.enabled
                
                DDLogDebug("Read ChargeTarget Response ChargeTarget Packet: " + chargeTargetPacket.getFullPacket().toHexString())
                if chargeTargetPacket.enabled {
                    strongSelf.defaultTargetControlEnabled.accept(true)
                    if strongSelf.isPluggedIn.value {
                        strongSelf.targetRangeTitle.accept("battery_target_range".localized)
                        strongSelf.targetRangeValueColor.accept(ColorUtil.color(.White))
                        strongSelf.targetRangeUnitColor.accept(ColorUtil.color(.White))
                        strongSelf.targetRangeTitleColor.accept(ColorUtil.color(.Green))

                        // Set target range
                        if strongSelf.dataService.usingMetricDistance {
                            strongSelf.distanceUnit.accept("global_km".localized)
                            let conversion = Int(Float(batteryManager.targetRange) * Constant.KM_IN_ONE_MILE)
                            strongSelf.targetRange.accept(conversion)
                        } else {
                            strongSelf.distanceUnit.accept("global_mi".localized)
                            strongSelf.targetRange.accept(batteryManager.targetRange)
                        }
                    }
                    if let scheduledCharging = strongSelf.dataService.currentMotorcycle?.scheduledCharging, scheduledCharging.enabled {
                        strongSelf.targetRangeTitleColor.accept(ColorUtil.color(.Yellow))
                    }
                    batteryManager.targetCharge = Float(chargeTargetPacket.chargeTargetSoc) / 100
                    strongSelf.targetCharge.accept(batteryManager.targetCharge)

                    // Hard/soft target
                    if batteryManager.continueCharging {
                        strongSelf.chargeToPercentage.accept(1.0)
                    } else {
                        strongSelf.chargeToPercentage.accept(batteryManager.targetCharge)
                    }
                    
                    // Default/recurring target
                    if batteryManager.setAsDefault {
                        strongSelf.defaultTargetIcon.accept(strongSelf.chargeToTargetIconTemplated)
                        strongSelf.defaultTargetIconTint.accept(ColorUtil.color(.White))
                    } else {
                        strongSelf.defaultTargetIcon.accept(strongSelf.chargeToTargetIconTemplated)
                        strongSelf.defaultTargetIconTint.accept(ColorUtil.color(.LightGray))
                    }
                } else {
                    strongSelf.defaultTargetControlEnabled.accept(false)
                    strongSelf.defaultTargetIcon.accept(nil)
                }
                
                strongSelf.dataService.currentMotorcycle?.batteryManager = batteryManager
                strongSelf.activateStorageMode(chargeTargetPacket.inStorageMode)
                strongSelf.isChargingOverridden.accept(chargeTargetPacket.chargeOverride)
                
                if let scheduledCharging = strongSelf.dataService.currentMotorcycle?.scheduledCharging {
                    if scheduledCharging.enabled {
                        let format = "battery_scheduled_charging_description".localized
                        var targetChargeDescription = "scheduled_charging_max_state_of_charge_full_description".localized
                        if !batteryManager.continueCharging {
                            targetChargeDescription = String.init(format: "scheduled_charging_max_state_of_charge_description".localized, Int(batteryManager.targetCharge * 100))
                        }
                        let str = String.init(format: format, scheduledCharging.activeChargingStartTimeFormatted,
                                              scheduledCharging.activeChargingEndTimeFormatted, targetChargeDescription)
                        strongSelf.scheduledChargingDescription.accept(str)
                    } else {
                        strongSelf.scheduledChargingDescription.accept("")
                    }
                }
            } else {
                DDLogError("Read ChargeTarget Response Unknown Packet")
            }
        }.disposed(by: chargeTargetDisposeBag)
        
        // Add resend handler
        if numChargeTargetRetries < Constant.BLUETOOTH_RESEND_RETRIES {
            numChargeTargetRetries = numChargeTargetRetries + 1
            
            zeroMotorcycleService.getChargeTargetResend().subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                strongSelf.chargeTargetDisposeBag = DisposeBag()
                DDLogDebug("Read ChargeTarget Response Resend Packet")
                strongSelf.readChargeTargetFromBike()
                }.disposed(by: chargeTargetDisposeBag)
        } else { // Skip handler and reset retries count
            numChargeTargetRetries = 0
        }
    }
    
    func readScheduledCharginging() {
        guard let scheduledCharginge = dataService.currentMotorcycle?.scheduledCharging else { return }
        
        if scheduledCharginge.enabled {
            isChargingGlowColor.accept(ColorUtil.color(.Yellow))
            backgroundTrackColor.accept(ColorUtil.color(.DarkYellow))
        } else {
            isChargingGlowColor.accept(ColorUtil.color(.Green))
            backgroundTrackColor.accept(ColorUtil.color(.DarkGreen))
        }
        /*
        let readPacket = ScheduledChargingPacket()
        readPacket.isEmpty = true
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: readPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil, let scheduledCharging = strongSelf.dataService.currentMotorcycle?.scheduledCharging else { return }
            
            if let scheduledChargingPacket = event.element as? ScheduledChargingPacket {
                strongSelf.scheduledChargingDisposeBag = DisposeBag()
                scheduledCharging.enabled = scheduledChargingPacket.enabled
                scheduledCharging.activeChargingStartTime = scheduledChargingPacket.getActiveStartTimeAsInt()
                scheduledCharging.activeChargingDuration = scheduledChargingPacket.getActiveLengthAsInt()
                scheduledCharging.plugInStartTime = scheduledChargingPacket.getPlugInStartTimeAsInt()
                scheduledCharging.plugInDuration = scheduledChargingPacket.getPlugInLengthAsInt()
            } else {
                DDLogError("Read ScheduledCharging Response Unknown Packet")
            }
            
        }.disposed(by: scheduledChargingDisposeBag)
         */
    }
    
    func readBikeInfoFromBike() {
        zeroMotorcycleService.getBikeInfo().subscribe { [weak self] event in
            guard let strongSelf = self, let bikeInfoPacket = event.element else { return }
            strongSelf.isCharging.accept(bikeInfoPacket.isCharging())
            strongSelf.currentCharge.accept(Int(bikeInfoPacket.stateOfCharge))
            
            // Set current range
            if let battery = strongSelf.dataService.currentMotorcycle?.batteryManager {
                if strongSelf.dataService.usingMetricDistance {
                    strongSelf.distanceUnit.accept("global_km".localized)
                    let conversion = Int(Float(battery.rangeGivenCharge(charge: Float(bikeInfoPacket.stateOfCharge) / 100.0)) * Constant.KM_IN_ONE_MILE)
                    strongSelf.currentRange.accept(conversion)
                } else {
                    strongSelf.distanceUnit.accept("global_mi".localized)
                    strongSelf.currentRange.accept(battery.rangeGivenCharge(charge: Float(bikeInfoPacket.stateOfCharge) / 100.0))
                }
            }
            
            strongSelf.isPluggedIn.accept(bikeInfoPacket.isPluggedIn())
            if bikeInfoPacket.isPluggedIn() {
                if let battery = strongSelf.dataService.currentMotorcycle?.batteryManager {
                    if !battery.chargeTargetEnabled {
                        strongSelf.showFullRange()
                    }
                } else {
                    strongSelf.showFullRange()
                }
                if bikeInfoPacket.isCharging() {
                    strongSelf.chargingState.accept("battery_charging".localized)
                    strongSelf.chargingStateColor.accept(ColorUtil.color(.Green))
                    strongSelf.chargingStateIcon.accept(UIImage.init(named: "ic_plugcharge_charge_active"))

                    strongSelf.chargeTime.accept(strongSelf.formatChargeTime(Int(bikeInfoPacket.chargeTime)))
                    strongSelf.chargeTimeValueColor.accept(ColorUtil.color(.White))
                    strongSelf.chargeTimeTitleColor.accept(ColorUtil.color(.White))
                    strongSelf.chargeRate.accept(strongSelf.formatChargeRate(Int(bikeInfoPacket.chargeRate)))
                    strongSelf.chargeRateValueColor.accept(ColorUtil.color(.White))
                    strongSelf.chargeRateTitleColor.accept(ColorUtil.color(.White))
                    strongSelf.chargeRateUnit.accept("global_kw".localized)
                    strongSelf.chargeRateTitle.accept("battery_charge_rate".localized)
                } else {
                    if let scheduledCharging = strongSelf.dataService.currentMotorcycle?.scheduledCharging, scheduledCharging.enabled {
                            strongSelf.chargingState.accept("battery_scheduled".localized)
                            strongSelf.chargingStateColor.accept(ColorUtil.color(.Yellow))
                            strongSelf.chargingStateIcon.accept(UIImage.init(named: "ic_battery_scheduled"))
                            
                            strongSelf.chargeRateTitle.accept("battery_scheduled_charge".localized)
                            strongSelf.chargeRateTitleColor.accept(ColorUtil.color(.Yellow))
                            strongSelf.formatActiveChargingStartTime(scheduledCharging.activeChargingStartTimeFormatted)
                            strongSelf.chargeTimeTitle.accept("battery_time_to_charge".localized)
                            strongSelf.chargeTime.accept(scheduledCharging.timeTillActiveChargingFormatted)
                            strongSelf.chargeTimeValueColor.accept(ColorUtil.color(.White))
                            strongSelf.chargeTimeTitleColor.accept(ColorUtil.color(.White))
                    } else {
                        strongSelf.chargingState.accept("")
                        strongSelf.chargingStateColor.accept(ColorUtil.color(.LightGray))
                        strongSelf.chargingStateIcon.accept(nil)

                        strongSelf.chargeTime.accept("global_not_applicable".localized)
                        strongSelf.chargeTimeValueColor.accept(ColorUtil.color(.LightGray))
                        strongSelf.chargeTimeTitleColor.accept(ColorUtil.color(.LightGray))
                        strongSelf.chargeRate.accept("global_not_applicable".localized)
                        strongSelf.chargeRateValueColor.accept(ColorUtil.color(.LightGray))
                        strongSelf.chargeRateTitleColor.accept(ColorUtil.color(.LightGray))
                        strongSelf.chargeRateUnit.accept("")
                        strongSelf.chargeRateTitle.accept("battery_charge_rate".localized)
                    }
                }
            } else {
                strongSelf.chargingState.accept("battery_unplugged".localized)
                strongSelf.chargingStateColor.accept(ColorUtil.color(.LightGray))
                strongSelf.chargingStateIcon.accept(nil)

                strongSelf.chargeTime.accept("global_not_applicable".localized)
                strongSelf.chargeTimeValueColor.accept(ColorUtil.color(.LightGray))
                strongSelf.chargeTimeTitleColor.accept(ColorUtil.color(.LightGray))
                strongSelf.chargeRate.accept("global_not_applicable".localized)
                strongSelf.chargeRateValueColor.accept(ColorUtil.color(.LightGray))
                strongSelf.chargeRateTitleColor.accept(ColorUtil.color(.LightGray))
                strongSelf.chargeRateUnit.accept("")
                strongSelf.chargeRateTitle.accept("battery_charge_rate".localized)

                strongSelf.showFullRange()
            }
        }.disposed(by: bluetoothDisposeBag)
    }
    
    func sendChargeTargetToBike() {
        guard let batteryManager = dataService.currentMotorcycle?.batteryManager, zeroMotorcycleService.isConnected() else {
            onUpdateComplete.accept(false)
            return
        }
        
        let chargeTargetPacket = ChargeTargetPacket()
        chargeTargetPacket.enabled = batteryManager.chargeTargetEnabled
        chargeTargetPacket.chargeTargetSoc = UInt8(batteryManager.targetChargeRounded * 100)
        chargeTargetPacket.hardTarget = !batteryManager.continueCharging
        chargeTargetPacket.defaultTarget = batteryManager.setAsDefault
        
        zeroMotorcycleService.sendSingleZeroPacketReplacingSameType(packet: chargeTargetPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            if let _ = event.element as? AckPacket {
                strongSelf.writeChargeTargetDisposeBag = DisposeBag()
                DDLogDebug("Write ChargeTarget Response ACK Packet")
                // Set target range
                if strongSelf.dataService.usingMetricDistance {
                    strongSelf.distanceUnit.accept("global_km".localized)
                    let conversion = Int(Float(batteryManager.targetRange) * Constant.KM_IN_ONE_MILE)
                    strongSelf.targetRange.accept(conversion)
                } else {
                    strongSelf.distanceUnit.accept("global_mi".localized)
                    strongSelf.targetRange.accept(batteryManager.targetRange)
                }
                strongSelf.onUpdateComplete.accept(true)
            }
            }.disposed(by: writeChargeTargetDisposeBag)
        
        zeroMotorcycleService.getChargeTargetResend().subscribe { [weak self] event in
            guard let strongSelf = self, let _ = event.element else { return }
            strongSelf.writeChargeTargetDisposeBag = DisposeBag()
            DDLogDebug("Write ChargeTarget Response Resend Packet")
            guard strongSelf.numChargeTargetRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                strongSelf.onUpdateComplete.accept(false)
                strongSelf.numChargeTargetRetries = 0
                return
            }
            strongSelf.numChargeTargetRetries = strongSelf.numChargeTargetRetries + 1
            strongSelf.sendChargeTargetToBike()
            }.disposed(by: writeChargeTargetDisposeBag)
    }

    // MARK: -
    
    fileprivate func formatActiveChargingStartTime(_ activeChargingStartTime: String) {
        chargeRateValueColor.accept(ColorUtil.color(.White))
        if dataService.using24HrClock() {
            chargeRate.accept(activeChargingStartTime)
            chargeRateUnit.accept("")
        } else {
            // Replace am/pm and add it to unit label
            if activeChargingStartTime.contains("global_am".localized) {
                chargeRate.accept(activeChargingStartTime.replacingOccurrences(of: "global_am".localized, with: ""))
                chargeRateUnit.accept("global_am".localized)
            } else if activeChargingStartTime.contains("global_pm".localized) {
                chargeRate.accept(activeChargingStartTime.replacingOccurrences(of: "global_pm".localized, with: ""))
                chargeRateUnit.accept("global_pm".localized)
            } else {
                // Should really get here
                chargeRate.accept(activeChargingStartTime)
                chargeRateUnit.accept("")
            }
        }

    }
    fileprivate func formatChargeTime(_ minutes: Int) -> String {
        let hr = minutes / 60
        let min = minutes - (hr * 60)
        let str = String.init(format: "%d:%02d", hr, min)
        return str
    }
    
    fileprivate func formatChargeRate(_ tenthKw: Int) -> String {
        let rateInKw = Double(tenthKw) / 10
        return "\(rateInKw)"
    }
    
    fileprivate func showFullRange() {
        targetRangeTitle.accept("battery_full_range".localized)
        targetRangeTitleColor.accept(ColorUtil.color(.LightGray))
        targetRangeValueColor.accept(ColorUtil.color(.LightGray))
        targetRangeUnitColor.accept(ColorUtil.color(.LightGray))
        if let battery = dataService.currentMotorcycle?.batteryManager {
            if dataService.usingMetricDistance {
                distanceUnit.accept("global_km".localized)
                let conversion = Int(Float(battery.rangeGivenCharge(charge: 1.0)) * Constant.KM_IN_ONE_MILE)
                targetRange.accept(conversion)
            } else {
                distanceUnit.accept("global_mi".localized)
                targetRange.accept(battery.rangeGivenCharge(charge: 1.0))
            }
        }
    }
}
