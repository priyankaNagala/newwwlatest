//
//  ChargeTargetViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 8/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class ChargeTargetViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var batteryManager: BatteryManager?
    var bluetoothDisposeBag = DisposeBag()
    var numRetries = 0
    
    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    // MARK: - Inputs
    var enabled = BehaviorRelay<Bool>(value: false)
    var targetRange = BehaviorRelay<Float>(value: Float(Constant.CHARGE_TARGET_DEFAULT_TARGET_RANGE))
    var maxRange = BehaviorRelay<Float>(value: Float(Constant.CHARGE_TARGET_MAX_RANGE))
    var targetRangeChargeFormatted = BehaviorRelay<String>(value: "")
    var chargeTargetNotificationEnabled = BehaviorRelay<Bool>(value: false)
    var airTemperature = BehaviorRelay<Int>(value: Constant.CHARGE_TARGET_DEFAULT_AIR_TEMPERATURE.rawValue)
    var terrain = BehaviorRelay<Int>(value: Constant.CHARGE_TARGET_DEFAULT_TERRAIN.rawValue)
    var rideStyle = BehaviorRelay<Int>(value: Constant.CHARGE_TARGET_DEFAULT_RIDE_STYLE.rawValue)
    var routeType = BehaviorRelay<String>(value: Constant.CHARGE_TARGET_DEFAULT_ROUTE_TYPE.localizedString())
    var continueCharging = BehaviorRelay<Bool>(value: false)
    var setAsDefault = BehaviorRelay<Bool>(value: false)
    var onUpdateComplete = PublishRelay<Bool>()
    var onQueueComplete = PublishRelay<Bool>()

    // MARK: - Outputs
    func onEnabledControlChanged(_ isOn: Bool) {
        guard let batteryManager = self.batteryManager else { return }
        batteryManager.chargeTargetEnabled = isOn
        enabled.accept(isOn)
    }
    
    func onTargetRangeChanged(_ value: Float) {
        guard let batteryManager = self.batteryManager else { return }
        batteryManager.lockRange = true
        batteryManager.targetRange = Int(value)
        acceptTargetRangeCharge()
        batteryManager.lockRange = false
    }
    
    func onRouteTypeChanged() {
        guard let batteryManager = self.batteryManager else { return }
        routeType.accept(batteryManager.routeType.localizedString())
        acceptTargetRangeCharge()
    }
    
    func onRideStyleChanged(_ value: Int) {
        guard let batteryManager = self.batteryManager,
            let rideStyleValue = BatteryManager.RideStyle(rawValue: value) else { return }
        batteryManager.rideStyle = rideStyleValue
        rideStyle.accept(value)
        acceptTargetRangeCharge()
    }

    func onTerrainChanged(_ value: Int) {
        guard let batteryManager = self.batteryManager,
            let terrainValue = BatteryManager.Terrain(rawValue: value) else { return }
        batteryManager.terrain = terrainValue
        terrain.accept(value)
        acceptTargetRangeCharge()
    }

    func onAirTemperatureChanged(_ value: Int) {
        guard let batteryManager = self.batteryManager,
            let airTemperatureValue = BatteryManager.AirTemperature(rawValue: value) else { return }
        batteryManager.airTemperature = airTemperatureValue
        airTemperature.accept(value)
        acceptTargetRangeCharge()
    }
    
    func onContinueChargingChanged(_ isOn: Bool) {
        guard let batteryManager = self.batteryManager else { return }
        batteryManager.continueCharging = isOn
        continueCharging.accept(isOn)
    }
    
    func onSetAsDefaultChanged(_ isOn: Bool) {
        guard let batteryManager = self.batteryManager else { return }
        batteryManager.setAsDefault = isOn
        setAsDefault.accept(isOn)
    }

    func onUpdatePressed() {
        sendChargeTargetToBike()
    }

    // MARK: -
    func loadModel() {
        if let batteryManager = dataService.currentMotorcycle?.batteryManager {
            self.batteryManager = batteryManager
            enabled.accept(batteryManager.chargeTargetEnabled)
            acceptTargetRangeCharge()
            chargeTargetNotificationEnabled.accept(batteryManager.chargeTargetNotificationEnabled)
            airTemperature.accept(batteryManager.airTemperature.rawValue)
            terrain.accept(batteryManager.terrain.rawValue)
            rideStyle.accept(batteryManager.rideStyle.rawValue)
            routeType.accept(batteryManager.routeType.localizedString())
            continueCharging.accept(batteryManager.continueCharging)
            setAsDefault.accept(batteryManager.setAsDefault)
            maxRange.accept(Float(batteryManager.maxRange))
        }
    }
    
    func acceptTargetRangeCharge() {
        guard let batteryManager = self.batteryManager else { return }
        maxRange.accept(Float(batteryManager.maxRange))
        targetRange.accept(Float(batteryManager.targetRange))
        let combinedStr = "\(batteryManager.targetRangeFormatted)\n\(batteryManager.targetChargeFormatted)"
        targetRangeChargeFormatted.accept(combinedStr)
    }
    
    func sendChargeTargetToBike() {
        guard let batteryManager = self.batteryManager else { return }
        
        let chargeTargetPacket = ChargeTargetPacket()
        chargeTargetPacket.enabled = batteryManager.chargeTargetEnabled
        chargeTargetPacket.chargeTargetSoc = UInt8(batteryManager.targetChargeRounded * 100)
        chargeTargetPacket.hardTarget = !batteryManager.continueCharging
        chargeTargetPacket.defaultTarget = batteryManager.setAsDefault
        
        if zeroMotorcycleService.isConnected() {
            zeroMotorcycleService.sendSingleZeroPacket(packet: chargeTargetPacket, priority: true).subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                if let _ = event.element as? AckPacket {
                    strongSelf.bluetoothDisposeBag = DisposeBag()
                    DDLogDebug("Write ChargeTarget Response ACK Packet")
                    strongSelf.dataService.currentMotorcycle?.batteryManager = batteryManager
                    strongSelf.onUpdateComplete.accept(true)
                }
            }.disposed(by: bluetoothDisposeBag)
            
            zeroMotorcycleService.getChargeTargetResend().subscribe { [weak self] event in
                guard let strongSelf = self, let _ = event.element else { return }
                strongSelf.bluetoothDisposeBag = DisposeBag()
                DDLogDebug("Write ChargeTarget Response Resend Packet")
                guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                    strongSelf.onUpdateComplete.accept(false)
                    strongSelf.numRetries = 0
                    return
                }
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.sendChargeTargetToBike()
            }.disposed(by: bluetoothDisposeBag)
        } else {
            dataService.currentMotorcycle?.batteryManager = batteryManager
            zeroMotorcycleService.queueSingleZeroPacket(packet: chargeTargetPacket)
            onQueueComplete.accept(true)
        }
    }
}
