//
//  BatteryViewController.swift
//  NextGenView
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import AEConicalGradient
import CocoaLumberjack

class BatteryViewController: ZeroTabViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_BATTERY }
    
    var batteryViewModel: BatteryViewModel?
    var scheduledChargingViewModel : ScheduledChargingViewModel?
    
    @IBOutlet weak var overrideScheduledChargeContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scheduledChargingDescription: UILabel!
    
    @IBOutlet weak var chargeNowButton: ZeroUIButton!
    // Constraints
    @IBOutlet weak var batteryGaugeTopToDividerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var targetRangeTitleLabelTrailingToContentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var chargeRateTitleLabelTrailingToContentViewConstraint: NSLayoutConstraint!
    
    // Battery Gauge Gradient Views
    @IBOutlet weak var batteryGaugeBackgroundGlowView: RadialGradientView!
    @IBOutlet weak var batteryGaugeBackgroundView: ConicalGradientView!
    @IBOutlet weak var batteryGaugeCentralLockupView: UIView!
    @IBOutlet weak var batteryGaugeTrackView: ConicalGradientView!
    
    // Battery Gauge Central Lockup
    @IBOutlet weak var batteryPowerTitleLabel: UILabel!
    @IBOutlet weak var batteryPowerValueLabel: UILabel!
    @IBOutlet weak var batteryPowerPercentLabel: UILabel!
    @IBOutlet weak var chargingStateLabel: UILabel!
    @IBOutlet weak var chargingStateIconImageView: UIImageView!
    @IBOutlet weak var storageModeLabel: ZeroUpperCaseUILabel!
    
    // Battery Gague Info Labels / Buttons
    @IBOutlet weak var currentRangeTitleLabel: UILabel!
    @IBOutlet weak var currentRangeValueLabel: UILabel!
    @IBOutlet weak var currentRangeUnitLabel: UILabel!
    @IBOutlet weak var targetRangeTitleLabel: UILabel!
    @IBOutlet weak var targetRangeValueLabel: UILabel!
    @IBOutlet weak var targetRangeUnitLabel: UILabel!
    @IBOutlet weak var chargeTimeTitleLabel: UILabel!
    @IBOutlet weak var chargeTimeValueLabel: UILabel!
    @IBOutlet weak var chargeRateTitleLabel: UILabel!
    @IBOutlet weak var chargeRateValueLabel: UILabel!
    @IBOutlet weak var chargeRateUnitLabel: UILabel!
    @IBOutlet weak var targetRangeButton: UIButton!
    @IBOutlet weak var targetRangeButtonCaretImage: UIImageView!
    @IBOutlet weak var targetBatteryPercentageLabel: UILabel!
    @IBOutlet weak var defaultTargetIconImageView: UIImageView!
    @IBOutlet weak var chargeRateButtonCaretImage: UIImageView!
    @IBOutlet weak var chargeRateButton: UIButton!
    @IBOutlet weak var chargeTargetControl: ChargeTargetControl!
    
    // Target Notification
    @IBOutlet weak var targetNotificationView: UIView!
    @IBOutlet weak var targetNotificationSwitch: UISwitch!

    @IBOutlet weak var manageScheduleChargingButton: ZeroUIButton!
    @IBOutlet weak var manageChargeTargetButton: ZeroUIButton!
    @IBOutlet weak var activateStorageModeButton: ZeroTertiaryUIButton!
    @IBOutlet weak var disableStorageModeButton: ZeroTertiaryUIButton!
    @IBOutlet weak var zeroNavigationItem: ZeroUINavigationItem!
    @IBOutlet weak var activateStorageModeButtonToBottomViewConstraint: NSLayoutConstraint!
    
    var currentTrackPercentage: CGFloat = 0
    var backgroundTrackPercentage: CGFloat = 0
    var batteryTrackSized: Bool = false
    var batteryGlowColor: UIColor = ColorUtil.color(.Green)
    var batteryBackgroundColor: UIColor = ColorUtil.color(.DarkGreen)
    var batteryAnimating: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBatteryGaugeViews()
        showChargeRateButton(false)
        showTargetRangeButton(false)
        bindViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        sizeBatteryGauge()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        batteryViewModel?.onViewWillAppear()
    }
    
    // MARK: - Setup Views
    func setupBatteryGaugeViews() {
        batteryGaugeBackgroundGlowView.locations = [0.15, 0.55]
        setBatteryGlowColor(batteryGlowColor)
        setBatteryBackgroundColor(batteryBackgroundColor)
        
        batteryGaugeBackgroundView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        
        batteryGaugeTrackView.gradient.colors = [ColorUtil.color(.Green),ColorUtil.color(.Green),ColorUtil.color(.Green),batteryGlowColor]
        
        targetRangeButton.semanticContentAttribute = .forceRightToLeft
    }
    
    func setBatteryGlowColor(_ color: UIColor) {
        batteryGlowColor = color
        if batteryAnimating { // Reset animation with different color
            animateBatteryGlowView(false)
            animateBatteryGlowView(true)
        }
    }
    
    func setBatteryBackgroundColor(_ color: UIColor) {
        batteryBackgroundColor = color
        batteryGaugeBackgroundView.gradient.colors = [batteryBackgroundColor, batteryBackgroundColor, batteryBackgroundColor,ColorUtil.color(.NavBackground)]
    }

    func sizeBatteryGauge() {
        guard !batteryTrackSized else { return }

        batteryGaugeBackgroundView.cornerRadius = batteryGaugeBackgroundView.frame.size.width / 2.0
        batteryGaugeCentralLockupView.cornerRadius = batteryGaugeCentralLockupView.frame.size.width / 2.0
        batteryGaugeTrackView.cornerRadius = batteryGaugeTrackView.frame.size.width / 2.0
        batteryTrackSized = true
        self.setBatteryTrackPercentage(currentTrackPercentage)
    }
    
    // MARK: - Bind Views
    func bindViews() {
        guard let viewModel = batteryViewModel else { return } // Should be injected

        // ZERO-548 Remove Storage Mode
        activateStorageMode(false)
        activateStorageModeButton.isHidden = true
        activateStorageModeButton.isEnabled = true
        
        disposeBag.insertAll(all:
            
            viewModel.distanceUnit.bind(to: targetRangeUnitLabel.rx.text),
            viewModel.distanceUnit.bind(to: currentRangeUnitLabel.rx.text),
            viewModel.targetRangeTitle.bind(to: targetRangeTitleLabel.rx.text),
            viewModel.chargingState.bind(to: chargingStateLabel.rx.text),
            viewModel.chargeRateTitle.bind(to: chargeRateTitleLabel.rx.text),
            viewModel.chargeRateUnit.bind(to: chargeRateUnitLabel.rx.text),
            viewModel.chargeTimeTitle.bind(to: chargeTimeTitleLabel.rx.text),
            viewModel.scheduledChargingDescription.bind(to: scheduledChargingDescription.rx.text),

            viewModel.chargeRateTitleColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.chargeRateTitleLabel.textColor = color
            },
            
            viewModel.chargeRateValueColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.chargeRateValueLabel.textColor = color
            },

            viewModel.targetRangeTitleColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.targetRangeTitleLabel.textColor = color
            },

            viewModel.targetRangeValueColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.targetRangeValueLabel.textColor = color
            },

            viewModel.targetRangeUnitColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.targetRangeUnitLabel.textColor = color
            },
            
            viewModel.chargeTimeTitleColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.chargeTimeTitleLabel.textColor = color
            },

            viewModel.chargeTimeValueColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.chargeTimeValueLabel.textColor = color
            },
            
            viewModel.chargingStateIcon.subscribe { [weak self] event in
                guard let strongSelf = self, let image = event.element else { return }
                strongSelf.chargingStateIconImageView.image = image
            },
            
            viewModel.defaultTargetIcon.subscribe { [weak self] event in
                guard let strongSelf = self, let image = event.element else { return }
                strongSelf.defaultTargetIconImageView.image = image
            },
            
            viewModel.chargeToPercentage.subscribe { [weak self] event in
                guard let strongSelf = self, let chargeToPercentage = event.element else { return }
                strongSelf.setBatteryBackgroundPercentage(CGFloat(chargeToPercentage))
            },
            
            viewModel.defaultTargetControlEnabled.subscribe { [weak self] (event) in
                guard let strongSelf = self, let enabled = event.element else { return }
                strongSelf.chargeTargetControl.isEnabled = enabled
                strongSelf.targetBatteryPercentageLabel.isHidden = !enabled
            },
            
            viewModel.defaultTargetIconTint.subscribe { [weak self] (event) in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.defaultTargetIconImageView.tintColor = color
            },
            
            viewModel.chargingStateColor.subscribe { [weak self] (event) in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.chargingStateLabel.textColor = color
            },

            viewModel.chargeRate.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let chargeRate = event.element else { return }
                strongSelf.chargeRateValueLabel.text = "\(chargeRate)"
            },
                             
            viewModel.chargeTime.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let chargeTime = event.element else { return }
                strongSelf.chargeTimeValueLabel.text = "\(chargeTime)"
            },

            viewModel.currentRange.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let currentRange = event.element else { return }
                strongSelf.currentRangeValueLabel.text = "\(currentRange)"
            },

            viewModel.currentCharge.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let currentCharge = event.element else { return }
                strongSelf.setBatteryTrackPercentage( CGFloat(currentCharge) / 100.0)
            },

            viewModel.targetCharge.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let targetCharge = event.element else { return }
                strongSelf.chargeTargetControl.percent = CGFloat(targetCharge)
            },
            
            viewModel.targetRange.subscribe { [weak self] (event) in
                guard let strongSelf = self, let targetRange = event.element else { return }
                strongSelf.targetRangeValueLabel.text = "\(targetRange)"
            },
            
            viewModel.storageMode.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let storageMode = event.element else { return }
                // ZERO-548 Remove Storage Mode
                //strongSelf.activateStorageMode(storageMode)
            },
            
            // Charging Glow
            viewModel.isCharging.subscribe{ [weak self] (event) in
                guard let strongSelf = self, let charging = event.element else { return }
                strongSelf.animateBatteryGlowView(charging)
                if charging {
                    
                }
            },
            viewModel.isChargingGlowColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.setBatteryGlowColor(color)
            },
            
            viewModel.backgroundTrackColor.subscribe { [weak self] event in
                guard let strongSelf = self, let color = event.element else { return }
                strongSelf.setBatteryBackgroundColor(color)
            },
            
            viewModel.isChargingOverridden.subscribe { [weak self] event in
                guard let strongSelf = self, let overridden = event.element else { return }
                strongSelf.showOverrideScheduledChargeButton(!overridden && viewModel.isPluggedIn.value && !viewModel.storageMode.value && !viewModel.isCharging.value)
            },
            
            viewModel.isPluggedIn.subscribe { [weak self] event in
                guard let strongSelf = self, let pluggedIn = event.element else { return }
                strongSelf.showOverrideScheduledChargeButton(!viewModel.isChargingOverridden.value && pluggedIn && !viewModel.storageMode.value && !viewModel.isCharging.value)
            }
        )
        
        viewModel.loadModel()
    }
    
    // MARK: - Show/Hide UI Elements
    
    func activateStorageMode(_ activate: Bool) {
        currentRangeTitleLabel.isHidden = activate
        currentRangeValueLabel.isHidden = activate
        currentRangeUnitLabel.isHidden = activate
        targetRangeTitleLabel.isHidden = activate
        targetRangeValueLabel.isHidden = activate
        targetRangeUnitLabel.isHidden = activate
        defaultTargetIconImageView.isHidden = activate
        chargeTimeTitleLabel.isHidden = activate
        chargeTimeValueLabel.isHidden = activate
        chargeRateTitleLabel.isHidden = activate
        chargeRateValueLabel.isHidden = activate
        chargeRateUnitLabel.isHidden = activate
        targetNotificationSwitch.isHidden = activate
        targetNotificationView.isHidden = activate
        manageScheduleChargingButton.isHidden = activate
        manageChargeTargetButton.isHidden = activate
        activateStorageModeButton.isHidden = activate
        batteryPowerTitleLabel.isHidden = activate
        batteryPowerValueLabel.isHidden = activate
        batteryPowerPercentLabel.isHidden = activate
        chargingStateLabel.isHidden = activate
        storageModeLabel.isHidden = !activate
        disableStorageModeButton.isHidden = !activate
        
        if let viewModel = self.batteryViewModel {
            // Re-enable control if we're supposed to.
            chargeTargetControl.isEnabled = viewModel.defaultTargetControlEnabled.value ? !activate : false
            targetBatteryPercentageLabel.isHidden = viewModel.defaultTargetControlEnabled.value ? activate : true
            // Show override button if not storage mode and not already overriden and plugged in and not charging
            showOverrideScheduledChargeButton(!activate && !viewModel.isChargingOverridden.value && viewModel.isPluggedIn.value && !viewModel.isCharging.value)
        }
            
        if activate {
            setBatteryTrackPercentage(0.5)
            batteryGaugeBackgroundGlowView.colors = [ColorUtil.color(.Orange), UIColor.clear]
            batteryGaugeBackgroundView.gradient.colors = [ColorUtil.color(.NavBackground),
                                                          ColorUtil.color(.Orange),
                                                          ColorUtil.color(.Orange),
                                                          ColorUtil.color(.Yellow)]
            batteryGaugeTrackView.gradient.colors = [ColorUtil.color(.NavBackground),
                                                     ColorUtil.color(.NavBackground),
                                                     UIColor.init(hexString: "#532D12")!,
                                                     ColorUtil.color(.NavBackground)]
            chargingStateIconImageView.image = UIImage.init(named: "ic_plugcharge_charge_storage")
            activateStorageModeButtonToBottomViewConstraint.constant = -300
        } else {
            batteryGaugeBackgroundGlowView.colors = [batteryGlowColor, UIColor.clear]
            setBatteryBackgroundColor(batteryBackgroundColor)
            batteryGaugeTrackView.gradient.colors = [ColorUtil.color(.Green),ColorUtil.color(.Green),ColorUtil.color(.Green),batteryGlowColor]
            activateStorageModeButtonToBottomViewConstraint.constant = 40
        }
    }
    
    func setBatteryTrackPercentage(_ percentage: CGFloat) {
        self.currentTrackPercentage = percentage

        guard batteryTrackSized else { // Only apply transforms after frames are all sized properly
            return
        }
        if percentage > 0 {
            batteryGaugeTrackView.isHidden = false

            let rotationRadians = (percentage - 0.75) * .pi * 2.0
            batteryGaugeTrackView.transform = CGAffineTransform(rotationAngle: rotationRadians)

            let startRadians = Double((1.0 - percentage) * .pi * 2.0)
            batteryGaugeTrackView.gradient.startAngle = startRadians
            
        } else {
            batteryGaugeTrackView.isHidden = true
        }
        let percent = Int(percentage * 100)
        batteryPowerValueLabel.text = "\(percent)"
    }
    
    
    func setBatteryBackgroundPercentage(_ percentage: CGFloat) {
        self.backgroundTrackPercentage = percentage
        
        guard batteryTrackSized else { // Only apply transforms after frames are all sized properly
            return
        }
        if percentage > 0 {
            batteryGaugeBackgroundView.isHidden = false
            
            let rotationRadians = (percentage - 0.75) * .pi * 2.0
            batteryGaugeBackgroundView.transform = CGAffineTransform(rotationAngle: rotationRadians)
            
            let startRadians = Double((1.0 - percentage) * .pi * 2.0)
            batteryGaugeBackgroundView.gradient.startAngle = startRadians
            
        } else {
            batteryGaugeBackgroundView.isHidden = true
        }
    }
    
    
    func showOverrideScheduledChargeButton(_ show: Bool) {
        if show {
            batteryGaugeTopToDividerBottomConstraint.constant = overrideScheduledChargeContainerView.frame.size.height + 20
        } else {
            batteryGaugeTopToDividerBottomConstraint.constant = 20
        }
        overrideScheduledChargeContainerView.isHidden = !show
    }
    
    func showChargeRateButton(_ show: Bool) {
        if show {
            chargeRateTitleLabelTrailingToContentViewConstraint.constant = chargeRateButtonCaretImage.frame.size.width + 8 + Constant.DEFAULT_GUTTER
        } else {
            chargeRateTitleLabelTrailingToContentViewConstraint.constant = Constant.DEFAULT_GUTTER
        }
        chargeRateButton.isHidden = !show
        chargeRateButtonCaretImage.isHidden = !show
    }
    
    func showTargetRangeButton(_ show: Bool) {
        if show {
            targetRangeTitleLabelTrailingToContentViewConstraint.constant = targetRangeButtonCaretImage.frame.size.width + 8 + Constant.DEFAULT_GUTTER
        } else {
            targetRangeTitleLabelTrailingToContentViewConstraint.constant = Constant.DEFAULT_GUTTER
        }
        targetRangeButton.isHidden = !show
        targetRangeButton.isEnabled = show
        targetRangeButtonCaretImage.isHidden = !show
    }
    
    func animateBatteryGlowView(_ animate: Bool) {
        if animate {
            if !batteryAnimating { // not already animating
                batteryAnimating = true
                UIView.animate(withDuration: 1.6, delay: 0, options: [.curveEaseIn, .autoreverse, .repeat], animations: {
                    self.batteryGaugeBackgroundGlowView.alpha = 0.2
                }, completion: nil)
            }
        } else {
            batteryAnimating = false
            batteryGaugeBackgroundGlowView.layer.removeAllAnimations()
            batteryGaugeBackgroundGlowView.alpha = 1.0
        }
    }

    // MARK: - IBAction
    
    @IBAction func overrideScheduledChargeButtonPressed(_ sender: Any) {
        if let viewModel = self.batteryViewModel {
            viewModel.onChargeOverridePressed()
        }
    }
    
    @IBAction func targetNotificationPressed(_ sender: Any) {
    }
    
    @IBAction func manageChargeTargetButtonPressed(_ sender: Any) {
        UILauncher.launchChargeTarget(self)
    }

    @IBAction func manageScheduledChargingButtonPressed(_ sender: Any) {
        UILauncher.launchScheduledChargingIntro(self)
    }
    
    @IBAction func activateStorageModePressed(_ sender: Any) {
        guard let viewModel = self.batteryViewModel else { return }
        let alert = UIAlertController(
            title: "battery_activate_storage_mode".localized,
            message: "battery_activate_storage_mode_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: { action in
            viewModel.onActivateStorageMode()
        }))
        self.present(alert, animated: true)
    }
    
    @IBAction func disableStorageMode(_ sender: Any) {
        guard let viewModel = self.batteryViewModel else { return }
        let alert = UIAlertController(
            title: "battery_disable_storage_mode".localized,
            message: "battery_disable_storage_mode_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: { action in
            viewModel.onDisableStorageMode()
        }))
        self.present(alert, animated: true)
    }
    
    @IBAction func chargeTargetControlValueChanged(_ sender: ChargeTargetControl) {
        guard let viewModel = self.batteryViewModel else { return }
        let percent = Int(round(sender.percent * 100.0))
        self.targetBatteryPercentageLabel.text = "\(percent)%"
        
        viewModel.onChargeTargetChange(percent)
    }
}
