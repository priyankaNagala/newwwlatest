//
//  ChargeTargetControl.swift
//  nextgen
//
//  Created by Scott Wang on 7/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ChargeTargetControl: UIControl {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var pinHeadIconImageView: UIImageView!
    @IBOutlet weak var pinHeadImageView: UIImageView!
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var pinHeadIconImageViewContainer: UIView!
    @IBOutlet weak var pinContainerView: UIView!
    
    private var pinContainerAnchorPoint:CGPoint!
    var percent:CGFloat = 0 {
        didSet {
            setHandlePercentage(percent)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ChargeTargetControl", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        pinContainerAnchorPoint = CGPoint.init(x: 0, y: ( contentView.frame.size.height - pinContainerView.frame.size.height) / 2.0)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        return true
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.continueTracking(touch, with: event)
        
        let location = touch.location(in: self)
        self.moveHandle(location)
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        self.setHandlePercentage(percent)
    }
    
    private func moveHandle(_ location: CGPoint) {
        let centerPoint = CGPoint.init(x: self.frame.width / 2, y: self.frame.height / 2)
        let diffX = location.x - centerPoint.x
        let diffY = location.y - centerPoint.y
        
        let angle = atan2(CGFloat(diffX), CGFloat(-diffY)) + .pi

        let percentage = angle / (2 * .pi)
        let roundedPercentage = round(percentage * 20) / 20 // Round to nearest 5

        percent = roundedPercentage
        
        setHandlePercentage(percentage)
        self.setNeedsDisplay()
    }
    
    private func setHandlePercentage(_ percentage: CGFloat) {
        guard percentage <= Constant.CHARGE_TARGET_MAX_PERCENTAGE,
            percentage >= Constant.CHARGE_TARGET_MIN_PERCENTAGE else {return}
        
        let radians = (percentage - 0.5) * .pi * 2.0
        
        // Move and rotate
        var transform = CGAffineTransform.identity
        transform = transform.translatedBy(x: pinContainerAnchorPoint.x, y: pinContainerAnchorPoint.y)
        transform = transform.rotated(by: radians)
        transform = transform.translatedBy(x: -pinContainerAnchorPoint.x, y: -pinContainerAnchorPoint.y)
        pinContainerView.transform = transform
        
        // rotate icon back
        transform = CGAffineTransform.identity
        transform = transform.rotated(by: -radians)
        pinHeadIconImageViewContainer.transform = transform
        
        sendActions(for: .valueChanged)
    }

    override var isEnabled: Bool {
        didSet {
            pinContainerView.isHidden = !isEnabled
        }
    }
}
