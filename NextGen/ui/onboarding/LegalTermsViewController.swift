//
//  LegalTermsViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class LegalTermsViewController: ZeroUIViewController, UIWebViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ONBOARDING_TERMS }
    
    var dataService: DataService?

    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var continueButton: ZeroUIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var check1Label: UILabel!
    @IBOutlet weak var check2Label: UILabel!
    @IBOutlet weak var check3Label: UILabel!
    @IBOutlet weak var check1Button: UIButton!
    @IBOutlet weak var check2Button: UIButton!
    @IBOutlet weak var check3Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        check1Button.isEnabled = false
        check2Button.isEnabled = false
        check3Button.isEnabled = false
        loadTermsAndConditions()
    }
    
    
    // MARK: -
    fileprivate func loadTermsAndConditions() {
        if let url = URL.init(string: Constant.PRIVACY_URL) {
            webView.scalesPageToFit = false
            webView.loadRequest(URLRequest(url:url))
            webView.delegate = self
        }
    }
    
    fileprivate func checkForNetwork() {
        if !NetworkUtil.isNetworkReachable() {
            let alert = UIAlertController(title: "onboarding_network".localized, message: "onboarding_network_required".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: { action in
                self.loadTermsAndConditions()
            }))
            
            self.present(alert, animated: true)
        }
    }
    
    fileprivate func updateContinueState() {
        continueButton.isEnabled = check1Button.isSelected && check2Button.isSelected && check3Button.isSelected
    }
    // MARK: - IBAction

    @IBAction func acceptClicked(_ sender: Any) {
        UILauncher.launchConnectFind(self)
    }
    
    @IBAction func check1Pressed(_ sender: Any) {
        check1Button.isSelected = !check1Button.isSelected
        updateContinueState()
    }
    @IBAction func check2Pressed(_ sender: Any) {
        check2Button.isSelected = !check2Button.isSelected
        updateContinueState()
    }
    @IBAction func check3Pressed(_ sender: Any) {
        check3Button.isSelected = !check3Button.isSelected
        updateContinueState()
    }

    // MARK: - UIWebViewDelegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        check1Button.isEnabled = true
        check2Button.isEnabled = true
        check3Button.isEnabled = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        checkForNetwork()
    }
}
