//
//  WelcomeViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class WelcomeViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ONBOARDING_WELCOME }

    @IBOutlet weak var getStartedButton: ZeroUIButton!
    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBAction func getStartedPressed(_ sender: Any) {
    }
    
}
