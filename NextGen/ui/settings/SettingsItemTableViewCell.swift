//
//  SettingsItemTableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import UIKit

class SettingsItemTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "SettingsItemTableViewCell"
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
        
    func configure(withViewModel viewModel: SettingsItemViewModel) {
        label.text = viewModel.label
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkMark.isHidden = !selected
    }
}
