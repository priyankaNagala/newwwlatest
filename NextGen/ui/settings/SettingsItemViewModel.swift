//
//  SettingsItemViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class SettingsItemViewModel {
    var settingType: Settings.SettingType?
    var distanceUnitValue: Settings.DistanceUnit?
    var temperatureValue: Settings.Temperature?
    var liquidValue: Settings.Liquid?
    var timeFormatValue: Settings.TimeFormat?
    
    var isCurrentValue: Bool = false
    
    var label: String? {
        get {
            guard let settingType = settingType else { return "" }
            switch settingType {
            case .DISTANCE_UNITS:
                return distanceUnitValue?.localizedString()
            case .TEMPERATURE:
                return temperatureValue?.localizedString()
            case .LIQUIDS:
                return liquidValue?.localizedString()
            case .TIME_FORMAT:
                return timeFormatValue?.localizedString()
            }
        }
    }
}
