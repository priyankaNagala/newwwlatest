//
//  SettingsViewModel.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift

class SettingsViewModel {
    var dataService: DataService
    var settingsSections = BehaviorRelay<[SettingsSection]>(value: [])
    typealias SettingsDataSource = RxTableViewSectionedReloadDataSource<SettingsSection>
    
    struct SettingsSection: SectionModelType  {
        typealias Item = SettingsItemViewModel
        var header: String
        var items: [Item]
        init(header: String, items: [Item]) {
            self.header = header
            self.items = items
        }
        init(original: SettingsSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func settingsDataSource() -> SettingsDataSource {
        let cellId = SettingsItemTableViewCell.ReuseIdentifier
        return SettingsDataSource(
            configureCell: { _, tv, ip, item  in
                let cell = tv.dequeueReusableCell(withIdentifier: cellId, for: ip) as! SettingsItemTableViewCell
                cell.configure(withViewModel: item)
                return cell
            }
        )
    }
    
    func loadModel() {
        let allSections = [
            SettingsSection(header:"settings_distance_units".localized, items:getDistanceUnitViewModels()),
            SettingsSection(header:"settings_temperature".localized, items:getTemperatureViewModels()),
            SettingsSection(header:"settings_liquids".localized, items:getLiquidViewModels()),
            SettingsSection(header:"settings_time_format".localized, items:getTimeFormatViewModels())
        ]

        settingsSections.accept(allSections)
    }
    
    func getDistanceUnitViewModels() -> [SettingsItemViewModel] {
        let distanceUnitMiles = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        distanceUnitMiles.settingType = Settings.SettingType.DISTANCE_UNITS
        distanceUnitMiles.distanceUnitValue = Settings.DistanceUnit.MILES
        distanceUnitMiles.isCurrentValue = (dataService.distanceUnitSetting == Settings.DistanceUnit.MILES)
        
        let distanceUnitKilometers = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        distanceUnitKilometers.settingType = Settings.SettingType.DISTANCE_UNITS
        distanceUnitKilometers.distanceUnitValue = Settings.DistanceUnit.KILOMETERS
        distanceUnitKilometers.isCurrentValue = (dataService.distanceUnitSetting == Settings.DistanceUnit.KILOMETERS)
        
        return [distanceUnitMiles, distanceUnitKilometers]
    }
    
    func getTemperatureViewModels() -> [SettingsItemViewModel] {
        let temperatureFahrenheit = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        temperatureFahrenheit.settingType = Settings.SettingType.TEMPERATURE
        temperatureFahrenheit.temperatureValue = Settings.Temperature.FAHRENHEIT
        temperatureFahrenheit.isCurrentValue = (dataService.temperatureUnitSetting == Settings.Temperature.FAHRENHEIT)
        
        let temperatureCelsius = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        temperatureCelsius.settingType = Settings.SettingType.TEMPERATURE
        temperatureCelsius.temperatureValue = Settings.Temperature.CELSIUS
        temperatureCelsius.isCurrentValue = (dataService.temperatureUnitSetting == Settings.Temperature.CELSIUS)

        return [temperatureFahrenheit, temperatureCelsius]
    }
    
    func getLiquidViewModels() -> [SettingsItemViewModel] {
        let liquidGallons = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        liquidGallons.settingType = Settings.SettingType.LIQUIDS
        liquidGallons.liquidValue = Settings.Liquid.GALLONS
        liquidGallons.isCurrentValue = (dataService.liquidUnitSetting == Settings.Liquid.GALLONS)
        
        let liquidLiters = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        liquidLiters.settingType = Settings.SettingType.LIQUIDS
        liquidLiters.liquidValue = Settings.Liquid.LITERS
        liquidLiters.isCurrentValue = (dataService.liquidUnitSetting == Settings.Liquid.LITERS)
        
        return [liquidGallons, liquidLiters]
    }
    
    func getTimeFormatViewModels() -> [SettingsItemViewModel] {
        let timeFormatAmPm = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        timeFormatAmPm.settingType = Settings.SettingType.TIME_FORMAT
        timeFormatAmPm.timeFormatValue = Settings.TimeFormat.AM_PM
        timeFormatAmPm.isCurrentValue = (dataService.timeFormatSetting == Settings.TimeFormat.AM_PM)
        
        let timeFormatTwentyFour = SwinjectUtil.sharedInstance.container.resolve(SettingsItemViewModel.self)!
        timeFormatTwentyFour.settingType = Settings.SettingType.TIME_FORMAT
        timeFormatTwentyFour.timeFormatValue = Settings.TimeFormat.TWENTY_FOUR_HOURS
        timeFormatTwentyFour.isCurrentValue = (dataService.timeFormatSetting == Settings.TimeFormat.TWENTY_FOUR_HOURS)
        
        return [timeFormatAmPm, timeFormatTwentyFour]
    }
    
    func onItemSelected(settingsItemViewModel: SettingsItemViewModel) {
        guard let settingType = settingsItemViewModel.settingType else { return }
        switch settingType {
        case .DISTANCE_UNITS:
            if let distanceValue = settingsItemViewModel.distanceUnitValue {
                onDistanceUnitSelected(distanceValue: distanceValue)
            }
        case .TEMPERATURE:
            if let temperatureValue = settingsItemViewModel.temperatureValue {
                onTemperatureSelected(temperatureValue: temperatureValue)
            }
        case .LIQUIDS:
            if let liquidValue = settingsItemViewModel.liquidValue {
                onLiquidSelected(liquidValue: liquidValue)
            }
        case .TIME_FORMAT:
            if let timeFormatValue = settingsItemViewModel.timeFormatValue {
                onTimeFormatSelected(timeFormatValue: timeFormatValue)
            }
        }
    }
    
    func onDistanceUnitSelected(distanceValue: Settings.DistanceUnit) {
        dataService.distanceUnitSetting = distanceValue
    }
    
    func onTemperatureSelected(temperatureValue: Settings.Temperature) {
        dataService.temperatureUnitSetting = temperatureValue
    }
    
    func onLiquidSelected(liquidValue: Settings.Liquid) {
        dataService.liquidUnitSetting = liquidValue
    }
    
    func onTimeFormatSelected(timeFormatValue: Settings.TimeFormat ) {
        dataService.timeFormatSetting = timeFormatValue
    }
}
