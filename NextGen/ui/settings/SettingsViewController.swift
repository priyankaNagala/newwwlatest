//
//  SettingsViewController.swift
//  nextgen
//
//  Created by David Crawford on 8/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import UIKit

class SettingsViewController: ZeroUIViewController, UITableViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_SETTINGS }

    @IBOutlet weak var closeButton: ZeroUIBarButtonItem!
    @IBOutlet weak var settingsTable: UITableView!
    @IBOutlet weak var currentLanguageLabel: UILabel!
    
    private let tableItemCellId = SettingsItemTableViewCell.ReuseIdentifier
    private let tableHeaderCellId = SettingsHeaderTableViewCell.ReuseIdentifier
    
    var settingsViewModel: SettingsViewModel?
    var settingsDataSource: SettingsViewModel.SettingsDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
    }
    
    private func bindView() {
        guard let viewModel = settingsViewModel else { return }
        
        settingsTable.delegate = self
        settingsTable.register(UINib(nibName: tableItemCellId, bundle: nil), forCellReuseIdentifier: tableItemCellId)
        settingsTable.register(UINib(nibName: tableHeaderCellId, bundle: nil), forCellReuseIdentifier: tableHeaderCellId)
        
        self.settingsDataSource = viewModel.settingsDataSource()
        
        let languageCode = Locale.current.languageCode ?? ""
        currentLanguageLabel.text = Locale.current.localizedString(forLanguageCode: languageCode)
        
        disposeBag.insertAll(all:
            // Back Button
            closeButton.rx.tap
                .subscribe({ _ in
                    self.dismiss(animated: true, completion: nil)
                }),
                             
            settingsTable.rx.modelSelected(SettingsItemViewModel.self)
                .subscribe({ itemViewModel in
                    if let itemViewModel = itemViewModel.element {
                        viewModel.onItemSelected(settingsItemViewModel: itemViewModel)
                        // Just reload the table
                        viewModel.loadModel()
                    }
                }),
            
            settingsTable.rx.willDisplayCell
                .subscribe({ [weak self] cellEvent in
                    guard let strongSelf = self else { return }
                    if let cell = cellEvent.element?.cell,
                        let indexPath = cellEvent.element?.indexPath,
                        let itemViewModel: SettingsItemViewModel = try? strongSelf.settingsTable.rx.model(at: indexPath) {

                        if itemViewModel.isCurrentValue {
                            cell.setSelected(true, animated: false)
                            strongSelf.settingsTable.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                        }
                    }
                }),
            
            viewModel.settingsSections
                .bind(to: settingsTable.rx.items(dataSource: settingsDataSource!))
        )
        
        viewModel.loadModel()
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerTitle = settingsDataSource?.sectionModels[section].header else { return nil }
        let cell = tableView.dequeueReusableCell(withIdentifier: tableHeaderCellId) as! SettingsHeaderTableViewCell
        cell.configure(withHeaderString: headerTitle)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        // prevent the user from deselecting any of the cells. only the selection of
        // another cell will allow you to do this. works like a radio group
        return nil
    }
}
