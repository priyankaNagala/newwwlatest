//
//  InfoSheetViewModel.swift
//  nextgen
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxSwift

final class InfoSheetViewModel {
    var cellModels = PublishSubject<[TitledValueTableViewCellModeling]>()
    var remoteConnectViewModel: RemoteConnectViewModel? {
        didSet {
            self.subscribeToRemoteConnectViewModel()
        }
    }
    
    private var defaultTitleColor = ColorUtil.uiColor(.whiteText)
    private var defaultValueColor = ColorUtil.uiColor(.yellowText)
    private var unselectedTitleColor = ColorUtil.uiColor(.whiteTextDeselected)
    private var unselectedValueColor = ColorUtil.uiColor(.yellowTextDeselected)
    private var lastTransmit: LastTransmit? = nil
    private var stateOfCharge = ""
    private var plugStatus = ""
    private var chargeActivity = ""
    private var timeToTarget = ""

    let _cellModels = PublishSubject<[TitledValueTableViewCellModeling]>()
    private let disposeBag = DisposeBag()
    
    init() {
    }
    
    func subscribeToRemoteConnectViewModel() {
        self.remoteConnectViewModel?.lastTransmit
            .subscribe(onNext: { (lastTransmit) in
                self.lastTransmit = lastTransmit
                self.stateOfCharge = "\(lastTransmit.stateOfCharge)%"
                self.plugStatus = lastTransmit.plugStatus.localizedString
                self.chargeActivity = lastTransmit.chargeActivity.localizedString
                self.timeToTarget = "\(lastTransmit.chargingTimeLeft)"
                self.updateCellModels()
            })
            .disposed(by: disposeBag)
    }

    private func updateCellModels() {
        let pluggedIn = (lastTransmit?.plugStatus == LastTransmit.PlugStatus.PLUGGED)
        let charging = (lastTransmit?.chargeActivity == LastTransmit.ChargeActivity.CHARGING)
        
        let cellViewModels = [
            TitledValueTableViewCellModel(title: "remote_connect_state_of_charge".localized,
                                          value: self.stateOfCharge,
                                          titleColor: defaultTitleColor,
                                          valueColor: defaultValueColor),
            TitledValueTableViewCellModel(title: "remote_connect_plug_status".localized,
                                          value: self.plugStatus,
                                          titleColor: defaultTitleColor,
                                          valueColor: defaultValueColor),
            TitledValueTableViewCellModel(title: "remote_connect_charge_activity".localized,
                                          value: self.chargeActivity,
                                          titleColor: charging ? defaultTitleColor : unselectedTitleColor,
                                          valueColor: charging ? defaultValueColor : unselectedValueColor),
            TitledValueTableViewCellModel(title: "remote_connect_time_to_target".localized,
                                          value: self.timeToTarget,
                                          titleColor: charging ? defaultTitleColor : unselectedTitleColor,
                                          valueColor: charging ? defaultValueColor : unselectedValueColor)
        ]

        cellModels.onNext(cellViewModels)
    }

}
