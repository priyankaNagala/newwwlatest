//
//  TitledValueTableViewCellModeling.swift
//  nextgen
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//
import UIKit

protocol TitledValueTableViewCellModeling {
    var title: String { get }
    var value: String { get }
    var titleColor: UIColor { get }
    var valueColor: UIColor { get }
}
