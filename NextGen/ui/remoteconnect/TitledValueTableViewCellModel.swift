//
//  TitledValueTableViewCellModel.swift
//  nextgen
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//
import UIKit

final class TitledValueTableViewCellModel : TitledValueTableViewCellModeling {

    var title: String
    var value: String
    var titleColor: UIColor
    var valueColor: UIColor

    init(title: String,
         value: String,
         titleColor: UIColor,
         valueColor: UIColor) {
        self.title = title.localizedUppercase
        self.value = value
        self.titleColor = titleColor
        self.valueColor = valueColor
    }
}
