//
//  RemoteConnectViewController.swift
//  NextGenView
//
//  Created by Scott on 2/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import GoogleMaps
import SwinjectStoryboard
import RxSwift

class RemoteConnectViewController: ZeroUIViewController, InfoSheetDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_REMOTE_CONNECT }

    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var mapContainerView: GMSMapView!
    var remoteConnectViewModel: RemoteConnectViewModel?
    var locationMarker: GMSMarker = GMSMarker()
    var motorcycleName: String?
    var dataService: DataService = DataService.shared
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapContainerView.isMyLocationEnabled = true

        if let unitNumber = dataService.unitNumber, let lastLocation = dataService.getLastLocationByStarcomUnit(unitNumber: unitNumber) {
            self.centerMapOnLocation(location: lastLocation)
        }
        
        self.setupLoadingView()
        self.subscribeToLocationUpdates()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.addBottomSheetView()
        self.remoteConnectViewModel?.getLastCloudLocation()
    }

    
    // MARK: -
    
    func setupLoadingView() {
        self.loadingView.layer.cornerRadius = 8.0
        self.loadingActivityIndicator.startAnimating()
        self.remoteConnectViewModel?.isConnecting.subscribe(onNext: { (connecting) in
            if !connecting {
                self.animateLoading(animate: false)
            }
        }).disposed(by: disposeBag)
    }
    
    func addBottomSheetView() {
        // https://github.com/AhmedElassuty/BottomSheetController
        // 1- Init InfoSheetVC
        let bundle = Bundle(for: InfoSheetViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RemoteConnect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let infoSheetVC = storyboard.instantiateViewController(withIdentifier: "InfoSheetViewController") as! InfoSheetViewController
        infoSheetVC.delegate = self
        infoSheetVC.infoSheetViewModel?.remoteConnectViewModel = self.remoteConnectViewModel

        // 2- Add InfoSheetVC as a child view
        self.addChildViewController(infoSheetVC)
        self.view.addSubview(infoSheetVC.view)
        infoSheetVC.didMove(toParentViewController: self)

        // 3- Adjust InfoSheet frame and initial position.
        let height = view.frame.height
        let width  = view.frame.width
        infoSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }

    func subscribeToLocationUpdates() {
        self.remoteConnectViewModel?.locationCoordinate
            .subscribe(onNext: { (coordinate) in
                self.centerMapOnLocation(location: coordinate)
            }).disposed(by: disposeBag)

        self.remoteConnectViewModel?.motocycleName
            .subscribe(onNext: { (name) in
                self.motorcycleName = name
            }).disposed(by: disposeBag)
     }
    
    // MARK: -
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: Constant.MAP_ZOOM_LEVEL_BUILDINGS)
        self.mapContainerView.camera = camera
        self.mapContainerView.isMyLocationEnabled = true
        
        // Creates a marker in the center of the map.
        self.locationMarker.position = location
        self.locationMarker.icon = UIImage.init(named: "MapPin")
        self.locationMarker.title = self.motorcycleName
        self.locationMarker.map = self.mapContainerView
    }
    
    func animateLoading(animate: Bool) {
        if animate {
            self.loadingView.isHidden = false
            self.loadingActivityIndicator.startAnimating()
        } else {
            self.loadingView.isHidden = true
            self.loadingActivityIndicator.stopAnimating()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - InfoSheetDelegate
    
    func onDirectionButtonPressed(_ sender: Any) {
        // https://www.google.com/maps/dir/?api=1&destination=
        let lon = self.locationMarker.position.longitude
        let lat = self.locationMarker.position.latitude
        let name = self.motorcycleName != nil ? self.motorcycleName?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) : ""
        if let url = URL(string: "https://www.google.com/maps/dir/?api=1&destination=\(lat),\(lon)&origin_place_id=\(name ?? "")") {
            UIApplication.shared.open(url, completionHandler: nil)
        }
    }

}
