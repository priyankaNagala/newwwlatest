//
//  RemoteConnectViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 2/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GoogleMaps

class RemoteConnectViewModel {
    var lastTransmit = PublishSubject<LastTransmit>()
    var motocycleName = PublishRelay<String>()
    var locationInformation = BehaviorSubject<String>(value: "")
    var hasRemoteConnectInformation = BehaviorSubject<Bool>(value: false)
    var isConnecting = PublishSubject<Bool>()
    var locationCoordinate = PublishSubject<CLLocationCoordinate2D>()
    
    var starcomApiService: StarcomApiService
    var dataService: DataService
    var disposeBag = DisposeBag()
    
    init(starcomApiService: StarcomApiService, dataService: DataService) {
        self.starcomApiService = starcomApiService
        self.dataService = dataService
    }
    
    func getLastCloudLocation() {
        guard let starcomuser = dataService.starcomUser, let starcomPassword = dataService.starcomPassword, let unitNumber = dataService.unitNumber else { return }
        isConnecting.onNext(true)
        Starcom.shared.getLastTransmit(starcomApiService: self.starcomApiService, user: starcomuser, password: starcomPassword, unitNumber: unitNumber)
            .subscribe(onNext: { [weak self] (lastTransmit) in
                guard let strongSelf = self else { return }
                // Save last location
                strongSelf.dataService.putLastLocationByStarcomUnit(lastTransmit: lastTransmit)
                
                strongSelf.hasRemoteConnectInformation.onNext(true)
                strongSelf.lastTransmit.onNext(lastTransmit)
                if let lastSeen = lastTransmit.lastSeen, let timeAgo = DateUtil.timeAgo(date: lastSeen) {
                    strongSelf.locationInformation.onNext(timeAgo)
                }

                strongSelf.motocycleName.accept(lastTransmit.name)
                if let loc = lastTransmit.locationCoordinate {
                    strongSelf.locationCoordinate.onNext(loc)
                }
            }, onError: { (error) in
                self.locationInformation.onNext(NSLocalizedString("remote_connect_unable_to_connect", comment: ""))
            }, onCompleted: {
                self.isConnecting.onNext(false)
            })
            .disposed(by: disposeBag)
    }
}
