//
//  InfoSheetViewController.swift
//  nextgen
//
//  Created by Scott on 2/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol InfoSheetDelegate {
    func onDirectionButtonPressed(_ sender: Any)
}

class InfoSheetViewController: BottomSheetViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var motocycleNameLabel: UILabel!
    @IBOutlet weak var locationInformationLabel: UILabel!
    @IBOutlet weak var directionsButton: ZeroUIButton!
    @IBOutlet weak var footerLabel: UILabel!
    
    var infoSheetViewModel: InfoSheetViewModel?
    var delegate: InfoSheetDelegate?
    
    let disposeBag = DisposeBag()

    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerCells()
        bindUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height - 100)
        })
    }

    // MARK: -
    func setupView() {
        autoOpenClose = true
        // Tableview
        tableView.backgroundColor = ColorUtil.color(.BackgroundBlack)
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none

        TableUtil.zeroMargins(tableView)
        
        // Button
        directionsButton.isEnabled = false
        directionsButton.setTitle(NSLocalizedString("remote_connect_directions", comment: ""), for: .normal)
    }

    func registerCells() {
        tableView.register(UINib(nibName: "TitledValueTableViewCell", bundle: nil), forCellReuseIdentifier: TitledValueTableViewCell.ReuseIdentifier)

        infoSheetViewModel?.cellModels.bind(to: tableView.rx.items(cellIdentifier: TitledValueTableViewCell.ReuseIdentifier, cellType: TitledValueTableViewCell.self)) { (row, element, cell) in
                cell.titleLabel.text = element.title
                cell.titleLabel.textColor = element.titleColor
                cell.valueLabel.text = element.value
                cell.valueLabel.textColor = element.valueColor
            }.disposed(by: disposeBag)
    }

    func bindUI() {
        guard let viewModel = self.infoSheetViewModel, let remoteConnectViewModel = viewModel.remoteConnectViewModel else { return }
        locationInformationLabel.text = "remote_connect_connecting".localized
        
        disposeBag.insertAll(all:
            remoteConnectViewModel.motocycleName.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.motocycleNameLabel.text = event.element
            },
                             
            remoteConnectViewModel.locationInformation.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.locationInformationLabel.text = event.element
            },
            
            remoteConnectViewModel.hasRemoteConnectInformation.subscribe { [weak self] event in
            guard let strongSelf = self else { return }
                strongSelf.directionsButton.isEnabled = event.element ?? false
            }
        )
    }
    
    // MARK: - IBAction
    @IBAction func directionButtonPressed(_ sender: Any) {
        delegate?.onDirectionButtonPressed(sender)
    }
}
