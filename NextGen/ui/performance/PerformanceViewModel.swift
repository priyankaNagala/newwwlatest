//
//  PerformanceViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift
import CocoaLumberjack

class PerformanceViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    
    var currentRideModeName = BehaviorRelay<String>(value: "")
    var errorCodes = BehaviorRelay<[ErrorCodeSection]>(value:[])
    var hideErrorCodes = BehaviorRelay<Bool>(value: true)
    var onErrorAlert = PublishRelay<UIAlertController>()

    var numRetries = 0
    var bluetoothDisposeBag = DisposeBag()
    var disposeBag = DisposeBag()

    typealias ErrorCodesDataSource = RxTableViewSectionedReloadDataSource<ErrorCodeSection>
    
    struct ErrorCodeSection: SectionModelType {
        typealias Item = ErrorCodesItemViewModel
        var items: [Item]
        init(items: [Item]) {
            self.items = items
        }
        init(original: ErrorCodeSection, items:[Item]) {
            self = original
            self.items = items
        }
    }
    
    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }

    func errorCodesDataSource() -> ErrorCodesDataSource {
        return ErrorCodesDataSource(configureCell: { (_, tableView, indexPath, item) -> UITableViewCell in
            let cell = tableView.dequeueReusableCell(withIdentifier: SystemMessageTableViewCell.ReuseIdentifier, for: indexPath) as! SystemMessageTableViewCell
            cell.configure(code: item.getCode(), title: item.getTitle(), viewModel: item)
            return cell
        })
    }
    
    func loadModel() {
        if let currentRideMode = dataService.currentMotorcycle?.currentRideMode {
            currentRideModeName.accept(currentRideMode.name)
        }
        readErrorFromBike()
    }
    
    func onViewWillAppear() {
        if let currentRideMode = dataService.currentMotorcycle?.currentRideMode {
            currentRideModeName.accept(currentRideMode.name)
        }
    }
    
    fileprivate func acceptErrorCodes(_ codes: [UInt16]) {
        var viewModels = [ErrorCodesItemViewModel]()
        for code in codes {
            if let viewModel = SwinjectUtil.sharedInstance.container.resolve(ErrorCodesItemViewModel.self) {
                viewModel.setErrorCode(Int(code))
                disposeBag.insertAll(all:
                    viewModel.onErrorCodeAlert.subscribe { [weak self] event in
                        guard let strongSelf = self, let alert = event.element else { return }
                        strongSelf.onErrorAlert.accept(alert)
                    }
                )
                viewModels.append(viewModel)
            }
        }
        errorCodes.accept([ErrorCodeSection(items: viewModels)])
        if codes.count == 0 {
            hideErrorCodes.accept(true)
        } else {
            hideErrorCodes.accept(false)
        }
    }
    
    fileprivate func readErrorFromBike() {
        zeroMotorcycleService.sendSingleZeroPacket(packet: ErrorCodesPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            if let errorPacket = event.element as? ErrorCodesPacket {
                strongSelf.bluetoothDisposeBag = DisposeBag()
                
                strongSelf.acceptErrorCodes(errorPacket.codes)
                DDLogDebug("Read ErrorCodes Response ErrorCodes Packet")
            } else {
                DDLogDebug("Read ErrorCodes Response Uknown Packet")
            }
        }.disposed(by: bluetoothDisposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self, let _ = event.element else { return }
            strongSelf.bluetoothDisposeBag = DisposeBag()
            DDLogDebug("Read ErrorCodes Response Resend Packet")
            guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                strongSelf.numRetries = 0
                return
            }
            strongSelf.numRetries = strongSelf.numRetries + 1
            strongSelf.readErrorFromBike()
        }.disposed(by: bluetoothDisposeBag)
    }
}
