//
//  PerformanceViewController.swift
//  NextGenView
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa

class PerformanceViewController: ZeroTabViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_PERFORMANCE }

    var performanceViewModel: PerformanceViewModel?

    @IBOutlet weak var systemMessagesTableView: UITableView!
    @IBOutlet weak var currentRideModeName: ZeroUpperCaseUILabel!
    @IBOutlet weak var fullyOperationalLabel: ZeroUpperCaseUILabel!
    
    var errorCodesDataSource: PerformanceViewModel.ErrorCodesDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let viewModel = performanceViewModel else { return }
        viewModel.onViewWillAppear()
    }
    
    func bindViews() {
        guard let viewModel = performanceViewModel else { return }
    
        systemMessagesTableView.register(UINib(nibName: "SystemMessageTableViewCell", bundle: nil), forCellReuseIdentifier: SystemMessageTableViewCell.ReuseIdentifier)
        errorCodesDataSource = viewModel.errorCodesDataSource()
        
        disposeBag.insertAll(all:
            viewModel.currentRideModeName.bind(to: currentRideModeName.rx.text),
                             
            viewModel.errorCodes.bind(to: systemMessagesTableView.rx.items(dataSource: errorCodesDataSource!)),
            
            viewModel.onErrorAlert.subscribe { [weak self] event in
                guard let strongSelf = self, let alert = event.element else { return }
                strongSelf.present(alert, animated: true)
            },
            
            viewModel.hideErrorCodes.subscribe { [weak self] event in
                guard let strongSelf = self, let hide = event.element else { return }
                if hide {
                    strongSelf.systemMessagesTableView.isHidden = true
                } else {
                    strongSelf.systemMessagesTableView.isHidden = false
                }
            }
        )
        viewModel.loadModel()
    }
    
    // MARK: - IBAction
    @IBAction func onManageRideModesClick(_ sender: Any) {
        UILauncher.launchManageRideModes(self)
    }
    
    @IBAction func onViewDashboardClick(_ sender: Any) {
        UILauncher.launchDashboardSettings(self)
    }
    @IBAction func onAccessLogsClick(_ sender: Any) {
        UILauncher.launchPullLogs(self)
    }
    // MARK: - IBAction
    @IBAction func unwindFromCancelLogs(segue: UIStoryboardSegue) { /* Unwind segue, do nothing */ }
}
