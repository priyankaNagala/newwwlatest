//
//  ErrorCodesItemViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 1/22/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import RxCocoa

class ErrorCodesItemViewModel {
    
    var errorCode: Int = 0
    var onErrorCodeAlert = PublishRelay<UIAlertController>()

    init() {
        
    }
    
    func setErrorCode(_ errorCode: Int) {
        self.errorCode = errorCode
    }
    
    func getCode() -> String {
        return "fault_code".localized + " \(errorCode)"
    }
    
    func getTitle() -> String {
        let title_key = "fault_code_title_\(errorCode)"
        return title_key.localized
    }
    
    func onTap() {
        let desc_key = "fault_code_description_\(errorCode)"
        
        let alert = UIAlertController(title: getCode(), message: desc_key.localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_ok".localized, style: .cancel, handler: nil))
                
        onErrorCodeAlert.accept(alert)
    }
}
