//
//  SystemMessageTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 1/22/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SystemMessageTableViewCell: UITableViewCell {
    static let ReuseIdentifier = "ErrorCodeTableViewCell"

    @IBOutlet weak var faultCodeLabel: UILabel!
    @IBOutlet weak var errorTitleLabel: ZeroUpperCaseUILabel!
    
    var disposeBag = DisposeBag()
    var errorDescription = ""
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        faultCodeLabel.text = ""
        errorTitleLabel.text = ""
    }
    
    func configure(code: String, title: String, viewModel: ErrorCodesItemViewModel) {
        faultCodeLabel.text = code
        errorTitleLabel.text = title

        disposeBag.insertAll(all:
            self.rx.tapGesture().subscribe { event in
                guard let tapGesture = event.element, tapGesture.state == .recognized else { return }
                viewModel.onTap()
            }
        )
    }
        
    func configure(_ viewModel: ErrorCodesItemViewModel) {
        let desc_key = "performance_fault_code_description_\(viewModel.errorCode)"
        errorDescription = desc_key.localized
        
        disposeBag.insertAll(all:
            self.rx.tapGesture().subscribe { [weak self] event in
                guard let strongSelf = self, let tapGesture = event.element, tapGesture.state == .recognized else { return }
                let alert = UIAlertController(title: nil, message: strongSelf.errorDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "global_ok".localized, style: .cancel, handler: nil))
                
                viewModel.onTap()
            }
        )
    }
}
