//
//  HelpViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 2/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HelpViewModel {
    
    var title = BehaviorRelay<String>(value: "")
    var description = BehaviorRelay<String>(value: "")
    var image = BehaviorRelay<UIImage?>(value: nil)
    
    init(title: String, description: String, image: UIImage?) {
        self.title.accept(title)
        self.description.accept(description)
        self.image.accept(image)
    }
}
