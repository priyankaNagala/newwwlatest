//
//  AppHelpPageViewModel.swift
//  nextgen
//
//  Created by Scott Wang on 2/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class AppHelpViewModel {
    
    enum HelpType {
        case
        help,
        tour
    }
    
    var title: Observable<String> { return _title.asObservable() }
    var type: HelpType {
        didSet {
            self.updateTitle()
        }
    }

    private var helpViewModels: [HelpViewModel] = [HelpViewModel]()
    private var tourViewModels: [HelpViewModel] = [HelpViewModel]()

    private var _title = BehaviorSubject<String>(value: "")
    
    init() {
        self.type = .help
        
        self.helpViewModels.append(HelpViewModel.init(
            title: "help_page_1_title".localized,
            description: "help_page_1_description".localized,
            image: UIImage.init(named: "Help1")))
        
        self.helpViewModels.append(HelpViewModel.init(
            title: "help_page_2_title".localized,
            description: "help_page_2_description".localized,
            image: UIImage.init(named: "Help2")))

        self.helpViewModels.append(HelpViewModel.init(
            title: "help_page_3_title".localized,
            description: "help_page_3_description".localized,
            image: UIImage.init(named: "Help3")))

        self.tourViewModels.append(HelpViewModel.init(
            title: "tour_page_1_title".localized,
            description: "tour_page_1_description".localized,
            image: UIImage.init(named: "Tour1")))
        
        self.tourViewModels.append(HelpViewModel.init(
            title: "tour_page_2_title".localized,
            description: "tour_page_2_description".localized,
            image: UIImage.init(named: "Tour2")))
        
        self.tourViewModels.append(HelpViewModel.init(
            title: "tour_page_3_title".localized,
            description: "tour_page_3_description".localized,
            image: UIImage.init(named: "Tour3")))

        self.updateTitle()
    }
    
    func updateTitle() {
        switch type {
        case .help:
            self._title.onNext("help_pairing_guide".localized)
        case .tour:
            self._title.onNext("tour".localized)
        }
    }
    
    func getHelpViewModels() -> [HelpViewModel] {
        switch self.type {
        case .help:
            return helpViewModels
        case .tour:
            return tourViewModels
        }
    }
    
}
