//
//  HelpViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HelpViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var helpViewModel: HelpViewModel?
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bindUIElements()
    }

    func bindUIElements() {
        guard let viewModel = helpViewModel else { return }
        viewModel.title
            .bind(to: self.titleLabel.rx.text)
            .disposed(by: disposeBag)
        viewModel.description
            .bind(to: self.descriptionLabel.rx.text)
            .disposed(by: disposeBag)
        viewModel.image
            .bind(to: self.imageView.rx.image)
            .disposed(by: disposeBag)
    }
}
