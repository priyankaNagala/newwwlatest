//
//  AppHelpViewController.swift
//  nextgen
//
//  Created by Scott Wang on 2/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard
import RxSwift

class AppHelpViewController: ZeroUIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    override var screenName: String {
        return self.appHelpViewModel?.type == AppHelpViewModel.HelpType.tour ? Constant.ANALYTICS_SCREEN_APP_TOUR : Constant.ANALYTICS_SCREEN_APP_HELP
    }

    @IBOutlet weak var pageControl: UIPageControl!

    var appHelpViewModel: AppHelpViewModel?
    var orderedViewControllers: [HelpViewController] = [HelpViewController]()
    var pendingChangePageTo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupPageVCs()
        
        self.appHelpViewModel?.title.subscribe(onNext: { (title) in
            self.title = title
        }).disposed(by: disposeBag)
    }

    func setupPageVCs() {
        let bundle = Bundle(for: AppHelpViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "AppHelp", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        
        if self.appHelpViewModel != nil {
            for helpViewModel in self.appHelpViewModel!.getHelpViewModels() {
                let helpViewController = storyboard
                    .instantiateViewController(withIdentifier: "HelpViewController")
                    as! HelpViewController
                helpViewController.helpViewModel = helpViewModel
                self.orderedViewControllers.append(helpViewController)
            }
        }

        let pageController = childViewControllers.first as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        pageController.view.backgroundColor = ColorUtil.color(.BackgroundBlack)
        
        if let firstViewController = orderedViewControllers.first {
            pageController.setViewControllers([firstViewController],
                                              direction: .forward,
                                              animated: true,
                                              completion: nil)
        }
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! HelpViewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! HelpViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    // MARK: - UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let viewControllerIndex = orderedViewControllers.index(of: pendingViewControllers[0] as! HelpViewController) else { return }
        pendingChangePageTo = viewControllerIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            pageControl.currentPage = pendingChangePageTo
        }
    }
    
    // IBAction
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
