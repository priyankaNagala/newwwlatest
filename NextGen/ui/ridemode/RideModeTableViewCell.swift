//
//  RideModeCell.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxSwift

class RideModeTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "RideModeTableViewCell"

    @IBOutlet weak var rideModeName: ZeroUpperCaseUILabel!
    @IBOutlet weak var overflow: UIButton!
    @IBOutlet weak var currentRideModeIconImageView: UIImageView!
    @IBOutlet weak var rideModeNameLabelLeadingConstraint: NSLayoutConstraint!
    
    var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    func configure(withViewModel viewModel: ManageRideModesItemViewModel) {
        if let rideMode = viewModel.rideMode {
            rideModeName.text = rideMode.name
        }
        
        currentRideModeIconImageView.isHidden = !viewModel.showCurrentIcon
        if viewModel.showCurrentIcon {
            rideModeNameLabelLeadingConstraint.constant = 70
        } else {
            rideModeNameLabelLeadingConstraint.constant = 15
        }
        
        disposeBag.insertAll(all:
            
            overflow.rx.tap.subscribe({ _ in
                viewModel.onOverflowTap()
            }),
            
            viewModel.isCurrentRideMode.subscribe { [weak self] event in
                guard let strongSelf = self, let current = event.element else { return }
                // Hide icon if not current or not showing
                strongSelf.currentRideModeIconImageView.isHidden = !current || !viewModel.showCurrentIcon
            }
        )
    }
}
