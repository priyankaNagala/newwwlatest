//
//  ManageRideModesViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/18/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import UIKit
import RxCocoa
import RxDataSources

class ManageRideModesViewController: ZeroUIViewController, UITableViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_MANAGE_RIDE_MODES }

    var dataService: DataService?
    fileprivate var syncView: SyncActivityOverlayView = SyncActivityOverlayView()

    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    @IBOutlet weak var rideModeTableView: UITableView!
    @IBOutlet weak var createNewRideModeButton: ZeroSecondaryUIButton!
    
    var manageRideModesViewModel: ManageRideModesViewModel?
    var rideModeDataSource: ManageRideModesViewModel.RideModeDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        syncView.frame = view.bounds
    }
    
    func bindView() {
        guard let viewModel = manageRideModesViewModel else { return }

        let cellId = RideModeTableViewCell.ReuseIdentifier
        rideModeTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        rideModeTableView.register(UINib(nibName: RideModeSectionTableViewCell.ReuseIdentifier, bundle: nil), forCellReuseIdentifier: RideModeSectionTableViewCell.ReuseIdentifier)
        self.rideModeDataSource = viewModel.rideModesDataSource()
        
        rideModeTableView.delegate = self
        
        disposeBag.insertAll(all:
            
            backButton.rx.tap.subscribe({ _ in
                self.navigationController?.popViewController(animated: true)
            }),

            viewModel.allRideModes
                .bind(to: rideModeTableView.rx.items(dataSource: rideModeDataSource!)),
            
            rideModeTableView.rx.modelSelected(ManageRideModesItemViewModel.self)
                .subscribe({ [weak self] itemViewModel in
                    guard let strongSelf = self else { return }
                    if let rideMode = itemViewModel.element?.rideMode {
                        UILauncher.launchRideMode(strongSelf, rideMode: rideMode)
                            .subscribe({ _ in
                                viewModel.loadModel()
                            })
                            .disposed(by: strongSelf.disposeBag)
                    }                    
                }),
                        
            viewModel.onRideModeOverflowAlert.subscribe({ [weak self] alert in
                guard let strongSelf = self else { return }
                if let alert = alert.element! {
                    strongSelf.present(alert, animated: true)
                }
            }),
            
            // Selecting Edit from the overflow menu
            viewModel.onRideModeView.subscribe({ [weak self] rideMode in
                guard let strongSelf = self else { return }
                UILauncher.launchRideMode(strongSelf, rideMode: rideMode.element!)
                    .subscribe({ _ in
                        viewModel.loadModel()
                    })
                    .disposed(by: strongSelf.disposeBag)
            }),
            
            viewModel.canCreateRideModes.bind(to: createNewRideModeButton.rx.isEnabled),
            
            createNewRideModeButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                
                if !viewModel.canAddNewCustomRideMode() {
                    let error = String(format: "ride_mode_too_many_ride_modes".localized, Constant.RIDE_MODES_MAX_USER_MODES)
                    strongSelf.showErrorDialog(message: error)
                    return
                }
                strongSelf.showCreateDialog(errorMessage: nil)
            }),
            
            viewModel.onUpdateStart.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showSyncActivity(true)
                strongSelf.syncView.showSyncUnsuccessful(false)
                strongSelf.view.addSubview(strongSelf.syncView)
            },
            
            viewModel.onUpdateComplete.subscribe { [weak self] event in
                guard let strongSelf = self, let complete = event.element else { return }
                if complete {
                    strongSelf.syncView.removeFromSuperview()
                } else {
                    strongSelf.syncView.showSyncUnsuccessful(true)
                }
            },
            
            viewModel.onQueueComplete.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showQueued(true)
            }
        )
        
        viewModel.loadModel()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerTitle = rideModeDataSource?.sectionModels[section].header else { return nil }
        let cell = rideModeTableView.dequeueReusableCell(withIdentifier: RideModeSectionTableViewCell.ReuseIdentifier) as! RideModeSectionTableViewCell
        cell.configure(headerTitle)
        return cell
    }
    
    private func showManageRideModesDialog() {
        guard let viewModel = manageRideModesViewModel else { return }
        let alert = UIAlertController(
            title: "ride_mode_active_modes_full_title".localized,
            message: "ride_mode_active_modes_full_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "ride_mode_active_modes_full_ok".localized, style: .default, handler: { action in
            UILauncher.launchDeactivateRideMode(self)
                .subscribe({ _ in
                    viewModel.loadModel()
                })
                .disposed(by: self.disposeBag)
        }))
        self.present(alert, animated: true)
    }
    
    private func showCreateDialog(errorMessage: String?) {
        guard let viewModel = manageRideModesViewModel else { return }
        // Check to see if there are already too many ride modes
        if !viewModel.canAddNewCustomRideMode() {
            let message = String(format: "ride_mode_too_many_ride_modes".localized, Constant.RIDE_MODES_MAX_USER_MODES)
            self.showErrorDialog(message: message)
            return
        }
        
        let alert = UIAlertController(
            title: "ride_mode_create".localized,
            message: "ride_mode_enter_name".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        
        let createAction = UIAlertAction(title: "ride_mode_create_ok".localized, style: .default, handler: { action in
            guard let rideModeName = alert.textFields?.first?.text else {
                self.showCreateDialog(errorMessage: "ride_mode_enter_name_hint".localized)
                return
            }
            
            let rideMode = RideMode(type: RideMode.RideModeType.CUSTOM, name: rideModeName)
            if let currentMotorcycle = self.dataService?.currentMotorcycle {
                var myRideModes = currentMotorcycle.myRideModes
                myRideModes.append(rideMode)
                self.dataService?.currentMotorcycle?.myRideModes = myRideModes
            }
            
            viewModel.loadModel()
            
            UILauncher.launchRideMode(self, rideMode: rideMode)
                .bind(onNext: { _ in
                    viewModel.loadModel()
                })
                .disposed(by: self.disposeBag)
        })
        createAction.isEnabled = false // Keep this disabled until input is valid
        alert.addAction(createAction)
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "ride_mode_enter_name_hint".localized
            textField.maxLength = Constant.RIDE_MODE_NAME_MAX_CHARACTERS
            textField.keyboardType = UIKeyboardType.asciiCapable
            
            NotificationCenter.default
                .rx.notification(NSNotification.Name.UITextFieldTextDidChange)
                .subscribe({ _ in
                    textField.text = textField.text?.stringAscii()
                    
                    let myRideModesUnique = (self.dataService?.currentMotorcycle?.myRideModes
                        .filter { $0.name.lowercased() == textField.text!.lowercased() } ?? [])
                        .count == 0

                    let activeRideModesUnique = (self.dataService?.currentMotorcycle?.activeRideModes
                        .filter { $0.name.lowercased() == textField.text!.lowercased() } ?? [])
                        .count == 0

                    createAction.isEnabled = textField.text!.count > 0 && activeRideModesUnique && myRideModesUnique
                })
                .disposed(by: self.disposeBag)
        })
        
        self.present(alert, animated: true)
    }
    
    func showErrorDialog(message: String) {
        let errorDialog = UIAlertController(
            title: "global_error".localized,
            message: message,
            preferredStyle: .alert)
        errorDialog.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: nil))
        self.present(errorDialog, animated: true)
    }

    // UITableViewDelegate
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    // Unwind Segue
    
    @IBAction func unwindFromActiveRideModeSaved(segue: UIStoryboardSegue) {
        // If the ride mode saved was the custom mode in the active list we need to update MBB
        manageRideModesViewModel?.sendRideModesToBike()
    }
}
