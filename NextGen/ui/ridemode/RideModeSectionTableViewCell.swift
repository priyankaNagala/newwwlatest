//
//  RideModeSectionTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 1/15/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit
import RxSwift

class RideModeSectionTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "RideModeSectionTableViewCell"
    
    @IBOutlet weak var sectionLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func configure(_ name: String) {
        sectionLabel.text = name
    }
}
