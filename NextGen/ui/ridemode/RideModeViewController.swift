//
//  RideModeViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import ActiveLabel
import CocoaLumberjack
import Foundation
import UIKit
import RxGesture

class RideModeViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_RIDE_MODE }
    
    var rideModeViewModel: RideModeViewModel?

    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    
    @IBOutlet weak var rideModeName: ZeroUpperCaseUILabel!
    
    @IBOutlet weak var maxSpeedSlider: UISlider! {
        didSet {
            maxSpeedSlider.minimumValue = Float(RideMode.DEFAULT_MAX_SPEED_MIN)
            maxSpeedSlider.maximumValue = Float(RideMode.DEFAULT_MAX_SPEED_MAX)
        }
    }
    
    @IBOutlet weak var isActive: UIImageView!
    
    @IBOutlet weak var powerSlider: UISlider!
    @IBOutlet weak var maxTorqueSlider: UISlider!
    @IBOutlet weak var neutralRegenSlider: UISlider!
    @IBOutlet weak var brakeRegenSlider: UISlider!
    
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var maxTorqueLabel: UILabel!
    @IBOutlet weak var neutralRegenLabel: UILabel!
    @IBOutlet weak var brakeRegenLabel: UILabel!
    
    @IBOutlet weak var tractionControlSwitch: UISwitch!
    @IBOutlet weak var tractionControlSegmentedControl: UISegmentedControl! {
        didSet {
            tractionControlSegmentedControl.textColor(color: UIColor.white)
            tractionControlSegmentedControl.setTitle("ride_mode_traction_control_street".localized, forSegmentAt: 0)
            tractionControlSegmentedControl.setTitle("ride_mode_traction_control_rain".localized, forSegmentAt: 1)
            tractionControlSegmentedControl.setTitle("ride_mode_traction_control_sport".localized, forSegmentAt: 2)
        }
    }
    
    @IBOutlet weak var cloneRideButton: ZeroTertiaryUIButton!
    
    @IBOutlet weak var dashboardThemeSegmentedControl: ZeroUISegmentedControl! {
        didSet {
            dashboardThemeSegmentedControl.setTitle("ride_mode_dark_green".localized, forSegmentAt: 0)
            dashboardThemeSegmentedControl.setTitle("ride_mode_dark_blue".localized, forSegmentAt: 1)
            dashboardThemeSegmentedControl.setTitle("ride_mode_light_orange".localized, forSegmentAt: 2)
            dashboardThemeSegmentedControl.setTitle("ride_mode_light_blue".localized, forSegmentAt: 3)
            dashboardThemeSegmentedControl.setTitle("ride_mode_dark_orange".localized, forSegmentAt: 4)
        }
    }
    
    @IBOutlet weak var saveButton: ZeroUIButton!
    @IBOutlet weak var deleteButton: ZeroTertiaryUIButton!
    @IBOutlet weak var cloneButton: ZeroTertiaryUIButton!
    @IBOutlet weak var dashboardThemeWarning: ActiveLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = rideModeViewModel else { return }
        
        let labelCustomType = ActiveType.custom(pattern: "ride_mode_theme_description_dashboard_settings".localized)
        dashboardThemeWarning.customize { label in
            label.enabledTypes = [labelCustomType]
            label.customColor[labelCustomType] = ColorUtil.color(.Yellow)
            label.handleCustomTap(for: labelCustomType) { element in
                UILauncher.launchDashboardSettings(self)
            }
        }
        
        disposeBag.insertAll(all:
            
            // Ride Mode Name
            viewModel.rideModeName.bind(to: rideModeName.rx.text),

            // Max Speed
            viewModel.maxSpeed.subscribe({ [weak self] maxSpeed in
                guard let strongSelf = self else { return }
                strongSelf.maxSpeedSlider.value = maxSpeed.element!
                let speedInt = Int(maxSpeed.element!)
                strongSelf.maxSpeedLabel.text = "\(speedInt)mph"
            }),
            
            maxSpeedSlider.rx.value.changed.subscribe({slider in
                viewModel.onMaxSpeedChanged(value: slider.element!)
            }),
            
            // Power
            viewModel.power.subscribe({ [weak self] power in
                guard let strongSelf = self else { return }
                strongSelf.powerSlider.value = power.element!
                let powerInt = Int(power.element!)
                strongSelf.powerLabel.text = "\(powerInt)%"
            }),
            
            viewModel.power.bind(to: powerSlider.rx.value),
            
            powerSlider.rx.value.changed.subscribe({slider in
                viewModel.onPowerChanged(value: slider.element!)
            }),
            
            // Torque
            viewModel.maxTorque.subscribe({ [weak self] maxTorque in
                guard let strongSelf = self else { return }
                strongSelf.maxTorqueSlider.value = maxTorque.element!
                let torqueInt = Int(maxTorque.element!)
                strongSelf.maxTorqueLabel.text = "\(torqueInt)%"
            }),
            
            viewModel.maxTorque.bind(to: maxTorqueSlider.rx.value),
            
            maxTorqueSlider.rx.value.changed.subscribe({slider in
                viewModel.onMaxTorqueChanged(value: slider.element!)
            }),
            
            // Neutral Regen
            viewModel.neutralRegeneration.subscribe({ [weak self] neutralRegen in
                guard let strongSelf = self else { return }
                strongSelf.neutralRegenSlider.value = neutralRegen.element!
                let regenInt = Int(neutralRegen.element!)
                strongSelf.neutralRegenLabel.text = "\(regenInt)%"
            }),
            
            neutralRegenSlider.rx.value.changed.subscribe({slider in
                viewModel.onNeutralRegenerationChanged(value: slider.element!)
            }),
            
            // Brake Regen
            viewModel.brakeRegeneration.subscribe({ [weak self] brakeRegen in
                guard let strongSelf = self else { return }
                strongSelf.brakeRegenSlider.value = brakeRegen.element!
                let regenInt = Int(brakeRegen.element!)
                strongSelf.brakeRegenLabel.text = "\(regenInt)%"
            }),
            
            brakeRegenSlider.rx.value.changed.subscribe({slider in
                viewModel.onBrakeRegenerationChanged(value: slider.element!)
            }),
            
            // Traction Control
            tractionControlSwitch.rx.isOn.changed.subscribe({ theSwitch in
                viewModel.onTractionControlChanged(isOn: theSwitch.element!)
            }),
            
            viewModel.tractionControlEnabled.bind(to: tractionControlSegmentedControl.rx.isEnabled),
            
            viewModel.tractionControlEnabled.subscribe({ [weak self] isOn in
                guard let strongSelf = self else { return }
                if let isOn = isOn.element {
                    strongSelf.tractionControlSwitch.isOn = isOn
                    if isOn {
                        strongSelf.tractionControlSegmentedControl.tintColor =
                            ColorUtil.color(ColorUtil.AppColors.Orange)
                    } else {
                        strongSelf.tractionControlSegmentedControl.tintColor =
                            ColorUtil.uiColor(.buttonDisabledBackground)
                    }
                }
            }),
            
            tractionControlSegmentedControl.rx.selectedSegmentIndex.changed
                .subscribe({ tractionControlType in
                    viewModel.onTractionControlTypeChanged(idx: tractionControlType.element!)
                }),
            
            viewModel.tractionControlType.bind(to: tractionControlSegmentedControl.rx.selectedSegmentIndex),
            
            // Dashboard Theme
            dashboardThemeSegmentedControl.rx.selectedSegmentIndex.changed
                .subscribe({ tractionControlType in
                    viewModel.onDashboardThemeChanged(idx: tractionControlType.element!)
                }),
            
            viewModel.dashboardTheme.bind(to: dashboardThemeSegmentedControl.rx.selectedSegmentIndex),
            
            // Back Button
            backButton.rx.tap.subscribe({ _ in
                self.navigationController?.popViewController(animated: true)
            }),
            
            // isEditable
            viewModel.isEditable.subscribe({[weak self] isEditable in
                guard let strongSelf = self else { return }
                let editable = isEditable.element!
                let enabledColor = ColorUtil.color(ColorUtil.AppColors.Orange)
                let enabledThemeColor = ColorUtil.color(ColorUtil.AppColors.Yellow)
                let disabledColor = ColorUtil.color(ColorUtil.AppColors.Gray)
                
                strongSelf.maxSpeedSlider.isEnabled = editable
                strongSelf.maxSpeedSlider.tintColor = editable ? enabledColor : disabledColor
                strongSelf.powerSlider.isEnabled = editable
                strongSelf.powerSlider.tintColor = editable ? enabledColor : disabledColor
                strongSelf.maxTorqueSlider.isEnabled = editable
                strongSelf.maxTorqueSlider.tintColor = editable ? enabledColor : disabledColor
                strongSelf.neutralRegenSlider.isEnabled = editable
                strongSelf.neutralRegenSlider.tintColor = editable ? enabledColor : disabledColor
                strongSelf.brakeRegenSlider.isEnabled = editable
                strongSelf.brakeRegenSlider.tintColor = editable ? enabledColor : disabledColor
            
                strongSelf.tractionControlSwitch.isEnabled = editable
                strongSelf.tractionControlSegmentedControl.isEnabled = editable
                strongSelf.dashboardThemeSegmentedControl.isEnabled = editable
                strongSelf.dashboardThemeSegmentedControl.tintColor = editable ? enabledThemeColor : disabledColor
                
                if (!editable) {
                    strongSelf.tractionControlSwitch.onTintColor = disabledColor
                    strongSelf.tractionControlSegmentedControl.tintColor = disabledColor
                }
            
                strongSelf.cloneRideButton.isHidden = !editable
                strongSelf.saveButton.isHidden = !editable
                strongSelf.deleteButton.isHidden = !editable || (editable && viewModel.isActiveRideMode())
            }),
            
            // isActive
            /*
            viewModel.isActive.subscribe({ isRideModeActive in
                if let isRideModeActive = isRideModeActive.element {
                    self.isActive?.image = isRideModeActive ?
                        UIImage(named: "RideModeBtnActive") : UIImage(named: "RideModeBtnInactive")
                }
            }),
            
            
            isActive.rx.tapGesture()
                .when(.recognized)
                .subscribe({ _ in
                    if (viewModel.isRideModeActive()) {
                        if (viewModel.isCurrentRideMode()) {
                            self.showErrorDialog(message: "ride_mode_deactivate_cant_disable".localized)
                            return
                        }
                        self.showDeactivateDialog()
                    } else {
                        if (viewModel.canActivateNewMode()) {
                            viewModel.onActiveIconTap()
                        } else {
                            // Go to manage
                            self.showManageRideModesDialog()
                        }
                    }
                }),
            */
            
            // Save Button
            saveButton.rx.tap.subscribe({ _ in
                viewModel.onSaveTap()
            }),
            
            viewModel.onRideModeSaved.subscribe({ _ in
                self.navigationController?.popViewController(animated: true)
            }),
            
            viewModel.onActiveRideModeSaved.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "unwindFromActiveRideModeSaved", sender: self)
            },
            
            // Delete Button
            deleteButton.rx.tap.subscribe({ _ in
                let alert = UIAlertController(
                    title: "ride_mode_delete".localized,
                    message: "ride_mode_delete_description".localized,
                    preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "ride_mode_delete_ok".localized, style: .default, handler: { action in
                    viewModel.onDeleteTap()
                }))
                self.present(alert, animated: true)
            }),
            
            viewModel.onRideModeDeleted.subscribe({ _ in
                self.navigationController?.popViewController(animated: true)
            }),
            
            // Clone
            cloneButton.rx.tap.subscribe({ _ in
                self.showCloneDialog(errorMessage: nil)
            })
        )
        
        if let rideMode = viewModel.rideMode {
            viewModel.loadModel(rideMode: rideMode)
        }
    }
    
    private func showCloneDialog(errorMessage: String?) {
        guard let viewModel = rideModeViewModel else { return }
        // Check to see if there are already too many ride modes
        if !viewModel.canAddNewCustomRideMode() {
            let message = String(format: "ride_mode_too_many_ride_modes".localized, Constant.RIDE_MODES_MAX_USER_MODES)
            self.showErrorDialog(message: message)
            return
        }
        
        let alert = UIAlertController(
            title: "ride_mode_clone".localized,
            message: "ride_mode_enter_name".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        
        let createAction = UIAlertAction(title: "ride_mode_clone_ok".localized, style: .default, handler: { action in
            guard let rideModeName = alert.textFields?.first?.text else {
                self.showCloneDialog(errorMessage: "ride_mode_enter_name_hint".localized)
                return
            }
            
            viewModel.onRideModeCloned.subscribe({ rideMode in
                if let newRideMode = rideMode.element {
                    _ = UILauncher.launchRideMode(self, rideMode: newRideMode)
                }
            }).disposed(by: self.disposeBag)
            
            viewModel.onRideModeClone(newRideModeName: rideModeName)
        })
        createAction.isEnabled = false // Keep this disabled until input is valid
        alert.addAction(createAction)
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "ride_mode_enter_name_hint".localized
            textField.maxLength = Constant.RIDE_MODE_NAME_MAX_CHARACTERS
            
            NotificationCenter.default
                .rx.notification(NSNotification.Name.UITextFieldTextDidChange)
                .subscribe({ _ in
                    let isUnique = viewModel.isUniqueRideModeName(name: textField.text!)
                    createAction.isEnabled = textField.text!.count > 0 && isUnique
                })
                .disposed(by: self.disposeBag)
        })
        
        self.present(alert, animated: true)
    }
    
    private func showManageRideModesDialog() {
        let alert = UIAlertController(
            title: "ride_mode_active_modes_full_title".localized,
            message: "ride_mode_active_modes_full_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "ride_mode_active_modes_full_ok".localized, style: .default, handler: { action in
            _ = UILauncher.launchDeactivateRideMode(self)
        }))
        self.present(alert, animated: true)
    }
    
    private func showErrorDialog(message: String) {
        let errorDialog = UIAlertController(
            title: "global_error".localized,
            message: message,
            preferredStyle: .alert)
        errorDialog.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: nil))
        self.present(errorDialog, animated: true)
    }
    
    private func showDeactivateDialog() {
        guard let viewModel = rideModeViewModel else { return }

        let alert = UIAlertController(
            title: "ride_mode_deactivate_title".localized,
            message: "ride_mode_deactivate_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "ride_mode_deactivate".localized, style: .default, handler: { action in
            viewModel.onActiveIconTap()
        }))
        self.present(alert, animated: true)
    }
    
}
