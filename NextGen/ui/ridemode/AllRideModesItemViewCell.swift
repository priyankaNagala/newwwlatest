//
//  AllRideModesItemViewCell.swift
//  nextgen
//
//  Created by David Crawford on 7/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxGesture

class AllRideModesItemViewCell : ZeroUITableViewCell {
    static let ReuseIdentifier = "AllRideModesItemViewCell"
    
    @IBOutlet weak var rideModeName: ZeroUpperCaseUILabel!
    @IBOutlet weak var isActiveDecorator: UIImageView!
    @IBOutlet weak var disclosure: UIImageView!

    var disposeBag = DisposeBag()

    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
    
    func configure(withViewModel viewModel: AllRideModesItemViewModel, isDeactivate: Bool = false) {
        if let rideMode = viewModel.rideMode {
            rideModeName.text = rideMode.name
            isActiveDecorator.isHidden = !viewModel.isActive
        }
        if (viewModel.activate) {
            disclosure.isHidden = true
            if (viewModel.isActive) {
                rideModeName.textColor = ColorUtil.color(ColorUtil.AppColors.Gray)
                isActiveDecorator.image = UIImage(named: "RideModeTableDecoratorInactive")
            }
        }
        if (isDeactivate) {
            disclosure.isHidden = true
        }
        
        disposeBag.insertAll(all:
            viewModel.onDeactivate.subscribe({ [weak self] isActive in
                guard let strongSelf = self else { return }
                guard let isActive = isActive.element else { return }
                
                if (isDeactivate) {
                    if (isActive) {
                        strongSelf.rideModeName.textColor = ColorUtil.color(ColorUtil.AppColors.White)
                        strongSelf.isActiveDecorator.image = UIImage(named: "RideModeTableDecoratorActive")
                        return
                    }
                    strongSelf.rideModeName.textColor = ColorUtil.color(ColorUtil.AppColors.Gray)
                    strongSelf.isActiveDecorator.image = UIImage(named: "RideModeTableDecoratorInactive")
                } else {
                    strongSelf.isActiveDecorator.isHidden = !isActive
                }
            }),
                             
            isActiveDecorator.rx
                .tapGesture()
                .when(.recognized)
                .subscribe({ _ in
                    viewModel.deactivateWithAlert()
                })
        )
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if (highlighted) {
            contentView.backgroundColor = ColorUtil.color(ColorUtil.AppColors.DarkGray)
        } else {
            contentView.backgroundColor = UIColor.clear
        }
    }
}
