//
//  ManageRideModesItemViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxCocoa

class ManageRideModesItemViewModel  {
    var dataService: DataService
    var rideMode: RideMode?
    var onRideModesEdit = PublishRelay<[RideMode]>()
    var onRideModeView = PublishRelay<RideMode>()
    //var onRideModeCurrent = PublishRelay<RideMode>()
    var onRideModeActive = PublishRelay<RideMode>()
    var onRideModeOverflowAlert = PublishRelay<UIAlertController?>()
    var isCurrentRideMode = BehaviorRelay<Bool>(value: false)
    var canEdit = false
    //var canMakeCurrent = false
    var canMakeActive = false
    var showCurrentIcon = false

    // Only used for section header types
    var sectionTitle: String? = nil
    
    init(dataService: DataService) {
        self.dataService = dataService
    }
    
    func setRideMode(rideMode: RideMode) {
        guard let currentMotorcycle = dataService.currentMotorcycle else { return }
        self.rideMode = rideMode
        canEdit = rideMode.type == RideMode.RideModeType.CUSTOM
        // ZERO-536 Remove ability to set current ride mode
        //let current = currentMotorcycle.isCurrentRideMode(rideMode: rideMode)
        let active = currentMotorcycle.isActiveRideMode(rideMode: rideMode)
        // ZERO-536 Remove ability to set current ride mode
        // canMakeCurrent = !current
        canMakeActive = !active && rideMode.type == RideMode.RideModeType.CUSTOM

        isCurrentRideMode.accept(currentMotorcycle.isCurrentRideMode(rideMode: rideMode))
    }
    
    func refreshCurrent() {
        guard let rideMode = self.rideMode, let currentMotorcycle = dataService.currentMotorcycle else { return }
        let current = currentMotorcycle.isCurrentRideMode(rideMode: rideMode)
        isCurrentRideMode.accept(current)
    }
    
    func onOverflowTap() {
        guard let rideMode = self.rideMode else { return }
        
        let alert = UIAlertController(
            title: rideMode.name.uppercased(),
            message: nil,
            preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        
        if (canMakeActive) {
            // ZERO-536 Remove ability to set current ride mode
            // if (canMakeCurrent) {
            // alert.addAction(UIAlertAction(title: "ride_mode_make_current".localized, style: .default, handler: { [weak self] action in
            alert.addAction(UIAlertAction(title: "ride_mode_make_active".localized, style: .default, handler: { [weak self] action in
                guard let strongSelf = self else { return }
                // ZERO-536 Remove ability to set current ride mode
                // strongSelf.dataService.currentMotorcycle?.currentRideMode = rideMode
               //strongSelf.onRideModeCurrent.accept(rideMode)
                strongSelf.onRideModeActive.accept(rideMode)
            }))
        }
        if (canEdit) {
            alert.addAction(UIAlertAction(title: "ride_mode_edit".localized, style: .default, handler: { [weak self] action in
                guard let strongSelf = self else { return }
                strongSelf.onRideModeView.accept(rideMode)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "ride_mode_view".localized, style: .default, handler: { [weak self] action in
                guard let strongSelf = self else { return }
                strongSelf.onRideModeView.accept(rideMode)
            }))
        }

        onRideModeOverflowAlert.accept(alert)
    }
}
