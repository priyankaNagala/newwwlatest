//
//  AllRideModesViewModel.swift
//  nextgen
//
//  Created by David Crawford on 7/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift

class AllRideModesViewModel {
    var dataService: DataService
    var disposeBag = DisposeBag()
    var allRideModes = BehaviorRelay<[AllRideModesSection]>(value: [])
    var onDeactivateAlert = PublishRelay<UIAlertController?>()
    var onDeactiveCurrentModeTap = PublishRelay<Bool>()
    var onActivateRideMode = PublishRelay<RideMode>()
    var activate: Bool = false

    typealias AllRideModesDataSource = RxTableViewSectionedReloadDataSource<AllRideModesSection>

    struct AllRideModesSection: SectionModelType  {
        typealias Item = AllRideModesItemViewModel
        var header: String
        var items: [Item]
        init(header: String, items: [Item]) {
            self.header = header
            self.items = items
        }
        init(original: AllRideModesSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    func allRideModesDataSource() -> AllRideModesDataSource {
        let cellId = AllRideModesItemViewCell.ReuseIdentifier
        return AllRideModesDataSource(
            configureCell: { _, tv, ip, item  in
                let cell = tv.dequeueReusableCell(withIdentifier: cellId, for: ip) as! AllRideModesItemViewCell
                cell.configure(withViewModel: item)
                return cell
            },
            titleForHeaderInSection: { ds, index in
                return ds.sectionModels[index].header
            }
        )
    }
    
    init(dataService:DataService) {
        self.dataService = dataService
    }
    
    func onActivateRideModeSelect(rideMode: RideMode) {
        // Check to see if the rideMode is already activated. If so, do nothing
        for activeRideMode in (dataService.currentMotorcycle?.activeRideModes ?? []) {
            if (activeRideMode.name == rideMode.name) {
                return
            }
        }
        onActivateRideMode.accept(rideMode)        
    }
    
    func loadModel() {
        let allViewModels = getViewModelsFromRideModes(rideModes: dataService.currentMotorcycle?.defaultRideModes ?? [])
        let myViewModels = getViewModelsFromRideModes(rideModes: (dataService.currentMotorcycle?.myRideModes ?? []))

        allRideModes.accept([
            AllRideModesSection(header: "ride_mode_all_ride_modes_zero".localized, items: allViewModels),
            AllRideModesSection(header: "ride_mode_all_ride_modes_my".localized, items: myViewModels)
            ])
    }
    
    private func getViewModelsFromRideModes(rideModes: [RideMode]) -> [AllRideModesItemViewModel] {
        var viewModels = [AllRideModesItemViewModel]()
        for rideMode in rideModes {
            if let viewModel = SwinjectUtil.sharedInstance.container.resolve(AllRideModesItemViewModel.self) {
                viewModel.rideMode = rideMode
                viewModel.activate = activate
                
                for activeRideMode in (dataService.currentMotorcycle?.activeRideModes ?? []) {
                    if (activeRideMode.name == rideMode.name) {
                        viewModel.isActive = true
                        break
                    }
                }
                
                disposeBag.insertAll(all:
                    viewModel.onDeactivateAlert.subscribe({ [weak self] alert in
                        guard let strongSelf = self else { return }
                        if let alert = alert.element {
                            strongSelf.onDeactivateAlert.accept(alert)
                        }
                    }),
                    viewModel.onDeactiveCurrentModeTap.subscribe({ [weak self] _ in
                        guard let strongSelf = self else { return }
                        strongSelf.onDeactiveCurrentModeTap.accept(true)
                    })
                )
                
                viewModels.append(viewModel)
            }
        }
        return viewModels
    }
    
    func canAddNewCustomRideMode() -> Bool {
        return (dataService.currentMotorcycle?.myRideModes.count ?? 0 ) < Constant.RIDE_MODES_MAX_USER_MODES
    }
    
}
