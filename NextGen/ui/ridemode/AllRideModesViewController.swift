//
//  AllRideModesViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import UIKit
import RxSwift

class AllRideModesViewController: ZeroUIViewController, UITableViewDelegate {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_ALL_RIDE_MODES }
    
    var dataService: DataService?
    
    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    @IBOutlet weak var createNewRideModeButton: ZeroSecondaryUIButton!
    @IBOutlet weak var rideModesTableView: UITableView!
    @IBOutlet weak var headerTitle: ZeroUpperCaseUILabel!
    
    private let tableItemCellId = AllRideModesItemViewCell.ReuseIdentifier
    private let tableHeaderCellId = AllRideModesHeaderViewCell.ReuseIdentifier
    
    var activate: Bool = false
    var allRideModesViewModel: AllRideModesViewModel?
    var allRideModesDataSource: AllRideModesViewModel.AllRideModesDataSource?
    var selectedRideMode: RideMode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
    }
    
    func bindView() {
        guard let viewModel = allRideModesViewModel else { return }
        viewModel.activate = activate
        
        rideModesTableView.delegate = self
        
        rideModesTableView.register(UINib(nibName: tableItemCellId, bundle: nil), forCellReuseIdentifier: tableItemCellId)
        rideModesTableView.register(UINib(nibName: tableHeaderCellId, bundle: nil), forCellReuseIdentifier: tableHeaderCellId)
        
        self.allRideModesDataSource = viewModel.allRideModesDataSource()

        disposeBag.insertAll(all:
            
            backButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            }),
                             
            createNewRideModeButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                
                if !viewModel.canAddNewCustomRideMode() {
                    let error = String(format: "ride_mode_too_many_ride_modes".localized, Constant.RIDE_MODES_MAX_USER_MODES)
                    strongSelf.showErrorDialog(message: error)
                    return
                }
                strongSelf.showCreateDialog(errorMessage: nil)
            }),
            
            viewModel.allRideModes
                .bind(to: rideModesTableView.rx.items(dataSource: allRideModesDataSource!))
        )
        
        if (activate) {
            headerTitle.text = "ride_mode_activate_a_ride_mode".localized
            createNewRideModeButton.isHidden = true
            
            disposeBag.insertAll(all:
                rideModesTableView.rx.modelSelected(AllRideModesItemViewModel.self)
                    .subscribe({ itemViewModel in
                        if let rideMode = itemViewModel.element?.rideMode {
                            viewModel.onActivateRideModeSelect(rideMode: rideMode)
                        }
                    }),
                                 
                viewModel.onActivateRideMode.subscribe({ [weak self] rideMode in
                    guard let strongSelf = self else { return }
                    if let rideMode = rideMode.element {
                        strongSelf.selectedRideMode = rideMode
                        strongSelf.navigationController?.popViewController(animated: true)
                    }
                })
            )
            
        } else {
            
            disposeBag.insertAll(all:
                viewModel.onDeactivateAlert.subscribe({ [weak self] alert in
                    guard let strongSelf = self else { return }
                    if let alert = alert.element {
                        strongSelf.present(alert!, animated: true)
                    }
                }),
                                 
                viewModel.onDeactiveCurrentModeTap.subscribe({ [weak self] _ in
                    guard let strongSelf = self else { return }
                    strongSelf.showErrorDialog(message: "ride_mode_deactivate_cant_disable".localized)
                }),
                                                 
                // Clicking a row in the table clicks through to the ride mode
                rideModesTableView.rx.modelSelected(AllRideModesItemViewModel.self)
                    .subscribe({ [weak self] itemViewModel in
                        guard let strongSelf = self else { return }
                        if let rideMode = itemViewModel.element?.rideMode {
                            UILauncher.launchRideMode(strongSelf, rideMode: rideMode)
                                .bind(onNext: { _ in
                                    viewModel.loadModel()
                                })
                                .disposed(by: strongSelf.disposeBag)
                        }
                    })
            )
        }
        
        viewModel.loadModel()
    }
    
    private func showCreateDialog(errorMessage: String?) {
        guard let viewModel = allRideModesViewModel else { return }
        // Check to see if there are already too many ride modes
        if !viewModel.canAddNewCustomRideMode() {
            let message = String(format: "ride_mode_too_many_ride_modes".localized, Constant.RIDE_MODES_MAX_USER_MODES)
            self.showErrorDialog(message: message)
            return
        }
        
        let alert = UIAlertController(
            title: "ride_mode_create".localized,
            message: "ride_mode_enter_name".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))

        let createAction = UIAlertAction(title: "ride_mode_create_ok".localized, style: .default, handler: { action in
            guard let rideModeName = alert.textFields?.first?.text else {
                self.showCreateDialog(errorMessage: "ride_mode_enter_name_hint".localized)
                return
            }

            let rideMode = RideMode(type: RideMode.RideModeType.CUSTOM, name: rideModeName)
            if let currentMotorcycle = self.dataService?.currentMotorcycle {
                var myRideModes = currentMotorcycle.myRideModes
                myRideModes.append(rideMode)
                self.dataService?.currentMotorcycle?.myRideModes = myRideModes
            }
            
            viewModel.loadModel()
            
            UILauncher.launchRideMode(self, rideMode: rideMode)
                .bind(onNext: { _ in
                    viewModel.loadModel()
                })
                .disposed(by: self.disposeBag)
        })
        createAction.isEnabled = false // Keep this disabled until input is valid
        alert.addAction(createAction)
        
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "ride_mode_enter_name_hint".localized
            textField.maxLength = Constant.RIDE_MODE_NAME_MAX_CHARACTERS
            textField.keyboardType = UIKeyboardType.asciiCapable
            
            NotificationCenter.default
                .rx.notification(NSNotification.Name.UITextFieldTextDidChange)
                .subscribe({ _ in
                    textField.text = textField.text?.stringAscii()

                    let isUnique = (self.dataService?.currentMotorcycle?.activeRideModes
                        .filter { $0.name.lowercased() == textField.text!.lowercased() } ?? [])
                        .count == 0
                    
                    createAction.isEnabled = textField.text!.count > 0 && isUnique
                })
                .disposed(by: self.disposeBag)
        })
        
        self.present(alert, animated: true)
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerTitle = allRideModesDataSource?.sectionModels[section].header else { return nil }
        let cell = tableView.dequeueReusableCell(withIdentifier: tableHeaderCellId) as! AllRideModesHeaderViewCell
        cell.configure(withHeaderString: headerTitle)
        return cell
    }
        
    func showErrorDialog(message: String) {
        let errorDialog = UIAlertController(
            title: "global_error".localized,
            message: message,
            preferredStyle: .alert)
        errorDialog.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: nil))
        self.present(errorDialog, animated: true)
    }
}
