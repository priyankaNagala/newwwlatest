//
//  DeactivateRideModeViewModel.swift
//  nextgen
//
//  Created by David Crawford on 7/24/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources

class DeactivateRideModeViewModel {
    var dataService: DataService

    var deactivateRideModes = BehaviorRelay<[DeactivateRideModeSection]>(value: [])
    var currentRideMode = PublishRelay<RideMode>()
    
    typealias DeactivateRideModeDataSource = RxTableViewSectionedReloadDataSource<DeactivateRideModeSection>
    
    struct DeactivateRideModeSection: SectionModelType  {
        typealias Item = AllRideModesItemViewModel
        var items: [Item]
        init(items: [Item]) {
            self.items = items
        }
        init(original: DeactivateRideModeSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    func deactivateRideModeDataSource() -> DeactivateRideModeDataSource {
        let cellId = AllRideModesItemViewCell.ReuseIdentifier
        return DeactivateRideModeDataSource(
            configureCell: { _, tv, ip, item  in
                let cell = tv.dequeueReusableCell(withIdentifier: cellId, for: ip) as! AllRideModesItemViewCell
                cell.configure(withViewModel: item, isDeactivate: true)
                return cell
            }
        )
    }
    
    init(dataService:DataService) {
        self.dataService = dataService
    }
    
    func loadModel() {
        guard let currentMotorcycle = dataService.currentMotorcycle else { return }
        
        currentRideMode.accept(currentMotorcycle.currentRideMode)

        var activeRideModeViewModels = [AllRideModesItemViewModel]()
        for rideMode in currentMotorcycle.activeRideModes {
            if (rideMode.name == currentMotorcycle.currentRideMode.name) {
                continue
            }
            
            if let viewModel = SwinjectUtil.sharedInstance.container.resolve(AllRideModesItemViewModel.self) {
                viewModel.rideMode = rideMode
                viewModel.isActive = true
                activeRideModeViewModels.append(viewModel)
            }
        }
        deactivateRideModes.accept([
            DeactivateRideModeSection(items: activeRideModeViewModels),
            ])
    }
    
}
