//
//  ManageRideModeViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/18/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxCocoa
import RxDataSources
import RxSwift
import CocoaLumberjack

class ManageRideModesViewModel {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var disposeBag = DisposeBag()
    var onRideModeOverflowAlert = BehaviorRelay<UIAlertController?>(value: nil)
    var onRideModeView = PublishRelay<RideMode>()
    var numRetries = 0
    var bluetoothDisposeBag = DisposeBag()
    var writeBluetoothDisposeBag = DisposeBag()

    var allRideModes = BehaviorRelay<[RideModesSection]>(value: [])
    var onUpdateComplete = PublishRelay<Bool>()
    var onUpdateStart = PublishRelay<Bool>()
    var onQueueComplete = PublishRelay<Bool>()
    var canCreateRideModes = BehaviorRelay<Bool>(value: false)

    typealias RideModeDataSource = RxTableViewSectionedReloadDataSource<RideModesSection>
    
    struct RideModesSection: SectionModelType  {
        typealias Item = ManageRideModesItemViewModel
        var header: String
        var items: [Item]
        init(header: String, items: [Item]) {
            self.header = header
            self.items = items
        }
        init(original: RideModesSection, items: [Item]) {
            self = original
            self.items = items
        }
    }
    
    // MARK: -

    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }

    func rideModesDataSource() -> RideModeDataSource {
        return RideModeDataSource(
            configureCell: { _, tv, ip, item  in
                let cell = tv.dequeueReusableCell(withIdentifier: RideModeTableViewCell.ReuseIdentifier, for: ip) as! RideModeTableViewCell
                cell.configure(withViewModel: item)
                return cell
            },
            titleForHeaderInSection: { ds, index in
                return ds.sectionModels[index].header
            }
        )
    }
    
    func activateRideMode(rideMode: RideMode) {
        var activeModes = dataService.currentMotorcycle?.activeRideModes ?? []
        activeModes.append(rideMode)
        dataService.currentMotorcycle?.activeRideModes = activeModes
        loadModel()
    }
    
    func loadModel() {
        let activeModels = getViewmodelsFromRideModes(dataService.currentMotorcycle?.activeRideModes ?? [])
        // Show current icon for this section
        for model in activeModels {
            model.showCurrentIcon = true
        }
        
        canCreateRideModes.accept(dataService.currentMotorcycle != nil)

        // Exclude current ride mode from list
        let myRideModes = dataService.currentMotorcycle?.myRideModes ?? []
        var inactiveCustomModels = [RideMode]()

        let activeCustomMode = dataService.currentMotorcycle?.getActiveCustomMode()
        for rideMode in myRideModes {
            if activeCustomMode != nil && activeCustomMode!.name == rideMode.name {
                continue
            }
            inactiveCustomModels.append(rideMode)
        }
        let inactiveModels = getViewmodelsFromRideModes(inactiveCustomModels)
        
        allRideModes.accept([
            RideModesSection(header: "ride_mode_active_modes".localized, items: activeModels),
            RideModesSection(header: "ride_mode_inactive_custom_modes".localized, items: inactiveModels)
            ])
        
        readRideModesFromBike()
    }
    
    func getViewmodelsFromRideModes(_ rideModes: [RideMode]) -> [ManageRideModesItemViewModel] {
        var viewModels = [ManageRideModesItemViewModel]()
        for rideMode in rideModes {
            if let viewModel = SwinjectUtil.sharedInstance.container.resolve(ManageRideModesItemViewModel.self) {
                
                viewModel.setRideMode(rideMode: rideMode)
                
                disposeBag.insertAll(all:
                    viewModel.onRideModeOverflowAlert.subscribe { [weak self] alert in
                        guard let strongSelf = self else { return }
                        if let alert = alert.element {
                            strongSelf.onRideModeOverflowAlert.accept(alert)
                        }
                    },
                                     
                    viewModel.onRideModesEdit.subscribe { [weak self] rideModes in
                        guard let strongSelf = self else { return }
                        strongSelf.loadModel()
                    },
                                     
                    viewModel.onRideModeView.subscribe { [weak self] rideMode in
                        guard let strongSelf = self else { return }
                        strongSelf.onRideModeView.accept(rideMode.element!)
                    },
                    
                    // ZERO-536 Remove ability to set current ride mode
                    //viewModel.onRideModeCurrent.subscribe { [weak self] event in
                    viewModel.onRideModeActive.subscribe { [weak self] event in
                        guard let strongSelf = self, let rideMode = event.element else { return }
                        // ZERO-536 Remove ability to set current ride mode
//                        if let currentRideMode = strongSelf.dataService.currentMotorcycle?.currentRideMode {
                            // If current ride mode is custom place in last active slot
                            if rideMode.type == .CUSTOM {
                                strongSelf.dataService.currentMotorcycle?.setActiveCustomMode(rideMode)
                                strongSelf.loadModel()
                            }
//                        }
                        let rideModeViewModels = strongSelf.allRideModes.value[0].items
                        for rideModeViewModel in rideModeViewModels {
                            rideModeViewModel.refreshCurrent()
                        }

                        strongSelf.sendRideModesToBike()
                    }
                )
                
                viewModels.append(viewModel)
            }
        }
        return viewModels
    }
    
    func canAddNewCustomRideMode() -> Bool {
        return (dataService.currentMotorcycle?.myRideModes.count ?? 0 ) < Constant.RIDE_MODES_MAX_USER_MODES
    }
    
    func onActiveRideModeSaved() {
        sendRideModesToBike()
    }
    
    // MARK: - Bluetooth
    
    func sendRideModesToBike() {
        onUpdateStart.accept(true)

        guard let activeRideModes = dataService.currentMotorcycle?.activeRideModes else {
            return
        }

        let customRideModeWritePacket = RideModeCustomSettingsPacket.init(activeRideModes)
        
        if zeroMotorcycleService.isConnected() {
            zeroMotorcycleService.sendSingleZeroPacket(packet: customRideModeWritePacket, priority: true).subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }

                if let _ = event.element as? AckPacket {
                    strongSelf.writeBluetoothDisposeBag = DisposeBag() // We got a response, no need for additional callbacks
                    strongSelf.numRetries = 0
                    strongSelf.onUpdateComplete.accept(true)
                    DDLogDebug("Write Custom RideMode Response ACK Packet")
                } else {
                    DDLogWarn("Write Custom RideMode Response Unknown")
                }
            }.disposed(by: writeBluetoothDisposeBag)
            
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self, let _ = event.element else { return }
                strongSelf.writeBluetoothDisposeBag = DisposeBag()
                DDLogDebug("Write Custom RideMode Response Resend Packet")
                guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                    strongSelf.onUpdateComplete.accept(false)
                    strongSelf.numRetries = 0
                    return
                }
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.sendRideModesToBike()
            }.disposed(by: writeBluetoothDisposeBag)
        } else {
            zeroMotorcycleService.queueSingleZeroPacket(packet: customRideModeWritePacket)
            onQueueComplete.accept(true)
        }
    }
    
    func readRideModesFromBike() {
        let customRideModePacket = RideModeCustomSettingsPacket()
        customRideModePacket.isEmpty = true
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: customRideModePacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, event.element != nil else { return }
            
            if let packet = event.element as? RideModeCustomSettingsPacket {
                strongSelf.bluetoothDisposeBag = DisposeBag() // We got a response, no need for additional callbacks
                strongSelf.numRetries = 0
                strongSelf.setActiveRideMode(Int(packet.activeMode))
                DDLogDebug("Read Custom RideMode Response Packet " + packet.getFullPacket().toHexString())
            } else {
                DDLogWarn("Read Custom RideMode Response Unknown")
            }
            }.disposed(by: bluetoothDisposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self, let _ = event.element else { return }
            strongSelf.bluetoothDisposeBag = DisposeBag()
            DDLogDebug("Write Custom RideMode Response Resend Packet")
            guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else { return }
            strongSelf.numRetries = strongSelf.numRetries + 1
            strongSelf.readRideModesFromBike()
            }.disposed(by: bluetoothDisposeBag)

    }
    
    func setActiveRideMode(_ index: Int) {
        let activeModes = dataService.currentMotorcycle?.activeRideModes ?? []
        if activeModes.count > index {
            let currentRideMode = activeModes[index]
            dataService.currentMotorcycle?.currentRideMode = currentRideMode

            let rideModeViewModels = allRideModes.value[0].items
            for rideModeViewModel in rideModeViewModels {
                rideModeViewModel.refreshCurrent()
            }
        }
    }
}
