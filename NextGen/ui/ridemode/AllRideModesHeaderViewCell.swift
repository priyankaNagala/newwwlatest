//
//  AllRideModesHeaderViewCell.swift
//  nextgen
//
//  Created by David Crawford on 7/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class AllRideModesHeaderViewCell: UITableViewCell {
    static let ReuseIdentifier = "AllRideModesHeaderViewCell"
    
    @IBOutlet weak var headerTitle: UILabel!
    
    func configure(withHeaderString: String) {
        headerTitle.text = withHeaderString
    }
}
