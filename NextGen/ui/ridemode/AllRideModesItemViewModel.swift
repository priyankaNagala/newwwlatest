//
//  AllRideModesItemViewModel.swift
//  nextgen
//
//  Created by David Crawford on 7/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AllRideModesItemViewModel {
    var dataService: DataService
    
    var onDeactivate = PublishRelay<Bool>()
    var onDeactivateAlert = PublishRelay<UIAlertController?>()
    var onDeactiveCurrentModeTap = PublishRelay<Bool>()

    var rideMode: RideMode?
    var isActive: Bool = false
    var activate: Bool = false
    
    init(dataService:DataService) {
        self.dataService = dataService
    }
    
    func deactivate() {
        guard let currentMotorcycle = dataService.currentMotorcycle else { return }
        guard let mode = rideMode else { return }
        if !isActive {
            return
        }
        
        let deactivated = currentMotorcycle.deactivateRideMode(rideMode: mode)
        if deactivated {
            onDeactivate.accept(false)
            isActive = false
        }
    }
    
    func deactivateWithAlert() {
        guard let currentMotorcycle = dataService.currentMotorcycle else { return }
        guard let mode = rideMode else { return }
        if currentMotorcycle.isCurrentRideMode(rideMode: mode) {
            onDeactiveCurrentModeTap.accept(true)
            return
        }
        
        let alert = UIAlertController(
            title: "ride_mode_deactivate_title".localized,
            message: "ride_mode_deactivate_description".localized,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "ride_mode_deactivate".localized, style: .default, handler: { action in
                self.deactivate()
        }))
        onDeactivateAlert.accept(alert)
    }
}
