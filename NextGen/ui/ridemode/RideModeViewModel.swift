//
//  RideModeViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RideModeViewModel {
    var dataService: DataService
    
    init(dataService:DataService) {
        self.dataService = dataService
    }
    
    var rideMode: RideMode?
    
    // Inputs
    var isEditable = BehaviorRelay<Bool>(value: false)
    var isActive = BehaviorRelay<Bool>(value: false)

    var tractionControlEnabled = BehaviorRelay<Bool>(value: false)
    var tractionControlType = BehaviorRelay<Int>(value: 0)
    var rideModeName = BehaviorRelay<String>(value: "")
    var dashboardTheme = BehaviorRelay<Int>(value: 0)
    
    var maxSpeed = BehaviorRelay<Float>(value: 0.0)
    var power = BehaviorRelay<Float>(value: 0.0)
    var maxTorque = BehaviorRelay<Float>(value: 0.0)
    var neutralRegeneration = BehaviorRelay<Float>(value: 0.0)
    var brakeRegeneration = BehaviorRelay<Float>(value: 0.0)
    
    var onRideModeSaved = PublishRelay<RideMode>()
    var onActiveRideModeSaved = PublishRelay<RideMode>()
    var onRideModeCloned = PublishRelay<RideMode>()
    var onRideModeDeleted = PublishRelay<RideMode>()
    
    // Outputs
    func onTractionControlChanged(isOn: Bool) {
        rideMode?.tractionControlEnabled = isOn
        tractionControlEnabled.accept(isOn)
    }
    
    func onTractionControlTypeChanged(idx: Int) {
        switch idx {
            case 0: rideMode?.tractionControlType = TractionControlType.STREET
            case 1: rideMode?.tractionControlType = TractionControlType.RAIN
            case 2: rideMode?.tractionControlType = TractionControlType.SPORT
            default: rideMode?.tractionControlType = TractionControlType.STREET
        }
    }
    
    func onDashboardThemeChanged(idx: Int) {
        switch idx {
            case 0: rideMode?.dashboardTheme = DashboardTheme.DARK_GREEN
            case 1: rideMode?.dashboardTheme = DashboardTheme.DARK_BLUE
            case 2: rideMode?.dashboardTheme = DashboardTheme.LIGHT_ORANGE
            case 3: rideMode?.dashboardTheme = DashboardTheme.LIGHT_BLUE
            case 4: rideMode?.dashboardTheme = DashboardTheme.DARK_ORANGE
            default: rideMode?.dashboardTheme = DashboardTheme.DARK_GREEN
        }
    }
    
    func onMaxSpeedChanged(value: Float) {
        rideMode?.maxSpeed = Int(value)
        maxSpeed.accept(value)
    }
    
    func onPowerChanged(value: Float) {
        rideMode?.maxPower = Int(value)
        power.accept(value)
    }
    
    func onMaxTorqueChanged(value: Float) {
        rideMode?.maxTorque = Int(value)
        maxTorque.accept(value)
    }
    
    func onNeutralRegenerationChanged(value: Float) {
        rideMode?.neutralRegeneration = Int(value)
        neutralRegeneration.accept(value)
        
        if (value > brakeRegeneration.value) {
            rideMode?.brakeRegeneration = Int(value)
            brakeRegeneration.accept(value)
        }
    }
    
    func onBrakeRegenerationChanged(value: Float) {
        rideMode?.brakeRegeneration = Int(value)
        brakeRegeneration.accept(value)
        
        if (value < neutralRegeneration.value) {
            rideMode?.neutralRegeneration = Int(value)
            neutralRegeneration.accept(value)
        }
    }
    
    func onSaveTap() {
        guard let localMode = self.rideMode else { return }
        
        if let currentMode = dataService.currentMotorcycle?.currentRideMode {
            if currentMode.name == localMode.name {
                dataService.currentMotorcycle?.currentRideMode = localMode
            }
        }
        if var myModes = dataService.currentMotorcycle?.myRideModes {
            for (idx, rideMode) in myModes.enumerated() {
                if rideMode.name == localMode.name {
                    myModes[idx] = localMode
                    dataService.currentMotorcycle?.myRideModes = myModes
                    if let activeCustom = dataService.currentMotorcycle?.getActiveCustomMode() {
                        if activeCustom.name == localMode.name {
                            // We need to send updates to MBB
                            dataService.currentMotorcycle?.setActiveCustomMode(localMode)
                            onActiveRideModeSaved.accept(localMode)
                            break
                        }
                    }
                    onRideModeSaved.accept(localMode)
                    break
                }
            }
        }
    }
    
    func onDeleteTap() {
        guard let localMode = self.rideMode else { return }
        
        if let currentMode = dataService.currentMotorcycle?.currentRideMode {
            if currentMode.name == localMode.name {
                return // Shouldn't be able to do this
            }
        }
        
        if var myModes = self.dataService.currentMotorcycle?.myRideModes {
            for (idx, rideMode) in myModes.enumerated() {
                if rideMode.name == localMode.name {
                    myModes.remove(at: idx)
                    self.dataService.currentMotorcycle?.myRideModes = myModes
                    self.onRideModeDeleted.accept(localMode)
                    break
                }
            }
        }
    }
    
    func onRideModeClone(newRideModeName: String) {
        guard let localMode = self.rideMode else { return }

        let copyRideMode = localMode.copy() as! RideMode
        copyRideMode.name = newRideModeName
        
        if let currentMotorcycle = self.dataService.currentMotorcycle {
            var myRideModes = currentMotorcycle.myRideModes
            myRideModes.append(copyRideMode)
            self.dataService.currentMotorcycle?.myRideModes = myRideModes
            
            onRideModeCloned.accept(copyRideMode)
        }
    }
    
    func canAddNewCustomRideMode() -> Bool {
        return (dataService.currentMotorcycle?.myRideModes.count ?? 0 ) < Constant.RIDE_MODES_MAX_USER_MODES
    }
    
    func canActivateNewMode() -> Bool {
        return (dataService.currentMotorcycle?.activeRideModes.count ?? 0 ) < Constant.RIDE_MODES_MAX_ACTIVE_MODES
    }
    
    func isUniqueRideModeName(name: String) -> Bool {
        return (self.dataService.currentMotorcycle?.activeRideModes
            .filter { $0.name.lowercased() == name.lowercased() } ?? [])
            .count == 0
    }
    
    func onActiveIconTap() {
        guard let localMode = self.rideMode else { return }
        guard let currentMotorcycle = dataService.currentMotorcycle else { return }

        if isActive.value {
            let deactivated = currentMotorcycle.deactivateRideMode(rideMode: localMode)
            if (deactivated) {
                isActive.accept(false)
            }
            return
        }
        
        // activate
        currentMotorcycle.activateRideMode(rideMode: localMode)
        isActive.accept(true)
    }
    
    func isRideModeActive() -> Bool {
        return isActive.value
    }
    
    func isCurrentRideMode() -> Bool {
        guard let localMode = self.rideMode else { return false }
        guard let currentMotorcycle = dataService.currentMotorcycle else { return false }        
        return currentMotorcycle.isCurrentRideMode(rideMode:localMode)
    }
    
    func isActiveRideMode() -> Bool {
        guard let localMode = self.rideMode else { return false }
        guard let currentMotorcycle = dataService.currentMotorcycle else { return false }
        return currentMotorcycle.isActiveRideMode(rideMode: localMode)
    }
    
    func loadModel(rideMode: RideMode) {
        self.rideMode = rideMode
        rideModeName.accept(rideMode.name)
        tractionControlEnabled.accept(rideMode.tractionControlEnabled)

        switch rideMode.tractionControlType {
            case TractionControlType.STREET: tractionControlType.accept(0)
            case TractionControlType.RAIN: tractionControlType.accept(1)
            case TractionControlType.SPORT: tractionControlType.accept(2)
        }
        
        switch rideMode.dashboardTheme {
            case DashboardTheme.DARK_GREEN: dashboardTheme.accept(0)
            case DashboardTheme.DARK_BLUE: dashboardTheme.accept(1)
            case DashboardTheme.LIGHT_ORANGE: dashboardTheme.accept(2)
            case DashboardTheme.LIGHT_BLUE: dashboardTheme.accept(3)
            case DashboardTheme.DARK_ORANGE: dashboardTheme.accept(4)
        }
        
        maxSpeed.accept(Float(rideMode.maxSpeed))
        power.accept(Float(rideMode.maxPower))
        maxTorque.accept(Float(rideMode.maxTorque))
        neutralRegeneration.accept(Float(rideMode.neutralRegeneration))
        brakeRegeneration.accept(Float(rideMode.brakeRegeneration))
        
        isEditable.accept(rideMode.type == RideMode.RideModeType.CUSTOM)
        isActive.accept(dataService.currentMotorcycle?.isActiveRideMode(rideMode: rideMode) ?? false)
    }
    
}
