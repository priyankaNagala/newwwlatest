//
//  DeactivateRideModeViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class DeactivateRideModeViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_DEACTIVATE_RIDE_MODE }
    
    @IBOutlet weak var backButton: ZeroUIBarButtonItem!
    @IBOutlet weak var headerTitle: ZeroUpperCaseUILabel!
    @IBOutlet weak var currentRideModeName: ZeroUpperCaseUILabel!
    @IBOutlet weak var deactivateTableView: UITableView!
    
    private let tableItemCellId = AllRideModesItemViewCell.ReuseIdentifier
    
    var deactivateRideModeViewModel: DeactivateRideModeViewModel?
    var deactivateRideModeDataSource: DeactivateRideModeViewModel.DeactivateRideModeDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
    }
    
    func bindView() {
        guard let viewModel = deactivateRideModeViewModel else { return }

        headerTitle.text = "ride_mode_deactivate_modes_title".localized
        deactivateTableView.register(UINib(nibName: tableItemCellId, bundle: nil), forCellReuseIdentifier: tableItemCellId)

        self.deactivateRideModeDataSource = viewModel.deactivateRideModeDataSource()
        
        disposeBag.insertAll(all:
            
            backButton.rx.tap.subscribe({ [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            }),
                             
            viewModel.deactivateRideModes
                .bind(to: deactivateTableView.rx.items(dataSource: deactivateRideModeDataSource!)),
                             
            viewModel.currentRideMode.subscribe({ [weak self] rideMode in
                guard let strongSelf = self else { return }
                if let rideMode = rideMode.element {
                    strongSelf.currentRideModeName.text = rideMode.name
                }
            }),
            
            deactivateTableView.rx.modelSelected(AllRideModesItemViewModel.self)
                .subscribe({ itemViewModel in
                    if !itemViewModel.element!.isActive {
                        return
                    }
                    
                    let alert = UIAlertController(
                        title: "ride_mode_deactivate_title".localized,
                        message: "ride_mode_deactivate_description".localized,
                        preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "global_cancel".localized, style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "ride_mode_deactivate".localized, style: .default, handler: { action in
                        if let item = itemViewModel.element {
                            item.deactivate()
                        }
                    }))
                    self.present(alert, animated: true)
                })
        )
        
        viewModel.loadModel()
    }
}
