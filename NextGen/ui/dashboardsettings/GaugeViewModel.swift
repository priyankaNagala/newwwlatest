//
//  GaugeViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class GaugeViewModel {
    var gauge: DashboardGauge
    var isCurrentGauge = false
    var gaugeName = String()
    
    init(gauge: DashboardGauge, isCurrentGauge: Bool) {
        self.gauge = gauge
        self.isCurrentGauge = isCurrentGauge        
    }
}
