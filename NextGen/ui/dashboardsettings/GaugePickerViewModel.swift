//
//  GaugePickerViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

class GaugePickerViewModel {
    var dataService: DataService

    private var gauges: Variable<[GaugeViewModel]> = Variable([])
    public let dataSource: Observable<[GaugeViewModel]>
    
    private var quadrant: DashboardGaugeQuadrant?
    var quadrantName = BehaviorRelay<String>(value: "")
    var currentGaugeName = BehaviorRelay<String>(value: "")
    
    init(dataService:DataService) {
        self.dataService = dataService
        self.dataSource = gauges.asObservable()
    }
    
    func loadModel(quadrant: DashboardGaugeQuadrant) {
        self.quadrant = quadrant
        self.quadrantName.accept(quadrant.localizedString())
        
        let currentGauge = dataService.currentMotorcycle?.dashboardGauges.gauges[quadrant]
        self.currentGaugeName.accept(currentGauge?.localizedString() ?? DashboardGauge.NONE.localizedString())
        
        let setGauges = dataService.currentMotorcycle?.dashboardGauges.gauges

        gauges.value.removeAll()
        _ = DashboardGauge.allCases.map { gauge in
            
            var isCurrentGauge = false
            _ = setGauges?
                .filter { $0.value == gauge && gauge != DashboardGauge.NONE }
                .map { _ in isCurrentGauge = true }
            
            let gaugeViewModel = GaugeViewModel(gauge: gauge, isCurrentGauge: isCurrentGauge)
            
            // Batt/Motor Temp can only go in A or B
            if (quadrant == DashboardGaugeQuadrant.A || quadrant == DashboardGaugeQuadrant.B) ||
                (gauge != DashboardGauge.MOTOR_TEMPERATURE && gauge != DashboardGauge.BATTERY_TEMPERATURE) {
                gauges.value.append(gaugeViewModel)
            }
        }
    }
}
