//
//  GaugePickerTableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 6/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class GaugePickerTableViewCell: ZeroUITableViewCell {
    static let ReuseIdentifier = "GaugePickerTableViewCell"
    
    @IBOutlet weak var gaugeName: ZeroUpperCaseUILabel!
    @IBOutlet weak var isSelectedIndicator: UIImageView!
    
    func configure(withViewModel viewModel: GaugeViewModel) {
        gaugeName.text = viewModel.gauge.localizedString()
        
        if (viewModel.isCurrentGauge) {
            gaugeName.textColor = ColorUtil.uiColor(AppUIColors.whiteTextDeselected)
            isSelectedIndicator.isHidden = false
            isSelectedIndicator.tintImageColor(color: ColorUtil.uiColor(AppUIColors.whiteTextDeselected))
        } else {
            gaugeName.textColor = ColorUtil.uiColor(AppUIColors.whiteText)
            isSelectedIndicator.isHidden = true
        }
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
}
