//
//  DashboardSettingsViewModel.swift
//  nextgen
//
//  Created by David Crawford on 6/12/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxSwift
import RxCocoa
import CocoaLumberjack

class DashboardSettingsViewModel : GaugePickerDelegate {
    var dataService: DataService
    var zeroMotorcycleService: ZeroMotorcycleService
    var disposeBag = DisposeBag()
    var bluetoothDisposeBag = DisposeBag()
    var numRetries = 0
    
    init(dataService:DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }
    
    // Inputs
    var rideModeColor = BehaviorRelay<Int>(value: 0)
    var gaugeAName = BehaviorRelay<String>(value: "")
    var gaugeBName = BehaviorRelay<String>(value: "")
    var gaugeCName = BehaviorRelay<String>(value: "")
    var gaugeDName = BehaviorRelay<String>(value: "")
    var basedOnRideModeEnabled = BehaviorRelay<Bool>(value: true)
    var onUpdateStart = PublishRelay<Bool>()
    var onUpdateComplete = PublishRelay<Bool>()
    var onQueueComplete = PublishRelay<Bool>()

    // Outputs
    var onQuadrantPressed = PublishRelay<DashboardGaugeQuadrant>()

    func quadrantAPressed() {
        onQuadrantPressed.accept(DashboardGaugeQuadrant.A)
    }
    
    func quadrantBPressed() {
        onQuadrantPressed.accept(DashboardGaugeQuadrant.B)
    }
    
    func quadrantCPressed() {
        onQuadrantPressed.accept(DashboardGaugeQuadrant.C)
    }
    
    func quadrantDPressed() {
        onQuadrantPressed.accept(DashboardGaugeQuadrant.D)
    }
    
    func onBasedOnRideModeChanged(isOn: Bool) {
        basedOnRideModeEnabled.accept(isOn)
        dataService.currentMotorcycle?.dashboardThemeBasedOnRideMode = isOn
    }
    
    func onRideModeColorChanged(idx: Int) {
        switch idx {
        case 0: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.DARK_GREEN
        case 1: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.DARK_BLUE
        case 2: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.LIGHT_ORANGE
        case 3: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.LIGHT_BLUE
        case 4: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.DARK_ORANGE
        default: dataService.currentMotorcycle?.dashboardTheme = DashboardTheme.DARK_GREEN
        }
    }
    
    func loadModel() {
        let basedOnRideMode = dataService.currentMotorcycle?.dashboardThemeBasedOnRideMode ?? true
        basedOnRideModeEnabled.accept(basedOnRideMode)
        
        if let theme = dataService.currentMotorcycle?.dashboardTheme {
            switch theme {
                case DashboardTheme.DARK_GREEN: rideModeColor.accept(0)
                case DashboardTheme.DARK_BLUE: rideModeColor.accept(1)
                case DashboardTheme.LIGHT_ORANGE: rideModeColor.accept(2)
                case DashboardTheme.LIGHT_BLUE: rideModeColor.accept(3)
                case DashboardTheme.DARK_ORANGE: rideModeColor.accept(4)
            }
        }
        
        acceptGaugeValues()
        
        // Get Info from MBB
        zeroMotorcycleService.getDashGauges().subscribe { [weak self] event in
            guard let strongSelf = self, let dashGaugesPacket = event.element else { return }
            var dashGauges = DashboardGauges()
            let gaugeA = DashboardGauge(rawValue: dashGaugesPacket.gauge1)
            dashGauges.gauges[DashboardGaugeQuadrant.A] = gaugeA ?? DashboardGauge.NONE
            strongSelf.gaugeAName.accept(dashGauges.gauges[DashboardGaugeQuadrant.A]!.localizedString())

            let gaugeB = DashboardGauge(rawValue: dashGaugesPacket.gauge2)
            dashGauges.gauges[DashboardGaugeQuadrant.B] = gaugeB ?? DashboardGauge.NONE
            strongSelf.gaugeBName.accept(dashGauges.gauges[DashboardGaugeQuadrant.B]!.localizedString())

            let gaugeC = DashboardGauge(rawValue: dashGaugesPacket.gauge3)
            dashGauges.gauges[DashboardGaugeQuadrant.C] = gaugeC ?? DashboardGauge.NONE
            strongSelf.gaugeCName.accept(dashGauges.gauges[DashboardGaugeQuadrant.C]!.localizedString())

            let gaugeD = DashboardGauge(rawValue: dashGaugesPacket.gauge4)
            dashGauges.gauges[DashboardGaugeQuadrant.D] = gaugeD ?? DashboardGauge.NONE
            strongSelf.gaugeDName.accept(dashGauges.gauges[DashboardGaugeQuadrant.D]!.localizedString())
            
            dashGauges.savedTimestamp = dashGaugesPacket.timestamp

            strongSelf.dataService.currentMotorcycle?.dashboardGauges = dashGauges
            }.disposed(by: disposeBag)
    }
    
    func acceptGaugeValues() {
        let dashboardGauges = dataService.currentMotorcycle?.dashboardGauges
        let none = DashboardGauge.NONE.localizedString()
        gaugeAName.accept(dashboardGauges?.gauges[DashboardGaugeQuadrant.A]?.localizedString() ?? none)
        gaugeBName.accept(dashboardGauges?.gauges[DashboardGaugeQuadrant.B]?.localizedString() ?? none)
        gaugeCName.accept(dashboardGauges?.gauges[DashboardGaugeQuadrant.C]?.localizedString() ?? none)
        gaugeDName.accept(dashboardGauges?.gauges[DashboardGaugeQuadrant.D]?.localizedString() ?? none)
    }
    
    func sendDashboardGaugesUpdateToMotorcycle() {
        guard let dashboardGauges = dataService.currentMotorcycle?.dashboardGauges else { return
        }

        let dashGaugePacket = DashGaugesPacket()
        dashGaugePacket.isEmpty = false
        dashGaugePacket.gauge1 = dashboardGauges.gauges[DashboardGaugeQuadrant.A]?.rawValue ?? 0
        dashGaugePacket.gauge2 = dashboardGauges.gauges[DashboardGaugeQuadrant.B]?.rawValue ?? 0
        dashGaugePacket.gauge3 = dashboardGauges.gauges[DashboardGaugeQuadrant.C]?.rawValue ?? 0
        dashGaugePacket.gauge4 = dashboardGauges.gauges[DashboardGaugeQuadrant.D]?.rawValue ?? 0
        dashGaugePacket.timestamp = UInt32(Date().timeIntervalSince1970)

        if zeroMotorcycleService.isConnected() {
            zeroMotorcycleService.sendSingleZeroPacket(packet: dashGaugePacket, priority: true).subscribe { [weak self] event in
                guard let strongSelf = self, event.element != nil else { return }
                if let packet = event.element as? AckPacket {
                    strongSelf.bluetoothDisposeBag = DisposeBag()
                    DDLogDebug("Write DashGauge Response ACK Packet " + packet.getFullPacket().toHexString())
                    // Sync timestamp
                    strongSelf.dataService.currentMotorcycle?.dashboardGauges.savedTimestamp = dashGaugePacket.timestamp
                    strongSelf.onUpdateComplete.accept(true)
                } else {
                    DDLogDebug("Write DashGauge Response Unknown Packet ")
                }
            }.disposed(by: bluetoothDisposeBag)
            
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self, let resendPacket = event.element else { return }
                strongSelf.bluetoothDisposeBag = DisposeBag()
                DDLogDebug("Write DashGauge Response Resend Packet " + resendPacket.getFullPacket().toHexString())
                guard strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES else {
                    strongSelf.numRetries = 0
                    strongSelf.onUpdateComplete.accept(false)
                    return
                }
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.sendDashboardGaugesUpdateToMotorcycle()
            }.disposed(by: bluetoothDisposeBag)
        } else {
            zeroMotorcycleService.queueSingleZeroPacket(packet: dashGaugePacket)
            onQueueComplete.accept(true)
        }
    }
    
    // MARK: - GaugePickerDelegate
    func gaugeSelected(quadrant: DashboardGaugeQuadrant, gauge: DashboardGauge) {
        onUpdateStart.accept(true)
        // Check to see if any of the current gauges is the one we just picked.
        // If so, switch that to None
        if (gauge != DashboardGauge.NONE) {
            if let setGauges = dataService.currentMotorcycle?.dashboardGauges.gauges {
                _ = setGauges
                    .filter { $0.value == gauge }
                    .map { dataService.currentMotorcycle?.dashboardGauges.gauges[$0.key] = DashboardGauge.NONE }
            }
        }
        
        dataService.currentMotorcycle?.dashboardGauges.gauges[quadrant] = gauge
        acceptGaugeValues()
        sendDashboardGaugesUpdateToMotorcycle()
    }
}
