//
//  DashboardSettingsViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import UIKit
import RxCocoa

final class DashboardSettingsViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_DASHBOARD_SETTINGS }
    
    var dashboardSettingsViewModel: DashboardSettingsViewModel?
    
    @IBOutlet weak var backButton: ZeroUIBarButtonItem!

    @IBOutlet weak var basedOnRideModeSwitch: UISwitch!
    @IBOutlet weak var rideModeColorSegmentedControl: UISegmentedControl! {
        didSet {
            rideModeColorSegmentedControl.setTitle("ride_mode_dark_green".localized, forSegmentAt: 0)
            rideModeColorSegmentedControl.setTitle("ride_mode_dark_blue".localized, forSegmentAt: 1)
            rideModeColorSegmentedControl.setTitle("ride_mode_light_orange".localized, forSegmentAt: 2)
            rideModeColorSegmentedControl.setTitle("ride_mode_light_blue".localized, forSegmentAt: 3)
            rideModeColorSegmentedControl.setTitle("ride_mode_dark_orange".localized, forSegmentAt: 4)
        }
    }
    
    @IBOutlet weak var quadrantALabel: UILabel! {
        didSet { quadrantALabel.text = DashboardGaugeQuadrant.A.localizedString() }
    }
    @IBOutlet weak var quadrantBLabel: UILabel! {
        didSet { quadrantBLabel.text = DashboardGaugeQuadrant.B.localizedString() }
    }
    @IBOutlet weak var quadrantCLabel: UILabel! {
        didSet { quadrantCLabel.text = DashboardGaugeQuadrant.C.localizedString() }
    }
    @IBOutlet weak var quadrantDLabel: UILabel! {
        didSet { quadrantDLabel.text = DashboardGaugeQuadrant.D.localizedString() }
    }
    
    @IBOutlet weak var quadrantAButton: UIButton!
    @IBOutlet weak var quadrantBButton: UIButton!
    @IBOutlet weak var quadrantCButton: UIButton!
    @IBOutlet weak var quadrantDButton: UIButton!
    
    @IBOutlet weak var quadrantARow: UIButton!
    @IBOutlet weak var quadrantBRow: UIButton!
    @IBOutlet weak var quadrantCRow: UIButton!
    @IBOutlet weak var quadrantDRow: UIButton!
    
    @IBOutlet weak var gaugeAName: ZeroUpperCaseUILabel!
    @IBOutlet weak var gaugeBName: ZeroUpperCaseUILabel!
    @IBOutlet weak var gaugeCName: ZeroUpperCaseUILabel!
    @IBOutlet weak var gaugeDName: ZeroUpperCaseUILabel!
    
    fileprivate var syncView: SyncActivityOverlayView = SyncActivityOverlayView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        syncView.frame = view.bounds
    }
    
    func bindViews() {
        guard let viewModel = dashboardSettingsViewModel else { return }
        
        /*
        (rideModeColorSegmentedControl.rx.selectedSegmentIndex <-> viewModel.rideModeColor)
            .disposed(by: disposeBag)
        */
                
        disposeBag.insertAll(all:
            // Based On Ride Mode
            basedOnRideModeSwitch.rx.isOn.changed.subscribe({ theSwitch in
                viewModel.onBasedOnRideModeChanged(isOn: theSwitch.element!)
            }),
            
            viewModel.basedOnRideModeEnabled.subscribe({ [weak self] isOn in
                guard let strongSelf = self else { return }
                if let isOn = isOn.element {
                    strongSelf.basedOnRideModeSwitch.isOn = isOn
                    strongSelf.rideModeColorSegmentedControl.isEnabled = !isOn
                    if isOn {
                        strongSelf.rideModeColorSegmentedControl.tintColor =
                            ColorUtil.uiColor(.buttonDisabledBackground)
                    } else {
                        strongSelf.rideModeColorSegmentedControl.tintColor =
                            ColorUtil.uiColor(.yellowText)
                    }
                }
            }),
            
            // Ride Mode Color
            rideModeColorSegmentedControl.rx.selectedSegmentIndex.changed.subscribe({ event in
                viewModel.onRideModeColorChanged(idx: event.element!)
            }),

            viewModel.rideModeColor.bind(to: rideModeColorSegmentedControl.rx.selectedSegmentIndex),
            
            // Gauges
            viewModel.gaugeAName.bind(to: gaugeAName.rx.text),
            viewModel.gaugeBName.bind(to: gaugeBName.rx.text),
            viewModel.gaugeCName.bind(to: gaugeCName.rx.text),
            viewModel.gaugeDName.bind(to: gaugeDName.rx.text),
            
            // Settings Close
            backButton.rx.tap.subscribe { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.navigationController?.popViewController(animated: true)
            },
            
            // Quadrants
            quadrantAButton.rx.tap.subscribe({ _ in viewModel.quadrantAPressed() }),
            quadrantBButton.rx.tap.subscribe({ _ in viewModel.quadrantBPressed() }),
            quadrantCButton.rx.tap.subscribe({ _ in viewModel.quadrantCPressed() }),
            quadrantDButton.rx.tap.subscribe({ _ in viewModel.quadrantDPressed() }),
            
            quadrantARow.rx.tap.subscribe({ _ in viewModel.quadrantAPressed() }),
            quadrantBRow.rx.tap.subscribe({ _ in viewModel.quadrantBPressed() }),
            quadrantCRow.rx.tap.subscribe({ _ in viewModel.quadrantCPressed() }),
            quadrantDRow.rx.tap.subscribe({ _ in viewModel.quadrantDPressed() }),
            
            viewModel.onQuadrantPressed.subscribe({ [weak self] quadrant in
                guard let strongSelf = self else { return }
                guard let value = quadrant.element else { return }
                
                UILauncher.launchGaugePicker(strongSelf, quadrant: value, delegate: viewModel)
            }),
            
            viewModel.onUpdateStart.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showSyncActivity(true)
                strongSelf.syncView.showSyncUnsuccessful(false)
                strongSelf.view.addSubview(strongSelf.syncView)
            },
            
            viewModel.onUpdateComplete.subscribe { [weak self] event in
                guard let strongSelf = self, let completed = event.element else { return }
                if completed {
                    strongSelf.syncView.removeFromSuperview()
                } else {
                    strongSelf.syncView.showSyncUnsuccessful(true)
                }
            },
            
            viewModel.onQueueComplete.subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.syncView.showQueued(true)
            }
        )
        
        viewModel.loadModel()
    }
}
