//
//  GaugePickerViewController.swift
//  nextgen
//
//  Created by David Crawford on 6/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import CocoaLumberjack
import RxCocoa
import RxSwift

protocol GaugePickerDelegate {
    func gaugeSelected(quadrant: DashboardGaugeQuadrant, gauge: DashboardGauge)
}

final class GaugePickerViewController: ZeroUIViewController {
    override var screenName: String { return Constant.ANALYTICS_SCREEN_GAUGE_PICKER }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var quadrantName: ZeroUpperCaseUILabel!
    @IBOutlet weak var currentGaugeName: ZeroUpperCaseUILabel!
    
    var pickerDelegate: GaugePickerDelegate?
    
    var quadrant: DashboardGaugeQuadrant?
    var gaugePickerViewModel: GaugePickerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let quadrant = quadrant else { return }
        
        let cellId = GaugePickerTableViewCell.ReuseIdentifier
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        
        disposeBag.insertAll(all:
            gaugePickerViewModel.quadrantName.bind(to: quadrantName.rx.text),
            gaugePickerViewModel.currentGaugeName.bind(to: currentGaugeName.rx.text),
        
            gaugePickerViewModel.dataSource
                .bind(to: tableView.rx.items(cellIdentifier: cellId, cellType:GaugePickerTableViewCell.self)) { (row, gaugeViewModel, cell) in
                    cell.configure(withViewModel: gaugeViewModel)
            },
            
            tableView.rx.modelSelected(GaugeViewModel.self).subscribe({ [weak self] viewModel in
                guard let strongSelf = self else { return }
                if let gaugeViewModel = viewModel.element {
                    strongSelf.pickerDelegate?.gaugeSelected(quadrant: quadrant, gauge: gaugeViewModel.gauge)
                    strongSelf.close()
                }
            })
        )
        
        gaugePickerViewModel.loadModel(quadrant: quadrant)
    }
    
    @IBAction func onClosePressed(_ sender: Any) {
        close()
    }
    
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
}
