//
//  ZeroUINavigationItem.swift
//  nextgen
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUINavigationItem: UINavigationItem {
    var zeroRightBarButtonItem: ZeroRightUIBarButtonItem?
    
    override var title: String? {
        didSet {
            self.setupCustomTitleView(self.title)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCustomTitleView(self.title)
    }

    override init(title: String) {
        super.init(title: title)
        self.setupCustomTitleView(self.title)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupCustomTitleView(self.title)
    }

    func setupCustomTitleView(_ title: String?) {
        let logo = UIImage(named: "Branding")
        let imageView = UIImageView(image:logo)
        self.titleView = imageView
        
        zeroRightBarButtonItem = ZeroRightUIBarButtonItem()
        rightBarButtonItem = zeroRightBarButtonItem
    }

    func titleForDisplayOnly(_ title: String) {
        self.title = " "
        self.setupCustomTitleView(title)
    }
}
