//
//  ZeroUITableViewCell.swift
//  nextgen
//
//  Created by David Crawford on 8/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class ZeroUITableViewCell: UITableViewCell {
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if (highlighted) {
            contentView.backgroundColor = ColorUtil.color(ColorUtil.AppColors.DarkGray)
        } else {
            contentView.backgroundColor = UIColor.clear
        }
    }
}
