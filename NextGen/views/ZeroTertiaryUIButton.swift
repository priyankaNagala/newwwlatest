//
//  ZeroTertiaryUIButton.swift
//  nextgen
//
//  Created by David Crawford on 6/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class ZeroTertiaryUIButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    func setupView () {
        self.setTitle(self.titleLabel?.text, for: .normal)
        self.clipsToBounds = true;
        self.layer.cornerRadius = 8.0
        self.adjustsImageWhenHighlighted = false
        self.layer.borderWidth = 1.0
        self.layer.borderColor = ColorUtil.uiColor(.tertiaryButtonBorder).cgColor
    }
    
    override func setTitle(_ title: String?, for state: UIControlState) {
        if title != nil {
            let buttonFont = FontUtil.uiFont(AppUIFonts.buttonTitle)
            
            let textAttributes: [NSAttributedStringKey : AnyObject] = [.font: buttonFont,
                                                                       .foregroundColor: currentTitleColor]
            let attributedTitle = NSAttributedString.init(string: title!, attributes: textAttributes)
            self.setAttributedTitle(attributedTitle, for: state)
            
            let disabledTextAttributes: [NSAttributedStringKey : AnyObject] = [.font: buttonFont,
                                                                               .foregroundColor: currentTitleColor]
            let disabledAttributedTitle = NSAttributedString.init(string: title!, attributes: disabledTextAttributes)
            self.setAttributedTitle(disabledAttributedTitle, for: .disabled)
        }
    }
    
}
