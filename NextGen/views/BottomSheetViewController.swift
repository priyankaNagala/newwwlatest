//
//  BottomSheetViewController.swift
//  nextgen
//
//  Created by Scott Wang on 12/16/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

// https://github.com/AhmedElassuty/BottomSheetController

class BottomSheetViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var handleView: UIView!
    @IBOutlet weak var scrollView: UIScrollView?
    
    var fullView: CGFloat = 100
    var partialView: CGFloat {
        return UIScreen.main.bounds.height - 200
    }
    var autoOpenClose: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Top Handle
        handleView.layer.cornerRadius = 2.5
        view.layer.cornerRadius = 8.0
        
        // Gesture
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
        })
    }
    
    // MARK: -

    @objc func panGesture(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: view)
        let y = view.frame.minY
        if ( y + translation.y >= fullView) && (y + translation.y <= partialView ) {
            view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: view)
        }
        
        // Auto expand/collapse
        if autoOpenClose {
            let velocity = recognizer.velocity(in: view)
            if recognizer.state == .ended {
                var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
                
                duration = duration > 1.3 ? 1 : duration
                
                UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                    if  velocity.y >= 0 {
                        self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                        self.scrollView?.isScrollEnabled = false
                    } else {
                        self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                        self.scrollView?.isScrollEnabled = true
                    }
                    
                }, completion: nil)
            }
        }
    }

    // MARK: - Gesture Recognizer Delegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let scrollView = self.scrollView else { return false }
        
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = view.frame.minY
        if (y == fullView && scrollView.contentOffset.y == 0 && direction > 0) || (y == partialView) {
            scrollView.isScrollEnabled = false
        } else {
            scrollView.isScrollEnabled = true
        }
        
        return false
    }
}
