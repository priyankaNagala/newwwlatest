//
//  ZeroUISlider.swift
//  nextgen
//
//  Created by Scott Wang on 8/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUISlider : UISlider {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupView()
    }
    
    func setupView () {
        setMinimumTrackImage(UIImage.from(color: ColorUtil.color(.Orange)), for: .disabled)
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                setThumbImage(nil, for: .normal)
            } else {
                setThumbImage(UIImage(), for: .normal)
                alpha = 1.0
            }
        }
    }
}
