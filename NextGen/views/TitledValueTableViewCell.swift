//
//  TitledValueTableViewCell.swift
//  nextgen
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class TitledValueTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    static let ReuseIdentifier: String = "TitledValueTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = ColorUtil.color(.BackgroundBlack)
    }
}
