//
//  MapMarkerView.swift
//  nextgen
//
//  Created by Scott Wang on 12/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class MapMarkerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var label: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView () {
        Bundle.main.loadNibNamed("MapMarkerView", owner: self, options: nil)
        addSubview(contentView)

        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
