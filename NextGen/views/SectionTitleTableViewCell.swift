//
//  SectionTitleTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 3/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class SectionTitleTableViewCell: UITableViewCell {

    static let ReuseIdentifier = "SectionTitleTableViewCell"
    
    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
}
