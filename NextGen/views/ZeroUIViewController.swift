//
//  ZeroUIViewController.swift
//  nextgen
//
//  Created by Scott Wang on 3/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import UIKit
import RxSwift
import KYDrawerController

class ZeroUIViewController: UIViewController {
    var analyticsService: ZeroAnalyticsService?
    var zeroMotorcycleService: ZeroMotorcycleService?
    var screenName: String { return self.description }
    var disposeBag = DisposeBag()
    
    var isPluggedIn = false
    var isCharging  = false
    var isConnected = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        analyticsService?.trackScreenView(viewController: self, screenName: screenName)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isConnected = zeroMotorcycleService?.isConnected() ?? false
        if (isConnected) {
            onMotorcycleConnect()
        } else {
            onMotorcycleDisconnect()
        }
        
        zeroMotorcycleService?
            .getConnectionState()
            .subscribe({ [weak self] connected in
                guard let strongSelf = self else { return }
                if let connected = connected.element {
                    strongSelf.isConnected = connected
                    connected ? strongSelf.onMotorcycleConnect() : strongSelf.onMotorcycleDisconnect()
                }
            })
            .disposed(by: disposeBag)
        
        zeroMotorcycleService?
            .getBikeInfo()
            .subscribe({ [weak self] bikeInfoPacket in
                guard let strongSelf = self else { return }
                guard let bikeInfoPacket = bikeInfoPacket.element else { return }
                let rightBarButtonItem = (strongSelf.navigationItem as? ZeroUINavigationItem)?.zeroRightBarButtonItem

                let packetPluggedIn = bikeInfoPacket.isPluggedIn()
                strongSelf.isPluggedIn = packetPluggedIn
                rightBarButtonItem?.pluggedIn = packetPluggedIn
                strongSelf.onMotorcyclePluggedChange(isPluggedIn: packetPluggedIn)

                let packetIsCharging = bikeInfoPacket.isCharging()
                strongSelf.isCharging = packetIsCharging
                rightBarButtonItem?.charging = packetIsCharging
                strongSelf.onMotorcycleChargingChange(isCharging: packetIsCharging)                
            })
            .disposed(by: disposeBag)
        
        zeroMotorcycleService?.offlineQueueIsEmpty().subscribe { [weak self] event in
            guard let strongSelf = self, let empty = event.element else { return }
            let rightBarButtonItem = (strongSelf.navigationItem as? ZeroUINavigationItem)?.zeroRightBarButtonItem
            rightBarButtonItem?.syncPending = !empty
        }.disposed(by: disposeBag)
    }

    @IBAction func hamburgerPressed(_ sender: Any) {
        openDrawer()
    }
    
    // MARK: -
    func openDrawer() {
        if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    func closeDrawer() {
        if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? KYDrawerController {
            drawerController.setDrawerState(.closed, animated: true)
        }
    }
    
    // MARK: -
    // If derived class overrides, it must call super
    func onMotorcycleConnect() {
        if let navItem = navigationItem as? ZeroUINavigationItem {
            navItem.zeroRightBarButtonItem?.connected = true
        }
    }
    
    // If derived class overrides, it must call super
    func onMotorcycleDisconnect() {
        if let navItem = navigationItem as? ZeroUINavigationItem {
            navItem.zeroRightBarButtonItem?.connected = false
            navItem.zeroRightBarButtonItem?.pluggedIn = false
            navItem.zeroRightBarButtonItem?.charging = false
        }
    }
    
    func onMotorcycleChargingChange(isCharging: Bool) {
        // Base class implementation does nothing
    }
    
    func onMotorcyclePluggedChange(isPluggedIn: Bool) {
        // Base class implementation does nothing
    }
}
