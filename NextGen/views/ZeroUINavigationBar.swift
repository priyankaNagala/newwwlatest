//
//  ZeroUINavigationBar.swift
//  nextgen
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUINavigationBar: UINavigationBar {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }

    func setupView() {
    }
}
