//
//  ZeroRightUIBarButtonItem.swift
//  nextgen
//
//  Created by Scott Wang on 8/30/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroRightUIBarButtonItem: UIBarButtonItem {

    var buttonView: ZeroRightUIBarButtonUIView = ZeroRightUIBarButtonUIView.init(frame: CGRect.init(x: 0, y: 0, width: 62, height: 40))

    var connected: Bool = false {
        didSet {
            buttonView.connected = connected
        }
    }
    
    var syncing: Bool = false {
        didSet {
            buttonView.syncing = syncing
        }
    }
    
    var syncPending: Bool = false {
        didSet {
            buttonView.syncPending = syncPending
        }
    }
    
    var pluggedIn: Bool = false {
        didSet {
            buttonView.pluggedIn = pluggedIn
        }
    }
    
    var charging: Bool = false {
        didSet {
            buttonView.charging = charging
        }
    }
    
    var scheduledCharging: Bool = false {
        didSet {
            buttonView.scheduledCharging = scheduledCharging
        }
    }
    
    override init() {
        super.init()
        customView = buttonView
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customView = buttonView
    }
}
