//
//  ZeroRightUIBarButtonUIView.swift
//  nextgen
//
//  Created by Scott Wang on 8/30/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroRightUIBarButtonUIView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var decoratorImageView: UIImageView!
    @IBOutlet weak var bluetoothImageView: UIImageView!
    @IBOutlet weak var plugImageView: UIImageView!
    @IBOutlet weak var chargeImageView: UIImageView!

    var connected: Bool = false {
        didSet {
            if connected {
                bluetoothImageView.tintColor = ColorUtil.color(.Blue)
            } else {
                bluetoothImageView.tintColor = ColorUtil.color(.LightGray)
            }
        }
    }
    
    var syncing: Bool = false {
        didSet {
            if syncing {
                UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
                    self.bluetoothImageView.alpha = 0.25
                }, completion: nil)
            } else {
                bluetoothImageView.layer.removeAllAnimations()
            }
        }
    }
    
    var syncPending: Bool = false {
        didSet {
            decoratorImageView.isHidden = !syncPending
        }
    }
    
    private let scheduledChargeImage = UIImage(named: "ic_plugcharge_scheduledcharge_inactive")?.withRenderingMode(.alwaysTemplate)
    private let plugChargeImage =  UIImage(named: "ic_plugcharge_charge_inactive")?.withRenderingMode(.alwaysTemplate)
    
    var pluggedIn: Bool = false {
        didSet {
            updatePlugImageView()
        }
    }
    
    var charging: Bool = false {
        didSet {
            updateChargingImageView()
        }
    }
    
    var scheduledCharging: Bool! = false {
        didSet {
            updateChargingImageView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView () {
        Bundle.main.loadNibNamed("ZeroRightUIBarButtonUIView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        decoratorImageView.image = UIImage(named: "ic_bluetooth_decorator")?.withRenderingMode(.alwaysTemplate)
        decoratorImageView.tintColor = ColorUtil.color(.Yellow)
        decoratorImageView.isHidden = true
        bluetoothImageView.image = UIImage(named: "Bluetooth")?.withRenderingMode(.alwaysTemplate)
        bluetoothImageView.tintColor = ColorUtil.color(.LightGray)

        plugImageView.image = UIImage(named: "ic_plugcharge_plug_inactive")?.withRenderingMode(.alwaysTemplate)
        plugImageView.tintColor = ColorUtil.color(.LightGray)
        chargeImageView.image = UIImage(named: "ic_plugcharge_charge_inactive")?.withRenderingMode(.alwaysTemplate)
        chargeImageView.tintColor = ColorUtil.color(.LightGray)
    }

    private func updatePlugImageView() {
        if pluggedIn {
            plugImageView.tintColor = ColorUtil.color(.Green)
        } else {
            plugImageView.tintColor = ColorUtil.color(.LightGray)
        }
    }
    
    private func updateChargingImageView() {
        if scheduledCharging {
            chargeImageView.image = scheduledChargeImage
        } else {
            chargeImageView.image = plugChargeImage
        }
        
        if charging {
            chargeImageView.tintColor = ColorUtil.color(.Green)
            UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
                self.chargeImageView.alpha = 0.25
            }, completion: nil)
        } else {
            chargeImageView.tintColor = scheduledCharging ? ColorUtil.color(.Yellow) : ColorUtil.color(.LightGray)
        }
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        if let viewController = UIApplication.shared.keyWindow!.rootViewController {
            UILauncher.launchConnectionStatus(viewController)
        }
    }
}
