//
//  ZeroSecondaryUIButton.swift
//  nextgen
//
//  Created by Scott Wang on 2/22/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroSecondaryUIButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)        
        self.setupView()
    }
    
    func setupView () {
        self.setTitle(self.titleLabel?.text, for: .normal)
        self.clipsToBounds = true;
        self.layer.cornerRadius = 8.0
        self.adjustsImageWhenHighlighted = false
        self.setBackgroundImage(ImageUtil.imageWithColor(ColorUtil.uiColor(.buttonDisabledBackground), size: CGSize.init(width: 1, height: 1)), for: .disabled)
        self.setBackgroundImage(ImageUtil.imageWithColor(ColorUtil.color(.BackgroundBlack), size: CGSize.init(width: 1, height: 1)), for: .normal)
        self.layer.borderWidth = 1.0
        self.layer.borderColor = ColorUtil.uiColor(.secondaryButtonBorder).cgColor
    }
    
    override func setTitle(_ title: String?, for state: UIControlState) {
        if title != nil {
            let buttonFont = FontUtil.uiFont(AppUIFonts.buttonTitle)
            
            let textAttributes: [NSAttributedStringKey : AnyObject] = [.font: buttonFont,
                                                                       .foregroundColor: ColorUtil.uiColor(AppUIColors.secondaryButtonTitle)]
            let attributedTitle = NSAttributedString.init(string: title!, attributes: textAttributes)
            self.setAttributedTitle(attributedTitle, for: state)
            
            let disabledTextAttributes: [NSAttributedStringKey : AnyObject] = [.font: buttonFont,
                                                                               .foregroundColor: ColorUtil.uiColor(AppUIColors.buttonTitle)]
            let disabledAttributedTitle = NSAttributedString.init(string: title!, attributes: disabledTextAttributes)
            self.setAttributedTitle(disabledAttributedTitle, for: .disabled)
        }
    }

}
