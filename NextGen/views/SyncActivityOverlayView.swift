//
//  SyncActivityOverlayView.swift
//  nextgen
//
//  Created by Scott Wang on 1/27/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import UIKit

protocol SyncActivityOverlayViewDelegate {
    func toastDismissed()
}

class SyncActivityOverlayView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var toastView: UIView!
    @IBOutlet weak var syncChangeView: UIView!
    @IBOutlet weak var toastLabel: UILabel!
    
    var delegate: SyncActivityOverlayViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    fileprivate func setupView () {
        Bundle.main.loadNibNamed("SyncActivityOverlayView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func showSyncActivity(_ show: Bool) {
        syncChangeView.isHidden = !show
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func showSyncUnsuccessful(_ show: Bool) {
        if show { // Sync already unsuccessful, hide activity
            showSyncActivity(false)
        }
        toastLabel.text = "global_sync_unsuccessful".localized
        toastView.isHidden = !show
    }
    
    func showQueued(_ show: Bool) {
        if show {
            showSyncActivity(false)
        }
        toastLabel.text = "global_changes_queued".localized
        toastView.isHidden = !show
    }
    

    // Dismiss if unsuccessful toast is shown
    @IBAction func onViewTapped(_ sender: Any) {
        if !toastView.isHidden {
            self.removeFromSuperview()
        }
        delegate?.toastDismissed()
    }
}
