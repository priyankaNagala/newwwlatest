//
//  SelectableBackgroundButton.swift
//  nextgen
//
//  Created by David Crawford on 8/6/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

final class SelectableBackgroundButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    var selectedBgColor: UIColor = UIColor.black.withAlphaComponent(0.2)
    
    @IBInspectable
    var normalBgColor: UIColor = UIColor.clear
    
    override var isSelected: Bool {
        didSet {
            self.backgroundColor = isSelected ? selectedBgColor : normalBgColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.backgroundColor = isHighlighted ? selectedBgColor : normalBgColor
        }
    }
}
