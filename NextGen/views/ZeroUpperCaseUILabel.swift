//
//  ZeroUpperCaseUILabel.swift
//  nextgen
//
//  Created by Scott Wang on 7/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUpperCaseUILabel: UILabel {

    override var text: String? {
        didSet {
           super.text = text?.uppercased()
        }
    }
}
