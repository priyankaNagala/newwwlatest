//
//  ZeroTabViewController.swift
//  nextgen
//
//  Created by Scott Wang on 10/10/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroTabViewController: ZeroUIViewController {

    var connectionService: ConnectionService?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        connectionService?.resetBackoff()
    }
}
