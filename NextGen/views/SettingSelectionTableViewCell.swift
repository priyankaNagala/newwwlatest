//
//  SettingSelectionTableViewCell.swift
//  nextgen
//
//  Created by Scott Wang on 8/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

protocol SettingSelectionViewModel {
    func isSelected() -> Bool
    func title() -> String
}

class SettingSelectionTableViewCell: UITableViewCell {
    static let ReuseIdentifier = "SettingSelectionTableViewCell"

    @IBOutlet weak var titleLabel: ZeroUpperCaseUILabel!
    @IBOutlet weak var checkIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareForReuse()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        checkIconImageView.isHidden = true
        titleLabel.text = ""
    }
    
    func showCheckMark(_ show: Bool) {
        checkIconImageView.isHidden = !show
        titleLabel.textColor = show ? ColorUtil.color(.LightGray) : ColorUtil.color(.White)
    }
    
    func configure(_ viewModel: SettingSelectionViewModel) {
        titleLabel.text = viewModel.title()
        showCheckMark(viewModel.isSelected())
    }
}
