//
//  ZeroUISegmentedControl.swift
//  nextgen
//
//  Created by Scott Wang on 8/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUISegmentedControl: UISegmentedControl {

    @IBInspectable var multiLine: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupView()
    }
    
    func setupView () {
        setTitleTextAttributes([NSAttributedStringKey.font : FontUtil.uiFont(.segmentedControlTitle)], for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if multiLine {
            for segment in subviews {
                for view in segment.subviews {
                    if view.isKind(of: UILabel.self) {
                        let label = view as! UILabel
                        label.numberOfLines = 0
                        label.lineBreakMode = .byWordWrapping
                        label.adjustsFontSizeToFitWidth = true
                    }
                }
            }
        }
    }
}
