//
//  ZeroUIButtonOversized.swift
//  nextgen
//
//  Created by David Crawford on 8/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ZeroUIButtonOversized: ZeroUIButton {
    
    override func getFont() -> UIFont {
        return FontUtil.uiFont(AppUIFonts.buttonTitleOversized)
    }
}
