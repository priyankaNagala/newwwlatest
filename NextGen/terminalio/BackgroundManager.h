//
//  BackgroundManager.h
//  TerminalIO
//
//  Created by Stollmann E+V GmbH
//  Copyright (c) 2013,2014 Stollmann E+V GmbH
//

#import <Foundation/Foundation.h>


@interface BackgroundManager : NSObject

+ (BackgroundManager *)sharedInstance;

- (void)handleAppDidEnterBackground;
- (void)handleAppWillEnterForeground;

@end
