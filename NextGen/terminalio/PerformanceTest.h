//
//  PerformanceTest.h
//  TerminalIO
//
//  Created by Telit
//  Copyright (c) Telit Wireless Solutions GmbH, Germany
//

#import <Foundation/Foundation.h>
#import "TIOPeripheral.h"


@class PerformanceTest;


@protocol PerformanceTestDelegate <NSObject>

- (void)performanceTest:(PerformanceTest *)test requestsSendingOfData:(NSData *)data;
@optional
- (void)performanceTestDidChangeActivity:(PerformanceTest *)test;

@end


@interface PerformanceTest : NSObject

@property	(weak, nonatomic) NSObject<PerformanceTestDelegate	> *delegate;

@property (nonatomic, readonly) NSString *formattedSendRunTime;
@property (nonatomic, readonly) NSNumber *bytesSent;
@property (nonatomic, readonly) NSNumber *sendRate;

@property (nonatomic, readonly) NSString *formattedReceiveRunTime;
@property (nonatomic, readonly) NSNumber *bytesReceived;
@property (nonatomic, readonly) NSNumber *receiveRate;

@property	(nonatomic, readonly) BOOL isSending;
@property	(nonatomic, readonly) BOOL isReceiving;

@property	(nonatomic) BOOL backgroundMode;

- (void)startSend:(NSNumber *)mtuSize;
- (void)stopSend;
- (void)startReceive;
- (void)stopReceive;

- (void)handleAppDidEnterBackground;
- (void)handleAppWillEnterForeground;

- (void)handleNumberOfBytesWritten:(NSInteger)numberOfBytes;
- (void)handleUARTWriteBufferEmpty;
- (void)handleDataReceived:(NSData *)data;

@end
