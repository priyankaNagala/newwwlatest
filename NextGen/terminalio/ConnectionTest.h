//
//  ConnectionTest.h
//  TerminalIO
//
//  Created by Stollmann E+V GmbH
//  Copyright (c) 2013,2014 Stollmann E+V GmbH
//

#import <Foundation/Foundation.h>
#import "TIOPeripheral.h"


@class ConnectionTest;


@protocol ConnectionTestDelegate <NSObject>

- (void)connectionTestRequestsConnect:(ConnectionTest *)test;
- (void)connectionTestRequestsDisconnect:(ConnectionTest *)test;
@optional
- (void)connectionTestDidChangeActivity:(ConnectionTest *)test;

@end


@interface ConnectionTest : NSObject

@property	(weak, nonatomic) NSObject<ConnectionTestDelegate> *delegate;

@property (nonatomic, readonly) NSString *formattedRunTime;
@property (nonatomic, readonly) NSUInteger connectionCount;
@property (nonatomic, readonly) NSNumber *connectionIntervalArray;
@property (nonatomic, readonly) NSNumber *maxConnectionInterval;
@property (nonatomic, readonly) NSNumber *minConnectionInterval;
@property (nonatomic, readonly) NSNumber *averageConnectionInterval;
@property (nonatomic) NSNumber *reconnectInterval;

@property	(nonatomic, readonly) BOOL isRunning;

- (void)start;
- (void)stop;
- (void)writeToFile:(NSString *)path;

- (void)handleConnected;
- (void)handleDisconnected;

@end
