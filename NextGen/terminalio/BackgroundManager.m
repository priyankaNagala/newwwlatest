//
//  BackgroundManager.m
//  TerminalIO
//
//  Created by Stollmann E+V GmbH
//  Copyright (c) 2013,2014 Stollmann E+V GmbH
//

#import "BackgroundManager.h"
#import "TIOManager.h"
#import "PerformanceTest.h"
#import "TIOPeripheral.h"


@implementation BackgroundManager


+ (BackgroundManager *)sharedInstance
{
	static __strong BackgroundManager *_sharedInstance = nil;
	if (!_sharedInstance)
	{
		_sharedInstance = [[BackgroundManager alloc] init];
	}
	return _sharedInstance;
}


- (void)handleAppDidEnterBackground
{
	for (TIOPeripheral *peripheral in [TIOManager sharedInstance].peripherals)
	{
		if (peripheral.tag == nil)
			continue;
		
		PerformanceTest *test = (PerformanceTest *)peripheral.tag;
		[test handleAppDidEnterBackground];
	}
}


- (void)handleAppWillEnterForeground
{
	for (TIOPeripheral *peripheral in [TIOManager sharedInstance].peripherals)
	{
		if (peripheral.tag == nil)
			continue;
		
		PerformanceTest *test = (PerformanceTest *)peripheral.tag;
		[test handleAppWillEnterForeground];
	}
}


@end
