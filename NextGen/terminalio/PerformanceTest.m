//
//  PerformanceTest.m
//  TerminalIO
//
//  Created by Telit
//  Copyright (c) Telit Wireless Solutions GmbH, Germany
//

#import "PerformanceTest.h"
#import "STTrace.h"


@interface PerformanceTest()

@property (strong, nonatomic) NSDate *sendStartTime;
@property (strong, nonatomic) NSDate *sendStopTime;
@property	(nonatomic) NSTimeInterval sendRunTime;
@property (strong, nonatomic) NSNumber *unconfirmedBytes;
@property (strong, nonatomic) NSNumber *remoteMtuSize;

@property (strong, nonatomic) NSDate *receiveStartTime;
@property (strong, nonatomic) NSDate *receiveStopTime;
@property	(nonatomic) NSTimeInterval receiveRunTime;

@property	(nonatomic) BOOL startSendPending;
@property	(nonatomic) BOOL startReceivePending;

@end


@implementation PerformanceTest

static __strong const NSString *TestDataLine = @"abcdefghijklmnopqrstuvwxyz 0123456789 ~";
static NSUInteger _testDataLineCounter = 0;
static NSUInteger _testDataLineLastIndex = 0;

- (PerformanceTest *)init
{
	self = [super init];
	if (self)
	{
		self.sendStartTime = self.sendStopTime = self.receiveStartTime = self.receiveStopTime = [NSDate date];
		self.bytesSent = self.unconfirmedBytes = self.bytesReceived = [NSNumber numberWithInt:0];
		self.isSending = self.isReceiving = NO;
	}
	return self;
}


- (void)setBytesSent:(NSNumber *)bytesSent
{
	self->_bytesSent = bytesSent;
}


- (NSNumber *)sendRate
{
	if (self.sendRunTime == 0)
		return [NSNumber numberWithFloat:0];
	return ([NSNumber numberWithDouble:(self.bytesSent.unsignedIntValue * 8.0 / 1000.0 / self.sendRunTime)]);
}


- (NSTimeInterval)sendRunTime
{
	if (self.isSending && !self.startSendPending)
		return [self.sendStartTime timeIntervalSinceNow] * -1;
	else
		return [self.sendStartTime timeIntervalSinceDate:self.sendStopTime] * -1;
}


- (NSString *)formattedSendRunTime
{
	if (self.isSending && !self.startSendPending)
		return [self formatTimeFrom: self.sendStartTime to: [NSDate date]];
	else
		return [self formatTimeFrom: self.sendStartTime to: self.sendStopTime];
}


- (void)setBytesReceived:(NSNumber *)bytesReceived
{
	self->_bytesReceived = bytesReceived;
}


- (NSNumber *)receiveRate
{
	if (self.receiveRunTime == 0)
		return [NSNumber numberWithFloat:0];
	return ([NSNumber numberWithDouble:(self.bytesReceived.unsignedIntValue * 8.0 / 1000.0 / self.receiveRunTime)]);
}


- (NSTimeInterval)receiveRunTime
{
	if (self.isReceiving && !self.startReceivePending)
		return [self.receiveStartTime timeIntervalSinceNow] * -1;
	else
		return [self.receiveStartTime timeIntervalSinceDate:self.receiveStopTime] * -1;
}


- (NSString *)formattedReceiveRunTime
{
	if (self.isReceiving && !self.startReceivePending)
		return [self formatTimeFrom: self.receiveStartTime to: [NSDate date]];
	else
		return [self formatTimeFrom: self.receiveStartTime to: self.receiveStopTime];
}


- (void)setIsSending:(BOOL)isSending
{
	self->_isSending = isSending;
}


- (void)setIsReceiving:(BOOL)isReceiving
{
	self->_isReceiving = isReceiving;
}



#pragma mark - Public methods

- (void)startSend:(NSNumber *)mtuSize
{
	if (self.isSending)
		return;

    self.remoteMtuSize = mtuSize;
    
	self.isSending	 = YES;
	[self raiseDidChangeActivity];

	if (!self.backgroundMode)
	{
		[self doStartSend];
	}
	else
	{
		self.startSendPending	= YES;
		self.sendStartTime = self.sendStopTime = [NSDate date];
		self.bytesSent = self.unconfirmedBytes = [NSNumber numberWithInt:0];
	}
}


- (void)doStartSend
{
	self.startSendPending = NO;
	self.sendStartTime = self.sendStopTime = [NSDate date];
	self.bytesSent = self.unconfirmedBytes = [NSNumber numberWithInt:0];
	[self sendNextTestData];
}


- (void)stopSend
{
	self.isSending = NO;
	self.startSendPending = NO;
	[self raiseDidChangeActivity];
	self.sendStopTime = [NSDate date];
}


- (void)startReceive
{
	if (self.isReceiving)
		return;
	
	self.isReceiving	 = YES;
	[self raiseDidChangeActivity];
	
	if (!self.backgroundMode)
	{
		[self doStartReceive];
	}
	else
	{
		self.startReceivePending	= YES;
		self.receiveStartTime = self.receiveStopTime = [NSDate date];
		self.bytesReceived = [NSNumber numberWithInt:0];
	}
}


- (void)doStartReceive
{
	self.startReceivePending = NO;
	self.receiveStartTime = self.receiveStopTime = [NSDate date];
	self.bytesReceived = [NSNumber numberWithInt:0];
}


- (void)stopReceive
{
	self.isReceiving = NO;
	self.startReceivePending = NO;
	[self raiseDidChangeActivity];
	self.receiveStopTime = [NSDate date];
}


- (void)handleAppDidEnterBackground
{
	STTraceMethod(self, @"handleAppDidEnterBackground");
	
	if (!self.backgroundMode)
		return;

	if (self.startSendPending)
		[self doStartSend];
	if (self.startReceivePending)
		[self doStartReceive];
}


- (void)handleAppWillEnterForeground
{
	STTraceMethod(self, @"handleAppWillEnterForeground");
	
	if (!self.backgroundMode)
		return;

	[self stopSend];
	[self stopReceive];
}


- (void)handleNumberOfBytesWritten:(NSInteger)numberOfBytes
{
	if (!self.isSending)
		return;
	
	self.bytesSent = [NSNumber numberWithUnsignedInteger:(self.bytesSent.unsignedIntValue + numberOfBytes)];
	self.unconfirmedBytes = [NSNumber numberWithUnsignedInteger:(self.unconfirmedBytes.unsignedIntValue - numberOfBytes)];
	[self sendNextTestData];
}


- (void)handleUARTWriteBufferEmpty
{
	if (!self.isSending)
		return;
	
//	[self sendNextTestData];
}


- (void)handleDataReceived:(NSData *)data
{
	if (!self.isReceiving)
		return;
	
	self.bytesReceived = [NSNumber numberWithUnsignedInteger:(self.bytesReceived.unsignedIntValue + data.length)];
}



#pragma mark - Internal methods

- (NSString *)formatTimeFrom:(NSDate *)fromTime to:(NSDate *)toTime
{
	NSUInteger flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
	NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:flags fromDate:fromTime toDate:toTime options:0];
	return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)conversionInfo.hour, (long)conversionInfo.minute, (long) conversionInfo.second];
}


- (void)sendNextTestData
{
	NSString *testString = @"";
	while (self.unconfirmedBytes.unsignedIntValue < 100)
	{
		testString = [testString stringByAppendingString:[self nextTestDataLine]];
		self.unconfirmedBytes = [NSNumber numberWithUnsignedInteger:(self.unconfirmedBytes.unsignedIntValue + testString.length)];
	}
	if (testString.length == 0)
		return;
	
	NSData *testData = [PerformanceTest stringToData:testString];

	[self raiseRequestsSendingOfData:testData];
}


- (NSString *)nextTestDataLine
{
    NSString *testDataLine = @"";
    int mtuSize = (int)_remoteMtuSize.integerValue - 11;
    int testDataLineLength = (int)TestDataLine.length;
    int testDataLineIndex = (int)_testDataLineLastIndex + 1;
    
    if(_remoteMtuSize.integerValue != TIO_MAX_UART_DATA_SIZE)
    {
        testDataLine = [testDataLine stringByAppendingString:[NSString stringWithFormat:@"%08lu ", (unsigned long)++_testDataLineCounter]];
        
        while (mtuSize--)
        {
            _testDataLineLastIndex = (testDataLineIndex%testDataLineLength);
            
            testDataLine = [testDataLine stringByAppendingString:[TestDataLine substringWithRange:NSMakeRange(_testDataLineLastIndex, 1)]];
            
            testDataLineIndex++;
        }
        
        testDataLine = [testDataLine stringByAppendingString:[NSString stringWithFormat:@"\r\n"]];
    }
    else
    {
        testDataLine = [NSString stringWithFormat:@"%08lu %@\r\n", (unsigned long)++_testDataLineCounter, TestDataLine];
    }
    
	return testDataLine;
}



#pragma mark - Delegate events

- (void)raiseRequestsSendingOfData:(NSData *)data
{
	if ([self.delegate respondsToSelector:@selector(performanceTest:requestsSendingOfData:)])
		[self.delegate performanceTest:self requestsSendingOfData:data];
}


- (void)raiseDidChangeActivity
{
	if ([self.delegate respondsToSelector:@selector(performanceTestDidChangeActivity:)])
		[self.delegate performanceTestDidChangeActivity:self];
}


#pragma mark - Utilities

+ (NSString *)dataToString:(NSData *)data
{
	return [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSASCIIStringEncoding];
}


+ (NSData *)stringToData:(NSString *)string
{
	return [string dataUsingEncoding:NSASCIIStringEncoding];
}



@end
