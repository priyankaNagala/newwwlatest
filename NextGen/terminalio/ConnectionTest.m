//
//  ConnectionTest.m
//  TerminalIO
//
//  Created by Telit
//  Copyright (c) Telit Wireless Solutions GmbH, Germany
//

#import "ConnectionTest.h"


@interface ConnectionTest()

@property (strong, nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSDate *stopTime;
@property	(nonatomic, readonly) NSTimeInterval runTime;

@property (strong, nonatomic) NSDate *connectionInitiatedTime;
@property (nonatomic) NSMutableArray *connectionIntervals;
@property	(nonatomic) NSNumber *connectionIntervalSum;

@end


@implementation ConnectionTest

static __strong const NSString *TestDataLine = @"abcdefghijklmnopqrstuvwxyz 0123456789 ~\r\n";
NSTimeInterval const CONNECTED_INTERVAL = 0.5;  // seconds
NSTimeInterval const DEFAULT_RECONNECT_INTERVAL = 3.0;  // seconds


- (ConnectionTest *)init
{
	self = [super init];
	if (self)
	{
		self.startTime = self.stopTime = [NSDate date];
		self.isRunning = NO;
		self.connectionIntervals = [[NSMutableArray alloc] init];
		self.reconnectInterval = [NSNumber numberWithDouble:DEFAULT_RECONNECT_INTERVAL];
	}
	return self;
}


#pragma mark - Properties

- (NSTimeInterval)runTime
{
	if (self.isRunning)
		return [self.startTime timeIntervalSinceNow] * -1;
	else
		return [self.startTime timeIntervalSinceDate:self.stopTime] * -1;
}


- (NSUInteger)connectionCount
{
	return self.connectionIntervals.count;
}


- (NSString *)formattedRunTime
{
	if (self.isRunning)
		return [self formatTimeFrom: self.startTime to: [NSDate date]];
	else
		return [self formatTimeFrom: self.startTime to: self.stopTime];
}


- (void)setIsRunning:(BOOL)isRunning
{
	self->_isRunning = isRunning;
}


- (void)setMinConnectionInterval:(NSNumber *)minConnectionInterval
{
	self->_minConnectionInterval	= minConnectionInterval;
}


- (void)setMaxConnectionInterval:(NSNumber *)maxConnectionInterval
{
	self->_maxConnectionInterval	= maxConnectionInterval;
}


- (void)setAverageConnectionInterval:(NSNumber *)averageConnectionInterval
{
	self->_averageConnectionInterval = averageConnectionInterval;
}


#pragma mark - Public methods

- (void)start
{
	if (self.isRunning)
		return;
	
	self.isRunning	 = YES;
	[self raiseDidChangeActivity];

	self.startTime = self.stopTime = [NSDate date];
	self.minConnectionInterval = [NSNumber numberWithDouble:DBL_MAX];
	self.maxConnectionInterval = [NSNumber numberWithDouble:DBL_MIN];
	self.averageConnectionInterval = [NSNumber numberWithDouble:0.0];
	self.connectionIntervalSum = [NSNumber numberWithDouble:0.0];
	[self.connectionIntervals removeAllObjects];

	[self raiseRequestsDisconnect];
}


- (void)stop
{
	self.isRunning = NO;
	[self raiseDidChangeActivity];
	self.stopTime = [NSDate date];
}


- (void)writeToFile:(NSString *)path
{
	NSMutableString *builder = [[NSMutableString alloc] init];
	for (NSNumber *interval in self.connectionIntervals)
	{
		[builder appendString:[NSString stringWithFormat:@"%.3f\r\n", interval.doubleValue]];
	}
	
	NSString *data = [builder stringByReplacingOccurrencesOfString:@"." withString:@","];
	[data writeToFile:path atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
}


- (void)handleConnected
{
	if (!self.isRunning)
		return;

	// calculate connection interval
	NSTimeInterval interval = [self.connectionInitiatedTime timeIntervalSinceNow] * -1;
	NSNumber *connectionInterval = [NSNumber numberWithDouble:(double) interval];

	// adjust min/max values
	if (connectionInterval.doubleValue > self.maxConnectionInterval.doubleValue)
	{
		self.maxConnectionInterval = [NSNumber numberWithDouble:connectionInterval.doubleValue];
	}
	if (connectionInterval.doubleValue < self.minConnectionInterval.doubleValue)
	{
		self.minConnectionInterval = [NSNumber numberWithDouble:connectionInterval.doubleValue];
	}

	// add interval to collection
	[self.connectionIntervals addObject:connectionInterval];
	
	// re-calculate average interval
	self.connectionIntervalSum = [NSNumber numberWithDouble:self.connectionIntervalSum.doubleValue + connectionInterval.doubleValue];
	self.averageConnectionInterval = [NSNumber numberWithDouble:self.connectionIntervalSum.doubleValue / self.connectionIntervals.count];
	
	// start connected timer
	[NSTimer scheduledTimerWithTimeInterval:CONNECTED_INTERVAL target:self selector:@selector(connectedTimerElapsed:) userInfo:nil repeats:NO];
}


- (void)handleDisconnected
{
	if (!self.isRunning)
		return;
	
	// start re-connect timer
	[NSTimer scheduledTimerWithTimeInterval:self.reconnectInterval.doubleValue target:self selector:@selector(reconnectTimerElapsed:) userInfo:nil repeats:NO];
}



#pragma mark - Internal methods

- (void)initiateConnection
{
	if (!self.isRunning)
		return;
	
	self.connectionInitiatedTime = [NSDate date];
	[self raiseRequestsConnect];
}


- (void)connectedTimerElapsed:(NSTimer *)timer
{
	[self raiseRequestsDisconnect];
}


- (void)reconnectTimerElapsed:(NSTimer *)timer
{
	[self initiateConnection];
}


- (NSString *)formatTimeFrom:(NSDate *)fromTime to:(NSDate *)toTime
{
	NSUInteger flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
	NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:flags fromDate:fromTime toDate:toTime options:0];
	return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)conversionInfo.hour, (long)conversionInfo.minute, (long) conversionInfo.second];
}



#pragma mark - Delegate events

- (void)raiseRequestsConnect
{
	if ([self.delegate respondsToSelector:@selector(connectionTestRequestsConnect:)])
		[self.delegate connectionTestRequestsConnect:self];
}


- (void)raiseRequestsDisconnect
{
	if ([self.delegate respondsToSelector:@selector(connectionTestRequestsDisconnect:)])
		[self.delegate connectionTestRequestsDisconnect:self];
}


- (void)raiseDidChangeActivity
{
	if ([self.delegate respondsToSelector:@selector(connectionTestDidChangeActivity:)])
		[self.delegate connectionTestDidChangeActivity:self];
}


@end
