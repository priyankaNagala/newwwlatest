//
//  ConnectionSequence.swift
//  nextgen
//
//  Created by Scott Wang on 11/26/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack
import Firebase

class ConnectionSequence {
    
    enum State: Int, CaseIterable {
        case START, PROTOCOL_VERSION, BIKE_INFO, DASH_GAUGES, TIMESTAMP, RIDE_MODES, ECUS, RANGE_ESTIMATION, COMPLETE
    }
    
    private var disposable = DisposeBag()
    private var zeroMotorcycleService: ZeroMotorcycleService?
    private var dataService: DataService?
    private var stateSubject = PublishSubject<State>()
    private var disposeBag = DisposeBag()
    
    private var executionTimer: CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
    
    init(zeroMotorcycleService: ZeroMotorcycleService, dataService: DataService) {
        self.zeroMotorcycleService = zeroMotorcycleService
        self.dataService = dataService
    }

    func start() -> Observable<State>{
        stateSubject.subscribe(onNext: { [weak self] (state) in
            guard let strongSelf = self else { return }
            strongSelf.onNext(state)
        }, onError: { (error) in
            DDLogDebug("Connection Sequence onError: \(error)")
        }, onCompleted: {
            DDLogDebug("Connection Sequence onCompleted")
        }) {
            DDLogDebug("Connection Sequence onDisposed")
        }.disposed(by: disposeBag)

        onNext(.START)
        return stateSubject
    }
    
    private func onNext(_ state: State) {
        guard let zeroMotorcycleService = self.zeroMotorcycleService else { return }
        
        switch state {
        case .START :
            // Does nothing, just advance
            onNext(.PROTOCOL_VERSION)

        case .PROTOCOL_VERSION:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.PROTOCOL_VERSION)
            }.disposed(by: disposable)
            sendProtocolVersionPacket()
            
        case .BIKE_INFO:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.BIKE_INFO)
                }.disposed(by: disposable)
            sendBikeInfoPacket()

        case .DASH_GAUGES:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.DASH_GAUGES)
            }.disposed(by: disposable)
            sendDashGaugesPacket()

        case .TIMESTAMP:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear previous
                strongSelf.sendTimestampPacket()
                strongSelf.stateSubject.onNext(.TIMESTAMP)
            }.disposed(by: disposable)
            sendTimestampPacket()

        case .RIDE_MODES:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.RIDE_MODES)
            }.disposed(by: disposable)
            sendRideModesPacket()
            
        case .ECUS:
            stateSubject.onNext(.RANGE_ESTIMATION) // Skip for now
            break
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.ECUS)
            }.disposed(by: disposable)
            sendEcuPacket()
            
        case .RANGE_ESTIMATION:
            zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                strongSelf.disposable = DisposeBag() // Clear prior subscriptions
                strongSelf.stateSubject.onNext(.RANGE_ESTIMATION)
            }.disposed(by: disposable)
            sendRangeEstimationPacket()

        case .COMPLETE :
            DDLogDebug("Connection Sequence: COMPLETE")
            stateSubject.onCompleted()
        }
    }
    
    // MARK: -
    
    fileprivate func sendProtocolVersionPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService, let dataService = self.dataService else { return }
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: ProtocolVersionPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, let versionPacket = event.element as? ProtocolVersionPacket else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Protocol Roundtrip: \(endTime - strongSelf.executionTimer)")
            
            strongSelf.disposable = DisposeBag() // Clear prior subscription
            DDLogDebug("Connection Sequence: Received Protocol Version Packet")
            dataService.protocolMajor = versionPacket.getMajorVersion()
            dataService.protocolMinor = versionPacket.getMinorVersion()
            dataService.protocolPatch = versionPacket.getPatchVersion()
            
            strongSelf.stateSubject.onNext(.BIKE_INFO)
        }.disposed(by: disposable)
    }
    
    fileprivate func sendBikeInfoPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService,
            let dataService = self.dataService else { return }
        
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: BikeInfoPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, let bikeInfoPacket = event.element as? BikeInfoPacket else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Bike Info Roundtrip: \(endTime - strongSelf.executionTimer)")
            
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            DDLogDebug("Connection Sequence: Received Bike Info Packet")
            dataService.currentVin = bikeInfoPacket.vin
            dataService.passcode = bikeInfoPacket.passcode
            dataService.currentMotorcycle?.model = bikeInfoPacket.model
            InstanceID.instanceID().instanceID { (result, error) in
                if error != nil {
                    DDLogDebug("Not registered for notification yet")
                } else {
                    if let model = dataService.currentMotorcycle?.model {
                        Messaging.messaging().subscribe(toTopic: model) { error in
                            if error == nil {
                                DDLogDebug("Subscribed to \(model) topic")
                            } else {
                                DDLogError("Failed to subscribe to \(model) topic: \(error!)")
                            }
                        }
                    }
                }
            }
            
            strongSelf.stateSubject.onNext(.DASH_GAUGES)
        }.disposed(by: disposable)
    }
    
    fileprivate func sendDashGaugesPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService,
            let dataService = self.dataService else { return }
        
        let dashGaugesReadPacket = DashGaugesPacket()
        dashGaugesReadPacket.isEmpty = true
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: dashGaugesReadPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self, let dashGaugesPacket = event.element as? DashGaugesPacket else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Dash Gauges Roundtrip: \(endTime - strongSelf.executionTimer)")
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            DDLogDebug("Connection Sequence: Received Dash Info Packet")
            
            if let currentVin = dataService.currentVin, let motorcycleData = dataService.getMotorcycleData(vin: currentVin) {
                // Save locally the dash gauges from MBB if they're empty locally
                if motorcycleData.dashboardGauges.gauges.count == 0 {
                    motorcycleData.dashboardGauges = DashboardGauges(dashGaugesPacket)
                }
            }
            
            strongSelf.stateSubject.onNext(.TIMESTAMP)
        }.disposed(by: disposable)
    }

    fileprivate func sendTimestampPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService else { return }

        let timeStampPacket = TimestampPacket()
        timeStampPacket.timestamp = UInt32(Date().timeIntervalSince1970)
        let utcCorrectionInHalfHours = TimeZone.current.secondsFromGMT() / (60 * 30)
        timeStampPacket.utcCorrection = Int8(utcCorrectionInHalfHours)
        
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: timeStampPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Timestamp Roundtrip: \(endTime - strongSelf.executionTimer)")
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            if let _ = event.element as? AckPacket {
                strongSelf.stateSubject.onNext(.RIDE_MODES)
            }
        }.disposed(by: disposable)
    }
    
    fileprivate func sendRideModesPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService, let dataService = self.dataService else { return }
        
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: RideModeManufacturingSettingsPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Ride Modes Roundtrip: \(endTime - strongSelf.executionTimer)")
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            if let rideModesPacket = event.element as? RideModeManufacturingSettingsPacket {
                DDLogDebug("Connection Sequence: Received Ride Modes Packet")
                dataService.currentMotorcycle?.defaultRideModes = rideModesPacket.rideModes
                strongSelf.stateSubject.onNext(.ECUS)
            }
            }.disposed(by: disposable)
    }
    
    fileprivate func sendEcuPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService else { return }
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: EcuPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            if let ecuPacket = event.element as? EcuPacket {
                DDLogDebug("Connection Sequence: Received ECU Packet")
                strongSelf.stateSubject.onNext(.RANGE_ESTIMATION)
            }
            }.disposed(by: disposable)
    }
    
    fileprivate func sendRangeEstimationPacket() {
        guard let zeroMotorcycleService = self.zeroMotorcycleService else { return }
        
        executionTimer = CFAbsoluteTimeGetCurrent()
        zeroMotorcycleService.sendSingleZeroPacket(packet: RangeEstimationPacket(), priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            let endTime = CFAbsoluteTimeGetCurrent()
            DDLogDebug("Range Estimation Roundtrip: \(endTime - strongSelf.executionTimer)")
            strongSelf.disposable = DisposeBag() // Clear prior subscriptions
            if let rangePacket = event.element as? RangeEstimationPacket {
                DDLogDebug("Connection Sequence: Received Range Estimation Packet")
                BatteryManager.updateLookups(rangePacket: rangePacket)
                
                strongSelf.stateSubject.onNext(.COMPLETE)
            }
            }.disposed(by: disposable)
    }
}
