//
//  BikeInfoPacket.swift
//  nextgen
//
//  Created by David Crawford on 9/5/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class BikeInfoPacket: ZeroPacket {
    
    var vin: String = ""
    var make: String = ""
    var model: String = ""
    var language: UInt8 = 0
    var motorcycleColor: UInt8 = 0
    var plugStatus: UInt8 = 0
    var chargeStatus: UInt8 = 0
    var stateOfCharge: UInt8 = 0
    var automaticallySetTime: UInt8 = 0
    var armed: UInt8 = 0
    var passcode: String = ""
    var chargeRate: UInt16 = 0
    var chargeTime: UInt16 = 0
    
    private let PACKET_VIN_END = 18
    private let PACKET_MAKE_END = 35
    private let PACKET_MODEL_END = 51
    private let PACKET_LANGUAGE_END = 52
    private let PACKET_MOTORCYCLE_COLOR_END = 53
    private let PACKET_PLUG_STATUS_END = 54
    private let PACKET_CHARGE_STATUS_END = 55
    private let PACKET_STATE_OF_CHARGE_END = 56
    private let PACKET_AUTOMATICALLY_SET_TIME_END = 57
    private let PACKET_ARMED_END = 58
    private let PACKET_PASSCODE_END = 66
    private let PACKET_CHARGE_RATE_END = 68
    private let PACKET_CHARGE_TIME_END = 70
    
    override init() {}
    
    init(_ data: Data) {
        let packetLength = data.count
        if (packetLength >= PACKET_VIN_END) {
            vin = data.subdata(in: 0..<PACKET_VIN_END).stringASCII!
        }
        if (packetLength >= PACKET_MAKE_END) {
            make = data.subdata(in: PACKET_VIN_END..<PACKET_MAKE_END).stringASCII!
        }
        if (packetLength >= PACKET_MODEL_END) {
            model = data.subdata(in: PACKET_MAKE_END..<PACKET_MODEL_END).stringASCII!
        }
        if (packetLength >= PACKET_LANGUAGE_END) {
            language = data.subdata(in: PACKET_MODEL_END..<PACKET_LANGUAGE_END).uint8
        }
        if (packetLength >= PACKET_MOTORCYCLE_COLOR_END) {
            motorcycleColor = data.subdata(in: PACKET_LANGUAGE_END..<PACKET_MOTORCYCLE_COLOR_END).uint8
        }
        if (packetLength >= PACKET_PLUG_STATUS_END) {
            plugStatus = data.subdata(in: PACKET_MOTORCYCLE_COLOR_END..<PACKET_PLUG_STATUS_END).uint8
        }
        if (packetLength >= PACKET_CHARGE_STATUS_END) {
            chargeStatus = data.subdata(in: PACKET_PLUG_STATUS_END..<PACKET_CHARGE_STATUS_END).uint8
        }
        if (packetLength >= PACKET_STATE_OF_CHARGE_END) {
            stateOfCharge = data.subdata(in: PACKET_CHARGE_STATUS_END..<PACKET_STATE_OF_CHARGE_END).uint8
        }
        if (packetLength >= PACKET_AUTOMATICALLY_SET_TIME_END) {
            automaticallySetTime = data.subdata(in: PACKET_STATE_OF_CHARGE_END..<PACKET_AUTOMATICALLY_SET_TIME_END).uint8
        }
        if (packetLength >= PACKET_ARMED_END) {
            armed = data.subdata(in: PACKET_AUTOMATICALLY_SET_TIME_END..<PACKET_ARMED_END).uint8
        }
        if (packetLength >= PACKET_PASSCODE_END) {
            let passcodeData = data.subdata(in: PACKET_ARMED_END..<PACKET_PASSCODE_END).reversed()
            for byte in passcodeData {
                passcode = passcode + String(format: "%02X", byte)
            }
        }
        if (packetLength >= PACKET_CHARGE_RATE_END) {
            chargeRate = data.subdata(in: PACKET_PASSCODE_END..<PACKET_CHARGE_RATE_END).uint16.byteSwapped
        }
        if (packetLength >= PACKET_CHARGE_TIME_END) {
            chargeTime = data.subdata(in: PACKET_CHARGE_RATE_END..<PACKET_CHARGE_TIME_END).uint16.byteSwapped
        }
    }
    
    enum MotorcycleColor: UInt8, Codable {
        case SEABRIGHT_BLUE = 1, BOARDWALK_RED = 2
        
        func namedImage() -> String {
            switch self {
            case .SEABRIGHT_BLUE: return "MY19BackgroundMotorcycleBlue"
            case .BOARDWALK_RED: return "MY19BackgroundMotorcycleRed"
            }
        }
    }
    
    func getMotorcycleColorName() -> String {
        if let color = BikeInfoPacket.MotorcycleColor(rawValue: motorcycleColor) {
            return color.namedImage()
        }
        return MotorcycleColor.BOARDWALK_RED.namedImage()
    }
    
    func isPluggedIn() -> Bool {
        return plugStatus == 1
    }
    
    func isCharging() -> Bool {
        return chargeStatus == 1
    }
    
    func isArmed() -> Bool {
        return armed == 1
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_BKINFO.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
