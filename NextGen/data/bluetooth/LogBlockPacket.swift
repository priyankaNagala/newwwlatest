//
//  LogBlockPacket.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class LogBlockPacket: ZeroPacket {
    
    var data: Data = Data()
    var fileNumber: UInt8 = 0
    var blockNumber: UInt16 = 0
    
    override init() {}
    
    init(_ data: Data) {
        self.data = data
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_BLOCK.rawValue.data
    }
    
    override func getBody() -> Data {
        var body = Data()
        body.append(fileNumber.data)
        body.append(blockNumber.byteSwapped.data)
        return body
    }
}
