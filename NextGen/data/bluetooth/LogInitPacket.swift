//
//  LogInitPacket.swift
//  nextgen
//
//  Created by Scott Wang on 1/30/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class LogInitPacket: ZeroPacket {
    
    var numFiles: UInt8 = 0
    fileprivate var numBytes: [UInt32] = [UInt32]()
    fileprivate var numBlocks: [UInt16] = [UInt16]()
    
    private let PACKET_NUM_FILES_END = 1
    private let PACKET_FILE_CHUNK_SIZE = 6
    
    private let PACKET_FILE_CHUNK_NUM_BYTES_END = 4
    private let PACKET_FILE_CHUNK_NUM_BLOCKS_END = 6
    
    override init() {}
    
    init(_ data: Data) {
        let packetLength = data.count
        
        if packetLength >= PACKET_NUM_FILES_END {
            numFiles = data.subdata(in: 0..<PACKET_NUM_FILES_END).uint8
        }
        
        for i in 0..<Int(numFiles) {
            let startIdx = PACKET_NUM_FILES_END + (i * PACKET_FILE_CHUNK_SIZE)
            let endIdx = startIdx + PACKET_FILE_CHUNK_SIZE
            if data.count > endIdx {
                let fileChunkData = data.subdata(in: startIdx..<endIdx)
                let fileChunkLen = fileChunkData.count
                
                if fileChunkLen >= PACKET_FILE_CHUNK_NUM_BYTES_END {
                    let bytes = fileChunkData.subdata(in: 0..<PACKET_FILE_CHUNK_NUM_BYTES_END).uint32.byteSwapped
                    numBytes.append(bytes)
                }
                if fileChunkLen >= PACKET_FILE_CHUNK_NUM_BLOCKS_END {
                    let blocks = fileChunkData.subdata(in: PACKET_FILE_CHUNK_NUM_BYTES_END..<PACKET_FILE_CHUNK_NUM_BLOCKS_END).uint16.byteSwapped
                    numBlocks.append(blocks)
                }
            }
        }
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_INIT.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
    
    func getNumBytes(_ fileIndex: Int) -> Int {
        guard fileIndex < numBytes.count else { return 0 }
        
        return Int(numBytes[fileIndex])
    }

    func getNumBlocks(_ fileIndex: Int) -> Int {
        guard fileIndex < numBlocks.count else { return 0 }
        
        return Int(numBlocks[fileIndex])
    }
}
