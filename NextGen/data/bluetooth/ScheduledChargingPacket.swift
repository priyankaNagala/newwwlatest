//
//  ScheduledChargingPacket.swift
//  nextgen
//
//  Created by Scott Wang on 1/8/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class ScheduledChargingPacket: ZeroPacket, NSCoding {
    
    override init() {}
    
    var daysToCharge: [Bool] = [false,false,false,false,false,false,false]
    var enabled: Bool = false
    var activeChargeStartTime: UInt8 = 0
    var activeChargeLength: UInt8 = 0
    var plugInStartTime: UInt8 = 0
    var plugInLength: UInt8 = 0

    private let PACKET_ENABLED_END = 1
    private let PACKET_ACTIVE_START_END = 1
    private let PACKET_ACTIVE_LENGTH_END = 2
    private let PACKET_PLUGIN_START_END = 3
    private let PACKET_PLUGIN_LENGTH_END = 4
    
    // MARK: - NSCoding
    func encode(with aCoder: NSCoder) {
        let body = getBody()
        aCoder.encode(body, forKey: "body")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        if let body = aDecoder.decodeObject(forKey: "body") as? Data {
            self.init(body)
        } else {
            self.init()
        }
    }
    
    // MARK: -
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_SCHED_CHARGE.rawValue.data
    }

    init(_ body: Data) {
        let packetLength = body.count
        
        if packetLength >= PACKET_ENABLED_END {
            enabled = body.subdata(in: 0..<PACKET_ENABLED_END).uint8 != 0
        }
        
        for day in 0..<7 {
            let startIdx = PACKET_ENABLED_END + (day * 4)
            let aStart = body.subdata(in: startIdx..<PACKET_ACTIVE_START_END+startIdx).uint8
            let aLength = body.subdata(in: startIdx+PACKET_ACTIVE_START_END..<PACKET_ACTIVE_LENGTH_END+startIdx).uint8
            let pStart = body.subdata(in: startIdx+PACKET_ACTIVE_LENGTH_END..<PACKET_PLUGIN_START_END+startIdx).uint8
            let pLength = body.subdata(in: startIdx+PACKET_PLUGIN_START_END..<PACKET_PLUGIN_LENGTH_END+startIdx).uint8

            if aLength > 0 && pLength > 0 {
                activeChargeStartTime = aStart
                activeChargeLength = aLength
                plugInStartTime = pStart
                plugInLength = pLength
                
                daysToCharge[day] = true
            }
        }
    }
    
    override func getBody() -> Data {
        if isEmpty {
            return ZeroBtTypes.emptyBody
        }
        
        var body = Data()
        body.append(enabled ? 0x01 : 0x00)
        for day in daysToCharge {
            if day {
                body.append(activeChargeStartTime.data)
                body.append(activeChargeLength.data)
                body.append(plugInStartTime.data)
                body.append(plugInLength.data)
            } else {
                body.append(0x00)
                body.append(0x00)
                body.append(0x00)
                body.append(0x00)
            }
        }
        return body
    }
    
    func chargingScheduledFor(_ day: ScheduledCharging.DayOfWeek) -> Bool {
        switch day {
        case .mon:
            return daysToCharge[0]
        case .tue:
            return daysToCharge[1]
        case .wed:
            return daysToCharge[2]
        case .thu:
            return daysToCharge[3]
        case .fri:
            return daysToCharge[4]
        case .sat:
            return daysToCharge[5]
        case .sun:
            return daysToCharge[6]
        }
    }
    
    func getActiveStartTimeAsInt() -> Int {
        return getTimeAsInt(activeChargeStartTime)
    }

    func getActiveLengthAsInt() -> Int {
        return getLengthAsInt(activeChargeLength)
    }
    
    func getPlugInStartTimeAsInt() -> Int {
        return getTimeAsInt(plugInStartTime)
    }
    
    func getPlugInLengthAsInt() -> Int {
        return getLengthAsInt(plugInLength)
    }
    
    func setActiveStartTimeWithInt(_ value: Int) {
        activeChargeStartTime = getTimeAsUInt8(value)
    }
    
    func setActiveLengthWithInt(_ value: Int) {
        activeChargeLength = getLengthAsUInt8(value)
    }

    func setPlugInStartTimeWithInt(_ value: Int) {
        plugInStartTime = getTimeAsUInt8(value)
    }
    
    func setPlugInLengthWithInt(_ value: Int) {
        plugInLength = getLengthAsUInt8(value)
    }
    
    func setDays(_ values: [ScheduledCharging.DayOfWeek]) {
        for day in values {
            switch day {
            case .mon:
                daysToCharge[0] = true
            case .tue:
                daysToCharge[1] = true
            case .wed:
                daysToCharge[2] = true
            case .thu:
                daysToCharge[3] = true
            case .fri:
                daysToCharge[4] = true
            case .sat:
                daysToCharge[5] = true
            case .sun:
                daysToCharge[6] = true
            }
        }
    }

    // 0 -> 0
    // 30 -> 1
    // 100 -> 2
    // 130 -> 3
    // This is how ScheduledCharging.swift represents Time as Int
    fileprivate func getTimeAsUInt8(_ value: Int) -> UInt8 {
        guard value >= 0, value < 2359 else { return 0 }
        let hr = value / 100
        let halfHr = (value - (hr * 100)) / 30
        
        return UInt8(hr * 2 + halfHr)
    }
    
    // 0 -> 0
    // 30 -> 1
    // 60 -> 2
    // 90 -> 3
    fileprivate func getLengthAsUInt8(_ value: Int) -> UInt8 {
        guard value > 0, value <= 1440 else { return 0 }
        return UInt8(value / 30)
    }
    
    // 0 -> 0
    // 1 -> 30
    // 2 -> 100
    // 3 -> 130
    // This is how ScheduledCharging.swift represents Time as Int
    fileprivate func getTimeAsInt(_ value: UInt8) -> Int {
        guard value < 48 else { return 0 }
        let hr = Int(value) / 2
        let halfHr = Int(value) % 2
        
        return (hr * 100) + (halfHr * 30)
    }

    // 0 -> 0
    // 1 -> 30
    // 2 -> 60
    fileprivate func getLengthAsInt(_ value: UInt8) -> Int {
        guard value <= 48 else { return 0 }
        return Int(value) * 30
    }
}
