//
//  ProtocolVersionPacket.swift
//  nextgen
//
//  Created by Scott Wang on 1/30/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class ProtocolVersionPacket : ZeroPacket {
    
    var version: UInt32 = 0
    
    private let PACKET_VERSION_END = 4
    
    override init() {}
    
    init(_ data: Data) {
        let packetLength = data.count
        if packetLength >= PACKET_VERSION_END {
            version = data.subdata(in: 0..<PACKET_VERSION_END).uint32.byteSwapped
        }
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_VER.rawValue.data

    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
    
    func getMajorVersion() -> Int {
        return Int(version >> 3)
    }
    
    func getMinorVersion() -> Int {
        return Int((version << 1) >> 3)
    }
    
    func getPatchVersion() -> Int {
        return Int((version << 2) >> 3)
    }
}
