//
//  CommandChannelPacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/6/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class CommandChannelPacket: ZeroPacket {
    
    enum CommandChannel: UInt16, CaseIterable {
        case ZERO_BT_CMD_CH_CPU                     = 0x0000
        case ZERO_BT_CMD_CH_BT                      = 0x0001
        case ZERO_BT_CMD_CH_DRIVE_PARAM             = 0x0002
        case ZERO_BT_CMD_CH_MEM_READ_SIZE_BYTES     = 0x0003
        case ZERO_BT_CMD_CH_MEM_READ_ADDR           = 0x0004
        case ZERO_BT_CMD_CH_SET_BMS_INDEX           = 0x0005
        case ZERO_BT_CMD_CH_SET_ECU_INDEX           = 0x0006
        case ZERO_BT_CMD_CH_ACTIVE_BOOT_BANK        = 0x0030
        case ZERO_BT_CMD_CH_UPDATE_IMAGE_INFO       = 0x0130
        case ZERO_BT_CMD_CH_UPDATE_STATUS           = 0x0031
        case ZERO_BT_CMD_CH_TEST_MODE               = 0x0032
    }
    
    enum CommandChannelReadWrite: UInt8, CaseIterable {
        case CMD_CH_READ = 0x01
        case CMD_CH_WRITE = 0x00
    }
    
    private let ZERO_BT_CMD_CH_TEXT_MAX_SIZE_WORDS = 10

    private let PACKET_CMCH_END = 2
    private let PACKET_CMCH_STATUS_END = 3
    private let PACKET_RW_END = 4
    private let PACKET_TEXT_END = 44
/*
     
     // command channel
     #define ZERO_BT_CMD_CH_TEXT_MAX_SIZE_WORDS     10
     #define ZERO_BT_CMD_CH_CPU                     0x0000
     #define ZERO_BT_CMD_CH_BT                      0x0001
     #define ZERO_BT_CMD_CH_DRIVE_PARAM             0x0002
     #define ZERO_BT_CMD_CH_MEM_READ_SIZE_BYTES     0x0003
     #define ZERO_BT_CMD_CH_MEM_READ_ADDR           0x0004
     #define ZERO_BT_CMD_CH_SET_BMS_INDEX           0x0005
     #define ZERO_BT_CMD_CH_SET_ECU_INDEX           0x0006
     //#define ZERO_BT_CMD_CH_START_TERMINAL_MODE     0x00000010
     #define ZERO_BT_CMD_CH_ACTIVE_BOOT_BANK        0x0030
     #define ZERO_BT_CMD_CH_UPDATE_IMAGE_INFO       0x0130
     #define ZERO_BT_CMD_CH_UPDATE_STATUS           0x0031
     #define ZERO_BT_CMD_CH_TEST_MODE               0x0032
     
     
 typedef struct ZeroBtBodyCommandChannel
 {
 u16    cmd_ch;
 u8     cmd_ch_status;
 u8     read_write_n;
 u32    text[ZERO_BT_CMD_CH_TEXT_MAX_SIZE_WORDS];
 } ZERO_BT_BODY_COMMAND_CHANNEL;
*/
    
    var commandChannel: UInt16 = 0
    var commandChannelStatus: UInt8 = 0
    var readWrite: UInt8 = 0
    var text: [UInt32] = [UInt32]()
    
    override init() {}
    
    init(_ data: Data) {
        let packetLength = data.count
        guard packetLength >= PACKET_CMCH_END else { return }
        commandChannel = data.subdata(in: 0..<PACKET_CMCH_END).uint16.byteSwapped
        guard packetLength >= PACKET_CMCH_STATUS_END else { return }
        commandChannelStatus = data.subdata(in: PACKET_CMCH_END..<PACKET_CMCH_STATUS_END).uint8
        guard packetLength >= PACKET_RW_END else { return }
        commandChannelStatus = data.subdata(in: PACKET_CMCH_STATUS_END..<PACKET_RW_END).uint8
        guard packetLength >= PACKET_TEXT_END else { return }
        for i in 0..<ZERO_BT_CMD_CH_TEXT_MAX_SIZE_WORDS {
            let startIdx = PACKET_RW_END + (i * 4)
            let endIdx = startIdx + 4
            text.append(data.subdata(in: startIdx..<endIdx).uint32.byteSwapped)
        }
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_CMD_CH.rawValue.data
    }
    
    override func getBody() -> Data {
        guard isEmpty == false else { return ZeroBtTypes.emptyBody }
        
        var body = Data()
        body.append(commandChannel.byteSwapped.data)
        body.append(commandChannelStatus.data)
        body.append(readWrite.data)
        for i in 0..<ZERO_BT_CMD_CH_TEXT_MAX_SIZE_WORDS {
            if text.count > i {
                body.append(text[i].byteSwapped.data)
            } else {
                body.append(0x00000000)
            }
        }
        
        return body
    }

    func getCommandChannel() -> CommandChannel? {
        return CommandChannel.init(rawValue: commandChannel)
    }
    
    static func getCommandEcuIndexPacket(readWrite: CommandChannelReadWrite, ecuIndex: UInt32) -> CommandChannelPacket {
        let packet = CommandChannelPacket()
        packet.commandChannel = CommandChannel.ZERO_BT_CMD_CH_SET_ECU_INDEX.rawValue
        packet.readWrite = readWrite.rawValue
        packet.text.append(ecuIndex)
        
        return packet
    }
}
