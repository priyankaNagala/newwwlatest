//
//  RideModePacket.swift
//  nextgen
//
//  Created by Scott Wang on 10/31/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import CocoaLumberjack

class RideModesPacket : ZeroPacket {
    let maxRideModes = 5
    let rideModePacketSize = 22
    let nameSize = 12

    var rideModes = [RideMode]()
    var reorderable: Bool = false
    
    private let PACKET_NAME_END = 12
    private let PACKET_SPEED_END = 14
    private let PACKET_TORQUE_END = 15
    private let PACKET_REGEN_TORQUE_END = 16
    private let PACKET_REGEN_BRAKE_END = 17
    private let PACKET_POWER_END = 18
    private let PACKET_TRACTION_CONTROL_END = 19
    private let PACKET_ABS_CONTROL_END = 20
    private let PACKET_DISPLAY_COLOR_END = 21
    private let PACKET_SWAP_END = 22
    
    override init() {}
    
    init(_ body: Data) {
        super.init()
        loadByteArray(body)
    }
    
    init(_ rideModes: [RideMode]) {
        self.rideModes = rideModes
    }

    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_MODES.rawValue.data
    }

    override func getBody() -> Data {
        if isEmpty { return ZeroBtTypes.emptyBody }
        var body = Data()
        for rideMode in rideModes {
            let rideModeData = dataForRideMode(rideMode)
            body.append(rideModeData)
        }
        body.append(reorderable ? 0x01 : 0x00)
        
        return body
    }
    
    func dataForRideMode(_ rideMode: RideMode) -> Data {
        var body = Data()
        if let name = rideMode.name.data(using: .ascii) {
            body.append(name.prefix(Constant.RIDE_MODE_NAME_MAX_CHARACTERS))
        }
        let padding = 12-body.count
        if padding > 0 {
            for _ in 0..<padding {
                body.append(0x00)
            }
        }
        
        body.append(UInt16(rideMode.maxSpeed).byteSwapped.data)
        body.append(UInt8(rideMode.maxTorque))
        body.append(UInt8(rideMode.neutralRegeneration))
        body.append(UInt8(rideMode.brakeRegeneration))
        body.append(UInt8(rideMode.maxPower))
        switch rideMode.tractionControlType {
        case .STREET:
            body.append(0x01)
        case .SPORT:
            body.append(0x02)
        case .RAIN:
            body.append(0x03)
        }
        body.append(0x01) // ABS always 1
        switch rideMode.dashboardTheme {
        case .DARK_GREEN:
            body.append(0x01)
        case .DARK_BLUE:
            body.append(0x02)
        case .LIGHT_ORANGE:
            body.append(0x03)
        case .LIGHT_BLUE:
            body.append(0x04)
        case .DARK_ORANGE:
            body.append(0x05)
        }
        body.append(0x00) // SWAP

        return body
    }
    
    func loadByteArray(_ body: Data) {
        var rideModeDataArr = [Data]()
        let numModes = body.count / rideModePacketSize
        for i in 1...numModes {
            let fromIndex = rideModePacketSize * (i - 1)
            let toIndex = fromIndex + rideModePacketSize
            rideModeDataArr.append(body.subdata(in: fromIndex..<toIndex))
        }
        
        for rideModeData in rideModeDataArr {
            let rideMode = parseSingleMode(rideModeData)
            DDLogDebug(rideModeData.toHexString())

            DDLogDebug("type \(rideMode.type)")
            DDLogDebug("name \(rideMode.name)")
            DDLogDebug("tractionControlEnabled \(rideMode.tractionControlEnabled)")
            DDLogDebug("tractionControlType \(rideMode.tractionControlType)")
            DDLogDebug("maxSpeed \(rideMode.maxSpeed)")
            DDLogDebug("maxPower \(rideMode.maxPower)")
            DDLogDebug("maxTorque \(rideMode.maxTorque)")
            DDLogDebug("neutralRegeneration \(rideMode.neutralRegeneration)")
            DDLogDebug("brakeRegeneration \(rideMode.brakeRegeneration)")
            DDLogDebug("absControl \(rideMode.absControl)")
            DDLogDebug("dashboardTheme \(rideMode.dashboardTheme)")

            rideModes.append(rideMode)
        }
        
        let startIdx = rideModePacketSize * numModes
        if body.count > startIdx {
            reorderable = body.subdata(in: startIdx..<startIdx+1).uint8 != 0
        }
    }
    
    func parseSingleMode(_ modeBody: Data) -> RideMode {
        let packetModeName: String = modeBody.subdata(in: 0..<PACKET_NAME_END).stringASCII!
        let packetSpeed: UInt16 = modeBody.subdata(in: PACKET_NAME_END..<PACKET_SPEED_END).uint16.byteSwapped
        let packetTorque: UInt8  = modeBody.subdata(in: PACKET_SPEED_END..<PACKET_TORQUE_END).uint8
        let packetRegenTorque: UInt8 = modeBody.subdata(in: PACKET_TORQUE_END..<PACKET_REGEN_TORQUE_END).uint8
        let packetRegenBrake: UInt8 = modeBody.subdata(in: PACKET_REGEN_TORQUE_END..<PACKET_REGEN_BRAKE_END).uint8
        let packetPower: UInt8 = modeBody.subdata(in: PACKET_REGEN_BRAKE_END..<PACKET_POWER_END).uint8
        let packetTractionControl: UInt8 = modeBody.subdata(in: PACKET_POWER_END..<PACKET_TRACTION_CONTROL_END).uint8
        let packetAbsControl: UInt8 = 1 // Always 1
        let packetDisplayColor: UInt8 = modeBody.subdata(in: PACKET_ABS_CONTROL_END..<PACKET_DISPLAY_COLOR_END).uint8
        let packetSwap: UInt8 = modeBody.subdata(in: PACKET_DISPLAY_COLOR_END..<PACKET_SWAP_END).uint8

        let rideModeType = RideMode.RideModeType.CUSTOM
        var tractionControlType: TractionControlType = .STREET
        switch packetTractionControl {
        case 1:
            tractionControlType = .STREET
        case 2:
            tractionControlType = .SPORT
        case 3:
            tractionControlType = .RAIN
        default:
            break
        }
        var dashboardTheme: DashboardTheme = .DARK_GREEN
        switch packetDisplayColor {
        case 1:
            dashboardTheme = .DARK_GREEN
        case 2:
            dashboardTheme = .DARK_BLUE
        case 3:
            dashboardTheme = .LIGHT_ORANGE
        case 4:
            dashboardTheme = .LIGHT_BLUE
        case 5:
            dashboardTheme = .DARK_ORANGE
        default:
            dashboardTheme = .DARK_GREEN
        }
        
        let rideMode = RideMode(type: rideModeType,
                                name: packetModeName,
                                tractionControlEnabled: packetTractionControl > 0,
                                tractionControlType: tractionControlType,
                                maxSpeed: Int(packetSpeed),
                                maxPower: Int(packetPower),
                                maxTorque: Int(packetTorque),
                                neutralRegeneration: Int(packetRegenTorque),
                                brakeRegeneration: Int(packetRegenBrake),
                                absControl: packetAbsControl > 0,
                                dashboardTheme: dashboardTheme,
                                swapable: packetSwap != 0)

        return rideMode
    }
}
