//
//  EcuPacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class EcuPacket: ZeroPacket {

    override init() {}
    
    init(_ data: Data) {
        
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_ECUS.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
