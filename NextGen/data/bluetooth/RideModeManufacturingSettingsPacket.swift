//
//  RideModeManufacturingSettingsPacket.swift
//  nextgen
//
//  Created by Scott Wang on 10/31/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class RideModeManufacturingSettingsPacket : RideModesPacket {

    override init() {super.init()}

    override init(_ body: Data) {
        super.init()
        loadByteArray(body)

    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
    
    override func loadByteArray(_ body: Data) {
        super.loadByteArray(body)
        
        for rideMode in rideModes {
            rideMode.type = .ZERO
        }
    }
}
