//
//  ZeroBtTypes.swift
//  nextgen
//
//  Created by David Crawford on 9/5/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//
//  This file includes any values that the app and MBB need to standardize on.
//  It's roughly equivalent to a swift version of the ZeroByTypes.h files that
//  we regularly receive from the MBB team.

import Foundation

struct ZeroBtTypes {
    
    enum Packet: UInt16, CaseIterable {
        case ZERO_BT_PKT_NULL              = 0x0000
        case ZERO_BT_PKT_VER               = 0x0001
        case ZERO_BT_PKT_RESEND            = 0x0002
        case ZERO_BT_PKT_BKINFO            = 0x0003
        case ZERO_BT_PKT_PWRPCK            = 0x0004
        case ZERO_BT_PKT_BTSTAT            = 0x0005
        case ZERO_BT_PKT_MBB_RD            = 0x0006
        case ZERO_BT_PKT_MBB_DIG           = 0x0007
        case ZERO_BT_PKT_BMS_DIG           = 0x0008
        case ZERO_BT_PKT_UPLOAD            = 0x0009
        case ZERO_BT_PKT_DASH_STATUS       = 0x000A
        case ZERO_BT_PKT_ECUS              = 0x000B
        case ZERO_BT_PKT_ERROR_CODES       = 0x000C
        case ZERO_BT_PKT_RIDE_MODES        = 0x000D
        case ZERO_BT_PKT_HASH              = 0x000E
        case ZERO_BT_PKT_CMD_CH            = 0x000F
        case ZERO_BT_PKT_DASH_GAUGES       = 0x0010
        case ZERO_BT_PKT_SCHED_CHARGE      = 0x0011
        case ZERO_BT_PKT_RIDE_MODES_CUSTOM = 0x0012
        case ZERO_BT_PKT_ACK               = 0x0013
        case ZERO_BT_PKT_TIMESTAMP         = 0x0014
        case ZERO_BT_PKT_CHARGE_TARGET     = 0x0015
        case ZERO_BT_PKT_RIDE_SHARE        = 0x0016
        case ZERO_BT_PKT_RANGE_ESTIMATE    = 0x0017
        case ZERO_BT_PKT_LOG_INIT          = 0x0018
        case ZERO_BT_PKT_LOG_BLOCK         = 0x0019
        case ZERO_BT_PKT_LOG_DONE          = 0x001A
        case ZERO_BT_PKT_STARCOM           = 0x001B
    }
    
    static let ZERO_BT_PKT_HEAD: UInt32 = 0xF1F2F4F8
    static let ZERO_BT_PKT_TAIL: UInt32 = 0xF8F4F2F1    
    static let MAX_BODY_SIZE = 148 // Currently RideModes Packet
    static let emptyBody = Data()
}
