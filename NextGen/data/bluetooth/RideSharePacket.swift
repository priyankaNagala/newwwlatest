//
//  RideSharePacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class RideSharePacket: ZeroPacket, Codable {
 
    var odometer: UInt32 = 0
    var totalEnergy: UInt32 = 0
    var avgEfficiency: UInt16 = 0
    var speed: UInt16 = 0
    var soc: UInt8 = 0
    var leanAngle: Int32 = 0
    var power: Int16 = 0
    var torque: Int32 = 0
    var coastRegen: Int32 = 0
    var brakeRegen: Int32 = 0
    var longitude: Double = 0
    var latitutude: Double = 0
    
    private let PACKET_ODOMETER_END = 4
    private let PACKET_ENERGY_END = 8
    private let PACKET_EFFICIENCY_END = 10
    private let PACKET_SPEED_END = 12
    private let PACKET_LEAN_ANGLE_END = 16
    private let PACKET_POWER_END = 18
    private let PACKET_TORQUE_END = 22
    private let PACKET_COAST_END = 26
    private let PACKET_BRAKE_END = 30
    private let PACKET_LONGITUDE_END = 34
    private let PACKET_LATITUDE_END = 38
    private let PACKET_SOC_END = 39

    override init() {}
    
    init(_ body: Data) {
        let packetLength = body.count
        if packetLength >= PACKET_ODOMETER_END {
            odometer = body.subdata(in: 0..<PACKET_ODOMETER_END).uint32.byteSwapped
        }
        if packetLength >= PACKET_ENERGY_END {
            totalEnergy = body.subdata(in: PACKET_ODOMETER_END..<PACKET_ENERGY_END).uint32.byteSwapped
        }
        if packetLength >= PACKET_EFFICIENCY_END {
            avgEfficiency = body.subdata(in: PACKET_ENERGY_END..<PACKET_EFFICIENCY_END).uint16.byteSwapped
        }
        if packetLength >= PACKET_SPEED_END {
            speed = body.subdata(in: PACKET_EFFICIENCY_END..<PACKET_SPEED_END).uint16.byteSwapped
        }
        if packetLength >= PACKET_LEAN_ANGLE_END {
            leanAngle =  body.subdata(in: PACKET_SPEED_END..<PACKET_LEAN_ANGLE_END).int32
        }
        if packetLength >= PACKET_POWER_END {
            power = body.subdata(in: PACKET_LEAN_ANGLE_END..<PACKET_POWER_END).int16
        }
        if packetLength >= PACKET_TORQUE_END {
            torque = body.subdata(in: PACKET_POWER_END..<PACKET_TORQUE_END).int32
        }
        if packetLength >= PACKET_COAST_END {
            coastRegen = body.subdata(in: PACKET_TORQUE_END..<PACKET_COAST_END).int32
        }
        if packetLength >= PACKET_BRAKE_END {
            brakeRegen = body.subdata(in: PACKET_COAST_END..<PACKET_BRAKE_END).int32
        }
        if packetLength >= PACKET_LONGITUDE_END {
            let radians = body.subdata(in: PACKET_BRAKE_END..<PACKET_LONGITUDE_END).int32
            
            longitude = Double(radians) * 180.0 / (Double.pi * 100000000.0)
        }
        if packetLength >= PACKET_LATITUDE_END {
            let radians = body.subdata(in: PACKET_LONGITUDE_END..<PACKET_LATITUDE_END).int32

            latitutude = Double(radians) * 180.0 / (Double.pi * 100000000.0)
        }
        if packetLength >= PACKET_SOC_END {
            soc = body.subdata(in: PACKET_LATITUDE_END..<PACKET_SOC_END).uint8
        }
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody // Read only packet
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_SHARE.rawValue.data
    }
}
