//
//  ErrorCodesPacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ErrorCodesPacket: ZeroPacket {
    
    var numCodes: UInt32 = 0
    var codes: [UInt16] = [UInt16]()
    
    private let PACKET_NUM_CODES_END = 4
    
    override init() {}
    
    init(_ data: Data) {
        numCodes = data.subdata(in: 0..<PACKET_NUM_CODES_END).uint32.byteSwapped
        
        for i in 0..<Int(numCodes) {
            let startIdx = PACKET_NUM_CODES_END + (i * 2)
            let endIdx = startIdx + 2
            if data.count > endIdx {
                let code = data.subdata(in: startIdx..<endIdx).uint16.byteSwapped
                if code != 0 {
                    codes.append(code)
                }
            }
        }
        numCodes = UInt32(codes.count)
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_ERROR_CODES.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
