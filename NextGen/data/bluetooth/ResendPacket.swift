//
//  ResendPacket.swift
//  nextgen
//
//  Created by David Crawford on 9/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ResendPacket: ZeroPacket {
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_RESEND.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
