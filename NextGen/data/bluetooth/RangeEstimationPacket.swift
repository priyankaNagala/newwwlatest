//
//  RangeEstimationPacket.swift
//  nextgen
//
//  Created by Scott Wang on 1/11/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import CocoaLumberjack

class RangeEstimationPacket : ZeroPacket {
    
    var routeTypeCity: UInt8 = 0
    var routeTypeHwy: UInt8 = 0
    var routeTypeCityHwy: UInt8 = 0
    var routeTypeFreeway: UInt8 = 0
    var routeTypeCityFreeway: UInt8 = 0

    var maxCapacityNew: UInt8 = 0
    var maxCapacityMidlife: UInt8 = 0
    var maxCapacityOld: UInt8 = 0

    var rideStyleConservative: UInt8 = 0
    var rideStyleAggresive: UInt8 = 0

    var tempCold: UInt8 = 0
    var tempMild: UInt8 = 0
    var tempWarm: UInt8 = 0

    var terrainFlat: UInt8 = 0
    var terrainHill: UInt8 = 0
    
    static let estimationValuesCount = 15
    
    override init() {}
    
    init(_ data: Data) {
        super.init()
        var idx = 0
        parseAndIncrement(data: data, idx: &idx, value: &routeTypeCity)
        parseAndIncrement(data: data, idx: &idx, value: &routeTypeHwy)
        parseAndIncrement(data: data, idx: &idx, value: &routeTypeCityHwy)
        parseAndIncrement(data: data, idx: &idx, value: &routeTypeFreeway)
        parseAndIncrement(data: data, idx: &idx, value: &routeTypeCityFreeway)

        parseAndIncrement(data: data, idx: &idx, value: &maxCapacityNew)
        parseAndIncrement(data: data, idx: &idx, value: &maxCapacityMidlife)
        parseAndIncrement(data: data, idx: &idx, value: &maxCapacityOld)

        parseAndIncrement(data: data, idx: &idx, value: &rideStyleConservative)
        parseAndIncrement(data: data, idx: &idx, value: &rideStyleAggresive)

        parseAndIncrement(data: data, idx: &idx, value: &tempCold)
        parseAndIncrement(data: data, idx: &idx, value: &tempMild)
        parseAndIncrement(data: data, idx: &idx, value: &tempWarm)

        parseAndIncrement(data: data, idx: &idx, value: &terrainFlat)
        parseAndIncrement(data: data, idx: &idx, value: &terrainHill)
    }
    
    fileprivate func parseAndIncrement(data: Data, idx: inout Int, value: inout UInt8) {
        let packetLength = data.count
        if packetLength > idx + 1 {
            value = data.subdata(in: idx..<idx+1).uint8
        }
        idx = idx + 1
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_RANGE_ESTIMATE.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
