//
//  RideModeCustomSettingsPacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class RideModeCustomSettingsPacket : RideModesPacket, NSCoding {
    private let PACKET_ACTIVE_RIDE_MODE_END = 112
    var activeMode: UInt8 = 0

    // MARK: - NSCoding
    func encode(with aCoder: NSCoder) {
        let body = getBody()
        aCoder.encode(body, forKey: "body")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        if let body = aDecoder.decodeObject(forKey: "body") as? Data {
            self.init(body)
        } else {
            self.init()
        }
    }
    
    // MARK: -
    
    override init() {super.init()}
    
    override init(_ body: Data) {
        super.init(body)
        
        if body.count >= PACKET_ACTIVE_RIDE_MODE_END {
            activeMode = body.subdata(in: PACKET_ACTIVE_RIDE_MODE_END-1..<PACKET_ACTIVE_RIDE_MODE_END).uint8
        }
    }
    
    override init(_ rideModes: [RideMode]) {
        super.init(rideModes)

        var activeModeIdx = 0
        if let currentRideMode = DataService.shared.currentMotorcycle?.currentRideMode {
            for mode in rideModes {
                if mode.name == currentRideMode.name {
                    break
                }
                activeModeIdx = activeModeIdx + 1
            }
        }
        activeMode = UInt8(activeModeIdx)
    }
    
    override func getBody() -> Data {
        if isEmpty { return ZeroBtTypes.emptyBody }

        var body = super.getBody()
        body.append(activeMode.data)
        
        return body
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_MODES_CUSTOM.rawValue.data
    }
}
