//
//  AckPacket.swift
//  nextgen
//
//  Created by Scott Wang on 10/31/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class AckPacket : ZeroPacket {
 
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_ACK.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
