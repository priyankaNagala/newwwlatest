//
//  StarcomRegistrationPacket.swift
//  nextgen
//
//  Created by Scott Wang on 3/5/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class StarcomRegistrationPacket : ZeroPacket {
    
    var event: UInt8 = 0
    
    override init() {}
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_STARCOM.rawValue.data
    }
    
    override func getBody() -> Data {
        var body = Data()
        body.append(event.data)
        return body
    }
    
    func successfulRegister() {
        event = 1
    }
    
    func successfulDeregister() {
        event = 2
    }
}
