//
//  LogDonePacket.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation

class LogDonePacket: ZeroPacket {
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_DONE.rawValue.data
    }
    
    override func getBody() -> Data {
        return ZeroBtTypes.emptyBody
    }
}
