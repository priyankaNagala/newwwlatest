//
//  ZeroPacket.swift
//  nextgen
//
//  Created by David Crawford on 9/5/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation

class ZeroPacket: NSObject {
    func getTag() -> Data {
        preconditionFailure("Subclasses are required to implement this method")
    }
    
    func getBody() -> Data {
        preconditionFailure("Subclasses are required to implement this method")
    }
    
    var isEmpty: Bool = false
    var sequenceNumber: UInt16 = 0
    
    final func getFullPacket() -> Data {
        var packet = Data()
        
        // head
        packet.append(ZeroBtTypes.ZERO_BT_PKT_HEAD.data)
        
        DDLogDebug(packet.toHexString())
        
        // len words
        let body = getBody()
        var bodyLength = UInt32(body.count)
        
        // need to pad the body to a word boundary
        let padding = UInt32(ceil(Double(bodyLength)/4.0)*4) - bodyLength
        bodyLength += padding
        let length32BitWords = UInt32(Double(bodyLength) / 4.0)
        packet.append(Data(length32BitWords.data.reversed()))

        // tag
        var sequenceTag = Data()
        sequenceTag.append(sequenceNumber.data)
        sequenceTag.append(getTag())
        packet.append(Data(sequenceTag.reversed()))
        
        // body
        packet.append(body)
        if (padding > 0) {
            packet.append(Data(repeating: 0x00, count: Int(padding)))
        }
        
        // crc
        let crc = Data(packet.crc32().reversed())
        packet.append(crc)

        // tail
        packet.append(ZeroBtTypes.ZERO_BT_PKT_TAIL.data)
        
        
        return packet
    }
}
