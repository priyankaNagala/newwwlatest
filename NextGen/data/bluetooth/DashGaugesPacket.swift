//
//  DashGaugesPacket.swift
//  nextgen
//
//  Created by Scott Wang on 10/31/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class DashGaugesPacket : ZeroPacket, NSCoding {
    
    
    var gauge1: UInt8 = DashboardGauge.NONE.rawValue
    var gauge2: UInt8 = DashboardGauge.NONE.rawValue
    var gauge3: UInt8 = DashboardGauge.NONE.rawValue
    var gauge4: UInt8 = DashboardGauge.NONE.rawValue
    var timestamp: UInt32 = 0

    private let PACKET_GAUGE1_END = 1
    private let PACKET_GAUGE2_END = 2
    private let PACKET_GAUGE3_END = 3
    private let PACKET_GAUGE4_END = 4
    private let PACKET_TIMESTAMP_END = 8

    // MARK: - NSCoding
    func encode(with aCoder: NSCoder) {
        let body = getBody()
        aCoder.encode(body, forKey: "body")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        if let body = aDecoder.decodeObject(forKey: "body") as? Data {
            self.init(body)
        } else {
            self.init()
        }
    }

    // MARK: -
    
    override init() {}
    
    init(_ body: Data) {
        let packetLength = body.count

        if packetLength >= PACKET_GAUGE1_END {
            gauge1 = body.subdata(in: 0..<PACKET_GAUGE1_END).uint8
        }
        if packetLength >= PACKET_GAUGE2_END {
            gauge2 = body.subdata(in: PACKET_GAUGE1_END..<PACKET_GAUGE2_END).uint8
        }
        if packetLength >= PACKET_GAUGE3_END {
            gauge3 = body.subdata(in: PACKET_GAUGE2_END..<PACKET_GAUGE3_END).uint8
        }
        if packetLength >= PACKET_GAUGE4_END {
            gauge4 = body.subdata(in: PACKET_GAUGE3_END..<PACKET_GAUGE4_END).uint8
        }
        if packetLength >= PACKET_TIMESTAMP_END {
            timestamp = body.subdata(in: PACKET_GAUGE4_END..<PACKET_TIMESTAMP_END).uint32.byteSwapped
        }
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_DASH_GAUGES.rawValue.data
    }
    
    override func getBody() -> Data {
        guard isEmpty == false else { return ZeroBtTypes.emptyBody }
        
        var body = Data()
        body.append(gauge1.data)
        body.append(gauge2.data)
        body.append(gauge3.data)
        body.append(gauge4.data)
        body.append(timestamp.byteSwapped.data)
        
        return body
    }
}
