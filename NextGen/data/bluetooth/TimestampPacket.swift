//
//  TimestampPacket.swift
//  nextgen
//
//  Created by Scott Wang on 11/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class TimestampPacket : ZeroPacket {
    
    var utcCorrection: Int8 = 0
    var timestamp: UInt32 = 0
    
    override init() {}
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_TIMESTAMP.rawValue.data
    }
    
    init(_  data: Data) {

    }
    
    override func getBody() -> Data {
        guard isEmpty == false else { return ZeroBtTypes.emptyBody }

        var body = Data()
        body.append(Data(bytes: &utcCorrection,
                         count: MemoryLayout.size(ofValue: utcCorrection)))
        body.append(timestamp.byteSwapped.data)
        
        return body
    }
}
