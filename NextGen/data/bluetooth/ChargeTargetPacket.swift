//
//  ChargeTargetPacket.swift
//  nextgen
//
//  Created by Scott Wang on 12/4/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ChargeTargetPacket: ZeroPacket, NSCoding {
    var enabled: Bool = false
    var inStorageMode: Bool = false
    var chargeTargetSoc: UInt8 = 0
    var hardTarget: Bool = false
    var defaultTarget: Bool = false
    var chargeOverride: Bool = false
    
    private let PACKET_ENABLED_END = 1
    private let PACKET_STORAGE_MODE_END = 2
    private let PACKET_CHARGE_TARGET_END = 3
    private let PACKET_HARD_TARGET_END = 4
    private let PACKET_DEFAULT_TARGET_END = 5
    private let PACKET_CHARGE_OVERRIDE = 6
    
    // MARK: - NSCoding
    func encode(with aCoder: NSCoder) {
        let body = getBody()
        aCoder.encode(body, forKey: "body")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        if let body = aDecoder.decodeObject(forKey: "body") as? Data {
            self.init(body)
        } else {
            self.init()
        }
    }
    
    // MARK: -
    
    override init() {}
    
    init(_ body: Data) {
        let packetLength = body.count
        
        if packetLength >= PACKET_ENABLED_END {
            enabled = body.subdata(in: 0..<PACKET_ENABLED_END).uint8 != 0
        }
        if packetLength >= PACKET_STORAGE_MODE_END {
            inStorageMode = body.subdata(in: PACKET_ENABLED_END..<PACKET_STORAGE_MODE_END).uint8 != 0
        }
        if packetLength >= PACKET_CHARGE_TARGET_END {
            chargeTargetSoc = body.subdata(in: PACKET_STORAGE_MODE_END..<PACKET_CHARGE_TARGET_END).uint8
        }
        if packetLength >= PACKET_HARD_TARGET_END {
            hardTarget = body.subdata(in: PACKET_CHARGE_TARGET_END..<PACKET_HARD_TARGET_END).uint8 != 0
        }
        if packetLength >= PACKET_CHARGE_TARGET_END {
            defaultTarget = body.subdata(in: PACKET_HARD_TARGET_END..<PACKET_DEFAULT_TARGET_END).uint8 != 0
        }
        if packetLength >= PACKET_CHARGE_OVERRIDE {
            chargeOverride = body.subdata(in: PACKET_DEFAULT_TARGET_END..<PACKET_CHARGE_OVERRIDE).uint8 != 0
        }
    }
    
    override func getTag() -> Data {
        return ZeroBtTypes.Packet.ZERO_BT_PKT_CHARGE_TARGET.rawValue.data
    }
    
    override func getBody() -> Data {
        guard isEmpty == false else { return ZeroBtTypes.emptyBody }
        
        var body = Data()
        body.append(enabled ? 0x01 : 0x00)
        body.append(inStorageMode ? 0x01 : 0x00)
        body.append(chargeTargetSoc.data)
        body.append(hardTarget ? 0x01 : 0x00)
        body.append(defaultTarget ? 0x01 : 0x00)
        body.append(chargeOverride ? 0x01 : 0x00)
        
        return body
    }
}
