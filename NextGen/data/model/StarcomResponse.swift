//
//  StarcomResponse.swift
//  nextgen
//
//  Created by Scott Wang on 10/1/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki

struct StarcomResponse: Himotoki.Decodable {
    
    let result: String?
    let error: String?
    
    static func decode(_ e: Extractor) throws -> StarcomResponse {
        return try StarcomResponse(result: e <|? "result",
                                   error: nil)
    }
}
