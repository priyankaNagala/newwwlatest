//
//  MotorcycleData.swift
//  nextgen
//
//  Created by David Crawford on 6/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class MotorcycleData: Codable {
    // TODO need to get the correct part numbers
    static let ZERO_BOARD_PN_2019 = "40-08064B"
    static let ZERO_BOARD_PN_2019_2 = "40-08084B"
    static let ZERO_BOARD_PN_2017 = "40-08064"

    init(vin: String) {
        self.vin = vin
    }
    
    var vin: String
    var model:String?

    var odometer: Int { // 10 = 1mi
        get { return UserDefaults.standard.integer(forKey: Constant.PREF_ODOMETER) }
        set(value) { UserDefaults.standard.set(value, forKey: Constant.PREF_ODOMETER) }
    }
    var totalEnergy: Int {
        get { return UserDefaults.standard.integer(forKey: Constant.PREF_TOTAL_ENERGY) }
        set(value) { UserDefaults.standard.set(value, forKey: Constant.PREF_TOTAL_ENERGY) }
    }

    var hasConnectedOnce: Bool {
        get { return getBool(key: Constant.PREF_HAS_CONNECTED_ONCE) }
        set(value) { setBool(value: value, key: Constant.PREF_HAS_CONNECTED_ONCE)}
    }
    
    var canTrackMotocycleUsageLogs: Bool {
        get { return getBool(key: Constant.PREF_TRACK_MOTORCYCLE_USAGE_LOGS) }
        set(value) { setBool(value: value, key: Constant.PREF_TRACK_MOTORCYCLE_USAGE_LOGS)}
    }
    
    var dashboardThemeBasedOnRideMode: Bool {
        get { return getBool(key: Constant.PREF_DASHBOARD_THEME_BASED_ON_RIDE_MODE, defaultValue: true) }
        set(value) { setBool(value: value, key: Constant.PREF_DASHBOARD_THEME_BASED_ON_RIDE_MODE)}
    }
    
    var dashboardTheme: DashboardTheme {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_DASHBOARD_THEME] {
                return DashboardTheme(rawValue: ret as! String)!
            }
            return DashboardTheme.DARK_GREEN
        }
        set(value) {
            var vinData = getData()
            vinData[Constant.PREF_DASHBOARD_THEME] = value.rawValue
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var dashboardGauges: DashboardGauges {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_DASHBOARD_GAUGES] as! Data? {
                if let decoded = try? JSONDecoder().decode(DashboardGauges.self, from: ret as Data) {
                    return decoded
                }
            }
            return DashboardGauges()
        }
        set(value) {
            var vinData = getData()
            let encoded = try? JSONEncoder().encode(value)
            vinData[Constant.PREF_DASHBOARD_GAUGES] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var currentRideMode: RideMode {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_CURRENT_RIDE_MODE] as! Data? {
                if let decoded = try? JSONDecoder().decode(RideMode.self, from: ret as Data) {
                    return decoded
                }
            }
            return defaultRideModes[0]
        }
        set(value) {
            var vinData = getData()
            let encoded = try? JSONEncoder().encode(value)
            vinData[Constant.PREF_CURRENT_RIDE_MODE] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var activeRideModes: [RideMode] {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_ACTIVE_RIDE_MODES] as! Data? {
                if let decoded = try? JSONDecoder().decode(RideMode.RideModes.self, from: ret as Data) {
                    return decoded.modes
                }
            }
            
            return defaultRideModes
        }
        set(value) {
            var vinData = getData()
            let rideModes = RideMode.RideModes(modes: value)
            let encoded = try? JSONEncoder().encode(rideModes)
            vinData[Constant.PREF_ACTIVE_RIDE_MODES] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var defaultRideModes: [RideMode] {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_DEFAULT_RIDE_MODES] as! Data? {
                if let decoded = try? JSONDecoder().decode(RideMode.RideModes.self, from: ret as Data) {
                    return decoded.modes
                }
            }
            return RideMode.DefaultModes
        }
        set(value) {
            var vinData = getData()
            let rideModes = RideMode.RideModes(modes: value)
            let encoded = try? JSONEncoder().encode(rideModes)
            vinData[Constant.PREF_DEFAULT_RIDE_MODES] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    func isActiveRideMode(rideMode: RideMode) -> Bool {
        for activeRideMode in activeRideModes.enumerated() {
            if rideMode.name == activeRideMode.element.name {
                return true
            }
        }
        return false
    }
    
    func isCurrentRideMode(rideMode: RideMode) -> Bool {
        return currentRideMode.name == rideMode.name
    }
    
    func deactivateRideMode(rideMode: RideMode) -> Bool {
        if isCurrentRideMode(rideMode: rideMode) {
            return false
        }
        var activeModes = activeRideModes
        for (idx, activeRideMode) in activeModes.enumerated() {
            if rideMode.name == activeRideMode.name {
                activeModes.remove(at: idx)
                activeRideModes = activeModes
                return true
            }
        }
        return false
    }
    
    func activateRideMode(rideMode: RideMode) {
        var activeModes = activeRideModes
        activeModes.append(rideMode)
        activeRideModes = activeModes
    }
    
    func getActiveCustomMode() -> RideMode? {
        if activeRideModes.count > Constant.RIDE_MODE_CUSTOM_POSITION {
            return activeRideModes[Constant.RIDE_MODE_CUSTOM_POSITION]
        }
        return nil
    }
    
    func setActiveCustomMode(_ rideMode: RideMode) {
        activeRideModes[Constant.RIDE_MODE_CUSTOM_POSITION] = rideMode
    }
    
    var myRideModes: [RideMode] {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_MY_RIDE_MODES] as! Data? {
                if let decoded = try? JSONDecoder().decode(RideMode.RideModes.self, from: ret as Data) {
                    return decoded.modes
                }
            }
            return [RideMode]()
        }
        set(value) {
            var vinData = getData()
            let rideModes = RideMode.RideModes(modes: value)
            let encoded = try? JSONEncoder().encode(rideModes)
            vinData[Constant.PREF_MY_RIDE_MODES] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var scheduledCharging: ScheduledCharging {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_SCHEDULED_CHARGING] as! Data? {
                if let decoded = try? JSONDecoder().decode(ScheduledCharging.self, from: ret as Data) {
                    return decoded
                }
            }
            return ScheduledCharging()
        }
        set(value) {
            var vinData = getData()
            let encoded = try? JSONEncoder().encode(value)
            vinData[Constant.PREF_SCHEDULED_CHARGING] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    var batteryManager: BatteryManager {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_BATTERY_MANAGER] as! Data? {
                if let decoded = try? JSONDecoder().decode(BatteryManager.self, from: ret as Data) {
                    return decoded
                }
            }
            return BatteryManager()
        }
        set(value) {
            var vinData = getData()
            let encoded = try? JSONEncoder().encode(value)
            vinData[Constant.PREF_BATTERY_MANAGER] = encoded
            UserDefaults.standard.set(vinData, forKey: getVinKey())
        }
    }
    
    
    var offlineQueue: [ZeroPacket] {
        get {
            let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
            if let ret = vinData?[Constant.PREF_OFFLINE_QUEUE] as! Data? {
                if let decodedQueue = NSKeyedUnarchiver.unarchiveObject(with: ret) as? [ZeroPacket] {
                    return decodedQueue
                }
            }
            return [ZeroPacket]()
        }
        set (value) {
            var vinData = getData()
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: value)
            vinData[Constant.PREF_OFFLINE_QUEUE] = encodedData
            UserDefaults.standard.set(vinData, forKey:getVinKey())
        }
    }
    
    func isMbbPartNumber(_ partNumber: String) -> Bool {
        return partNumber == MotorcycleData.ZERO_BOARD_PN_2019 ||
            partNumber == MotorcycleData.ZERO_BOARD_PN_2019_2 ||
            partNumber == MotorcycleData.ZERO_BOARD_PN_2017
    }
    
    private func getBool(key: String, defaultValue: Bool = false) -> Bool {
        let vinData = UserDefaults.standard.dictionary(forKey: getVinKey())
        if let ret = vinData?[key] {
            return ret as! Bool
        }
        return defaultValue
    }
    
    private func setBool(value: Bool, key: String) {
        var vinData = getData()
        vinData[key] = value
        UserDefaults.standard.set(vinData, forKey: getVinKey())
    }
    
    private func getData() -> [String:Any] {
        return UserDefaults.standard.dictionary(forKey: getVinKey()) ?? [String:Any]()
    }
    
    private func getVinKey() -> String {
        let theKey = String(format:"vin_%@", vin)
        return theKey
    }
}
