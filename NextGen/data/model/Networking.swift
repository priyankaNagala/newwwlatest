//
//  Networking.swift
//  NextGenModel
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxSwift

protocol Networking {
    func requestJSON(url: String, parameters: [String : AnyObject]?, headers:[String: String]?)
        -> Observable<(HTTPURLResponse, Any)>
    func requestDownload(url: String, parameters: [String: AnyObject]?, headers: [String: String]?)
        -> Observable<(HTTPURLResponse, Data)>

}
