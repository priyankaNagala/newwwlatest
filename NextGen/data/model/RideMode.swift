//
//  RideMode.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class RideMode: NSObject, Codable, NSCopying {
    
    var type: RideModeType
    var name: String
    var tractionControlEnabled: Bool
    var tractionControlType: TractionControlType
    var maxSpeed: Int
    var maxPower: Int
    var maxTorque: Int
    var neutralRegeneration: Int
    var brakeRegeneration: Int
    var absControl: Bool
    var dashboardTheme: DashboardTheme
    var swapable: Bool
    var customizable: Bool
    
    struct RideModes: Codable {
        var modes = [RideMode]()
    }
    
    enum RideModeType: String, Codable {
        case ZERO, CUSTOM
    }
    
    // TODO: These will ultimately be based on min/max we query from the MBB
    static let DEFAULT_MAX_SPEED = 75
    static let DEFAULT_MAX_SPEED_MIN = 60
    static let DEFAULT_MAX_SPEED_MAX = 120
    
    static let DEFAULT_MAX_POWER = 75
    static let DEFAULT_MAX_TORQUE = 75
    static let DEFAULT_NEUTRAL_REGENERATION = 50
    static let DEFAULT_BRAKE_REGENERATION = 50
    
    static let DEFAULT_TRACTION_CONTROL_ENABLED = true
    static let DEFAULT_TRACTION_CONTROL_TYPE = TractionControlType.STREET
    static let DEFAULT_DASHBOARD_THEME = DashboardTheme.DARK_GREEN
    static let DEFAULT_ABS_CONTROL_ENABLED = true
    
    static let ZeroSport = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_sport".localized,
        tractionControlEnabled: true,
        tractionControlType: TractionControlType.SPORT,
        maxSpeed: 100,
        maxPower: 80,
        maxTorque: 80,
        neutralRegeneration: 80,
        brakeRegeneration: 80,
        dashboardTheme: DashboardTheme.DARK_ORANGE
    )
    
    static let ZeroSportPlus = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_sport_plus".localized,
        tractionControlEnabled: true,
        tractionControlType: TractionControlType.SPORT,
        maxSpeed: 120,
        maxPower: 100,
        maxTorque: 100,
        neutralRegeneration: 100,
        brakeRegeneration: 100,
        dashboardTheme: DashboardTheme.DARK_ORANGE
    )
    
    static let ZeroRain = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_rain".localized,
        tractionControlEnabled: true,
        tractionControlType: TractionControlType.RAIN,
        maxSpeed: 60,
        maxPower: 60,
        maxTorque: 60,
        neutralRegeneration: 70,
        brakeRegeneration: 70,
        dashboardTheme: DashboardTheme.DARK_BLUE
    )
    
    static let ZeroStreet = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_street".localized,
        tractionControlEnabled: true,
        tractionControlType: TractionControlType.STREET,
        maxSpeed: 80,
        maxPower: 75,
        maxTorque: 75,
        neutralRegeneration: 75,
        brakeRegeneration: 75,
        dashboardTheme: DashboardTheme.DARK_GREEN
    )
    
    static let ZeroEco = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_eco".localized,
        tractionControlEnabled: true,
        tractionControlType: TractionControlType.STREET,
        maxSpeed: 70,
        maxPower: 60,
        maxTorque: 60,
        neutralRegeneration: 80,
        brakeRegeneration: 100,
        dashboardTheme: DashboardTheme.DARK_GREEN
    )
    
    static let DefaultModes = [
        ZeroSport,
        ZeroSportPlus,
        ZeroRain,
        ZeroStreet,
        ZeroEco
    ]

    static let DefaultRideMode = RideMode(
        type: RideModeType.ZERO,
        name: "ride_mode_sport".localized,
        tractionControlEnabled: DEFAULT_TRACTION_CONTROL_ENABLED,
        tractionControlType: DEFAULT_TRACTION_CONTROL_TYPE,
        maxSpeed: DEFAULT_MAX_SPEED,
        maxPower: DEFAULT_MAX_POWER,
        maxTorque: DEFAULT_MAX_TORQUE,
        neutralRegeneration: DEFAULT_NEUTRAL_REGENERATION,
        brakeRegeneration: DEFAULT_BRAKE_REGENERATION,
        absControl: DEFAULT_ABS_CONTROL_ENABLED,
        dashboardTheme: DEFAULT_DASHBOARD_THEME
    )
    
    init(type: RideModeType,
         name: String,
         tractionControlEnabled: Bool = DEFAULT_TRACTION_CONTROL_ENABLED,
         tractionControlType: TractionControlType = DEFAULT_TRACTION_CONTROL_TYPE,
         maxSpeed: Int = DEFAULT_MAX_SPEED,
         maxPower: Int = DEFAULT_MAX_POWER,
         maxTorque: Int = DEFAULT_MAX_TORQUE,
         neutralRegeneration: Int = DEFAULT_NEUTRAL_REGENERATION,
         brakeRegeneration: Int = DEFAULT_BRAKE_REGENERATION,
         absControl: Bool = DEFAULT_ABS_CONTROL_ENABLED,
         dashboardTheme: DashboardTheme = DashboardTheme.DARK_GREEN,
         swapable: Bool = false,
         customizable: Bool = false,
         order: Int = 0) {
        
        self.type = type
        self.name = name
        self.tractionControlEnabled = tractionControlEnabled
        self.tractionControlType = tractionControlType
        self.maxSpeed = maxSpeed
        self.maxPower = maxPower
        self.maxTorque = maxTorque
        self.neutralRegeneration = neutralRegeneration
        self.brakeRegeneration = brakeRegeneration
        self.absControl = absControl
        self.dashboardTheme = dashboardTheme
        self.swapable = swapable
        self.customizable = customizable
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        // TODO: Use Codable interface instead?
        return RideMode(type: RideModeType.CUSTOM,
                        name: name,
                        tractionControlEnabled: tractionControlEnabled,
                        tractionControlType: tractionControlType,
                        maxSpeed: maxSpeed,
                        maxPower: maxPower,
                        maxTorque: maxTorque,
                        neutralRegeneration: neutralRegeneration,
                        brakeRegeneration: brakeRegeneration,
                        absControl: absControl,
                        dashboardTheme: dashboardTheme)
    }
}
