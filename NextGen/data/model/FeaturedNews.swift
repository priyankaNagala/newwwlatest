//
//  FeaturedNews.swift
//  nextgen
//
//  Created by Scott Wang on 3/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki


struct Link: Himotoki.Decodable {
    let url: String
    let title: String
    let source: String
    
    static func decode(_ e: Extractor) throws -> Link {
        return try Link(
            url: e <| "url",
            title: e <| "title_no_source",
            source: e <|? "source" ?? ""
        )
    }
}

struct FeaturedNews: Himotoki.Decodable {
    
    let links: [Link]
    
    static func decode(_ e: Extractor) throws -> FeaturedNews {
        return try FeaturedNews(links: e <|| "links")
    }
}
