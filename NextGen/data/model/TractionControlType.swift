//
//  TractionControlType.swift
//  nextgen
//
//  Created by David Crawford on 6/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

enum TractionControlType: String, Codable {
    case STREET, SPORT, RAIN
    
    func localizedString() -> String {
        switch self {
        case .STREET: return "ride_mode_traction_control_street".localized
        case .SPORT: return "ride_mode_traction_control_sport".localized
        case .RAIN: return "ride_mode_traction_control_rain".localized
        }
    }
}
