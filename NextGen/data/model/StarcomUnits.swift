//
//  StarcomUnits.swift
//  nextgen
//
//  Created by Scott Wang on 11/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki

struct StarcomUnits: Himotoki.Decodable {
    let units: [StarcomUnit]
    
    static func decode(_ e: Extractor) throws -> StarcomUnits {
        return try StarcomUnits(units: e <|| "")
    }
}

struct StarcomUnit: Himotoki.Decodable {

    let unitNumber: String
    let name: String?
    let address: String?
    let vehicleModel: String?
    let vehicleColor: String?
    let unitType: String?
    let icon: String?
    let active: String?
    
    static func decode(_ e: Extractor) throws -> StarcomUnit {
        return try StarcomUnit(unitNumber: e <| "unitnumber",
                               name: e <|? "name",
                               address: e <|? "address",
                               vehicleModel: e <|? "vehiclemodel",
                               vehicleColor: e <|? "vehiclecolor",
                               unitType: e <|? "unittype",
                               icon: e <|? "icon",
                               active: e <|? "active")
    }
}
