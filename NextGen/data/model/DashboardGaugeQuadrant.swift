//
//  DashboardGaugeQuadrant.swift
//  nextgen
//
//  Created by David Crawford on 6/12/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

enum DashboardGaugeQuadrant : String, Codable {
    case A, B, C, D
    
    func localizedString() -> String {
        return String(format:"dashboard_settings_quadrant_gauge".localized, self.rawValue)
    }
}
