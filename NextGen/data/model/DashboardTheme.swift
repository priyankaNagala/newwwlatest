//
//  DashboardTheme.swift
//  nextgen
//
//  Created by David Crawford on 6/11/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

enum DashboardTheme : String, Codable {
    
    case DARK_GREEN, DARK_BLUE, LIGHT_ORANGE, LIGHT_BLUE, DARK_ORANGE
    
    func localizedString() -> String {
        switch self {
        case .DARK_GREEN: return "ride_mode_dark_green".localized
        case .DARK_BLUE: return "ride_mode_dark_blue".localized
        case .LIGHT_ORANGE: return "ride_mode_light_orange".localized
        case .LIGHT_BLUE: return "ride_mode_light_blue".localized
        case .DARK_ORANGE: return "ride_mode_dark_orange".localized
        }
    }
}
