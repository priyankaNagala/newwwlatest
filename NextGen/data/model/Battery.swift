//
//  Battery.swift
//  nextgen
//
//  Created by Scott Wang on 7/19/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation


class Battery {
    var targetRange: Int
    var chargeTime: Int
    var chargeRate: Int
    var currentCharge: Float
    var currentRange: Int
    var isCharging: Bool
    var targetCharge: Float
    var storageMode: Bool
    
    init(currentCharge: Float,
         currentRange: Int,
         chargeRate: Int,
         chargeTime: Int,
         targetRange: Int,
         isCharging: Bool,
         targetCharge: Float,
         storageMode: Bool) {
        self.currentRange = currentRange
        self.currentCharge = currentCharge
        self.chargeRate = chargeRate
        self.chargeTime = chargeTime
        self.targetRange = targetRange
        self.isCharging = isCharging
        self.targetCharge = targetCharge
        self.storageMode = storageMode
    }
}
