//
//  LastTransmit.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki
import GoogleMaps

struct LastTransmit: Himotoki.Decodable {
 
    let name: String
    let dateTimeUtc: String?
    let dateTimeActual: String?
    let longitude: String?
    let latitude: String?
    let unitNumber: String
    let stateOfCharge: Int
    let charging: Bool
    let tippedOver: Bool
    let chargingTimeLeft: Int
    let pluggedin: Int
    
    enum PlugStatus {
        case UNPLUGGED
        case PLUGGED
        
        var localizedString: String {
            switch self {
            case .UNPLUGGED: return "remote_connect_plug_status_unplugged".localized
            case .PLUGGED: return "remote_connect_plug_status_plugged".localized
            }
        }
    }
    
    enum ChargeActivity {
        case NOTCHARGING
        case CHARGING
        
        var localizedString: String {
            switch self {
            case .NOTCHARGING: return "remote_connect_charge_activity_not_charging".localized
            case .CHARGING: return "remote_connect_charge_activity_charging".localized
            }
        }
    }
    
    var lastSeen: Date? {
        if dateTimeActual == nil {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        let date = formatter.date(from: dateTimeActual!)
        return date
    }
    
    var plugStatus: PlugStatus {
        return pluggedin > 0 ? PlugStatus.PLUGGED : PlugStatus.UNPLUGGED
    }
    
    var chargeActivity: ChargeActivity {
        return charging ? ChargeActivity.CHARGING : ChargeActivity.NOTCHARGING
    }

    var locationCoordinate: CLLocationCoordinate2D? {
        if let lon = longitude, let lonDbl = Double(lon),
            let lat = latitude, let latDbl =  Double(lat) {
            return CLLocationCoordinate2D(latitude: latDbl, longitude: lonDbl)
        }
        return nil
    }
 
    static func decode(_ e: Extractor) throws -> LastTransmit {
        return try LastTransmit(
            name: e <| "name",
            dateTimeUtc: e <|? "datetime_utc",
            dateTimeActual: e <|? "datetime_actual",
            longitude: e <|? "longitude",
            latitude: e <|? "latitude",
            unitNumber: e <| "unitnumber",
            stateOfCharge: e <| "soc",
            charging: e <| "charging",
            tippedOver: e <| "tipover",
            chargingTimeLeft: e <| "chargingtimeleft",
            pluggedin: e <| "pluggedin"
        )
    }
}
