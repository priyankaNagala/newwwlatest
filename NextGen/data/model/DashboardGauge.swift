//
//  DashboardGauge.swift
//  nextgen
//
//  Created by David Crawford on 6/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//
import Foundation

struct DashboardGauges: Codable {
    var gauges = [DashboardGaugeQuadrant:DashboardGauge]()
    var savedTimestamp: UInt32 = 0
    
    init() { }
    
    init(_ dashGaugesPacket: DashGaugesPacket) {
        gauges[.A] = DashboardGauge(rawValue: dashGaugesPacket.gauge1)
        gauges[.B] = DashboardGauge(rawValue: dashGaugesPacket.gauge2)
        gauges[.C] = DashboardGauge(rawValue: dashGaugesPacket.gauge3)
        gauges[.D] = DashboardGauge(rawValue: dashGaugesPacket.gauge4)
    }
}

enum DashboardGauge : UInt8, Codable, CaseIterable {
    
    /*
     1    State of Charge
     2    Estimated Range
     3    Motor Temperature     Limited to only Gauge A and Gauge B
     4    Battery Temperature     Limited to only Gauge A and Gauge B
     5    Efficiency Instantaneous
     6    Efficiency Trip
     7    Efficiency Lifetime
     8    RPM
     9    None
     */

    case
        STATE_OF_CHARGE = 1,
        ESTIMATED_RANGE = 2,
        MOTOR_TEMPERATURE = 3,
        BATTERY_TEMPERATURE = 4,
        EFFICIENCY_INSTANT = 5,
        EFFICIENCY_AVERAGE = 6,
        EFFICIENCY_LIFETIME = 7,
        RPM = 8,
        NONE = 9
    
    func localizedString() -> String {
        switch self {
        case .NONE: return "dashboard_settings_gauge_none".localized
        case .STATE_OF_CHARGE: return "dashboard_settings_gauge_state_of_charge".localized
        case .ESTIMATED_RANGE: return "dashboard_settings_gauge_estimated_range".localized
        case .MOTOR_TEMPERATURE: return "dashboard_settings_gauge_motor_temperature".localized
        case .BATTERY_TEMPERATURE: return "dashboard_settings_gauge_battery_temperature".localized
        case .EFFICIENCY_INSTANT: return "dashboard_settings_gauge_efficiency_instant".localized
        case .EFFICIENCY_AVERAGE: return "dashboard_settings_gauge_efficiency_average".localized
        case .EFFICIENCY_LIFETIME: return "dashboard_settings_gauge_efficiency_lifetime".localized
        case .RPM: return "dashboard_settings_gauge_rpm".localized
        }
    }
}
