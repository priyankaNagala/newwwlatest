//
//  Network.swift
//  NextGenModel
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxSwift
import RxAlamofire
import CocoaLumberjack

final class Network: Networking {
    private let queue = DispatchQueue(label: "NextGenModel.Network.Queue")

    init() { }

    func requestJSON(url: String, parameters: [String : AnyObject]?, headers: [String: String]?)
        -> Observable<(HTTPURLResponse, Any)>
    {
        DDLogDebug("URL: \(url)")
        if let parameters = parameters {
            DDLogDebug("Parameters: \(parameters)")
        }
        return RxAlamofire.requestJSON(.get, url, parameters: parameters, headers: headers)
    }
    
    func requestDownload(url: String, parameters: [String: AnyObject]?, headers: [String: String]?)
        -> Observable<(HTTPURLResponse, Data)>
    {
        DDLogDebug("URL: \(url)")
        if let parameters = parameters {
            DDLogDebug("Parameters: \(parameters)")
        }
        return RxAlamofire.requestData(.get, url, parameters: parameters, headers: headers)
    }
}
