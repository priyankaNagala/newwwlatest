//
//  BatteryManager.swift
//  nextgen
//
//  Created by Scott Wang on 8/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class BatteryManager: Codable {
    // Ride Style
    enum RideStyle: Int, Codable {
        case CONSERVATIVE, AGGRESSIVE
        
        func calculationValue() -> UInt {
            return UInt(BatteryManager.rideStyleLookup[self]! * 100)
        }
    }
    static var rideStyleLookup: [RideStyle : Float] = [.CONSERVATIVE: 1.0, .AGGRESSIVE: 0.90]

    // Terrain
    enum Terrain: Int, Codable {
        case FLAT, HILLS
        
        func calculationValue() -> UInt {
            return UInt(BatteryManager.terrainLookup[self]! * 100)
        }
    }
    static var terrainLookup: [Terrain : Float] = [.FLAT: 1.0, .HILLS: 0.90]
    
    // Air Temp
    enum AirTemperature: Int, Codable {
        case COLD, MILD, WARM
        
        func calculationValue() -> UInt {
            return UInt(BatteryManager.airTemperatureLookup[self]! * 100)
        }
    }
    static var airTemperatureLookup: [AirTemperature : Float] = [.COLD: 0.90, .MILD: 0.95, .WARM: 1.0]
    
    // Route Type
    enum RouteType: Int, Codable, CaseIterable {
        case CITY, HIGHWAY, CITYHIGHWAY, FREEWAY, CITYFREEWAY
        
        func localizedString() -> String {
            switch self {
            case .CITY: return "charge_target_route_type_city".localized
            case .HIGHWAY: return "charge_target_route_type_highway".localized
            case .CITYHIGHWAY: return "charge_target_route_type_cityhighway".localized
            case .FREEWAY: return "charge_target_route_type_freeway".localized
            case .CITYFREEWAY: return "charge_target_route_type_cityfreeway".localized
            }
        }
        
        func calculationValue() -> UInt {
            return UInt(BatteryManager.routeTypeLookup[self]!)
        }
    }
    static var routeTypeLookup: [RouteType : Float] = [.CITY:        179,
                                                       .HIGHWAY:     109,
                                                       .CITYHIGHWAY: 135,
                                                       .FREEWAY:     90,
                                                       .CITYFREEWAY: 120]
    
    // Max Capacity
    enum MaxCapacity: Int, Codable {
        case NEW, MIDLIFE, OLD
        
        func calculationValue() -> UInt {
            return UInt(BatteryManager.maxCapacityLookup[self]! * 100)
        }
    }
    static var maxCapacityLookup: [MaxCapacity : Float] = [.NEW: 1.0, .MIDLIFE: 0.90, .OLD: 0.80]
    
    static func updateLookups(rangePacket: RangeEstimationPacket) {
        rideStyleLookup[.CONSERVATIVE] = Float(rangePacket.rideStyleConservative) / 100
        rideStyleLookup[.AGGRESSIVE] = Float(rangePacket.rideStyleAggresive) / 100
        
        terrainLookup[.FLAT] = Float(rangePacket.terrainFlat) / 100
        terrainLookup[.HILLS] = Float(rangePacket.terrainHill) / 100
        
        airTemperatureLookup[.COLD] = Float(rangePacket.tempCold) / 100
        airTemperatureLookup[.MILD] = Float(rangePacket.tempMild) / 100
        airTemperatureLookup[.WARM] = Float(rangePacket.tempWarm) / 100
        
        routeTypeLookup[.CITY] = Float(rangePacket.routeTypeCity)
        routeTypeLookup[.HIGHWAY] = Float(rangePacket.routeTypeHwy)
        routeTypeLookup[.CITYHIGHWAY] = Float(rangePacket.routeTypeCityHwy)
        routeTypeLookup[.FREEWAY] = Float(rangePacket.routeTypeFreeway)
        routeTypeLookup[.CITYFREEWAY] = Float(rangePacket.routeTypeCityFreeway)
        
        maxCapacityLookup[.NEW] = Float(rangePacket.maxCapacityNew) / 100
        maxCapacityLookup[.MIDLIFE] = Float(rangePacket.maxCapacityMidlife) / 100
        maxCapacityLookup[.OLD] = Float(rangePacket.maxCapacityOld) / 100
    }
    
    var chargeTargetEnabled: Bool = false
    
    var targetRange: Int = Constant.CHARGE_TARGET_DEFAULT_TARGET_RANGE {
        didSet {
            calculateTargetCharge()
        }
    }
    
    var targetChargeRounded: Float = Constant.CHARGE_TARGET_DEFAULT_TARGET_CHARGE {
        didSet {
            calculateTargetRange()
        }
    }

    var targetCharge: Float {
        set (newValue) {
            let roundedValue = round(newValue * 20) / 20 // Nearest 5
            let roundedValueInt = Int(roundedValue * 100)
            targetChargeRounded = Float(roundedValueInt) / 100.0
        }
        get {
            return targetChargeRounded
        }
    }
    var chargeTargetNotificationEnabled: Bool = false
    var airTemperature: AirTemperature = Constant.CHARGE_TARGET_DEFAULT_AIR_TEMPERATURE {
        didSet {
            calculateTargetRange()
        }
    }
    var terrain: Terrain = Constant.CHARGE_TARGET_DEFAULT_TERRAIN {
        didSet {
            calculateTargetRange()
        }
    }
    var rideStyle: RideStyle = Constant.CHARGE_TARGET_DEFAULT_RIDE_STYLE {
        didSet {
            calculateTargetRange()
        }
    }
    var routeType: RouteType = Constant.CHARGE_TARGET_DEFAULT_ROUTE_TYPE {
        didSet {
            calculateTargetRange()
        }
    }
    var continueCharging: Bool = true
    var setAsDefault: Bool = false
    var maxCapacity: MaxCapacity = .NEW  {
        didSet {
            calculateTargetRange()
        }
    }
    var lockRange: Bool = false
    var lockCharge: Bool {
        set (newValue) {
            lockRange = !newValue
        }
        get {
            return !lockRange
        }
    }
    var maxRange: Int {
        get {
            return Int(BatteryManager.routeTypeLookup[routeType]! *
                BatteryManager.terrainLookup[terrain]! *
                BatteryManager.airTemperatureLookup[airTemperature]! *
                BatteryManager.rideStyleLookup[rideStyle]! *
                BatteryManager.maxCapacityLookup[maxCapacity]!)
        }
    }

    var targetRangeFormatted: String {
        get {
            if DataService.shared.usingMetricDistance {
                let unit = "global_km".localized
                let conversion = Int(Float(targetRange) * Constant.KM_IN_ONE_MILE)
                return "\(conversion)\(unit)"
            } else {
                let unit = "global_mi".localized
                return "\(targetRange)\(unit)"
            }
        }
    }
    
    var targetChargeFormatted: String {
        get {
            let percentage = Int(roundf(targetCharge * 100.0))
            return "\(percentage)%"
        }
    }
    
    init() {
        calculateTargetRange()
    }
    
    init (chargeTargetEnabled: Bool = false,
          targetRange: Int,
          targetCharge: Float,
          chargeTargetNotificationEnabled: Bool,
          airTemperature: AirTemperature,
          terrain: Terrain,
          rideStyle: RideStyle,
          routeType: RouteType,
          continueCharging: Bool,
          setAsDefault: Bool,
          maxCapacity: MaxCapacity) {
        self.chargeTargetEnabled = chargeTargetEnabled
        self.targetRange = targetRange
        self.targetCharge = targetCharge
        self.chargeTargetNotificationEnabled = chargeTargetNotificationEnabled
        self.airTemperature = airTemperature
        self.terrain = terrain
        self.rideStyle = rideStyle
        self.routeType = routeType
        self.continueCharging = continueCharging
        self.setAsDefault = setAsDefault
        self.maxCapacity = maxCapacity
    }
    
    // MARK: - Helpers
    func calculateTargetRange() {
        guard lockRange == false else { return }
        let result = rangeGivenCharge(charge: targetCharge)
        
        if targetRange != result { // Only set if different to avoid infinite didSet loop
            targetRange = result
        }
    }
    
    func calculateTargetCharge() {
        guard lockCharge == false else { return }
        let result = Float(targetRange) / (BatteryManager.routeTypeLookup[routeType]! *
            BatteryManager.terrainLookup[terrain]! *
            BatteryManager.airTemperatureLookup[airTemperature]! *
            BatteryManager.rideStyleLookup[rideStyle]! *
            BatteryManager.maxCapacityLookup[maxCapacity]!)

        if fabs(result - targetCharge) >= 0.01 { // Only set if different to avoid infinite didSet loop
            targetCharge = result > 1.0 ? 1.0 : result // Max 100%
        }
    }
    
    func rangeGivenCharge(charge: Float) -> Int {
        return Int(charge *
            BatteryManager.routeTypeLookup[routeType]! *
            BatteryManager.terrainLookup[terrain]! *
            BatteryManager.airTemperatureLookup[airTemperature]! *
            BatteryManager.rideStyleLookup[rideStyle]! *
            BatteryManager.maxCapacityLookup[maxCapacity]!)
    }
}
