//
//  ScheduledCharging.swift
//  nextgen
//
//  Created by Scott Wang on 7/25/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class ScheduledCharging: Codable {
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "H:mm"
        return formatter
    }()
    static let amPmFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        return formatter
    }()
    
    enum DayOfWeek: Int, Codable {
        case sun, mon, tue, wed, thu, fri, sat
    }
    
    static let allDaysOfWeek:[DayOfWeek] = [.sun, .mon, .tue, .wed, .thu, .fri, .sat]
    
    var enabled: Bool = false
    
    // times are stored as Ints in 24 hr clock.  100 -> 1:00, 1300 -> 13:00, 1530 -> 15:30
    var plugInStartTime: Int = Constant.SCHEDULED_CHARGING_DEFAULT_PLUG_IN_START {
        didSet {
            enforceActiveChargingConstraints()
        }
    }
    // Duration stored in minutes
    var plugInDuration: Int = Constant.SCHEDULED_CHARGING_DEFAULT_PLUG_IN_DURATION {
        didSet {
            enforceActiveChargingConstraints()
        }
    }
    var activeChargingStartTime: Int = Constant.SCHEDULED_CHARGING_DEFAULT_ACTIVE_CHARGING_START {
        didSet {
            enforceActiveChargingConstraints()
        }
    }
    var activeChargingDuration: Int = Constant.SCHEDULED_CHARGING_DEFAULT_ACTIVE_CHARGING_DURATION {
        didSet {
            enforceActiveChargingConstraints()
        }
    }
    var daysOfWeek: [DayOfWeek] = ScheduledCharging.allDaysOfWeek
    var maxStateOfCharge: Float = Constant.CHARGE_TARGET_DEFAULT_MAX_SOC
    
    var plugInStartTimeFormatted: String {
        get {
            return ScheduledCharging.getTimeAsString(time: plugInStartTime,
                                                     using24HrClock: DataService.shared.using24HrClock())
        }
    }
    var plugInDurationFormatted: String {
        get {
            return ScheduledCharging.getDurationAsString(plugInDuration)
        }
    }
    var plugInEndTimeFormatted: String {
        get {
            return ScheduledCharging.getEndTimeAsString(time: plugInStartTime,
                                                        duration: plugInDuration,
                                                        using24HrClock: DataService.shared.using24HrClock())
        }
    }
    var activeChargingStartTimeFormatted: String {
        get {
            return ScheduledCharging.getTimeAsString(time: activeChargingStartTime,
                                                     using24HrClock: DataService.shared.using24HrClock())
        }
    }
    var activeChargingDurationFormatted: String {
        get {
            return ScheduledCharging.getDurationAsString(activeChargingDuration)
        }
    }
    var activeChargingEndTimeFormatted: String {
        get {
            return ScheduledCharging.getEndTimeAsString(time: activeChargingStartTime,
                                                        duration: activeChargingDuration,
                                                        using24HrClock: DataService.shared.using24HrClock())
        }
    }
    var timeTillActiveChargingFormatted: String {
        get {
            return ScheduledCharging.timeTill(time: activeChargingStartTime)
        }
    }
    
    var activeChargingMaxDuration: Int {
        get {
            // Find difference between plug-in and active charging start times
            var diff = activeChargingStartTime - plugInStartTime
            if diff < 0 {
                diff = 2400 + diff
            }
            let hrs = diff / 100
            let mins = diff % 100
            let maxDuration = (24 - hrs) * 60 - mins
            return maxDuration
        }
    }
    
    init() {
        // Default values
    }
    
    init(enabled: Bool,
         plugInStartTime: Int,
         plugtInDuration: Int,
         activeChargingStartTime: Int,
         activeChargingDuration: Int,
         daysOfWeek: [DayOfWeek],
         maxStateOfCharge: Float) {
        
        self.enabled = enabled
        self.plugInStartTime = plugInStartTime
        self.plugInDuration = plugtInDuration
        self.activeChargingStartTime = activeChargingStartTime
        self.activeChargingDuration = activeChargingDuration
        self.daysOfWeek = daysOfWeek
        self.maxStateOfCharge = maxStateOfCharge
    }
    
    func toggleDayOfWeek(_ dayOfWeek: DayOfWeek) {
        if daysOfWeek.contains(dayOfWeek) {
            daysOfWeek = daysOfWeek.filter { $0 != dayOfWeek }
        } else {
            daysOfWeek.append(dayOfWeek)
        }
    }
    
    // MARK: - Enforce constraints
    func enforceActiveChargingConstraints() {
        enforceActiveChargingStart()
        enforceActiveChargingDuration()
    }
    
    // Make sure active charging start time is within plug in start time and duration
    func enforceActiveChargingStart() {
        let plugInEndTime24HrStr = ScheduledCharging.getEndTimeAsString(time: plugInStartTime,
                                                                 duration: plugInDuration,
                                                                 using24HrClock: true)
        if let plugInEndTimeInt = ScheduledCharging.getTimeAsInt(time: plugInEndTime24HrStr, using24HrClock: true) {

            // Simple case
            if activeChargingStartTime >= plugInStartTime && activeChargingStartTime <= plugInEndTimeInt {
                return
            }
            // Times could wrap around midnight 2200 -> 500
            if plugInStartTime > plugInEndTimeInt {
                if activeChargingStartTime <= plugInEndTimeInt || activeChargingStartTime >= plugInStartTime {
                    return
                }
            }
            // Handle 24 window
            if plugInDuration == (24 * 60) {
                return
            }
            
            // Not within plug in start and duration.  Adjust active charging start time
            // Pick the closer of the plugin start and end
            if abs(activeChargingStartTime - plugInStartTime) > abs(activeChargingStartTime - plugInEndTimeInt) {
                activeChargingStartTime = plugInEndTimeInt
            } else {
                activeChargingStartTime = plugInStartTime
            }
        }
    }
    
    // Make sure active charging end time is within 24 hr of plug in start time
    func enforceActiveChargingDuration() {
        if activeChargingDuration > activeChargingMaxDuration {
            activeChargingDuration = activeChargingMaxDuration
        }
    }
    
    
    // MARK: - Helpers

    // Used for a countdown till active charging
    static func timeTill(time: Int) -> String {
        let hour = time / 100
        let min = time % 100
        let now = Date()
        var later: Date?
        later = Calendar.current.date(bySettingHour: hour, minute: min, second: 0, of: now)
        if later != nil {
            if later! < now {
                later = Calendar.current.date(byAdding: .day, value: 1, to: later!)
            }
        
            if later != nil {
                let timeLeft = Calendar.current.dateComponents([.hour, .minute], from: now, to: later!)
                return String(format: "%d:%02d", timeLeft.hour ?? 0, timeLeft.minute ?? 0)
            } else {
                return "0:00"
            }
        }
        return "0:00"
    }

    static func getTimeAsString(time: Int, using24HrClock: Bool) -> String {
        let hour = time / 100
        let minute = time % 100
        
        if let date = dateFormatter.date(from: String(format: "%d:%02d", hour, minute)) {
            if using24HrClock {
                return dateFormatter.string(from: date)
            } else {
                return amPmFormatter.string(from: date)
            }
        }
        return ""
    }
    
    static func getDurationAsString(_ duration: Int) -> String {
        guard duration > 0 else { return "global_off".localized }
        
        let hours = Double(duration) / 60.0
        return "\(hours)" + "global_abbreviation_hour".localized
    }
    
    static func getTimeAsInt(time: String, using24HrClock: Bool) -> Int? {
        let date:Date? = using24HrClock ? dateFormatter.date(from: time) : amPmFormatter.date(from: time)
        if let date = date {
            let hour = Calendar.current.component(.hour, from: date)
            let min = Calendar.current.component(.minute, from: date)
                
            return hour * 100 + min
        }
        return nil
    }
    
    static func getDurationAsInt(duration: String) -> Int? {
        if duration == "global_off".localized {
            return 0
        }
        
        let hour = "global_abbreviation_hour".localized
        let digitsOnly = duration.replacingOccurrences(of: hour, with: "")
        if let dblValue = Double(digitsOnly) {
            return Int(dblValue * 60)
        }
        return nil
    }
    
    // Returns - eg "10:00pm"
    static func getEndTimeAsString(time: Int, duration: Int, using24HrClock: Bool) -> String {
        if let startDate = dateFormatter.date(from: getTimeAsString(time: time, using24HrClock: true)) {
            var timeInterval = DateComponents()
            timeInterval.hour = duration >= 60 ? duration / 60 : 0
            timeInterval.minute = (duration % 60) > 0 ? 30 : 0
            if let endDate = Calendar.current.date(byAdding: timeInterval, to: startDate) {
                if !using24HrClock {
                    return amPmFormatter.string(from: endDate)
                } else {
                    return dateFormatter.string(from: endDate)
                }
            }
        }
        return ""
    }
}
