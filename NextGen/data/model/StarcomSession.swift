//
//  StarcomSession.swift
//  nextgen
//
//  Created by Scott Wang on 10/2/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki

struct StarcomSession: Himotoki.Decodable {
    
    let sessionId: String?
    let result: String?
    let error: String? 
    
    static func decode(_ e: Extractor) throws -> StarcomSession {
        return try StarcomSession(sessionId: e <|? "sid",
                                  result: e <|? "result",
                                  error: e <|? "error")
    }
}
