//
//  CapturedRide.swift
//  nextgen
//
//  Created by Scott Wang on 12/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import GoogleMaps
import RxCocoa
import RxSwift

class RideDataPoint: NSObject, NSCoding, Encodable {
    
    var odometer: Int
    var totalEnergy: Int
    var avgEfficiency: Int
    var speed: Int
    var soc: Int
    var leanAngle: Int
    var power: Int
    var torque: Int
    var coastRegen: Int
    var brakeRegen: Int
    var longitude: Double
    var latitutude: Double
    var date: Date = Date()
    
    init(_ packet: RideSharePacket) {
        odometer = Int(packet.odometer)
        totalEnergy = Int(packet.totalEnergy)
        avgEfficiency = Int(packet.avgEfficiency)
        speed = Int(packet.speed)
        soc = Int(packet.soc)
        leanAngle = Int(packet.leanAngle)
        power = Int(packet.power)
        torque = Int(packet.torque)
        coastRegen = Int(packet.coastRegen)
        brakeRegen = Int(packet.brakeRegen)
        longitude = packet.longitude
        latitutude = packet.latitutude
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(odometer, forKey: "odometer")
        aCoder.encode(totalEnergy, forKey: "totalEnergy")
        aCoder.encode(avgEfficiency, forKey: "avgEfficiency")
        aCoder.encode(speed, forKey: "speed")
        aCoder.encode(soc, forKey: "soc")
        aCoder.encode(leanAngle, forKey: "leanAngle")
        aCoder.encode(power, forKey: "power")
        aCoder.encode(torque, forKey: "torque")
        aCoder.encode(coastRegen, forKey: "coastRegen")
        aCoder.encode(brakeRegen, forKey: "brakeRegen")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(latitutude, forKey: "latitutude")
        aCoder.encode(date, forKey: "date")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.odometer = aDecoder.decodeInteger(forKey: "odometer")
        self.totalEnergy = aDecoder.decodeInteger(forKey: "totalEnergy")
        self.avgEfficiency = aDecoder.decodeInteger(forKey: "avgEfficiency")
        self.speed = aDecoder.decodeInteger(forKey: "speed")
        self.soc = aDecoder.decodeInteger(forKey: "soc")
        self.leanAngle = aDecoder.decodeInteger(forKey: "leanAngle")
        self.power = aDecoder.decodeInteger(forKey: "power")
        self.torque = aDecoder.decodeInteger(forKey: "torque")
        self.coastRegen = aDecoder.decodeInteger(forKey: "coastRegen")
        self.brakeRegen = aDecoder.decodeInteger(forKey: "brakeRegen")
        self.longitude = aDecoder.decodeDouble(forKey: "longitude")
        self.latitutude = aDecoder.decodeDouble(forKey: "latitutude")
        if let obj = aDecoder.decodeObject(forKey: "date") as? Date {
            self.date = obj
        }
    }
}
class CapturedRide: NSObject, NSCoding, Encodable {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(rideShareDataPoints, forKey: "RideDataPoints")
        aCoder.encode(startTimestamp, forKey: "startTimestamp")
        aCoder.encode(endTimestamp, forKey: "endTimestamp")
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let obj = aDecoder.decodeObject(forKey: "RideDataPoints") as? [RideDataPoint] {
            self.rideShareDataPoints = obj
        }
        if let obj = aDecoder.decodeObject(forKey: "startTimestamp") as? Date {
            self.startTimestamp = obj
        }
        if let obj = aDecoder.decodeObject(forKey: "endTimestamp") as? Date {
            self.endTimestamp = obj
        }
    }
    
    private var rideShareDataPoints = [RideDataPoint]()
    private var startTimestamp: Date = Date()
    private var endTimestamp: Date = Date()

    override init() {
        super.init()
    }
    
    func appendRideShare(_ rideShare: RideSharePacket) {
        let rideDataPoint = RideDataPoint(rideShare)
        if rideDataPoint.longitude != 0 && rideDataPoint.latitutude != 0 {
            rideShareDataPoints.append(rideDataPoint)
        }
    }
    
    func getRideDataPointAt(_ index: Int) -> RideDataPoint? {
        if index < rideShareDataPoints.count {
            return rideShareDataPoints[index]
        }
        return nil
    }

    func getDataPointCount() -> Int {
        return rideShareDataPoints.count
    }
    
    func getInitialDataPoint() -> RideDataPoint? {
        if rideShareDataPoints.count > 0 {
            return rideShareDataPoints[0]
        }
        return nil
    }
    
    func getStartTime() -> Date {
        return startTimestamp
    }
    
    func getEndTime() -> Date {
        return endTimestamp
    }
    
    func setEndTime(_ endTimestamp: Date) {
        self.endTimestamp = endTimestamp
    }
    
    func getLocations() -> [CLLocationCoordinate2D] {
        var locations = [CLLocationCoordinate2D]()
        
        for rideShareDataPoint in rideShareDataPoints {
            let location = CLLocationCoordinate2D(latitude: rideShareDataPoint.latitutude, longitude: rideShareDataPoint.longitude)
            locations.append(location)
        }
        return locations
    }
    
    func getDistanceData() -> [Int] {
        var distances = [Int]()
        var startingOdometer:Int = 0
        for rideShareDataPoint in rideShareDataPoints {
            if startingOdometer == 0 {
                startingOdometer = rideShareDataPoint.odometer
            }
            distances.append(rideShareDataPoint.odometer - startingOdometer)
        }
        return distances
    }
    
    func getSocData() -> [Int] {
        var socs = [Int]()
        for rideShareDataPoint in rideShareDataPoints {
            socs.append(rideShareDataPoint.soc)
        }
        return socs
    }
    
    func getSpeedData() -> [Int] {
        var speeds = [Int]()
        for rideShareDataPoint in rideShareDataPoints {
            speeds.append(rideShareDataPoint.speed)
        }
        return speeds
    }

    func getPowerData() -> [Int] {
        var powers = [Int]()
        for rideShareDataPoint in rideShareDataPoints {
            powers.append(rideShareDataPoint.power)
        }
        return powers
    }
    
    func getTorqRegenData() -> [Int] {
        var torqRegen = [Int]()
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.torque > 0 {
                torqRegen.append(rideShareDataPoint.torque)
            } else {
                if abs(rideShareDataPoint.brakeRegen) > abs(rideShareDataPoint.coastRegen) {
                    torqRegen.append(rideShareDataPoint.brakeRegen)
                } else {
                    torqRegen.append(rideShareDataPoint.coastRegen)
                }
            }
        }
        return torqRegen
    }
    
    func getLeanData() -> [Int] {
        var lean = [Int]()
        for rideShareDataPoint in rideShareDataPoints {
            lean.append(rideShareDataPoint.leanAngle)
        }
        return lean
    }
    
    func getDuration() -> Int { // In Seconds
        let sec = Calendar.current.dateComponents([.second], from: startTimestamp, to: endTimestamp).second ?? 0
        return sec
    }
    
    func getDurationAsString() -> String {
        let duration = getDuration()
        let hour = duration / (60 * 60)
        let min = (duration / 60) % 60
        let durationStr = String.init(format: "%d:%02d", hour, min)
        return durationStr
    }
    
    func getDistance() -> Int {
        if rideShareDataPoints.count > 0 {
            let firstDataPoint = rideShareDataPoints[0]
            let lastDataPoint = rideShareDataPoints[rideShareDataPoints.count - 1]
            let startOdometer = firstDataPoint.odometer
            let lastOdometer = lastDataPoint.odometer
            return Int(lastOdometer) - Int(startOdometer)
        }
        return 0
    }
    
    func getMaxSpeedDataPoint() -> RideDataPoint? {
        var maxSpeed:Int = 0
        var maxSpeedDataPoint:RideDataPoint?
        for dataPoint in rideShareDataPoints {
            if dataPoint.speed > maxSpeed {
                maxSpeed = dataPoint.speed
                maxSpeedDataPoint = dataPoint
            }
        }
        return maxSpeedDataPoint
    }

    func getMaxPowerDataPoint() -> RideDataPoint? {
        var maxPower:Int = 0
        var maxPowerDataPoint:RideDataPoint?
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.power > maxPower {
                maxPower = rideShareDataPoint.power
                maxPowerDataPoint = rideShareDataPoint
            }
        }
        return maxPowerDataPoint
    }
    
    func getMaxTorqueDataPoint() -> RideDataPoint? {
        var maxTorque:Int = 0
        var maxTorqueDataPoint:RideDataPoint?
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.torque > maxTorque {
                maxTorque = rideShareDataPoint.torque
                maxTorqueDataPoint = rideShareDataPoint
            }
        }
        return maxTorqueDataPoint
    }
    
    // TODO confirm max regen is for brake regen
    func getMaxRegenDataPoint() -> RideDataPoint? {
        // We want to biggest negative number
        var maxRegen:Int = 0
        var maxRegenDataPoint:RideDataPoint?
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.brakeRegen < maxRegen {
                maxRegen = rideShareDataPoint.brakeRegen
                maxRegenDataPoint = rideShareDataPoint
            }
            if rideShareDataPoint.coastRegen < maxRegen {
                maxRegen = rideShareDataPoint.brakeRegen
                maxRegenDataPoint = rideShareDataPoint
            }
        }
        return maxRegenDataPoint
    }
    
    // Negative values are lean left
    func getMaxLeanLeftDataPoint() -> RideDataPoint? {
        var maxLeanLeft:Int = 0
        var maxLeanLeftDataPoint:RideDataPoint?
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.leanAngle < maxLeanLeft {
                maxLeanLeft = rideShareDataPoint.leanAngle
                maxLeanLeftDataPoint = rideShareDataPoint
            }
        }
        return maxLeanLeftDataPoint
    }
    
    // Positive values are lean right
    func getMaxLeanRightDataPoint() -> RideDataPoint? {
        var maxLeanRight:Int = 0
        var maxLeanRightDataPoint:RideDataPoint?
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.leanAngle > maxLeanRight {
                maxLeanRight = rideShareDataPoint.leanAngle
                maxLeanRightDataPoint = rideShareDataPoint
            }
        }
        return maxLeanRightDataPoint
    }
    
    func getSocUsed() -> Int {
        if rideShareDataPoints.count > 0 {
            let firstDataPoint = rideShareDataPoints[0]
            let lastDataPoint = rideShareDataPoints[rideShareDataPoints.count - 1]
            
            return Int(firstDataPoint.soc) - Int(lastDataPoint.soc)
        }
        return 0
    }
    
    // TODO how do we get this?
    func getEnergyRegenerated() -> Int {
        var totalRegen = 0
        for rideShareDataPoint in rideShareDataPoints {
            if rideShareDataPoint.brakeRegen < 0 {
                totalRegen = totalRegen + rideShareDataPoint.brakeRegen
            }
            if rideShareDataPoint.coastRegen < 0 {
                totalRegen = totalRegen + rideShareDataPoint.coastRegen
            }
        }

        return totalRegen
    }
    
    func getAvgSpeed() -> Double {
        let duration = getDuration() // In Seconds
        let distance = getDistance() // In 0.1 Km
        
        guard duration > 0 else { return 0 }
        
        let speed = ((Double(distance) / 10) / Double(duration)) * (60 * 60)
        
        return speed // kph
    }
    
    func getName() -> String {
        // Start Time
        let startTime = getStartTime()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd"
        let dateStr = dateFormatter.string(from: startTime)
        
        // Duration
        let durationStr = getDurationAsString()
        
        // Distance
        var distance = getDistance()
        var unitStr = ""
        if DataService.shared.usingMetricDistance {
            distance = Int(Float(distance) / 10)
            unitStr = "global_km".localized
        } else {
            distance = Int((Float(distance) / 10) / Constant.KM_IN_ONE_MILE)
            unitStr = "global_mi".localized
        }
        let distanceStr = "\(distance)\(unitStr)"
        
        let captureName = String.init(format: "%@, %@ %@", dateStr, distanceStr, durationStr)
        return captureName
    }
    
    static func acceptStatValueUnit(capturedRide: CapturedRide, value: BehaviorRelay<String>, unit: BehaviorRelay<String>?, stat: Settings.ShareStat) {
        switch stat {
        case .Duration:
            let durationStr = capturedRide.getDurationAsString()
            value.accept(durationStr)
            unit?.accept("")
        case .Distance:
            let distance = capturedRide.getDistance()
            if DataService.shared.usingMetricDistance {
                let converted = Int(Float(distance) / 10)
                value.accept("\(converted)")
                unit?.accept("global_km".localized)
            } else {
                let converted = Int((Float(distance) / 10) / Constant.KM_IN_ONE_MILE)
                value.accept("\(converted)")
                unit?.accept("global_mi".localized)
            }
        case .MaxSpeed:
            if let dataPoint = capturedRide.getMaxSpeedDataPoint() {
                let maxSpeed = dataPoint.speed
                if DataService.shared.usingMetricDistance {
                    let converted = Int(Float(maxSpeed) / 100)
                    value.accept("\(converted)")
                    unit?.accept("global_kph".localized)
                } else {
                    let converted = Int((Float(maxSpeed) / 100) / Constant.KM_IN_ONE_MILE)
                    value.accept("\(converted)")
                    unit?.accept("global_mph".localized)
                }
            }
        case .AvgSpeed:
            let speed = capturedRide.getAvgSpeed()
            if DataService.shared.usingMetricDistance {
                value.accept("\(Int(speed))")
                unit?.accept("global_kph".localized)
            } else {
                let converted = Int(Float(speed) / Constant.KM_IN_ONE_MILE)
                value.accept("\(converted)")
                unit?.accept("global_mph".localized)
            }
        case .MaxPower:
            if let dataPoint = capturedRide.getMaxPowerDataPoint() {
                let maxPower = dataPoint.power
                value.accept("\(maxPower)")
                unit?.accept("global_kw".localized)
            }
        case .MaxTorque:
            if let dataPoint = capturedRide.getMaxTorqueDataPoint() {
                let maxTorque = dataPoint.torque
                value.accept("\(maxTorque)")
                unit?.accept("global_nm".localized)
            }
        case .SocUsed:
            let soc = capturedRide.getSocUsed()
            value.accept("\(soc)")
            unit?.accept("%")
        case .EnergyRegenerated:
            let regen = capturedRide.getEnergyRegenerated()
            value.accept("\(regen)")
            unit?.accept("global_kwh".localized)
        case .MaxLeanLeft:
            if let dataPoint = capturedRide.getMaxLeanLeftDataPoint() {
                let lean = abs(Int(Float(dataPoint.leanAngle) / 10))
                value.accept("\(lean)")
                unit?.accept("°")
            }
        case .MaxLeanRight:
            if let dataPoint = capturedRide.getMaxLeanRightDataPoint() {
                let lean = abs(Int(Float(dataPoint.leanAngle) / 10))
                value.accept("\(lean)")
                unit?.accept("°")
            }
        case .LifetimeOdo:
            if let motorcycle = DataService.shared.currentMotorcycle {
                let odo = motorcycle.odometer
                if DataService.shared.usingMetricDistance {
                    let converted = Int(Float(odo) / 10)
                    value.accept("\(converted)")
                    unit?.accept("global_km".localized)
                } else {
                    let converted = Int((Float(odo) / 10) / Constant.KM_IN_ONE_MILE)
                    value.accept("\(converted)")
                    unit?.accept("global_mi".localized)
                }
            } else {
                value.accept("0")
                if DataService.shared.usingMetricDistance {
                    unit?.accept("global_km".localized)
                } else {
                    unit?.accept("global_mi".localized)
                }
            }
        }
    }}
