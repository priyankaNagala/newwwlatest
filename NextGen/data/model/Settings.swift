//
//  Settings.swift
//  nextgen
//
//  Created by David Crawford on 8/23/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class Settings {
    
    enum SettingType {
        case DISTANCE_UNITS, TEMPERATURE, LIQUIDS, TIME_FORMAT
    }
    
    enum DistanceUnit: String, Codable {
        case MILES, KILOMETERS
        
        func localizedString() -> String {
            switch self {
            case .MILES: return "settings_distance_units_miles".localized
            case .KILOMETERS: return "settings_distance_units_kilometers".localized
            }
        }
    }
    
    enum Temperature: String, Codable {
        case FAHRENHEIT, CELSIUS
        
        func localizedString() -> String {
            switch self {
            case .FAHRENHEIT: return "settings_temperature_fahrenheit".localized
            case .CELSIUS: return "settings_temperature_celsius".localized
            }
        }
    }
    
    enum Liquid: String, Codable {
        case GALLONS, LITERS
        
        func localizedString() -> String {
            switch self {
            case .GALLONS: return "settings_liquids_gallons".localized
            case .LITERS: return "settings_liquids_liters".localized
            }
        }
    }
    
    enum TimeFormat: String, Codable {
        case TWENTY_FOUR_HOURS, AM_PM
        
        func localizedString() -> String {
            switch self {
            case .TWENTY_FOUR_HOURS: return "settings_time_format_24_hours".localized
            case .AM_PM: return "settings_time_format_am_pm".localized
            }
        }
    }
    
    enum FuelEfficiency: String, Codable {
        case MILES_PER_GALLON, LITERS_PER_100KM
        
        func localizedString() -> String {
            switch self {
            case .MILES_PER_GALLON: return "comp_settings_fuel_efficiency_miles".localized
            case .LITERS_PER_100KM: return "comp_settings_fuel_efficiency_km".localized
            }
        }
        
        func abbreviation() -> String {
            switch self {
            case .MILES_PER_GALLON: return "comp_settings_fuel_efficiency_miles_abbreviation".localized
            case .LITERS_PER_100KM: return "comp_settings_fuel_efficiency_km_abbreviation".localized
            }
        }
        
        func multiplier() -> Double {
            switch self {
            case .MILES_PER_GALLON: return 0.9
            case .LITERS_PER_100KM: return -0.207
            }
        }
        
        func minValue() -> Double {
            switch self {
            case .MILES_PER_GALLON: return 10
            case .LITERS_PER_100KM: return 23
            }
        }
        
        func maxValue() -> Double {
            switch self {
            case .MILES_PER_GALLON: return 100
            case .LITERS_PER_100KM: return 2.3
            }
        }
        
        func costText() -> String {
            switch self {
            case .MILES_PER_GALLON: return "comp_settings_cost_per_gallon".localized
            case .LITERS_PER_100KM: return "comp_settings_cost_per_liter".localized
            }
        }
    }
    
    enum Currency: String, Codable {
        case DOLLARS, EUROS
        
        func localzedString() -> String {
            switch self {
            case .DOLLARS: return "comp_settings_currency_dollar".localized
            case .EUROS: return "comp_settings_currency_euro".localized
            }
        }
    }
    
    enum ShareStat: String, Codable {
        case Duration, Distance, MaxSpeed, AvgSpeed, MaxPower, MaxTorque, SocUsed, EnergyRegenerated, MaxLeanLeft, MaxLeanRight, LifetimeOdo
        
        func localizedString() -> String {
            switch self {
            case .Duration:
                return "global_duration".localized
            case .Distance:
                return "relive_distance".localized
            case .MaxSpeed:
                return "relive_max_speed".localized
            case .AvgSpeed:
                return "relive_avg_speed".localized
            case .MaxPower:
                return "relive_max_power".localized
            case .MaxTorque:
                return "relive_max_torque".localized
            case .SocUsed:
                return "relive_soc_used".localized
            case .EnergyRegenerated:
                return "relive_energy_regenerated".localized
            case .MaxLeanLeft:
                return "relive_max_lean_left".localized
            case .MaxLeanRight:
                return "relive_max_lean_right".localized
            case .LifetimeOdo:
                return "relive_lifetime_odometer".localized
            }
        }
    }

}
