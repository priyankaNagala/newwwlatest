//
//  Fup.swift
//  nextgen
//
//  Created by Scott Wang on 11/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki

struct Fup: Himotoki.Decodable {
    var version: String?
    let packageId: String
    let packageCreateDate: String
    let firmware: [String: FupFirmware]

    static func decode(_ e: Extractor) throws -> Fup {
        return try Fup(version: nil,
                       packageId: e <| "package-id",
                       packageCreateDate: e <| "package-create-date",
                       firmware: e <|-| "firmware")
    }
}

struct FupFirmware: Himotoki.Decodable {
    let bankA: FupFirmwareBank?
    let bankB: FupFirmwareBank?
    
    // TODO Current Gen isn't parsing these below currently, needed?
    let binFile: String?
    let hexFileSha512: String?
    let binFileSha512: String?
    let hexFile: String?

    static func decode(_ e: Extractor) throws -> FupFirmware {
        return try FupFirmware(bankA: e <|? "banka", bankB: e <|? "bankb", binFile: e <|? "bin_file", hexFileSha512: e <|? "hex_file_sha512", binFileSha512: e <|? "bin_file_sha512", hexFile: e <|? "hex_file")
    }
}


struct FupFirmwareBank: Himotoki.Decodable {
    let binFile: String
    let hexFileSha512: String
    let binFileSha512: String
    let hexFile: String

    static func decode(_ e: Extractor) throws -> FupFirmwareBank {
        return try FupFirmwareBank(binFile: e <| "bin_file",
                                   hexFileSha512: e <| "hex_file_sha512",
                                   binFileSha512: e <| "bin_file_sha512",
                                   hexFile: e <| "hex_file")
    }
}
