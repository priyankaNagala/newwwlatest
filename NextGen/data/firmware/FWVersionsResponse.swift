//
//  FWVersionsResponse.swift
//  nextgen
//
//  Created by Scott Wang on 11/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Himotoki

struct FWVersionsResponse: Himotoki.Decodable {
    
    let buildTime: String?
    let formatRevision: Int?
    let version: Int?
    let fwVersions: [String : FWVersion]?
    
    static func decode(_ e: Extractor) throws -> FWVersionsResponse {
        return try FWVersionsResponse(buildTime: e <|? "build_time_string",
                                      formatRevision: e <|? "format_revision",
                                      version: e <|? "version",
                                      fwVersions: e <|-|? "fwVersions")
    }
}


struct FWVersion : Himotoki.Decodable {
    let bom: String?
    let buildNumber: String?
    let description: String?
    let filename: String?
    let firmwareDate: String?
    let firmwareRevision: String?
    let folder: String?
    let notes: String?
    let partName: String?
    let partNumber: String?
    let type: String?
    
    static func decode(_ e: Extractor) throws -> FWVersion {
        return try FWVersion(bom: e <|? "bom",
                             buildNumber: e <|? "build_number",
                             description: e <|? "description",
                             filename: e <|? "filename",
                             firmwareDate: e <|? "firmware-date",
                             firmwareRevision: e <|? "firmware-revision",
                             folder: e <|? "folder",
                             notes: e <|? "notes",
                             partName: e <|? "part_name",
                             partNumber: e <|? "part_number",
                             type: e <|? "type")
    }
}
