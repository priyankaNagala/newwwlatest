//
//  FupdaterResponse.swift
//  nextgen
//
//  Created by Scott Wang on 11/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

struct FupdaterResponse {
    
    let error: String?
    let result: String?
    
}
