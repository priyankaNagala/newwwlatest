//
//  DateUtil.swift
//  nextgen
//
//  Created by Scott Wang on 2/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation


class DateUtil {

    static let SECOND: Double = 1
    static let MINUTE: Double = 60 * SECOND
    static let HOUR: Double = 60 * MINUTE
    static let DAY: Double = 24 * HOUR
    static let WEEK: Double = 7 * DAY
    static let MONTH: Double = 30 * DAY
    static let YEAR: Double = 365 * DAY

    class func timeAgo(date: Date) -> String? {
        let diff = Date.init().timeIntervalSince(date)
        if diff < 0 {
            return nil
        }
        
        if (diff < MINUTE) {
            return "time_ago_just_now".localized
        } else if (diff < 2 * MINUTE) {
            return "time_ago_a_minute_ago".localized
        } else if (diff < HOUR) {
            return String(format: "time_ago_minutes_ago".localized, Int(floor(diff / MINUTE)))
        } else if (diff < 2 * HOUR) {
            return "time_ago_an_hour_ago".localized
        } else if (diff < DAY) {
            return String(format: "time_ago_hours_ago".localized, Int(floor(diff / HOUR)))
        } else if (diff < 2 * DAY) {
            return "time_ago_a_day_ago".localized
        } else if (diff < WEEK) {
            return String(format: "time_ago_days_ago".localized, Int(floor(diff / DAY)))
        } else if (diff < 2 * WEEK) {
            return "time_ago_a_week_ago".localized
        } else if (diff < MONTH) {
            return String(format: "time_ago_weeks_ago".localized, Int(floor(diff / WEEK)))
        } else if (diff < YEAR) {
            let months = Int(floor(diff / MONTH))
            if (months > 1) {
                return String(format: "time_ago_months_ago".localized, months)
            }
            return "time_ago_a_month_ago".localized
        } else if (diff < 2 * YEAR) {
            return "time_ago_a_year_ago".localized
        } else {
            return String(format: "time_ago_years_ago".localized, Int(floor(diff / YEAR)))
        }
    }
    
    class func timeAgoShort(date: Date) -> String? {
        let diff = Date.init().timeIntervalSince(date)
        
        if diff < 0 {
            return nil
        }
        
        if (diff < MINUTE) {
            return String(format: "time_ago_seconds_ago_short".localized, Int(floor(diff/SECOND)))
        } else if (diff < HOUR) {
            return String(format: "time_ago_minutes_ago_short".localized, Int(floor(diff/MINUTE)))
        } else if (diff < DAY) {
            return String(format: "time_ago_hours_ago_short".localized, Int(floor(diff/HOUR)))
        } else if (diff < WEEK) {
            return String(format: "time_ago_days_ago_short".localized, Int(floor(diff/DAY)))
        } else if (diff < MONTH) {
            return String(format: "time_ago_weeks_ago_short".localized, Int(floor(diff/WEEK)))
        } else if (diff < YEAR) {
            return String(format: "time_ago_months_ago_short".localized, Int(floor(diff/MONTH)))
        } else {
            return String(format: "time_ago_years_ago_short".localized, Int(floor(diff/YEAR)))
        }

        
    }
}
