//
//  Starcom.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift

class Starcom {
    
    static let shared: Starcom = Starcom()
    
    func getLastTransmit(starcomApiService: StarcomApiService, user: String, password: String, unitNumber: String) -> Observable<LastTransmit> {
        return starcomApiService.lastTransmit(user: user, password: password, unitNumber: unitNumber)
    }
    
    func getUnit(starcomApiService: StarcomApiService, user: String, password:String) -> Observable<StarcomUnit?> {
        return starcomApiService.getUnit(user: user, password: password)
    }
    
    func registerForNotifications(starcomApiService: StarcomApiService, sessionId: String, registrationId: String) -> Observable<StarcomResponse> {
        return starcomApiService.registerForNotifications(sessionId: sessionId, registrationId: registrationId)
    }

    func deregisterForNotifications(starcomApiService: StarcomApiService, sessionId: String, registrationId: String) -> Observable<StarcomResponse> {
        return starcomApiService.deregisterForNotifications(sessionId: sessionId, registrationId: registrationId)
    }

    func login(starcomApiService: StarcomApiService, user: String, password: String) -> Observable<StarcomSession> {
        return starcomApiService.login(user: user, password: password)
    }
    
    func register(starcomApiService: StarcomApiService, user: String, password: String, vin: String, passcode: String) -> Observable<StarcomSession> {
        return starcomApiService.createUser(user: user, password: password, vin: vin, passcode: passcode)
    }

    func logout(starcomApiService: StarcomApiService, sessionId: String) -> Observable<StarcomResponse> {
        return starcomApiService.logout(sessionId: sessionId)
    }
    
    func deregister(starcomApiService: StarcomApiService, sessionId: String, vin: String) -> Observable<StarcomResponse> {
        return starcomApiService.deregister(sessionId: sessionId, vin: vin)
    }
    
    func deleteUser(starcomApiService: StarcomApiService, user: String, sessionId: String) -> Observable<StarcomResponse> {
        return starcomApiService.deleteUser(user: user, sessionId: sessionId)
    }
}
