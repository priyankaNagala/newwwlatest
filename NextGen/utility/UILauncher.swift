//
//  UILauncher.swift
//  NextGenView
//
//  Created by Scott on 2/8/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import RxCocoa
import RxSwift
import Swinject
import SwinjectStoryboard
import KYDrawerController

class UILauncher {
    // MARK: - Entry Points
    class func launchWelcome() {
        let bundle = Bundle(for: WelcomeViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Onboarding", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
    
    class func launchRootTabBar() {
        let bundle = Bundle(for : RootTabBarController.self)
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController() as! KYDrawerController
    }
    
    // MARK: -
    class func launchRemoteConnect(_ sender: UIViewController) {
        let bundle = Bundle(for: RemoteConnectViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RemoteConnect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let remoteConnectViewController = storyboard
            .instantiateViewController(withIdentifier: "RemoteConnectNavViewController")
            as! UINavigationController

        sender.show(remoteConnectViewController, sender: sender)
    }

    class func launchConnectFind(_ sender: UIViewController) {
        let bundle = Bundle(for: ConnectFindViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Connect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let connectFindViewController = storyboard
            .instantiateInitialViewController()
            as! UINavigationController
        sender.show(connectFindViewController, sender: sender)
    }
    
    class func launchConnectConnect(_ sender: UIViewController, motorcycles: [TIOPeripheral]) {
        let bundle = Bundle(for: ConnectConnectViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Connect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let connectConnectViewController = storyboard.instantiateViewController(withIdentifier: "ConnectConnectViewController") as! ConnectConnectViewController
        connectConnectViewController.foundMotorcycles = motorcycles
        sender.show(connectConnectViewController, sender: sender)
    }
    
    class func launchConnectImprove(_ sender: UIViewController) {
        let bundle = Bundle(for: ConnectImproveViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Connect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let connectImproveViewController = storyboard.instantiateViewController(withIdentifier: "ConnectImproveViewController") as! ConnectImproveViewController
        sender.show(connectImproveViewController, sender: sender)
    }
    
    class func launchConnectComplete(_ sender: UIViewController) {
        let bundle = Bundle(for: ConnectCompleteViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Connect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let connectCompleteViewController = storyboard.instantiateViewController(withIdentifier: "ConnectCompleteViewController") as! ConnectCompleteViewController
        sender.show(connectCompleteViewController, sender: sender)
    }
    
    class func launchConnectEnableBluetooth(_ sender: UIViewController) {
        let bundle = Bundle(for: ConnectCompleteViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Connect", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let connectCompleteViewController = storyboard.instantiateViewController(withIdentifier: "ConnectEnableBluetoothNavViewController") as! UINavigationController
        sender.show(connectCompleteViewController, sender: sender)
    }

    class func launchAppHelp(_ sender: UIViewController, helpType: AppHelpViewModel.HelpType) {
        let bundle = Bundle(for : AppHelpViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "AppHelp", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "AppHelpNavViewController") as! UINavigationController
        let appHelpViewController = navVC.topViewController as! AppHelpViewController
        appHelpViewController.appHelpViewModel?.type = helpType
        
        sender.present(navVC, animated: true, completion: nil)
    }
    
    class func launchWebView(_ sender: UIViewController, url: URL) {
        let bundle = Bundle(for : WebViewViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "WebView", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let webViewViewController = storyboard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        webViewViewController.url = url
        
        sender.show(webViewViewController, sender: sender)
    }

    class func launchWebViewModal(_ sender: UIViewController, url: URL) {
        let bundle = Bundle(for : WebViewViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "WebView", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "WebViewNavViewController") as! UINavigationController
        let webViewViewController = navVC.viewControllers.first as! WebViewViewController
        webViewViewController.url = url
        
        sender.show(navVC, sender: sender)
    }

    class func launchDashboardSettings(_ sender: UIViewController) {
        let bundle = Bundle(for : DashboardSettingsViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "DashboardSettings", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let dashboardSettingsViewController = storyboard
            .instantiateViewController(withIdentifier: "DashboardSettingsViewController") as! DashboardSettingsViewController
        
        sender.show(dashboardSettingsViewController, sender: sender)
    }
    
    /*
        Launch the Gauge Picker View Controller.
        @return viewWillDisappear ControlEvent
     */
    class func launchGaugePicker(_ sender: UIViewController, quadrant: DashboardGaugeQuadrant, delegate: GaugePickerDelegate) {
        let bundle = Bundle(for : GaugePickerViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "DashboardSettings", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let gaugePickerViewController = storyboard
            .instantiateViewController(withIdentifier: "GaugePickerViewController") as!
                GaugePickerViewController
        gaugePickerViewController.quadrant = quadrant
        gaugePickerViewController.pickerDelegate = delegate
        
        sender.show(gaugePickerViewController, sender: sender)
    }
    
    class func launchManageRideModes(_ sender: UIViewController) {
        let bundle = Bundle(for : ManageRideModesViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RideMode", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let manageRideModesViewController = storyboard
            .instantiateViewController(withIdentifier: "ManageRideModesViewController") as! ManageRideModesViewController
        
        sender.show(manageRideModesViewController, sender: sender)
    }
    
    class func launchActivateRideMode(_ sender: UIViewController, activate: Bool) -> Observable<RideMode?> {
        let bundle = Bundle(for : AllRideModesViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RideMode", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let allRideModesViewController = storyboard
            .instantiateViewController(withIdentifier: "AllRideModesViewController") as! AllRideModesViewController
        allRideModesViewController.activate = true
        
        sender.show(allRideModesViewController, sender: sender)
        
        return Observable.create { observer -> Disposable in
            _ = allRideModesViewController.rx.viewWillDisappear
                .bind(onNext: { _ in
                    if let rideMode = allRideModesViewController.selectedRideMode {
                        observer.on(.next(rideMode))
                    }
                })
            return Disposables.create()
        }
    }
    
    class func launchAllRideModes(_ sender: UIViewController) -> ControlEvent<Bool>  {
        let bundle = Bundle(for : AllRideModesViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RideMode", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let allRideModesViewController = storyboard
            .instantiateViewController(withIdentifier: "AllRideModesViewController") as! AllRideModesViewController
        
        sender.show(allRideModesViewController, sender: sender)
        
        return allRideModesViewController.rx.viewWillDisappear
    }
    
    class func launchDeactivateRideMode(_ sender: UIViewController) -> ControlEvent<Bool> {
        let bundle = Bundle(for : DeactivateRideModeViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RideMode", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let deactivateRideModeViewController = storyboard
            .instantiateViewController(withIdentifier: "DeactivateRideModeViewController") as! DeactivateRideModeViewController
        
        sender.show(deactivateRideModeViewController, sender: sender)
        
        return deactivateRideModeViewController.rx.viewWillDisappear
    }
    
    class func launchRideMode(_ sender: UIViewController, rideMode: RideMode) -> ControlEvent<Bool> {
        let bundle = Bundle(for : RideModeViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "RideMode", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let rideModeViewController = storyboard
            .instantiateViewController(withIdentifier: "RideModeViewController") as! RideModeViewController
        rideModeViewController.rideModeViewModel?.rideMode = rideMode
        
        sender.show(rideModeViewController, sender: sender)
        
        return rideModeViewController.rx.viewWillDisappear
    }
    
    class func launchScheduledChargingIntro(_ sender: UIViewController) {
        if DataService.shared.hasSeenScheduledChargingIntro {
            launchScheduledCharging(sender, navController: nil)
            return
        }
        let bundle = Bundle(for: ScheduledChargingIntroViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Battery", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let scheduledChargingIntroVC = storyboard.instantiateViewController(withIdentifier: "ScheduledChargingIntroViewController") as! ScheduledChargingIntroViewController
        sender.show(scheduledChargingIntroVC, sender: sender)
    }
    
    class func launchScheduledCharging(_ sender: UIViewController, navController: UINavigationController?) {
        let bundle = Bundle(for: ScheduledChargingViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Battery", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        if (navController != nil) {
            let scheduledChargingVC = storyboard.instantiateViewController(withIdentifier: "ScheduledChargingViewController") as! ScheduledChargingViewController

            navController?.pushViewController(scheduledChargingVC, animated: true)

            if var viewControllers = navController?.viewControllers {
                // Remove Intro from stack
                let index = viewControllers.count - 2
                if index >= 0 {
                    viewControllers.remove(at: index)
                    navController?.viewControllers = viewControllers
                }
            }
            
        } else {
            let scheduledChargingVC = storyboard.instantiateViewController(withIdentifier: "ScheduledChargingViewController") as! ScheduledChargingViewController
            
            sender.show(scheduledChargingVC, sender: sender)
        }
    }
    
    class func launchChargeTarget(_ sender: UIViewController) {
        let bundle = Bundle(for: ChargeTargetViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Battery", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let chargeTargetVC = storyboard.instantiateViewController(withIdentifier: "ChargeTargetViewController") as! ChargeTargetViewController
        
        sender.show(chargeTargetVC, sender: sender)
    }
    
    class func launchRouteTypePicker(_ sender: UIViewController, batteryManager: BatteryManager?) -> ControlEvent<Bool> {
        let bundle = Bundle(for: ChargeTargetViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Battery", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let pickerVC = storyboard.instantiateViewController(withIdentifier: "RouteTypePickerViewController") as! RouteTypePickerViewController
        pickerVC.batteryManager = batteryManager
        
        sender.show(pickerVC, sender: sender)
        
        return pickerVC.rx.viewWillDisappear
    }
    
    class func launchSettings(_ sender: UIViewController) {
        let bundle = Bundle(for: SettingsViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let settingsVC = storyboard.instantiateViewController(withIdentifier: "SettingsNavViewController") as! UINavigationController
        sender.show(settingsVC, sender: sender)
    }
    
    class func launchAdmin(_ sender: UIViewController) {
        let bundle = Bundle(for: AdminViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Admin", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let adminVC = storyboard.instantiateViewController(withIdentifier: "AdminViewController") as! AdminViewController
        sender.show(adminVC, sender: sender)
    }
    
    class func launchPacketDebug(_ sender: UIViewController) {
        let bundle = Bundle(for: PacketDebugViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Admin", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let adminVC = storyboard.instantiateViewController(withIdentifier: "PacketDebugViewController") as! PacketDebugViewController
        sender.show(adminVC, sender: sender)
    }
    
    class func launchConnectionStatus(_ sender: UIViewController) {
        let bundle = Bundle(for: ConnectionStatusViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "ConnectionStatus", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "ConnectionStatusNavVC") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func launchRegisterPrompt(_ sender: UIViewController) {
        let bundle = Bundle(for: RegisterPromptViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let registerPromptVC = storyboard.instantiateViewController(withIdentifier: "RegisterPromptViewController") as! RegisterPromptViewController
        sender.show(registerPromptVC, sender: sender)
    }

    class func launchLogin(_ sender: UIViewController) {
        let bundle = Bundle(for: LoginViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "LoginNavViewController") as! UINavigationController
        sender.show(navVC, sender: sender)
    }

    class func launchRegister(_ sender: UIViewController) {
        let bundle = Bundle(for: RegisterViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "RegisterNavViewController") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func switchToRegister(_ sender: LoginViewController) {
        let bundle = Bundle(for: RegisterViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let registerVC = storyboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        sender.navigationController?.popViewController(animated: false)
        sender.navigationController?.pushViewController(registerVC, animated: false)
    }

    class func switchToLogin(_ sender: RegisterViewController) {
        let bundle = Bundle(for: LoginViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        sender.navigationController?.popViewController(animated: false)
        sender.navigationController?.pushViewController(loginVC, animated: false)
    }

    class func launchAccount(_ sender: UIViewController) {
        let bundle = Bundle(for: AccountViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Starcom", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "AccountNavViewController") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func launchFirmwareManager(_ sender: UIViewController) {
        let bundle = Bundle(for: AccountViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Firmware", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "FirmwareManagerNavViewController") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func launchReliveRide(_ sender: UIViewController, rideName: String) {
        let bundle = Bundle(for: ReliveViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Relive", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "ReliveNavViewController") as! UINavigationController
        if let reliveRideVC = navVC.viewControllers[0] as? ReliveViewController {
           reliveRideVC.title = rideName
        }
        sender.show(navVC, sender: sender)
    }
    
    class func launchRelivePlayback(_ sender: UIViewController, rideName: String) {
        let bundle = Bundle(for: ReliveViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Relive", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "RelivePlaybackNavViewController") as! UINavigationController
        if let reliveRideVC = navVC.viewControllers[0] as? RelivePlaybackViewController {
            reliveRideVC.title = rideName
        }
        sender.show(navVC, sender: sender)
    }
    
    class func launchEditComparableSettingViewController(_ sender: UIViewController) {
        let bundle = Bundle(for: EditComparableSettingViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Data", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "EditComparableSettingsNavVC") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func launchArmedViewController(_ sender: UIViewController) -> UIViewController {
        let bundle = Bundle(for: ArmedViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "ArmedNavVC") as! UINavigationController
        sender.show(navVC, sender: sender)
        return navVC
    }
    
    class func launchChargingStations(_ sender: UIViewController) {
        UIApplication.shared.open(URL(string:"https://www.google.com/maps/search/ev+charging/")!, options: [:], completionHandler: nil)
    }
    
    class func launchRoadsideAssist(_ sender: UIViewController) {
        if let phone = DataService.shared.roadsideAssistPhone, phone.count > 0, let url = URL(string: "tel:" + phone) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            launchRoadsideAssistSetup(sender)
        }
    }
    
    class func launchRoadsideAssistSetup(_ sender: UIViewController) {
        let bundle = Bundle(for: RoadsideAssistSetupViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "RoadsideAssistSetupNavVC") as! UINavigationController
        sender.show(navVC, sender: sender)
    }
    
    class func launchPullLogs(_ sender: UIViewController) {
        let bundle = Bundle(for: LogSelectViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Logs", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let logVC = storyboard.instantiateViewController(withIdentifier: "LogSelectVC") as! LogSelectViewController
        sender.show(logVC, sender: sender)
    }
    
    class func launchSyncChanges(_ sender: UIViewController) -> UIViewController {
        let bundle = Bundle(for: SyncChangesViewController.self)
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: bundle, container: SwinjectUtil.sharedInstance.container)
        let navVC = storyboard.instantiateViewController(withIdentifier: "SyncChangesNavVC") as! UINavigationController
        sender.show(navVC, sender: sender)
        return navVC
    }
    
    // MARK: -
    class func currentlySelectedViewController() -> UIViewController? {
        if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? KYDrawerController,
            let rootTabBarController = drawerController.mainViewController as? RootTabBarController {
            return rootTabBarController.selectedViewController
        }
        return nil
    }
}
