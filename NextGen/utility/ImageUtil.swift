//
//  ImageUtil.swift
//  NextGenModel
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

class ImageUtil {

    class func imageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    static func scaledImageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
        let pixelScale = UIScreen.main.scale
        let scaledHeight = size.height / pixelScale
        let fillSize = CGSize(width: size.width, height: scaledHeight)
        let fillRect = CGRect(origin: CGPoint.zero, size: fillSize)
        UIGraphicsBeginImageContextWithOptions(fillRect.size, false, pixelScale)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color.cgColor)
        graphicsContext?.fill(fillRect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

}
