//
//  AppEnvironment.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import CocoaLumberjack

class AppEnvironment {
    
    enum ConfigurationEnvironment : String {
        case Production, Stage, Development
        static let allValues: [ConfigurationEnvironment] = [Production, Stage, Development]
    }
    
    var configurationEnvironment : ConfigurationEnvironment?
    
    var starcomApiBaseUrl : String?
    var fupdaterApiAuthToken : String?
    
    init(withEnvironmentId environment: String = Bundle.main.infoDictionary!["Configuration"] as! String) {
        self.configurationEnvironment = ConfigurationEnvironment.init(rawValue: environment)
        
        if let path = Bundle.main.path(forResource: "Environments", ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: path) as? [String:AnyObject] {
        }
    }
}
