//
//  BindUtil.swift
//  nextgen
//
//  Created by David Crawford on 3/6/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import RxSwift

class BindUtil {
    
    class func disposed(disposables: [Disposable], disposeBag: DisposeBag) {
        _ = disposables.map { $0.disposed(by: disposeBag) }
    }
}
