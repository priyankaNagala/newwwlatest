//
//  Constant.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

struct Constant {
    
    // Starcom
    static let STARCOM_API_BASE_URL = "https://mongol.brono.com/mongol/"
    static let STARCOM_NOTIFICATION_TYPES = ["COMPLETE_CHARGING",
                                             "INTERRUPTED_CHARGING",
                                             /*"TIPPED_OVER",*/
                                             "LOW_SOC",
                                             "FIRMWARE_UPDATE",
                                             "SHOCK_DETECTED",
                                             "MOVEMENT_DETECTED"]
    static let STARCOM_SOURCE_CLIENT = "zero"
    
    // Featured News
    static let FEATURED_NEWS_API_BASE_URL = "https://www.zeromotorcycles.com/app/featured.php"
    
    // Maps
    static let GOOGLE_MAPS_API_KEY = "AIzaSyCmqFBd_j6G8UJE4ZyixH8aXol8bDz6qDg"
    static let MAP_ZOOM_LEVEL_CITY:Float = 10.0
    static let MAP_ZOOM_LEVEL_STREETS:Float = 15.0
    static let MAP_ZOOM_LEVEL_BUILDINGS:Float = 18.0
    static let MAP_ZOOM_DEFAULT_RELIVE:Float=10.0
    
    // Miscellaneous
    static let STATE_OF_CHARGE_LOW_PERCENTAGE = 20
    
    // Ride Modes
    static let RIDE_MODES_MAX_USER_MODES = 10
    static let RIDE_MODES_MAX_ACTIVE_MODES = 5
    static let RIDE_MODE_NAME_MAX_CHARACTERS = 8
    static let RIDE_MODE_CUSTOM_POSITION = 4

    // Scheduled Charging
    static let SCHEDULED_CHARGING_DEFAULT_PLUG_IN_START             = 1700
    static let SCHEDULED_CHARGING_DEFAULT_PLUG_IN_DURATION          = 420
    static let SCHEDULED_CHARGING_DEFAULT_ACTIVE_CHARGING_START     = 2100
    static let SCHEDULED_CHARGING_DEFAULT_ACTIVE_CHARGING_DURATION  = 600
    
    // Charge Target
    static let CHARGE_TARGET_DEFAULT_MAX_SOC:Float = 0.75
    static let CHARGE_TARGET_DEFAULT_TARGET_RANGE:Int = 100
    static let CHARGE_TARGET_DEFAULT_TARGET_CHARGE:Float = 0.75
    static let CHARGE_TARGET_DEFAULT_RIDE_STYLE:BatteryManager.RideStyle = .CONSERVATIVE
    static let CHARGE_TARGET_DEFAULT_TERRAIN:BatteryManager.Terrain = .FLAT
    static let CHARGE_TARGET_DEFAULT_AIR_TEMPERATURE:BatteryManager.AirTemperature = .COLD
    static let CHARGE_TARGET_DEFAULT_ROUTE_TYPE:BatteryManager.RouteType = .CITY
    static let CHARGE_TARGET_MAX_RANGE:Int = 200
    static let CHARGE_TARGET_MAX_PERCENTAGE:CGFloat = 0.95
    static let CHARGE_TARGET_MIN_PERCENTAGE:CGFloat = 0.30

    
    // Preferences
    static let PREF_LAST_LOCATIONS                      = "PREF_LAST_LOCATIONS"
    static let PREF_SEEN_ONBOARDING                     = "PREF_SEEN_ONBOARDING"
    static let PREF_HAS_CONNECTED_ONCE                  = "PREF_HAS_CONNECTED_ONCE"
    static let PREF_TRACK_MOTORCYCLE_USAGE_LOGS         = "PREF_TRACK_MOTORCYCLE_USAGE_LOGS"
    static let PREF_CURRENT_MOTORCYCLE_VIN              = "PREF_CURRENT_MOTORCYCLE_VIN"
    static let PREF_DASHBOARD_THEME_BASED_ON_RIDE_MODE  = "PREF_DASHBOARD_THEME_BASED_ON_RIDE_MODE"
    static let PREF_DASHBOARD_THEME                     = "PREF_DASHBOARD_THEME"
    static let PREF_DASHBOARD_GAUGES                    = "PREF_DASHBOARD_GAUGES"
    static let PREF_DEFAULT_RIDE_MODES                  = "PREF_DEFAULT_RIDE_MODES"
    static let PREF_ACTIVE_RIDE_MODES                   = "PREF_ACTIVE_RIDE_MODES"
    static let PREF_MY_RIDE_MODES                       = "PREF_MY_RIDE_MODES"
    static let PREF_CURRENT_RIDE_MODE                   = "PREF_CURRENT_RIDE_MODE"
    static let PREF_SETTINGS_DISTANCE_UNITS             = "PREF_SETTINGS_DISTANCE_UNITS"
    static let PREF_SETTINGS_TEMPERATURE_UNITS          = "PREF_SETTINGS_TEMPERATURE_UNITS"
    static let PREF_SETTINGS_LIQUID_UNITS               = "PREF_SETTINGS_LIQUID_UNITS"
    static let PREF_SETTINGS_TIME_FORMAT                = "PREF_SETTINGS_TIME_FORMAT"
    static let PREF_LAST_CONNECTED_PERIPHERAL           = "PREF_LAST_CONNECTED_PERIPHERAL"
    static let PREF_LAST_CONNECTED_DATE                 = "PREF_LAST_CONNECTED_DATE"
    static let PREF_24_HR_CLOCK                         = "PREF_24_HR_CLOCK"
    static let PREF_METRIC_DISTANCE                     = "PREF_METRIC_DISTANCE"
    static let PREF_SCHEDULED_CHARGING                  = "PREF_SCHEDULED_CHARGING"
    static let PREF_SEEN_SCHEDULED_INTRO                = "PREF_SEEN_SCHEDULED_INTRO"
    static let PREF_BATTERY_MANAGER                     = "PREF_BATTERY_MANAGER"
    static let PREF_STARCOM_USER                        = "PREF_STARCOM_USER"
    static let PREF_STARCOM_PASSWORD                    = "PREF_STARCOM_PASSWORD"
    static let PREF_STARCOM_UNIT_NUMBER                 = "PREF_STARCOM_UNIT_NUMBER"
    static let PREF_STARCOM_REGISTRATION_TOKEN          = "PREF_STARCOM_REGISTRATION_TOKEN"
    static let PREF_VIN                                 = "PREF_VIN"
    static let PREF_PASSCODE                            = "PREF_PASSCODE"
    static let PREF_CAPTURED_RIDE                       = "PREF_CAPTURED_RIDE"
    static let PREF_CAPTURED_RIDE_START                 = "PREF_CAPTURED_RIDE_START"
    static let PREF_COST_PER_GALLON                     = "PREF_COST_PER_GALLON"
    static let PREF_COST_PER_KWATT_HOUR                 = "PREF_COST_PER_KWATT_HOUR"
    static let PREF_COMPARABLE_EFFICIENCY               = "PREF_COMPARABLE_EFFICIENCY"
    static let PREF_FUEL_EFFICIENCY                     = "PREF_FUEL_EFFICIENCY"
    static let PREF_CURRENCY                            = "PREF_CURRENCY"
    static let PREF_LEFT_STAT                           = "PREF_LEFT_STAT"
    static let PREF_RIGHT_STAT                          = "PREF_RIGHT_STAT"
    static let PREF_CENTER_STAT                         = "PREF_CENTER_STAT"
    static let PREF_ODOMETER                            = "PREF_ODOMETER"
    static let PREF_TOTAL_ENERGY                        = "PREF_TOTAL_ENERGY"
    static let PREF_ROADSIDE_ASSIST_PHONE               = "PREF_ROADSIDE_ASSIST_PHONE"
    static let PREF_ROADSIDE_ASSIST_POLICY              = "PREF_ROADSIDE_ASSIST_POLICY"
    static let PREF_OFFLINE_QUEUE                       = "PREF_OFFLINE_QUEUE"

    // Analytics
    static let ANALYTICS_SCREEN_APP_HELP = "App Help"
    static let ANALYTICS_SCREEN_APP_TOUR = "App Tour"
    static let ANALYTICS_SCREEN_ALL_RIDE_MODES = "All Ride Modes"
    static let ANALYTICS_SCREEN_BATTERY = "Battery"
    static let ANALYTICS_SCREEN_CONNECT_FIND = "Connect Find"
    static let ANALYTICS_SCREEN_CONNECT_CONNECT = "Connect Connect"
    static let ANALYTICS_SCREEN_CONNECT_IMPROVE = "Connect Improve"
    static let ANALYTICS_SCREEN_CONNECT_COMPLETE = "Connect Complete"
    static let ANALYTICS_SCREEN_CONNECT_ENABLE_BLUETOOTH = "Connect Enable Bluetooth"
    static let ANALYTICS_SCREEN_CONNECTION_STATUS = "Connection Status"
    static let ANALYTICS_SCREEN_DASHBOARD = "Dashboard"
    static let ANALYTICS_SCREEN_DASHBOARD_SETTINGS = "Dashboard Settings"
    static let ANALYTICS_SCREEN_DATA = "Data"
    static let ANALYTICS_SCREEN_DEACTIVATE_RIDE_MODE = "Deactivate Ride Mode"
    static let ANALYTICS_SCREEN_GAUGE_PICKER = "Gauge Picker"
    static let ANALYTICS_SCREEN_ONBOARDING_WELCOME = "Onboarding Welcome"
    static let ANALYTICS_SCREEN_ONBOARDING_TERMS = "Onboarding Terms"
    static let ANALYTICS_SCREEN_PERFORMANCE = "Performance"
    static let ANALYTICS_SCREEN_REMOTE_CONNECT = "Remote Connect"
    static let ANALYTICS_SCREEN_SCHEDULED_CHARGING = "Scheduled Charging"
    static let ANALYTICS_SCREEN_SCHEDULED_CHARGING_INTRO = "Scheduled Charging Intro"
    static let ANALYTICS_SCREEN_CHARGE_TARGET = "Charge Target"
    static let ANALYTICS_SCREEN_ROUTE_TYPE_PICKER = "Route Type Picker"
    static let ANALYTICS_SCREEN_MANAGE_RIDE_MODES = "Manage Ride Modes"
    static let ANALYTICS_SCREEN_RIDE_MODE = "Ride Mode"
    static let ANALYTICS_SCREEN_SETTINGS = "Settings"
    static let ANALYTICS_SCREEN_SETUP = "Setup"
    static let ANALYTICS_SCREEN_FIRMWARE_MANAGER = "Firmware Manager"
    static let ANALYTICS_SCREEN_ADMIN = "Admin"
    static let ANALYTICS_SCREEN_PACKET_DEBUG = "Packet Debug"
    static let ANALYTICS_SCREEN_DRAWER = "Hamburger Menu"
    static let ANALYTICS_SCREEN_LOGIN = "Login"
    static let ANALYTICS_SCREEN_REGISTER_PROMPT = "Register Prompt"
    static let ANALYTICS_SCREEN_REGISTER = "Register"
    static let ANALYTICS_SCREEN_ACCOUNT = "Account"
    static let ANALYTICS_SCREEN_RELIVE_RIDE = "Relive Ride"
    static let ANALYTICS_SCREEN_SHARE_RIDE = "Share Ride"
    static let ANALYTICS_SCREEN_SHARE_RIDE_STAT_PICKER = "Share Ride Pick Stat"
    static let ANALYTICS_SCREEN_RELIVE_RIDE_PLAYBACK = "Relive Ride Playback"
    static let ANALYTICS_SCREEN_EDIT_COMPARABLE_SETTINGS = "Edit Comparable Settings"
    static let ANALYTICS_WEB_VIEW = "Web View"
    static let ANALYTICS_SCREEN_LOG_SELECT = "Select Motorcycle Logs"
    static let ANALYTICS_SCREEN_LOG_DOWNLOAD = "Select Motorcycle Download"
    static let ANALYTICS_SCREEN_SYNC_CHANGES = "Sync Changes"
    
    // Conversions
    static let KM_IN_ONE_MILE:Float = 1.60934
    static let GAL_IN_ONE_LITER:Float = 0.264172

    // Style Guide Constants
    static let DEFAULT_GUTTER:CGFloat = 15.0
    
    // Bluetooth
    static let BLUETOOTH_SCAN_SECONDS: RxTimeInterval = 4
    static let BLUETOOTH_MALFORMED_PACKET_WAIT_SECONDS: RxTimeInterval = 1
    static let BLUETOOTH_AVAILABLE_WAIT_SECONDS: RxTimeInterval = 2
    static let BLUETOOTH_RESEND_RETRIES = 20
    static let BLUETOOTH_PACKETS_PER_MINUTE = 60.0
    static let BLUETOOTH_CONNECT_TIMEOUT: RxTimeInterval = 15
    static let BLUETOOTH_CONTINUOUS_PACKET_DELAY: RxTimeInterval = 0.75
    
    // The amount of time that we wait for a new byte to come in for a packet
    // before we abort the packet and retry
    static let BLUETOOTH_DATA_TIMEOUT_SECONDS: RxTimeInterval = 2
    
    // The amount of time where there's no packets flowing before we disconnect
    static let BLUETOOTH_CONNECTION_IDLE_TIMEOUT: RxTimeInterval = 60
    
    // Register
    static let MIN_PASSWORD_LENGTH: Int = 8
    static let PRIVACY_URL: String = "onboarding_privacy_url".localized
    static let TERMS_URL: String = "onboarding_terms_url".localized
    
    // Data
    static let DATA_USD_TO_EURO_EXCHANGE_RATE = 0.88
    static let DATA_DEFAULT_GAS_COST = 2.80
    static let DATA_DEFAULT_ENERGY_COST = 0.12
    static let DATA_DEFAULT_EFFICIENCY = 29.0

}
