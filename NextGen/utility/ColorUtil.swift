//
//  ColorUtil.swift
//  nextgen
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//


import SwiftHEXColors

public indirect enum AppUIColors: Int {
    case
    navBarBackground,
    navBarTint,
    navBarTitle,
    navBarShadow,

    tabBarTint,
    tabBarBackground,
    tabBarShadow,
    tabBarItem,
    tabBarItemSelected,

    buttonTitle,
    buttonBackground,
    buttonDisabledBackground,

    secondaryButtonBackground,
    secondaryButtonTitle,
    secondaryButtonBorder,
    
    tertiaryButtonBackground,
    tertiaryButtonTitle,
    tertiaryButtonBorder,
    
    divider,
    
    whiteText,
    whiteTextDeselected,
    yellowText,
    yellowTextDeselected,
    grayText,
    greenText
}

class ColorUtil {

    enum AppColors: String {
        case Yellow                = "#FFC600",
        TabBackground              = "#1E1E1E",
        NavBackground              = "#191919",
        BarShadow                  = "#2D2D2D",
        DarkGray                   = "#2A2A2A",
        Gray                       = "#4C4C4C",
        LightGray                  = "#8C8C8C",
        White                      = "#FFFFFF",
        Black                      = "#000000",
        BackgroundBlack            = "#0F0F0F",
        Blue                       = "#007DFF",
        Green                      = "#4BDC64",
        Orange                     = "#FF6900",
        DarkGreen                  = "#264D2D",
        DarkYellow                 = "#806400"
    }

    class func color(_ appColor: AppColors) -> UIColor {
        return UIColor(hexString: appColor.rawValue)!
    }

    class func colorWithAlpha(_ appColor: AppColors, alpha: Float) -> UIColor {
        return UIColor(hexString: appColor.rawValue, alpha: alpha)!
    }

    class func uiColor(_ appUIColor: AppUIColors) -> UIColor {
        switch appUIColor {
        case
        .navBarTint,
        .tabBarTint,
        .tabBarItemSelected,
        .secondaryButtonTitle,
        .tertiaryButtonTitle,
        .buttonBackground,
        .yellowText:
            return color(AppColors.Yellow)

        case
        .tertiaryButtonBorder,
        .tertiaryButtonBackground:
            return color(AppColors.BackgroundBlack)
            
        case
        .tabBarShadow,
        .navBarShadow:
            return color(AppColors.BarShadow)

        case
        .navBarBackground:
            return color(AppColors.NavBackground)

        case
        .tabBarBackground:
            return color(AppColors.TabBackground)

        case
        .tabBarItem,
        .secondaryButtonBorder,
        .divider,
        .grayText:
            return color(AppColors.LightGray)
        case
        .whiteText,
        .navBarTitle:
            return color(AppColors.White)
        case
        .greenText:
            return color(AppColors.Green)
        case
        .buttonTitle,
        .secondaryButtonBackground:
            return color(AppColors.Black)
        case
        .buttonDisabledBackground:
            return color(AppColors.Gray)
        case
        .whiteTextDeselected:
            return colorWithAlpha(AppColors.White, alpha: 0.5)
        case
        .yellowTextDeselected:
            return colorWithAlpha(AppColors.Yellow, alpha: 0.5)
        }
    }
}

