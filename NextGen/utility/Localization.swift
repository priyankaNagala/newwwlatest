//
//  Localization.swift
//  nextgen
//
//  Created by David Crawford on 2/28/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

protocol Localizable {
    var localized: String { get }
}

protocol XIBLocalizable {
    var xibLocKey: String? { get set }
}

