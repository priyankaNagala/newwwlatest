//
//  UIAppearanceManager.swift
//  nextgen
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

// https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIAppearance_Protocol/

class UIAppearanceManager {

    class func establishAppAppearance() {
        self.styleUINavigationBar()
        self.styleUIBarButtonItem()
        self.styleUITabBar()
        self.styleUIButton()
    }

    class func styleUINavigationBar() {
        ZeroUINavigationBar.appearance().tintColor = ColorUtil.uiColor(AppUIColors.navBarTint)
        ZeroUINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        ZeroUINavigationBar.appearance().backgroundColor = ColorUtil.uiColor(AppUIColors.navBarBackground)
        ZeroUINavigationBar.appearance().barTintColor = ColorUtil.uiColor(AppUIColors.navBarBackground)
        ZeroUINavigationBar.appearance().isTranslucent = false
        let titleFont = FontUtil.uiFont(.navBarTitle)
        let navBarTextAttributes = [NSAttributedStringKey.font: titleFont, NSAttributedStringKey.foregroundColor: ColorUtil.uiColor(AppUIColors.navBarTitle)]
        ZeroUINavigationBar.appearance().titleTextAttributes = navBarTextAttributes
    }

    class func styleUIBarButtonItem() {
        ZeroUIBarButtonItem.appearance().tintColor = ColorUtil.uiColor(AppUIColors.navBarTint)
    }

    class func styleUITabBar() {
        ZeroUITabBar.appearance().tintColor = ColorUtil.uiColor(AppUIColors.tabBarTint)
        ZeroUITabBar.appearance().backgroundImage = UIImage()
        ZeroUITabBar.appearance().backgroundColor = ColorUtil.uiColor(AppUIColors.tabBarBackground)
        ZeroUITabBar.appearance().barTintColor = ColorUtil.uiColor(AppUIColors.tabBarBackground)
        ZeroUITabBar.appearance().isTranslucent = false
        ZeroUITabBar.appearance().shadowImage = ImageUtil.imageWithColor(ColorUtil.uiColor(AppUIColors.tabBarShadow), size: CGSize(width: 1,height: 1))
        self.styleUITabBarItem()
    }

    class func styleUITabBarItem() {
        let tabBarFont = FontUtil.uiFont(AppUIFonts.tabBarItem)
        let normalTextAttributes = [NSAttributedStringKey.font: tabBarFont, NSAttributedStringKey.foregroundColor: ColorUtil.uiColor(AppUIColors.tabBarItem)]
        let selectedTextAttributes = [NSAttributedStringKey.font: tabBarFont, NSAttributedStringKey.foregroundColor: ColorUtil.uiColor(AppUIColors.tabBarItemSelected)]
        ZeroUITabBarItem.appearance().setTitleTextAttributes(normalTextAttributes, for: .normal)
        ZeroUITabBarItem.appearance().setTitleTextAttributes(selectedTextAttributes, for: .selected)
    }

    class func styleUIButton() {
        ZeroSecondaryUIButton.appearance().backgroundColor = ColorUtil.uiColor(AppUIColors.secondaryButtonBackground)
    }
}

