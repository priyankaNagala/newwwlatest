//
//  FontUtil.swift
//  NextGenModel
//
//  Created by Scott on 2/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import UIKit

public indirect enum AppUIFonts: Int {
    case navBarTitle,
    tabBarItem,
    buttonTitle,
    buttonTitleOversized,
    pickerItem,
    newsTitle,
    segmentedControlTitle
}

class FontUtil {

    enum AppFonts: String {
        case BertholdBoldExtended = "Berthold Akzidenz Grotesk Bold Extended.otf",
        HelveticaNeueMed = "HelveticaNeue-Medium",
        HelveticaNeueBold  = "HelveticaNeue-Bold",
        HelveticaNeue = "HelveticaNeue",
        HelveticaNeueLight = "HelveticaNeue-Light"
    }

    class func uiFont(_ appUIFont: AppUIFonts) -> UIFont {

        switch appUIFont {
        case .navBarTitle :
            return self.getHelveticaNeueMed(size: 17.0)
        case .tabBarItem :
            return self.getHelveticaNeue(size: 10.0)
        case .buttonTitle,
             .segmentedControlTitle :
            return self.getHelveticaNeueMed(size: 12.0)
        case .buttonTitleOversized :
            return self.getHelveticaNeueMed(size: 16.0)
        case .pickerItem :
            return self.getHelveticaNeueBold(size: 16.0)
        case .newsTitle :
            return self.getHelveticaNeue(size: 14.0)
        }
    }

    class func getHelveticaNeueMed(size fontSize: CGFloat) -> UIFont {
        return UIFont(name: AppFonts.HelveticaNeueMed.rawValue, size: fontSize)!
    }

    class func getHelveticaNeue(size fontSize: CGFloat) -> UIFont {
        return UIFont(name: AppFonts.HelveticaNeue.rawValue, size: fontSize)!
    }
    
    class func getHelveticaNeueBold(size fontSize: CGFloat) -> UIFont {
        return UIFont(name: AppFonts.HelveticaNeueBold.rawValue, size: fontSize)!
    }
    
    class func getHelveticaNeueLight(size fontSize: CGFloat) -> UIFont {
        return UIFont(name: AppFonts.HelveticaNeueLight.rawValue, size: fontSize)!
    }
    
    class func getTwoLineAttributedStringForButton(title: String, subtitle: String?) -> NSAttributedString {
        if let subtitle = subtitle {
            let paragraphStyle = NSMutableParagraphStyle.init()
            paragraphStyle.alignment = .center
            let attribString = NSMutableAttributedString.init(string: title,
                                                              attributes: [NSAttributedStringKey.font : FontUtil.getHelveticaNeueBold(size: 12.0),
                                                                           NSAttributedStringKey.foregroundColor : ColorUtil.color(.Black),
                                                                           NSAttributedStringKey.paragraphStyle: paragraphStyle])
            attribString.append(NSAttributedString.init(string: "\n"))
            
            let subtitleAttrib = NSAttributedString.init(string: subtitle,
                                                           attributes: [NSAttributedStringKey.font : FontUtil.getHelveticaNeueLight(size: 10),
                                                                        NSAttributedStringKey.foregroundColor : ColorUtil.color(.BackgroundBlack),
                                                                        NSAttributedStringKey.paragraphStyle: paragraphStyle])
            attribString.append(subtitleAttrib)
            return attribString
        } else {
            let attribString = NSMutableAttributedString.init(string: title,
                                                              attributes: [NSAttributedStringKey.font : FontUtil.getHelveticaNeueMed(size: 16.0),
                                                                           NSAttributedStringKey.foregroundColor : ColorUtil.color(.Black)])
            return attribString
        }
    }

}
