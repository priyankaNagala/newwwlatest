//
//  TableUtil.swift
//  nextgen
//
//  Created by Scott on 2/9/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

class TableUtil {

    class func zeroMargins(_ tableView: UITableView) {
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.preservesSuperviewLayoutMargins = false
        tableView.cellLayoutMarginsFollowReadableWidth = false
    }
}
