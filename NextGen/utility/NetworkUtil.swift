//
//  NetworkUtil.swift
//  nextgen
//
//  Created by Scott Wang on 1/27/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import SystemConfiguration

class NetworkUtil {
    
    
    static func isNetworkReachable() -> Bool {
        // https://medium.com/@marcosantadev/network-reachability-with-swift-576ca5070e4b
        
        // Initializes the socket IPv4 address struct
        var address = sockaddr_in()
        address.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        address.sin_family = sa_family_t(AF_INET)
        // Passes the reference of the struct
        let reachability = withUnsafePointer(to: &address, { pointer in
            // Converts to a generic socket address
            return pointer.withMemoryRebound(to: sockaddr.self, capacity: MemoryLayout<sockaddr>.size) {
                // $0 is the pointer to `sockaddr`
                return SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability!, &flags)
        // Now `flags` has the right data set by `SCNetworkReachabilityGetFlags`

        if !isNetworkReachable(with: flags) {
            // Device doesn't have internet connection
            return false
        }
        return true
    }
    
    fileprivate static func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
}
