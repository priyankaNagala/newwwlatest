//
//  App.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation

class App {
    static let sharedInstance = App()
 
    var appEnvironment : AppEnvironment
    
    fileprivate init() {
        appEnvironment = AppEnvironment.init()
    }
}
