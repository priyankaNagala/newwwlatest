//
//  MapUtil.swift
//  nextgen
//
//  Created by Scott Wang on 1/19/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import GoogleMaps
import CocoaLumberjack

class MapUtil {
    
    static func getStyledPolyline() -> GMSPolyline {
        let routePolyline = GMSPolyline()

        routePolyline.strokeColor = ColorUtil.color(.White)
        routePolyline.strokeWidth = 2.0

        return routePolyline
    }
    
    static func getMapStyle() -> GMSMapStyle {
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "GoogleMapStyle", withExtension: "json") {
                let mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                return mapStyle
            }
        } catch {
            DDLogError("One or more of the map styles failed to load. \(error)")
        }
        return GMSMapStyle()
    }
    
    static func centerMapOnLocation(map: GMSMapView, start: CLLocationCoordinate2D, end: CLLocationCoordinate2D, padding: CGFloat = 50.0) {
        let bounds = GMSCoordinateBounds.init(coordinate: start, coordinate: end)
        
        map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: padding))
    }
}
