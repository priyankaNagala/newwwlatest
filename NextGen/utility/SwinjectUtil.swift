//
//  SwinjectUtil.swift
//  nextgen
//
//  Created by Scott Wang on 3/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import KYDrawerController

class SwinjectUtil {
    
    static let sharedInstance: SwinjectUtil = SwinjectUtil()
    
    fileprivate init() {}
    
    let container = Container() { container in
        // Utils
        container.register(ZeroAnalyticsService.self) {_ in GoogleAnalyticsService() }
        
        // Services
        container.register(Networking.self) { _ in
            Network()
        }.inObjectScope(.container) // singleton

        container.register(StarcomApiService.self) { r in
            StarcomApiService(network: r.resolve(Networking.self)!)
        }.inObjectScope(.container) // singleton

        container.register(FeaturedNewsApiService.self) { r in
            FeaturedNewsApiService(network: r.resolve(Networking.self)!)
        }.inObjectScope(.container) // singleton

        container.register(DataService.self) { r in
            DataService.shared
        }.inObjectScope(.container) // singleton
        
        container.register(TioService.self) { r in
            TioService()
        }.inObjectScope(.container) // singleton
        
        container.register(ZeroMotorcycleService.self) { r in
            ZeroMotorcycleServiceImpl(tioService: r.resolve(TioService.self)!,
                                      dataService: r.resolve(DataService.self)!)
        }.inObjectScope(.container) // singleton
        
        container.register(ConnectionService.self) { r in
            ConnectionService(tioService: r.resolve(TioService.self)!,
                              dataService: r.resolve(DataService.self)!,
                              zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }.inObjectScope(.container) // singleton
        
        container.register(FirmwareService.self) { r in
            FirmwareService(network: r.resolve(Networking.self)!)
        }.inObjectScope(.container) // singleton
        
        container.register(LogPullService.self) { r in
            LogPullService(zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!,
                           dataService: r.resolve(DataService.self)!)
        }.inObjectScope(.container) // Singleton

        // View models
        container.register(AllRideModesViewModel.self) { r in
            AllRideModesViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(AllRideModesItemViewModel.self) { r in
            AllRideModesItemViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(AppHelpViewModel.self) { r in
            AppHelpViewModel()
        }
        container.register(BatteryViewModel.self) { r in
            BatteryViewModel(dataService: r.resolve(DataService.self)!,
                             zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(ChargeTargetViewModel.self) { r in
            ChargeTargetViewModel(dataService: r.resolve(DataService.self)!,
                                  zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(ConnectConnectViewModel.self) { r in
            ConnectConnectViewModel(dataService: r.resolve(DataService.self)!,
                                    zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(ConnectFindViewModel.self) { r in
            ConnectFindViewModel(tioService: r.resolve(TioService.self)!)
        }
        container.register(ConnectImproveViewModel.self) { r in
            ConnectImproveViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(DashboardSettingsViewModel.self) { r in
            DashboardSettingsViewModel(dataService: r.resolve(DataService.self)!,
                                       zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(DashboardViewModel.self) { r in
            DashboardViewModel(featuredNewsApiService: r.resolve(FeaturedNewsApiService.self)!,
                               dataService: r.resolve(DataService.self)!,
                               zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!,
                               starcomApiService: r.resolve(StarcomApiService.self)!)
        }
        container.register(DeactivateRideModeViewModel.self) { r in
            DeactivateRideModeViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(DrawerViewModel.self) { r in
            DrawerViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(GaugePickerViewModel.self) { r in
            GaugePickerViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(InfoSheetViewModel.self) { r in
            InfoSheetViewModel()
        }
        container.register(ManageRideModesViewModel.self) { r in
            ManageRideModesViewModel(dataService: r.resolve(DataService.self)!,
                                     zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(ManageRideModesItemViewModel.self) { r in
            ManageRideModesItemViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(PerformanceViewModel.self) { r in
            PerformanceViewModel(dataService: r.resolve(DataService.self)!,
                                 zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(RemoteConnectViewModel.self) { r in
            RemoteConnectViewModel(starcomApiService: r.resolve(StarcomApiService.self)!,
                                   dataService: r.resolve(DataService.self)!)
        }
        container.register(RideModeViewModel.self) { r in
            RideModeViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(RouteTypePickerViewModel.self) { r in
            RouteTypePickerViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(ScheduledChargingViewModel.self) { r in
            ScheduledChargingViewModel(dataService: r.resolve(DataService.self)!,
                                       zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(SettingsItemViewModel.self) { r in
            SettingsItemViewModel()
        }
        container.register(SettingsViewModel.self) { r in
            SettingsViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(RegisterViewModel.self) { r in
            RegisterViewModel(starcomApiService: r.resolve(StarcomApiService.self)!,
                              dataService: r.resolve(DataService.self)!,
                              zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(LoginViewModel.self) { r in
            LoginViewModel(starcomApiService: r.resolve(StarcomApiService.self)!,
                           dataService: r.resolve(DataService.self)!)
        }
        container.register(AccountViewModel.self) { r in
            AccountViewModel(starcomApiService: r.resolve(StarcomApiService.self)!,
                             dataService: r.resolve(DataService.self)!,
                             zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(FirmwareManagerViewModel.self) { r in
            FirmwareManagerViewModel(firmwareService: r.resolve(FirmwareService.self)!,
                                     dataService: r.resolve(DataService.self)!)
        }
        container.register(DataViewModel.self) { r in
            DataViewModel(dataService: r.resolve(DataService.self)!,
                          zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        container.register(ReliveViewModel.self) { r in
            ReliveViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(RideDataSheetViewModel.self) { r in
            RideDataSheetViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(RelivePlaybackViewModel.self) { r in
            RelivePlaybackViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(EditComparableSettingsViewModel.self) { r in
            EditComparableSettingsViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(ShareRideViewModel.self) { r in
            ShareRideViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(ShareStatPickerViewModel.self) { r in
            ShareStatPickerViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(ErrorCodesItemViewModel.self) { r in
            ErrorCodesItemViewModel()
        }
        container.register(RoadsideAssistSetupViewModel.self) { r in
            RoadsideAssistSetupViewModel(dataService: r.resolve(DataService.self)!)
        }
        container.register(LogSelectViewModel.self) { r in
            LogSelectViewModel(dataService: r.resolve(DataService.self)!,
                               zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!,
                               logPullService: r.resolve(LogPullService.self)!)
        }
        container.register(LogDownloadViewModel.self) { r in
            LogDownloadViewModel(dataService: r.resolve(DataService.self)!,
                                 zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!,
                                 logPullService: r.resolve(LogPullService.self)!)
        }
        container.register(SyncChangesViewModel.self) { r in
            SyncChangesViewModel(zeroMotorcycleService: r.resolve(ZeroMotorcycleService.self)!)
        }
        // main
        container.storyboardInitCompleted(KYDrawerController.self) {
            r, c in
        }
        container.storyboardInitCompleted(RootTabBarController.self) {
            r, c in
        }
        container.storyboardInitCompleted(ZeroNavigationController.self) {
            r, c in
        }
        container.storyboardInitCompleted(DrawerViewController.self) {
            r, c in
            c.drawerViewModel = r.resolve(DrawerViewModel.self)!
        }
        container.storyboardInitCompleted(AdminViewController.self) {
            r, c in
        }
        container.storyboardInitCompleted(PacketDebugViewController.self) {
            r, c in
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(SyncChangesViewController.self) {
            r, c in
            c.syncChangesViewModel = r.resolve(SyncChangesViewModel.self)!
        }
        
        // dashboard
        container.storyboardInitCompleted(DashboardViewController.self) {
            r, c in
            c.dashboardViewModel = r.resolve(DashboardViewModel.self)!
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.dataService = r.resolve(DataService.self)!
            c.connectionService = r.resolve(ConnectionService.self)!
        }
        
        // battery
        container.storyboardInitCompleted(BatteryViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.batteryViewModel = r.resolve(BatteryViewModel.self)!
            c.scheduledChargingViewModel = r.resolve(ScheduledChargingViewModel.self)!
            c.connectionService = r.resolve(ConnectionService.self)!
        }
        container.storyboardInitCompleted(ScheduledChargingViewController.self) {
            r, c in
            c.scheduledChargingViewModel = r.resolve(ScheduledChargingViewModel.self)!
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(ScheduledChargingIntroViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(ChargeTargetViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.chargeTargetViewModel = r.resolve(ChargeTargetViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(RouteTypePickerViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.routeTypePickerViewModel = r.resolve(RouteTypePickerViewModel.self)!
        }
        // performance
        container.storyboardInitCompleted(PerformanceViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.performanceViewModel = r.resolve(PerformanceViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.connectionService = r.resolve(ConnectionService.self)!
        }

        // dashboard settings
        container.storyboardInitCompleted(DashboardSettingsViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.dashboardSettingsViewModel = r.resolve(DashboardSettingsViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(GaugePickerViewController.self) {
            r, c in
            c.gaugePickerViewModel = r.resolve(GaugePickerViewModel.self)!
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // data
        container.storyboardInitCompleted(DataViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.connectionService = r.resolve(ConnectionService.self)!
            c.dataViewModel = r.resolve(DataViewModel.self)!
        }
        
        // remote connect
        container.storyboardInitCompleted(RemoteConnectViewController.self) {
            r, c in
            c.remoteConnectViewModel = r.resolve(RemoteConnectViewModel.self)
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
        }
        container.storyboardInitCompleted(InfoSheetViewController.self) {
            r, c in
            c.infoSheetViewModel = r.resolve(InfoSheetViewModel.self)
        }
        
        // onboarding
        container.storyboardInitCompleted(WelcomeViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
        }
        container.storyboardInitCompleted(LegalTermsViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        
        // connect
        container.storyboardInitCompleted(ConnectFindViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.connectFindViewModel = r.resolve(ConnectFindViewModel.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        container.storyboardInitCompleted(ConnectConnectViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.connectConnectViewModel = r.resolve(ConnectConnectViewModel.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        container.storyboardInitCompleted(ConnectImproveViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.connectImproveViewModel = r.resolve(ConnectImproveViewModel.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        container.storyboardInitCompleted(ConnectCompleteViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        
        // apphelp
        container.storyboardInitCompleted(HelpViewController.self) {
            r, c in
            c.helpViewModel = r.resolve(HelpViewModel.self)
        }
        container.storyboardInitCompleted(AppHelpViewController.self) {
            r, c in
            c.appHelpViewModel = r.resolve(AppHelpViewModel.self)
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
        }
        
        // ride modes
        container.storyboardInitCompleted(AllRideModesViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.allRideModesViewModel = r.resolve(AllRideModesViewModel.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        container.storyboardInitCompleted(DeactivateRideModeViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.deactivateRideModeViewModel = r.resolve(DeactivateRideModeViewModel.self)!
        }
        container.storyboardInitCompleted(ManageRideModesViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.manageRideModesViewModel = r.resolve(ManageRideModesViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
            c.dataService = r.resolve(DataService.self)!
        }
        container.storyboardInitCompleted(RideModeViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.rideModeViewModel = r.resolve(RideModeViewModel.self)
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // settings
        container.storyboardInitCompleted(SettingsViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.settingsViewModel = r.resolve(SettingsViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // connection status
        container.storyboardInitCompleted(ConnectionStatusViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.dataService = r.resolve(DataService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // starcom
        container.storyboardInitCompleted(RegisterPromptViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.dataService = r.resolve(DataService.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }

        container.storyboardInitCompleted(LoginViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(LoginViewModel.self)!
        }

        container.storyboardInitCompleted(RegisterViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(RegisterViewModel.self)!
        }
        
        container.storyboardInitCompleted(AccountViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(AccountViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // firmware
        container.storyboardInitCompleted(FirmwareManagerViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(FirmwareManagerViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        // Relive
        container.storyboardInitCompleted(ReliveViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(ReliveViewModel.self)!
        }
        
        container.storyboardInitCompleted(RideDataSheetViewController.self) {
            r, c in
            c.viewModel = r.resolve(RideDataSheetViewModel.self)!
        }
        
        container.storyboardInitCompleted(RelivePlaybackViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.viewModel = r.resolve(RelivePlaybackViewModel.self)!
        }
        
        container.storyboardInitCompleted(EditComparableSettingViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.settingsViewModel = r.resolve(EditComparableSettingsViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        
        container.storyboardInitCompleted(ShareRideViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.shareRideViewModel = r.resolve(ShareRideViewModel.self)!
        }
        
        container.storyboardInitCompleted(ShareStatPickerViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.pickerViewModel = r.resolve(ShareStatPickerViewModel.self)!
        }
        
        container.storyboardInitCompleted(RoadsideAssistSetupViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.roadsideAssistViewModel = r.resolve(RoadsideAssistSetupViewModel.self)!
        }
        
        // Log Pulling
        container.storyboardInitCompleted(LogSelectViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.logSelectViewModel = r.resolve(LogSelectViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
        container.storyboardInitCompleted(LogDownloadViewController.self) {
            r, c in
            c.analyticsService = r.resolve(ZeroAnalyticsService.self)!
            c.logDownloadViewModel = r.resolve(LogDownloadViewModel.self)!
            c.zeroMotorcycleService = r.resolve(ZeroMotorcycleService.self)!
        }
    }
}
