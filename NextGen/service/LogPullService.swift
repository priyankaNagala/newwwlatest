//
//  LogPullService.swift
//  nextgen
//
//  Created by Scott Wang on 1/31/19.
//  Copyright © 2019 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CocoaLumberjack

class LogPullService {
    enum LogType: Int {
        case MBB = 0, BMS = 1
    }
    
    var zeroMotorcycleService: ZeroMotorcycleService
    var dataService: DataService
    
    var mbbNumBytes: Int = 0
    var mbbNumBlocks : Int = 0
    var bmsNumBytes : Int = 0
    var bmsNumBlocks : Int = 0
    
    var downloadMbbAndBms: Bool = false
    
    var progress = BehaviorRelay<Float>(value: 0)
    var failed = BehaviorRelay<Bool>(value: false)
    var complete = BehaviorRelay<Bool>(value: false)
    
    fileprivate var mbbCurrentBlock = 0
    fileprivate var bmsCurrentBlock = 0
    fileprivate var mbbData = Data()
    fileprivate var bmsData = Data()
    fileprivate var disposeBag = DisposeBag()
    fileprivate var numRetries = 0
    fileprivate var mbbFileUrl: URL?
    fileprivate var bmsFileUrl: URL?
    
    init(zeroMotorcycleService: ZeroMotorcycleService, dataService: DataService) {
        self.zeroMotorcycleService = zeroMotorcycleService
        self.dataService = dataService
    }
    
    func setLogInit(_ logInitPacket: LogInitPacket) {
        mbbNumBytes = logInitPacket.getNumBytes(0)
        mbbNumBlocks = logInitPacket.getNumBlocks(0)
        bmsNumBytes = logInitPacket.getNumBytes(1)
        bmsNumBlocks = logInitPacket.getNumBlocks(1)
    }
    
    func clear() {
        mbbNumBytes = 0
        mbbNumBlocks = 0
        bmsNumBytes = 0
        bmsNumBlocks = 0
        mbbCurrentBlock = 0
        bmsCurrentBlock = 0
        mbbData = Data()
        bmsData = Data()
        disposeBag = DisposeBag()
        numRetries = 0
        mbbFileUrl = nil
        bmsFileUrl = nil
        progress.accept(0)
        failed.accept(false)
        complete.accept(false)
    }
    
    func mbbHasLogs() -> Bool {
        return mbbNumBlocks > 0
    }
    
    func bmsHasLogs() -> Bool {
        return bmsNumBlocks > 0
    }
    
    func minutesForMbb() -> Int {
        return Int(ceil(Double(mbbNumBlocks) / Constant.BLUETOOTH_PACKETS_PER_MINUTE))
    }
    
    func minutesForBms() -> Int {
        return Int(ceil(Double(mbbNumBlocks + bmsNumBlocks) / Constant.BLUETOOTH_PACKETS_PER_MINUTE))
    }
    
    func start() {
        zeroMotorcycleService.setSendRoundRobin(false) // Stop round robin
        requestOneBlock(.MBB)
    }
    
    fileprivate func requestOneBlock(_ logType: LogType) {
        let logBlockPacket = LogBlockPacket()
        logBlockPacket.fileNumber = UInt8(logType.rawValue)
        if logType == .MBB {
            logBlockPacket.blockNumber = UInt16(mbbCurrentBlock)
        } else {
            logBlockPacket.blockNumber = UInt16(bmsCurrentBlock)
        }
        
        zeroMotorcycleService.sendSingleZeroPacket(packet: logBlockPacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let packet = event.element as? LogBlockPacket {
                strongSelf.disposeBag = DisposeBag()
                if logType == .MBB {
                    strongSelf.mbbData.append(packet.data)
                    strongSelf.mbbCurrentBlock = strongSelf.mbbCurrentBlock + 1
                    strongSelf.updateProgress()
                    if strongSelf.mbbCurrentBlock < strongSelf.mbbNumBlocks {
                        strongSelf.requestOneBlock(.MBB) // Get Next Block
                    } else if strongSelf.downloadMbbAndBms {
                        strongSelf.requestOneBlock(.BMS) // Move onto BMS
                    } else {
                        strongSelf.writeToDisk()
                        strongSelf.sendLogDone()
                    }
                } else {
                    strongSelf.bmsData.append(packet.data)
                    strongSelf.bmsCurrentBlock = strongSelf.bmsCurrentBlock + 1
                    strongSelf.updateProgress()
                    if strongSelf.bmsCurrentBlock < strongSelf.bmsNumBlocks {
                        strongSelf.requestOneBlock(.BMS) // Get next block
                    } else {
                        strongSelf.writeToDisk()
                        strongSelf.sendLogDone()
                    }
                }
                strongSelf.numRetries = 0
            } else {
                DDLogDebug("Read LogBlock Received Unknown Packet")
            }
        }.disposed(by: disposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            strongSelf.disposeBag = DisposeBag()
            DDLogDebug("Read LogBlock Received Resend")
            if strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES {
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.requestOneBlock(logType)
            } else {
                strongSelf.numRetries = 0
                strongSelf.failed.accept(true)
            }
            
        }.disposed(by: disposeBag)
    }
    
    fileprivate func sendLogDone() {
        let logDonePacket = LogDonePacket()
        zeroMotorcycleService.sendSingleZeroPacket(packet: logDonePacket, priority: true).subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let _ = event.element as? AckPacket {
                strongSelf.disposeBag = DisposeBag()
                strongSelf.complete.accept(true) // Done
                strongSelf.zeroMotorcycleService.setSendRoundRobin(true) // Restart continuous
            } else {
                DDLogDebug("LogDone received unknown packet")
            }
        }.disposed(by: disposeBag)
        
        zeroMotorcycleService.getResendSubject().subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            strongSelf.disposeBag = DisposeBag()
            DDLogDebug("LogDone received Resend")
            if strongSelf.numRetries < Constant.BLUETOOTH_RESEND_RETRIES {
                strongSelf.numRetries = strongSelf.numRetries + 1
                strongSelf.sendLogDone()
            } else {
                strongSelf.numRetries = 0
                strongSelf.failed.accept(true) // Failed
                strongSelf.zeroMotorcycleService.setSendRoundRobin(true) // Restart continuous
            }
        }.disposed(by: disposeBag)
    }
    
    fileprivate func updateProgress() {
        let totalBlocks = downloadMbbAndBms ? mbbNumBlocks + bmsNumBlocks : mbbNumBlocks
        
        let current = downloadMbbAndBms ? mbbCurrentBlock + bmsCurrentBlock : mbbCurrentBlock
        
        progress.accept(Float(current) / Float(totalBlocks))
    }
    
    fileprivate func writeToDisk() {
        if let mbbFile = getFile(logType: .MBB, generateNew: true) {
            if mbbData.count > 0 {
                try? mbbData.write(to: mbbFile)
                mbbFileUrl = mbbFile
            }
        }
        if let bmsFile = getFile(logType: .BMS, generateNew: true) {
            if bmsData.count > 0 {
                try? bmsData.write(to: bmsFile)
                bmsFileUrl = bmsFile
            }
        }
    }
    
    fileprivate func getFile(logType: LogType, generateNew: Bool) -> URL? {
        if generateNew {
            let newFile = getNewFileName(logType)
            if let baseDir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
                return baseDir.appendingPathComponent(newFile)
            }
        }
        if logType == .MBB {
            return mbbFileUrl
        } else {
            return bmsFileUrl
        }
    }
    
    fileprivate func getNewFileName(_ logType: LogType) -> String {
        /*
 1.)    No spaces in the filenames
 
 2.)    File names must have .bin extension
 
 3.)    Files should be named  [DATE]_[TIME (Hour Min in 24 hour format)]_[VIN]_[ECU].bin …So as an example:  20190131_08.35_ 538zfaz77lck10523_MBB.bin
 
 a.      DATE can be YearMoDay
 
 b.      Time is in 24 hour format in UTC time
 
 c.      ECU is either MBB or BMS. When we go to multiple BMS, we can use BMS0, BMS1, etc..
 */
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyyMMdd_HH.mm"
        let date = dateFormatter.string(from: Date())
        
        let vin = dataService.currentVin ?? "xxx"
        let type = logType == .MBB ? "MBB" : "BMS"
        
        let fileName = "\(date)_\(vin)_\(type).bin"
        return fileName
    }
    
    func getFilesToShare() -> [Any] {
        var filesToShare = [Any]()
        if let mbbFile = getFile(logType: .MBB, generateNew: false) {
            filesToShare.append(mbbFile)
        }
        if downloadMbbAndBms, let bmsFile = getFile(logType: .BMS, generateNew: false) {
            filesToShare.append(bmsFile)
        }
        return filesToShare
    }
    
    func cancel() {
        zeroMotorcycleService.setSendRoundRobin(true) // Restart continuous
        disposeBag = DisposeBag()
    }
}
