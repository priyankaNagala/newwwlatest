//
//  ZeroMotorcycleService.swift
//  nextgen
//
//  Created by David Crawford on 8/17/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ZeroMotorcycleServiceState {
    case INITIAL_CONNECTION, QUEUE, CONTINUOUS
}

protocol ZeroMotorcycleService {
    func getConnectionState() -> BehaviorRelay<Bool>
    func connect(peripheral: TIOPeripheral)
    func isConnected() -> Bool
    func isConnecting() -> Bool
    func getConnectedPeripheral() -> TIOPeripheral?
    
    func sendSingleZeroPacket(packet: ZeroPacket, priority: Bool) -> Observable<ZeroPacket>
    func sendSingleZeroPacketReplacingSameType(packet: ZeroPacket, priority: Bool) -> Observable<ZeroPacket>

    func queueSingleZeroPacket(packet: ZeroPacket)
    func offlineQueueIsEmpty() -> BehaviorRelay<Bool>
    func clearOfflineQueue()
    func sendOfflinePackets()
    
    // For debug purposes only. Sends the raw body bytes as a string to the observer
    func getPacketByteOutput() -> PublishRelay<String>
    
    //
    // Used to continuously send packets of a certain type.
    //
    // If the caller is only interested in continuously sending a single packet, when we receive
    // one packet, we will then request it again.
    //
    // If the caller is interested in continuously sending multiple packets, we request
    // the packets in a round robin fashion.
    //
    func getResendSubject() -> Observable<ResendPacket>

    func getBikeInfo() -> Observable<BikeInfoPacket>

    func getDashGauges() -> Observable<DashGaugesPacket>

    func getChargeTarget() -> Observable<ChargeTargetPacket>
    func getChargeTargetResend() -> Observable<ResendPacket>
    
    func getScheduledChargingResend() -> Observable<ResendPacket>

    func sendRawBytes(_ data: Data) -> PublishRelay<String>
    func setSendRoundRobin(_ send: Bool)

}
