//
//  ConnectionService.swift
//  nextgen
//
//  Created by Scott Wang on 10/10/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import CocoaLumberjack
import RxSwift
import RxCocoa

class ConnectionService {
    
    static let CONNECTION_TIMER_DELAY: Double = 15
    
    var tioService: TioService
    var zeroMotorcycleService: ZeroMotorcycleService
    var dataService: DataService
    var disposeBag = DisposeBag()
    
    var autoConnecting = BehaviorRelay<Bool>(value: false)
    
    private var timer: Timer?
    private var connectionAttempts: Double = 1
    private var tryingToConnect: Bool = false
    
    init(tioService: TioService, dataService: DataService, zeroMotorcycleService: ZeroMotorcycleService) {
        self.tioService = tioService
        self.dataService = dataService
        self.zeroMotorcycleService = zeroMotorcycleService
    }

    // Reset the backoff
    func resetBackoff() {
        connectionAttempts = 1
        start()
    }

    // Immediately try to connect then sets timer for next attempt
    func start() {
        DispatchQueue.main.asyncAfter(deadline: .now() + Constant.BLUETOOTH_AVAILABLE_WAIT_SECONDS) {
            self.tryToReconnect()
        }
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: ConnectionService.CONNECTION_TIMER_DELAY, target: self, selector: #selector(self.timerFired), userInfo: nil, repeats: false)
        }
    }
    
    func stop() {
        DispatchQueue.main.async {
            self.timer?.invalidate()
        }
    }
    
    @objc func timerFired() {
        tryToReconnect()
        // Setup Next Timer
        let delay = ConnectionService.CONNECTION_TIMER_DELAY * self.connectionAttempts
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(self.timerFired),  userInfo: nil, repeats: false)
        }
        connectionAttempts = connectionAttempts + 1
    }
    
    func tryToReconnect() {
        guard let lastConnectedUUID = dataService.lastConnectedPeripheralUUID,
            zeroMotorcycleService.isConnected() == false,
            zeroMotorcycleService.isConnecting() == false,
            tryingToConnect == false else { return }

        tryingToConnect = true
        autoConnecting.accept(true)
        if (!self.tioService.bluetoothAvailable.value) {
            self.tryingToConnect = false
            autoConnecting.accept(false)
            return
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + Constant.BLUETOOTH_SCAN_SECONDS) {
            self.tioService.stopScan()
            self.tryingToConnect = false
            self.autoConnecting.accept(false)
        }
    
        DDLogDebug("START SCAN == TRY TO CONNECT")
        
        guard self.zeroMotorcycleService.isConnected() == false else { return }
        self.tioService.startScan()
            .subscribe({ [weak self] foundPeripheral in
                guard let strongSelf = self, let peripheral = foundPeripheral.element else { return }
                if peripheral.identifier.uuidString == lastConnectedUUID {
                    strongSelf.disposeBag = DisposeBag()
                    strongSelf.tioService.stopScan()
                    strongSelf.zeroMotorcycleService.connect(peripheral: peripheral)
                }
            })
            .disposed(by: self.disposeBag)
    }
}
