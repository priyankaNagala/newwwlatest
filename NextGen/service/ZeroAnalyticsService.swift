//
//  ZeroAnalyticsService.swift
//  nextgen
//
//  Created by Scott Wang on 3/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import UIKit

protocol ZeroAnalyticsService {    
    func trackScreenView(viewController: UIViewController, screenName: String)
    func trackEvent()
}
