//
//  FirmwareService.swift
//  nextgen
//
//  Created by Scott Wang on 11/7/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import Himotoki
import CocoaLumberjack
import Zip

class FirmwareService {
    static let API_URL = "https://fupdater.zeromotorcycles.com/builds"
    static let API_HEADERS = ["Authorization" : "Bearer 6VTRefKAheOOE2rsOwSiL6Vz/HD/rNj3hHCIralhnEk="]
    
    private let network: Networking
    var disposeBag = DisposeBag()
    
    
    public init(network: Networking) {
        self.network = network
    }
    
    func makeCurrentBuildApiRequest() -> Observable<FWVersionsResponse> {
        return Observable<FWVersionsResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: FirmwareService.API_URL,
                                     parameters: ["getFWVersions" : "1" as AnyObject],
                                     headers: FirmwareService.API_HEADERS)
            .debug()
            .subscribe(onNext: { (r, json) in
                if let dictionary = json as? [String: Any] {
                    let response = (try? FWVersionsResponse.decodeValue(dictionary)) as FWVersionsResponse?
                    if response != nil {
                        observer.onNext(response!)
                    }
                } else {
                    observer.onNext(FWVersionsResponse(buildTime: nil, formatRevision: nil, version: nil, fwVersions: nil))
                }
                observer.onCompleted()
            }, onError: { (error) in
                DDLogError("On Error")
                DDLogError(error.localizedDescription)
            }, onCompleted: {
                DDLogDebug("On Completed")
            }, onDisposed: {
                DDLogDebug("On Disposed")
            })
        })
    }
    
    // Make build.zip download request
    // Save build.zip to download directory
    // Unzip build.zip file
    //
    func makeBuildDownloadApiRequest(_ version: String) -> Observable<FupdaterResponse> {
        return Observable<FupdaterResponse>.create({ (observer) -> Disposable in
            self.network.requestDownload(url: FirmwareService.API_URL,
                                     parameters: ["forcebuild" : version as AnyObject, "download" : "1" as AnyObject],
                                     headers: FirmwareService.API_HEADERS)
            .debug()
            .subscribe(onNext: { [weak self] (r, data) in
                guard let strongSelf = self else { return }
                // Check SHA512 hash
                let zeroSha = r.allHeaderFields["ZeroSha512"] as! String
                let dataSha = data.sha512().toHexString()
                if zeroSha != dataSha {
                    DDLogWarn("Zip file SHA512 \(dataSha) does not match header SHA512 \(zeroSha)")
                    observer.onNext(FupdaterResponse(error: "Corrupted Download", result: nil))
                    observer.onCompleted()
                    return
                } else {
                    DDLogWarn("Zip file SHA512 matches header SHA512 \(zeroSha)")
                }
                
                if let buildFile = strongSelf.getBuildFile(version), let buildDirectory = strongSelf.getBuildDirectory(version) {
                    do {
                        // Wrtite to disk
                        try data.write(to: buildFile)

                        // Unzip files
                        try Zip.unzipFile(buildFile, destination: buildDirectory, overwrite: true, password: nil)
                    } catch let error {
                        observer.onNext(FupdaterResponse(error: "Failed to write firmware to disk", result: nil))
                        observer.onCompleted()
                        DDLogError("\(error)")
                        return
                    }
                    observer.onNext(FupdaterResponse(error: nil, result: "Success"))
                    observer.onCompleted()
                } else {
                    observer.onNext(FupdaterResponse(error: "Failed to write firmware to disk", result: nil))
                    observer.onCompleted()
                }
            }, onError: { (error) in
                DDLogError("On Error")
                DDLogError(error.localizedDescription)
            }, onCompleted: {
                DDLogDebug("On Completed")
            }, onDisposed: {
                DDLogDebug("On Disposed")
            })
        })
    }
    
    func getBuildDirectory(_ version: String) -> URL? {
        let fileManager = FileManager.default
        do {
            let baseDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let buildDirectory = baseDirectory.appendingPathComponent(version, isDirectory: true)
            try fileManager.createDirectory(atPath: buildDirectory.path, withIntermediateDirectories: true, attributes: nil)
            return buildDirectory
        } catch let error {
            DDLogError("\(error)")
        }
        return nil
    }
    
    func cleanBuildDirectory(_ version: String) {
        guard let buildDirectory = getBuildFile(version) else { return }
        do {
            try FileManager.default.removeItem(at: buildDirectory)
        } catch let error {
            DDLogError("\(error)")
        }
    }
    
    func getBuildFile(_ version: String) -> URL? {
        if let base = getBuildDirectory(version) {
            let fileUrl = base.appendingPathComponent("build.zip")
            return fileUrl
        }
        return nil
    }
    
    func buildFileDownloaded(_ version: String) -> Bool {
        if let buildFile = getBuildFile(version) {
            let fileManager = FileManager.default
            return fileManager.fileExists(atPath: buildFile.path)

        }
        return false
    }
    
    func getBuildPackageInfoFileLocation(_ version: String) -> URL? {
        if let buildDirectory = getBuildDirectory(version) {
            let infoFile = buildDirectory.appendingPathComponent("info.json")
            return infoFile
        }
        return nil
    }
    
    func getFupForBuildPackage(_ version: String) -> Fup? {
        if let infoFile = getBuildPackageInfoFileLocation(version) {
            do {
                let jsonString = try String(contentsOf: infoFile)
                if let jsonData = jsonString.data(using: .utf8),
                    let json = try JSONSerialization.jsonObject(with: jsonData, options:[]) as? [String: Any],
                    var fup = (try? Fup.decodeValue(json)) as Fup? {
                    fup.version = version
                    return fup
                }
            } catch let error {
                DDLogError("\(error)")
            }
        }
        return nil
    }
    
    func fupPackageIsComplete(_ fup: Fup) -> Bool {
        guard fup.firmware.count > 0, let version = fup.version, let baseDir = getBuildDirectory(version) else { return false }
        for (_, fupFirmware) in fup.firmware {
            if let bankA = fupFirmware.bankA {
                let binFile = baseDir.appendingPathComponent(bankA.binFile)
                if !FileManager.default.fileExists(atPath: binFile.path) {
                    return false
                }
            }
            if let bankB = fupFirmware.bankB {
                let binFile = baseDir.appendingPathComponent(bankB.binFile)
                if !FileManager.default.fileExists(atPath: binFile.path) {
                    return false
                }
            }
        }
        return true
    }
}
