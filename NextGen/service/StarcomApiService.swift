//
//  StartcomApiService.swift
//  nextgen
//
//  Created by Scott Wang on 2/14/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import Himotoki
import CocoaLumberjack

class StarcomApiService {
    
    private let network: Networking
    
    var disposeBag = DisposeBag()
    
    public init(network: Networking) {
        self.network = network
    }
    
    func getUnit(user: String, password: String, format: String = "json") -> Observable<StarcomUnit?> {
        return Observable<StarcomUnit?>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "get_units" as AnyObject,
                                                  "user" : user as AnyObject,
                                                  "pass" : password  as AnyObject,
                                                  "format" : format  as AnyObject],
                                     headers: nil)
            .debug()
                .subscribe(onNext: { (r, json) in
                    if let arr = json as? [Any] {
                        // Just pick the first one
                        if let lt = arr.first, let starcomUnit = (try? StarcomUnit.decodeValue(lt)) as StarcomUnit? {
                            observer.onNext(starcomUnit)
                        }
                    } else {
                        observer.onNext(nil)
                    }
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func lastTransmit(user: String, password: String, unitNumber: String, format: String = "json") -> Observable<LastTransmit> {
        
        return Observable<LastTransmit>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "get_last_transmit" as AnyObject,
                                                  "user" : user as AnyObject,
                                                  "pass" : password  as AnyObject,
                                                  "unitnumber" : unitNumber  as AnyObject,
                                                  "format" : format  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                   let arr = json as! [Any]
                    
                    if let lt = arr.first, let lastTransmit = (try? LastTransmit.decodeValue(lt)) as LastTransmit? {
                        observer.onNext(lastTransmit)
                    }
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func registerForNotifications(sessionId: String, registrationId: String) -> Observable<StarcomResponse> {
        return Observable<StarcomResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "notify_set" as AnyObject,
                                                  "sid" : sessionId as AnyObject,
                                                  "registrationid" : registrationId  as AnyObject,
                                                  "source" : Constant.STARCOM_SOURCE_CLIENT as AnyObject,
                                                  "format" : "json"  as AnyObject,
                                                  "reasons" : Constant.STARCOM_NOTIFICATION_TYPES.joined(separator: ",") as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func deregisterForNotifications(sessionId: String, registrationId: String) -> Observable<StarcomResponse> {
        return Observable<StarcomResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "notify_unset" as AnyObject,
                                                  "sid" : sessionId as AnyObject,
                                                  "registrationid" : registrationId  as AnyObject,
                                                  "source" : Constant.STARCOM_SOURCE_CLIENT as AnyObject,
                                                  "format" : "json"  as AnyObject,
                                                  "reasons" : Constant.STARCOM_NOTIFICATION_TYPES.joined(separator: ",") as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func createUser(user: String, password: String, vin: String, passcode: String) -> Observable<StarcomSession> {
        return Observable<StarcomSession>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "create_user" as AnyObject,
                                                  "newuser" : user as AnyObject,
                                                  "newpassword" : password  as AnyObject,
                                                  "vin" : vin as AnyObject,
                                                  "passcode" : passcode  as AnyObject,
                                                  "source" : Constant.STARCOM_SOURCE_CLIENT as AnyObject,
                                                  "format" : "json"  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    guard let strongSelf = self else { return }
                    if let _ = json as? [String: Any] {
                        // Need to login to get sessionId after register
                        strongSelf.login(user: user, password: password).subscribe { event in
                            if let session = event.element {
                                observer.onNext(session)
                            }
                            observer.onCompleted()
                        }.disposed(by: strongSelf.disposeBag)
                    } else if let error = json as? String {
                        DDLogDebug("Create User error: \(error)")
                        let response = StarcomSession(sessionId: nil, result: nil, error: "starcom_registration_error".localized)
                        observer.onNext(response)
                        observer.onCompleted()
                    } else {
                        observer.onCompleted()
                    }
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }

    func deleteUser(user: String, sessionId: String) -> Observable<StarcomResponse> {
        return Observable<StarcomResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "unset_user" as AnyObject,
                                                  "targetuser" : user as AnyObject,
                                                  "sid" : sessionId as AnyObject,
                                                  "source" : Constant.STARCOM_SOURCE_CLIENT as AnyObject,
                                                  "format" : "json1"  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                    if let dictionary = json as? [String: Any] {
                        let response = (try? StarcomResponse.decodeValue(dictionary)) as StarcomResponse?
                        if response != nil {
                            observer.onNext(response!)
                        }
                    } else if let error = json as? String {
                        DDLogDebug("Deregister error: \(error)")
                        let response = StarcomResponse(result: nil, error: "starcom_deregister_error".localized)
                        observer.onNext(response)
                    }
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func login(user: String, password: String) -> Observable<StarcomSession> {
        return Observable<StarcomSession>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "session_create" as AnyObject,
                                                  "user" : user as AnyObject,
                                                  "pass" : password  as AnyObject,
                                                  "format" : "json"  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    guard let strongSelf = self else { return }
                    if let dictionary = json as? [String: Any] {
                        if let response = (try? StarcomSession.decodeValue(dictionary)) as StarcomSession? {
                            if response.sessionId != nil {
                                // Get unit number
                                strongSelf.getUnit(user: user, password: password).subscribe { event in
                                    if let unitResponse = event.element, let starcomUnit = unitResponse {
                                        DataService.shared.unitNumber = starcomUnit.unitNumber
                                        observer.onNext(response)
                                        observer.onCompleted()
                                    } else {
                                        let session = StarcomSession(sessionId: nil, result: nil, error: "starcom_login_error".localized)
                                        observer.onNext(session)
                                        observer.onCompleted()
                                    }
                                }.disposed(by: strongSelf.disposeBag)
                            } else {
                                if let error = response.error {
                                    DDLogDebug("Login Error: \(error)")
                                    let session = StarcomSession(sessionId: nil, result: nil, error: "starcom_login_error".localized)
                                    observer.onNext(session)
                                    observer.onCompleted()
                                }
                            }
                        } else {
                            let session = StarcomSession(sessionId: nil, result: nil, error: "starcom_login_error".localized)
                            observer.onNext(session)
                            observer.onCompleted()
                        }
                    } else if let error = json as? String {
                        DDLogDebug("Login Error: \(error)")
                        let session = StarcomSession(sessionId: nil, result: nil, error: "starcom_login_error".localized)
                        observer.onNext(session)
                        observer.onCompleted()
                    } else {
                        observer.onCompleted()
                    }
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func logout(sessionId: String) -> Observable<StarcomResponse> {
        return Observable<StarcomResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "session_destroy" as AnyObject,
                                                  "sid" : sessionId as AnyObject,
                                                  "format" : "json"  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                    if let dictionary = json as? [String: Any] {
                        let response = (try? StarcomResponse.decodeValue(dictionary)) as StarcomResponse?
                        if response != nil {
                            observer.onNext(response!)
                        }
                    } else if let error = json as? String {
                        DDLogDebug("Logout error: \(error)")
                        let response = StarcomResponse(result: nil, error: "starcom_login_error".localized)
                        observer.onNext(response)
                    }
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
    
    func deregister(sessionId: String, vin: String) -> Observable<StarcomResponse> {
        return Observable<StarcomResponse>.create({ (observer) -> Disposable in
            self.network.requestJSON(url: Constant.STARCOM_API_BASE_URL + "api.php",
                                     parameters: ["commandname" : "unset_unit" as AnyObject,
                                                  "sid" : sessionId as AnyObject,
                                                  "vin" : vin as AnyObject,
                                                  "source" : Constant.STARCOM_SOURCE_CLIENT as AnyObject,
                                                  "format" : "json"  as AnyObject],
                                     headers: nil)
                .debug()
                .subscribe(onNext: { (r, json) in
                    if let dictionary = json as? [String: Any] {
                        let response = (try? StarcomResponse.decodeValue(dictionary)) as StarcomResponse?
                        if response != nil {
                            observer.onNext(response!)
                        }
                    } else if let error = json as? String {
                        DDLogDebug("Deregister error: \(error)")
                        let response = StarcomResponse(result: nil, error: "starcom_deregister_error".localized)
                        observer.onNext(response)
                    }
                    observer.onCompleted()
                }, onError: { (error) in
                    DDLogError("On Error")
                    DDLogError(error.localizedDescription)
                }, onCompleted: {
                    DDLogDebug("On Completed")
                }, onDisposed: {
                    DDLogDebug("On Disposed")
                })
        })
    }
}
