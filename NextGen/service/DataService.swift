//
//  DataService.swift
//  nextgen
//
//  Created by Scott Wang on 2/16/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import GoogleMaps
import RxSwift
import RxCocoa
import Locksmith

class DataService {
    
    static let shared = DataService()
    
    private var motorcycleData = [String:MotorcycleData]()
    
    var protocolMajor: Int = 0
    var protocolMinor: Int = 0
    var protocolPatch: Int = 0
    
    var currentVin : String? {
        get {
            let vin = UserDefaults.standard.string(forKey: Constant.PREF_VIN)
            return vin ?? "DefaultVIN"
        }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_VIN) }
    }
    var passcode: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_PASSCODE) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_PASSCODE) }
    }

    var unitNumber: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_STARCOM_UNIT_NUMBER) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_STARCOM_UNIT_NUMBER) }
    }

    var onLoginChange = BehaviorRelay<Bool>(value: false)
    var starcomSessionId: String? {
        didSet {
            if starcomSessionId == nil {
                onLoginChange.accept(false)
            } else {
                onLoginChange.accept(true)
            }
        }
    }

    var currentMotorcycle: MotorcycleData? {
        guard self.currentVin != nil else { return nil }
        return getMotorcycleData(vin: self.currentVin!)
    }
    
    func getMotorcycleData(vin: String) -> MotorcycleData? {
        if (!motorcycleData.keys.contains(vin)) {
            motorcycleData[vin] = MotorcycleData(vin: vin)
        }
        return motorcycleData[vin]
    }
    
    var hasSeenOnboarding: Bool {
        get { return UserDefaults.standard.bool(forKey: Constant.PREF_SEEN_ONBOARDING) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_SEEN_ONBOARDING) }
    }
    
    var hasSeenScheduledChargingIntro: Bool {
        get { return UserDefaults.standard.bool(forKey: Constant.PREF_SEEN_SCHEDULED_INTRO) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_SEEN_SCHEDULED_INTRO) }
    }
    
    var hasConnectedOnce: Bool {
        get { return UserDefaults.standard.bool(forKey: Constant.PREF_HAS_CONNECTED_ONCE) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_HAS_CONNECTED_ONCE) }
    }

    var lastConnectedDate: Date? {
        get { return UserDefaults.standard.object(forKey: Constant.PREF_LAST_CONNECTED_DATE) as? Date }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_LAST_CONNECTED_DATE) }
    }
    
    var lastConnectedDateFormatted: String {
        get {
            guard let lastConnectedDate = self.lastConnectedDate else { return "" }
            let dateFormatter = DateFormatter.init()
            if self.using24HrClock() {
                dateFormatter.dateFormat = "MMMM d, yyyy 'at' H:mm"
            } else {
                dateFormatter.dateFormat = "MMMM d, yyyy 'at' h:mma"
            }
            return dateFormatter.string(from: lastConnectedDate)
        }
    }
    
    var distanceUnitSetting: Settings.DistanceUnit {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_SETTINGS_DISTANCE_UNITS) {
                return Settings.DistanceUnit(rawValue: val as! String)!
            }
            return Settings.DistanceUnit.MILES
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_SETTINGS_DISTANCE_UNITS) }
    }
    
    var usingMetricDistance: Bool {
        return DataService.shared.distanceUnitSetting == Settings.DistanceUnit.KILOMETERS
    }
    
    var temperatureUnitSetting: Settings.Temperature {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_SETTINGS_TEMPERATURE_UNITS) {
                return Settings.Temperature(rawValue: val as! String)!
            }
            return Settings.Temperature.FAHRENHEIT
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_SETTINGS_TEMPERATURE_UNITS) }
    }
    
    var liquidUnitSetting: Settings.Liquid {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_SETTINGS_LIQUID_UNITS) {
                return Settings.Liquid(rawValue: val as! String)!
            }
            return Settings.Liquid.GALLONS
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_SETTINGS_LIQUID_UNITS) }
    }
    
    func using24HrClock() -> Bool {
        return timeFormatSetting == Settings.TimeFormat.TWENTY_FOUR_HOURS
    }
    
    var timeFormatSetting: Settings.TimeFormat {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_SETTINGS_TIME_FORMAT) {
                return Settings.TimeFormat(rawValue: val as! String)!
            }
            return Settings.TimeFormat.AM_PM
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_SETTINGS_TIME_FORMAT) }
    }

    func getLastLocationByStarcomUnit(unitNumber: String) -> CLLocationCoordinate2D? {
        if let val = UserDefaults.standard.value(forKey: Constant.PREF_LAST_LOCATIONS) {
            let locations = val as! [String: [String:String]]
            if let latlon = locations[unitNumber],
                let lat = latlon["lat"], let lon = latlon["lon"],
                let latDbl = Double(lat), let lonDbl = Double(lon) {
                let coord = CLLocationCoordinate2D.init(latitude: latDbl, longitude: lonDbl)
                return coord
            }
        }
        return nil
    }
    
    func putLastLocationByStarcomUnit(lastTransmit: LastTransmit) {
        guard lastTransmit.longitude != nil, lastTransmit.latitude != nil else {
            return
        }
        var locations:[String: [String:String]]
        
        if let val = UserDefaults.standard.value(forKey: Constant.PREF_LAST_LOCATIONS) {
            locations = val as! [String: [String:String]]
        } else {
            locations = [String: [String:String]]()
        }
        
        locations[lastTransmit.unitNumber] = ["lon" : lastTransmit.longitude!,
                                       "lat" : lastTransmit.latitude!]
        
        UserDefaults.standard.set(locations, forKey: Constant.PREF_LAST_LOCATIONS)
    }
    
    var trackMotorcycleUsageLogs: Bool {
        get { return UserDefaults.standard.bool(forKey: Constant.PREF_TRACK_MOTORCYCLE_USAGE_LOGS) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_TRACK_MOTORCYCLE_USAGE_LOGS) }
    }
    
    var lastConnectedPeripheralUUID: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_LAST_CONNECTED_PERIPHERAL) }
        set(value) { UserDefaults.standard.set(value, forKey:Constant.PREF_LAST_CONNECTED_PERIPHERAL) }
    }
    
    var starcomUser: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_STARCOM_USER) }
        set(value) { UserDefaults.standard.set(value, forKey: Constant.PREF_STARCOM_USER) }
    }
    
    var starcomPassword: String? {
        get {
            if let dictionary = Locksmith.loadDataForUserAccount(userAccount: Constant.PREF_STARCOM_USER) {
                return dictionary[Constant.PREF_STARCOM_PASSWORD] as? String
            }
            return nil
        }
        set(value) {
            if value != nil {
                try? Locksmith.saveData(data: [Constant.PREF_STARCOM_PASSWORD: value!], forUserAccount: Constant.PREF_STARCOM_USER)
            } else {
                try? Locksmith.deleteDataForUserAccount(userAccount: Constant.PREF_STARCOM_USER)
            }
        }
    }
    
    func persistLoginCredentials(user: String, password: String) {
        starcomPassword = password
        starcomUser = user
    }
    
    func clearCredentials() {
        starcomPassword = nil
        starcomUser = nil
    }
    
    var loggedIn:Bool {
        get { return starcomSessionId != nil }
    }
    
    var registrationToken: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_STARCOM_REGISTRATION_TOKEN) }
        set(value) { UserDefaults.standard.set(value, forKey: Constant.PREF_STARCOM_REGISTRATION_TOKEN) }
    }
    
    var capturingRide:Bool = false

    var capturedRide:CapturedRide? {
        get {
            if let decoded = UserDefaults.standard.object(forKey: Constant.PREF_CAPTURED_RIDE) as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? CapturedRide
            }
            return nil
        }
        set(value) {
            if value != nil {
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: value!)
                UserDefaults.standard.set(encodedData, forKey: Constant.PREF_CAPTURED_RIDE)
            } else {
                UserDefaults.standard.set(value, forKey: Constant.PREF_CAPTURED_RIDE)
            }
        }
    }
    
    var costOfGas: Double {
        get {
            let value = UserDefaults.standard.double(forKey: Constant.PREF_COST_PER_GALLON)
            if value > 0 {
                return value
            }
            return Constant.DATA_DEFAULT_GAS_COST
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: Constant.PREF_COST_PER_GALLON)
        }
    }
    
    var costOfElectricity: Double {
        get {
            let value = UserDefaults.standard.double(forKey: Constant.PREF_COST_PER_KWATT_HOUR)
            if value > 0 {
                return value
            }
            return Constant.DATA_DEFAULT_ENERGY_COST
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: Constant.PREF_COST_PER_KWATT_HOUR)
        }
    }
    
    var comparableEfficiency: Double {
        get {
            let value = UserDefaults.standard.double(forKey: Constant.PREF_COMPARABLE_EFFICIENCY)
            if value > 0 {
                return value
            }
            return Constant.DATA_DEFAULT_EFFICIENCY
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: Constant.PREF_COMPARABLE_EFFICIENCY)
        }
    }
    
    var fuelEfficiency: Settings.FuelEfficiency {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_FUEL_EFFICIENCY) {
                return Settings.FuelEfficiency(rawValue: val as! String)!
            }
            return Settings.FuelEfficiency.MILES_PER_GALLON
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_FUEL_EFFICIENCY) }
    }
    
    var currency: Settings.Currency {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_CURRENCY) {
                return Settings.Currency(rawValue: val as! String)!
            }
            return Settings.Currency.DOLLARS
        }
        set(value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_CURRENCY) }
    }
    
    func resetComparableSettingsToDefault() {
        currency = .DOLLARS
        fuelEfficiency = .MILES_PER_GALLON
        comparableEfficiency = Constant.DATA_DEFAULT_EFFICIENCY
        costOfElectricity = Constant.DATA_DEFAULT_ENERGY_COST
        costOfGas = Constant.DATA_DEFAULT_GAS_COST
    }
    
    var leftShareStat: Settings.ShareStat {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_LEFT_STAT) {
                return Settings.ShareStat(rawValue: val as! String)!
            }
            return Settings.ShareStat.Distance
        }
        set (value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_LEFT_STAT) }
    }
    var rightShareStat: Settings.ShareStat {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_RIGHT_STAT) {
                return Settings.ShareStat(rawValue: val as! String)!
            }
            return Settings.ShareStat.Duration
        }
        set (value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_RIGHT_STAT) }
    }
    var centerShareStat: Settings.ShareStat {
        get {
            if let val = UserDefaults.standard.value(forKey: Constant.PREF_CENTER_STAT) {
                return Settings.ShareStat(rawValue: val as! String)!
            }
            return Settings.ShareStat.AvgSpeed
        }
        set (value) { UserDefaults.standard.set(value.rawValue, forKey:Constant.PREF_CENTER_STAT) }
    }
    
    var roadsideAssistPhone: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_ROADSIDE_ASSIST_PHONE)}
        set (value) { UserDefaults.standard.set(value, forKey: Constant.PREF_ROADSIDE_ASSIST_PHONE)}
    }

    var roadsideAssistPolicy: String? {
        get { return UserDefaults.standard.string(forKey: Constant.PREF_ROADSIDE_ASSIST_POLICY)}
        set (value) { UserDefaults.standard.set(value, forKey: Constant.PREF_ROADSIDE_ASSIST_POLICY)}
    }
    
    var offlineQueue: [ZeroPacket] {
        get {
            guard let motorcycle = currentMotorcycle else { return [ZeroPacket]() }

            return motorcycle.offlineQueue
        }
        set (value) {
            guard let motorcycle = currentMotorcycle else { return }
            motorcycle.offlineQueue = value
        }
    }
}
