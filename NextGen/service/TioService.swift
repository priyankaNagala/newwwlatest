//
//  TioService.swift
//  nextgen
//
//  Created by David Crawford on 8/20/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Foundation
import RxSwift
import RxCocoa

class TioService: NSObject, TIOManagerDelegate   {
    var bluetoothAvailable = BehaviorRelay<Bool>(value: false)
    var didDiscover = PublishRelay<TIOPeripheral>()

    override init() {
        super.init()
        TIOManager.sharedInstance().delegate = self
        TIOManager.sharedInstance()?.loadPeripherals()

    }
    
    func startScan() -> Observable<TIOPeripheral> {
        DDLogDebug("======= START SCAN =======")
        
        // Start from scratch every single time
        TIOManager.sharedInstance().removeAllPeripherals()
        TIOManager.sharedInstance().disconnectAllPeripherals()
        
        return Observable.create { observer -> Disposable in
            let disposable = self.didDiscover.subscribe({ peripheral in
                if let foundPeripheral = peripheral.element {
                    observer.on(.next(foundPeripheral))
                }
            })
            
            TIOManager.sharedInstance().startScan()
            
            return disposable
        }
    }
    
    func stopScan() {
        TIOManager.sharedInstance().stopScan()
    }
    
    // MARK: TIOManagerDelegate
    func tioManagerBluetoothAvailable(_ manager: TIOManager!) {
        DDLogDebug("TioService: BLUETOOTH AVAILABLE")
        bluetoothAvailable.accept(true)
    }
    
    func tioManagerBluetoothUnavailable(_ manager: TIOManager!) {
        DDLogDebug("TioService: BLUETOOTH NOT AVAILABLE")
        bluetoothAvailable.accept(false)
    }
    
    // We are going to not use any cached peripherals and instead try to discover
    // them every single time we scan. Because of that, we only publish events for
    // the "didDiscover" event.
    func tioManager(_ manager: TIOManager!, didDiscover peripheral: TIOPeripheral!) {
        DDLogDebug("TioService: DID DISCOVER PERIPHERAL")
        didDiscover.accept(peripheral)
    }
    
    func tioManager(_ manager: TIOManager!, didRetrievePeripheral peripheral: TIOPeripheral!) {
        DDLogDebug("TioService: DID RETRIEVE PERIPHERAL")
    }
}
