//
//  GoogleAnalyticsService.swift
//  nextgen
//
//  Created by Scott Wang on 3/15/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import Firebase


final class GoogleAnalyticsService : ZeroAnalyticsService {
    func trackScreenView(viewController: UIViewController, screenName: String) {
        Analytics.setScreenName(screenName, screenClass: viewController.description)
    }
    
    func trackEvent() {
    }
}
