//
//  ZeroMotorcycleServiceImpl.swift
//  nextgen
//
//  Created by David Crawford on 8/21/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import CryptoSwift
import Foundation
import RxSwift
import RxCocoa

class ZeroMotorcycleServiceImpl: NSObject, ZeroMotorcycleService, TIOPeripheralDelegate {
    private var tioService: TioService
    private var dataService: DataService
    private var connectionState = BehaviorRelay<Bool>(value: false)
    private var rawBytes = PublishRelay<String>()
    private var connected: Bool = false
    private var peripheral: TIOPeripheral?
    private var serviceState: ZeroMotorcycleServiceState = ZeroMotorcycleServiceState.INITIAL_CONNECTION
    private var receiveBuffer = Data()
    private var packetReadState: PacketReadState = PacketReadState.EMPTY
    private var malformedWait = false
    private var outstandingPacket: OutstandingPacket?
    private var dataTimeoutDisposable = DisposeBag()
    private var connectionIdleTimeoutDisposable = DisposeBag()
    private var responseTimeoutDisposable = DisposeBag()
    private var queue = [ZeroPacket]()
    private var offlineQueue = [ZeroPacket]()
    private var offlineQueueEmpty = BehaviorRelay<Bool>(value: true)
    private let serialDispatchQueue = DispatchQueue(label: "com.zero.singlepacketqueue")
    private let offlineDispatchQueue = DispatchQueue(label: "com.zero.offlinepacketqueue")
    private let disposeBag = DisposeBag()
    private var bluetoothAvailDisposeBag = DisposeBag()
    private var connectionSequence: ConnectionSequence?
    private var sendRoundRobin = true
    private var bytesReceived = 0
    private var bytesSent = 0
    
    // Subject used for subscriptions for singly send packets
    private var queueSubject = PublishSubject<ZeroPacket>()
    private var bikeInfoSubject = PublishSubject<BikeInfoPacket>()
    private var dashGaugesSubject = PublishSubject<DashGaugesPacket>()
    private var rideModesSubject = PublishSubject<RideModesPacket>()
    private var chargeTargetSubject = PublishSubject<ChargeTargetPacket>()
    
    // Subjects used for special packets/observables
    private var resendSubject = PublishSubject<ResendPacket>()
    private var chargeTargetResendSubject = PublishSubject<ResendPacket>()
    private var scheduledChargingResendSubject = PublishSubject<ResendPacket>()
    

    enum PacketReadState {
        case EMPTY,
        HEAD,
        LENGTH,
        TAG,
        BODY,
        TAIL,
        CRC
    }
    
    // Round Robin
    enum RoundRobinState: Int, CaseIterable {
        case BIKE_INFO, DASH_GAUGES, WAIT
    }
    private var roundRobinInternalInt:Int = RoundRobinState.BIKE_INFO.rawValue
    private var currentRoundRobinState: RoundRobinState {
        get {
            let currentVal = self.roundRobinInternalInt
            self.roundRobinInternalInt = self.roundRobinInternalInt + 1
            return RoundRobinState(rawValue: currentVal % RoundRobinState.allCases.count) ??  RoundRobinState.BIKE_INFO
        }
    }

    class OutstandingPacket {
        var packet: ZeroPacket
        var type: OutstandingPacketType
        
        enum OutstandingPacketType {
            case SINGLE, CONTINUOUS
        }
        
        init(packet: ZeroPacket, type: OutstandingPacketType) {
            self.packet = packet
            self.type = type
        }
    }
    
    init(tioService: TioService, dataService: DataService) {
        self.tioService = tioService
        self.dataService = dataService
        self.offlineQueueEmpty.accept(dataService.offlineQueue.isEmpty)
    }
    
    // MARK: - TioPeripheralDelegate
    
    func tioPeripheralDidConnect(_ peripheral: TIOPeripheral!) {
        DDLogDebug("TIO PERIPHERAL CONNECT")

        serviceState = ZeroMotorcycleServiceState.QUEUE
        self.peripheral = peripheral
        dataService.lastConnectedPeripheralUUID = peripheral.identifier.uuidString
        
        // TODO: Why, oh why do we need this timeout. Get rid of it, if possible.
        _ = Observable<Int>.timer(3, scheduler: MainScheduler.instance)
            .subscribe { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.startConnectionSequence()
                
                // Don't say we are really connected until this timer goes off
                strongSelf.connected = true
                strongSelf.connectionState.accept(true)
                strongSelf.dataService.lastConnectedDate = Date()
            }
        
        // Check for bluetooth avail
        self.tioService.bluetoothAvailable.subscribe { [weak self] event in
            guard let strongSelf = self else { return }
            if let available = event.element {
                if !available {
                    strongSelf.resetConnectionState()
                    strongSelf.bluetoothAvailDisposeBag = DisposeBag()
                }
            }
            }.disposed(by: bluetoothAvailDisposeBag)

        setConnectionIdleTimeout()
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didFailToConnectWithError error: Error!) {
        DDLogDebug("TIO PERIPHERAL FAIL TO CONNECT")

        resetConnectionState()
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didDisconnectWithError error: Error!) {
        DDLogDebug("TIO PERIPHERAL DISCONNECT")

        resetConnectionState()
        stopConnectionIdleTimeout()
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didReceiveUARTData data: Data!) {
        bytesReceived += data.count
        DDLogDebug("didReceiveUARTData")
        DDLogDebug(data.toHexString())
        DDLogDebug("totalBytesReceived: \(bytesReceived)")
        
        if data.count == 0 {
            return
        }
        
        if malformedWait {
            return
        }
        
        receiveBuffer.append(data)
        packetStateMachineTransition()
        setConnectionIdleTimeout()
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didUpdateLocalUARTCreditsCount creditsCount: NSNumber!) {
        if let count = creditsCount {
            DDLogDebug("didUpdateLocalUARTCreditsCount - \(count)")
        }
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didUpdateRemoteUARTCreditsCount creditsCount: NSNumber!) {
        if let count = creditsCount {
            DDLogDebug("didUpdateRemoteUARTCreditsCount - \(count)")
        }
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didUpdateLocalUARTMtuSize mtuSize: NSNumber!) {
        if let size = mtuSize {
            DDLogDebug("didUpdateLocalUARTMtuSize - \(size)")
        }
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didUpdateRemoteUARTMtuSize mtuSize: NSNumber!) {
        if let size = mtuSize {
            DDLogDebug("didUpdateRemoteUARTMtuSize - \(size)")
        }
    }
    
    func resetConnectionState() {
        connected = false
        if (outstandingPacket?.type == OutstandingPacket.OutstandingPacketType.CONTINUOUS) {
            outstandingPacket = nil
        }
        connectionState.accept(false)
        self.peripheral = nil
        serviceState = ZeroMotorcycleServiceState.INITIAL_CONNECTION

    }
    
    // MARK: - Receiving Packets
    
    private let PACKET_HEAD_END = 4
    private let PACKET_LENGTH_END = 8
    private let PACKET_TAG_END = 10 // 10 because of little endian
    private let PACKET_SEQ_TAG_END = 12
    private let PACKET_TAIL_LENGTH = 4
    private let PACKET_CRC_LENGTH = 4
    
    private func packetStateMachineTransition() {
        stopResponseTimeout()
        
        switch(packetReadState) {
        case PacketReadState.EMPTY:
            if (receiveBuffer.count >= PACKET_HEAD_END) {
                if let range = receiveBuffer.range(of: ZeroBtTypes.ZERO_BT_PKT_HEAD.data) {
                    if range.startIndex != 0 {
                        DDLogDebug("HEAD not at start of receive buffer - dropping bytes ahead of header.")
                        // Drop whatever's before the Packet Header
                        receiveBuffer = receiveBuffer.subdata(in: range.startIndex..<receiveBuffer.count)
                    }

                    packetReadState = PacketReadState.HEAD
                    packetStateMachineTransition()
                } else {
                    DDLogDebug("onMalformedPacket: EMPTY")
                    onMalformedPacket()
                }
                return
            }
            setDataTimeout()

        case PacketReadState.HEAD:
            if (receiveBuffer.count >= PACKET_LENGTH_END) {
                let lengthBytes = getReceiveBufferSpecifiedLengthBytes()
                if lengthBytes <= ZeroBtTypes.MAX_BODY_SIZE {
                    packetReadState = PacketReadState.LENGTH
                    packetStateMachineTransition()
                } else {
                    DDLogDebug("onMalformedPacket: HEAD")
                    onMalformedPacket()
                }
                DDLogDebug("numBytes: \(lengthBytes)")
                return
            }
            setDataTimeout()

        case PacketReadState.LENGTH:
            if (receiveBuffer.count >= PACKET_SEQ_TAG_END) {
                let tag = getReceiveBufferTag()
                let firstTag = ZeroBtTypes.Packet.allCases[0]
                let lastTag = ZeroBtTypes.Packet.allCases[ZeroBtTypes.Packet.allCases.count-1]
                if (tag >= firstTag.rawValue && tag <= lastTag.rawValue) {
                    packetReadState = PacketReadState.TAG
                    packetStateMachineTransition()
                } else {
                    DDLogDebug("onMalformedPacket: LENGTH")
                    onMalformedPacket()
                }
                return
            }
            setDataTimeout()

        case PacketReadState.TAG:
            let lengthBytes = getReceiveBufferSpecifiedLengthBytes()
            if (receiveBuffer.count >= PACKET_SEQ_TAG_END + lengthBytes) {
                packetReadState = PacketReadState.BODY
                packetStateMachineTransition()
                return
            }
            setDataTimeout()

        case PacketReadState.BODY:
            let lengthBytes = getReceiveBufferSpecifiedLengthBytes()
            let tailEnd = PACKET_SEQ_TAG_END + lengthBytes + PACKET_TAIL_LENGTH + PACKET_CRC_LENGTH
            if (receiveBuffer.count >= tailEnd) {
                let tail = Data(receiveBuffer.subdata(in: tailEnd-PACKET_TAIL_LENGTH..<tailEnd)).uint32
                if (tail == ZeroBtTypes.ZERO_BT_PKT_TAIL) {
                    packetReadState = PacketReadState.TAIL
                    packetStateMachineTransition()
                } else {
                    DDLogDebug("onMalformedPacket: BODY")
                    onMalformedPacket()
                }
                return
            }
            setDataTimeout()

        case PacketReadState.TAIL:
            let lengthBytes = getReceiveBufferSpecifiedLengthBytes()
            let packetEnd = PACKET_SEQ_TAG_END + lengthBytes + PACKET_TAIL_LENGTH + PACKET_CRC_LENGTH
            if (receiveBuffer.count >= packetEnd) {
                // If there were any other bytes after the end of the packet,
                // drop them on the floor
                receiveBuffer = receiveBuffer.dropLast(receiveBuffer.count - packetEnd)
                
                if (!crcCheckFailed(fullPacket: receiveBuffer)) {
                    packetReadState = PacketReadState.CRC
                    packetStateMachineTransition()
                } else {
                    DDLogDebug("onMalformedPacket: TAIL")
                    onMalformedPacket()
                }
                return
            }
            setDataTimeout()

        case PacketReadState.CRC:
            stopDataTimeout()
            
            // We passed the CRC. Pass this on to anyone who is interested
            let lengthBytes = getReceiveBufferSpecifiedLengthBytes()
            let tag = getReceiveBufferTag()
            let body = receiveBuffer.subdata(in:PACKET_SEQ_TAG_END..<PACKET_SEQ_TAG_END+lengthBytes)
            
            rawBytes.accept(receiveBuffer.toHexString())
            
            DDLogDebug("Packet Tag: \(tag)")
            
            switch(tag) {
            case ZeroBtTypes.Packet.ZERO_BT_PKT_BKINFO.rawValue:
                let bikeInfoPacket = BikeInfoPacket(body)
                if outstandingPacket?.type == OutstandingPacket.OutstandingPacketType.CONTINUOUS {
                    bikeInfoSubject.onNext(bikeInfoPacket)
                } else {
                    queueSubject.onNext(bikeInfoPacket)
                }
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_RESEND.rawValue:
                // If we are in continuous mode, we can drop this packet on the floor
                if outstandingPacket?.type != OutstandingPacket.OutstandingPacketType.CONTINUOUS {
                    receiveBuffer = Data()
                    packetReadState = PacketReadState.EMPTY
                    if let _ = outstandingPacket?.packet as? ChargeTargetPacket {
                        chargeTargetResendSubject.onNext(ResendPacket())
                    } else if let _ = outstandingPacket?.packet as? ScheduledChargingPacket {
                        scheduledChargingResendSubject.onNext(ResendPacket())
                    } else {
                        resendSubject.onNext(ResendPacket())
                    }
                    outstandingPacket = nil
                }
            
            case ZeroBtTypes.Packet.ZERO_BT_PKT_DASH_GAUGES.rawValue:
                let dashGaugesPacket = DashGaugesPacket(body)
                if outstandingPacket?.type == OutstandingPacket.OutstandingPacketType.CONTINUOUS {
                    dashGaugesSubject.onNext(dashGaugesPacket)
                } else {
                    queueSubject.onNext(dashGaugesPacket)
                }

            case ZeroBtTypes.Packet.ZERO_BT_PKT_ACK.rawValue:
                let ackPacket = AckPacket()
                queueSubject.onNext(ackPacket)

            case ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_MODES.rawValue:
                let rideModesPacket = RideModeManufacturingSettingsPacket(body)
                if outstandingPacket?.type == OutstandingPacket.OutstandingPacketType.CONTINUOUS {
                    rideModesSubject.onNext(rideModesPacket)
                } else {
                    queueSubject.onNext(rideModesPacket)
                }

            case ZeroBtTypes.Packet.ZERO_BT_PKT_CHARGE_TARGET.rawValue:
                let chargeTargetPacket = ChargeTargetPacket(body)
                if outstandingPacket?.type == OutstandingPacket.OutstandingPacketType.CONTINUOUS {
                    chargeTargetSubject.onNext(chargeTargetPacket)
                } else {
                    queueSubject.onNext(chargeTargetPacket)
                }
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_CMD_CH.rawValue:
                let commandChannelPacket = CommandChannelPacket(body)
                queueSubject.onNext(commandChannelPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_TIMESTAMP.rawValue:
                let timestampPacket = TimestampPacket(body)
                queueSubject.onNext(timestampPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_ECUS.rawValue:
                let ecuPacket = EcuPacket(body)
                queueSubject.onNext(ecuPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_SHARE.rawValue:
                let rideSharePacket = RideSharePacket(body)
                queueSubject.onNext(rideSharePacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_ERROR_CODES.rawValue:
                let errorCodesPacket = ErrorCodesPacket(body)
                queueSubject.onNext(errorCodesPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_RANGE_ESTIMATE.rawValue:
                let rangePacket = RangeEstimationPacket(body)
                queueSubject.onNext(rangePacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_SCHED_CHARGE.rawValue:
                let scheduledChargingPacket = ScheduledChargingPacket(body)
                queueSubject.onNext(scheduledChargingPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_RIDE_MODES_CUSTOM.rawValue:
                let customRideModesPacket = RideModeCustomSettingsPacket(body)
                queueSubject.onNext(customRideModesPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_INIT.rawValue:
                let logInitPacket = LogInitPacket(body)
                queueSubject.onNext(logInitPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_BLOCK.rawValue:
                let logBlockPacket = LogBlockPacket(body)
                queueSubject.onNext(logBlockPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_VER.rawValue:
                let versionPacket = ProtocolVersionPacket(body)
                queueSubject.onNext(versionPacket)
                
            case ZeroBtTypes.Packet.ZERO_BT_PKT_LOG_DONE.rawValue:
                let logDonePacket = LogDonePacket()
                queueSubject.onNext(logDonePacket)
                
            default:
                DDLogDebug("Unknown Packet Tag")
            }
            
            outstandingPacket = nil
            packetReadState = PacketReadState.EMPTY
            receiveBuffer = Data()
            
            sendOutstandingPackets()
        }
    }
    
    private func onMalformedPacket() {
        stopDataTimeout()
        
        self.malformedWait = true
        _ = Observable<Int>.timer(Constant.BLUETOOTH_MALFORMED_PACKET_WAIT_SECONDS, scheduler: MainScheduler.instance)
            .subscribe { [weak self] in
                guard let strongSelf = self else { return }
                
                DDLogDebug("========= MALFORMED PACKET =========")
                strongSelf.malformedWait = false
                strongSelf.packetReadState = PacketReadState.EMPTY
                strongSelf.receiveBuffer = Data()
                
                if let _ = strongSelf.outstandingPacket?.packet as? ChargeTargetPacket {
                    strongSelf.chargeTargetResendSubject.onNext(ResendPacket())
                } else if let _ = strongSelf.outstandingPacket?.packet as? ScheduledChargingPacket {
                    strongSelf.scheduledChargingResendSubject.onNext(ResendPacket())
                } else {
                    strongSelf.resendSubject.onNext(ResendPacket())
                }
                strongSelf.outstandingPacket = nil
                strongSelf.sendOutstandingPackets()
            }
        
        DDLogDebug("onMalformedPacket")
    }
    
    private func getReceiveBufferSpecifiedLengthBytes() -> Int {
        let bodyLengthInWords = Data(receiveBuffer.subdata(in: PACKET_HEAD_END..<PACKET_LENGTH_END).reversed())
        let wordLength = 4
        return Int(bodyLengthInWords.uint32) * wordLength
    }
    
    private func getReceiveBufferTag() -> UInt16 {
        return Data(receiveBuffer.subdata(in: PACKET_LENGTH_END..<PACKET_TAG_END).reversed()).uint16
    }
    
    private func crcCheckFailed(fullPacket: Data) -> Bool {
        let packetSize = fullPacket.count
        let crcStartIndex = packetSize - PACKET_CRC_LENGTH - PACKET_TAIL_LENGTH
        
        //DDLogDebug("crcCheck")
        //DDLogDebug(fullPacket.toHexString())

        let fullPacketNoCrc = fullPacket.subdata(in: 0..<crcStartIndex)
        let packetCrc = fullPacket.subdata(in: crcStartIndex..<crcStartIndex+PACKET_CRC_LENGTH)
        
        let calculatedCrc = Data(fullPacketNoCrc.crc32().reversed())
        //DDLogDebug(calculatedCrc.toHexString())
        
        return calculatedCrc != packetCrc
    }
    
    private func setDataTimeout() {
        stopDataTimeout()
        
        let disposable = Observable<Int>.timer(Constant.BLUETOOTH_DATA_TIMEOUT_SECONDS, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] value in
                    guard let strongSelf = self else { return }
                    DDLogDebug("DATA TIMEOUT")
                    strongSelf.onMalformedPacket()
                })
        _ = dataTimeoutDisposable.insert(disposable)
    }
    
    private func stopDataTimeout() {
        dataTimeoutDisposable = DisposeBag()
    }
    
    private func setConnectionIdleTimeout() {
        stopConnectionIdleTimeout()
        
        let disposable = Observable<Int>.timer(Constant.BLUETOOTH_CONNECTION_IDLE_TIMEOUT, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] value in 
                guard let strongSelf = self else { return }
                DDLogDebug("CONNECTION IDLE TIMEOUT")
                strongSelf.peripheral?.cancelConnection()

                _ = Observable<Int>.timer(2, scheduler: MainScheduler.instance)
                    .subscribe { [weak self] in
                        guard let _ = self else { return }
                         if let connectionService = SwinjectUtil.sharedInstance.container.resolve(ConnectionService.self) {
                            connectionService.resetBackoff()
                        }
                }
            })
        _ = connectionIdleTimeoutDisposable.insert(disposable)
    }
    
    private func stopConnectionIdleTimeout() {
        connectionIdleTimeoutDisposable = DisposeBag()
    }

    // MARK: - Sending Packets
    private func sendOutstandingPackets() {
        if outstandingPacket != nil {
            DDLogDebug("sendOutstandingPacket: Already an outstanding packet")
            return
        }
    
        guard let motorcycle = self.peripheral else { return }
        
        var nextPacket: ZeroPacket?
        serialDispatchQueue.sync {
            if (queue.count > 0) {
                nextPacket = queue.removeFirst()
            }
        }
        if let packet = nextPacket {
            DDLogDebug("SOP: Sending queued packet: \(packet)")
            
            // Move back to the queue state if we were sending stuff continuously before.
            // Note that if we are in INTIAL_CONNECTION, we won't change state if we are
            // sending queued packets as part of that initial back and forth.
            if (serviceState == ZeroMotorcycleServiceState.CONTINUOUS) {
                serviceState = ZeroMotorcycleServiceState.QUEUE
            }
            outstandingPacket = OutstandingPacket(packet: packet, type: OutstandingPacket.OutstandingPacketType.SINGLE)
            sendData(packet.getFullPacket())
            return
        }
        
        if !motorcycle.isConnected {
            DDLogDebug("sendOutstandingPacket: Not connected")
            return
        }
        
        DDLogDebug("sendOutstandingPacket: No queued packets")

        serviceState = ZeroMotorcycleServiceState.CONTINUOUS
        
        if sendRoundRobin {
            switch currentRoundRobinState {
            case .BIKE_INFO:
                if (self.bikeInfoSubject.hasObservers) {
                    DDLogDebug("SOP: Sending Bike Info")
                    let bikeInfoPacket = BikeInfoPacket()
                    bikeInfoPacket.isEmpty = true
                    self.sendContinuousZeroPacket(packet: bikeInfoPacket)
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.sendOutstandingPackets()
                    }
                }
            case .DASH_GAUGES :
                if self.dashGaugesSubject.hasObservers {
                    DDLogDebug("SOP: Sending Dash Gauges")
                    let dashGaugesPacket = DashGaugesPacket()
                    dashGaugesPacket.isEmpty = true
                    self.sendContinuousZeroPacket(packet: dashGaugesPacket)
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.sendOutstandingPackets()
                    }
                }
            case .WAIT :
                DispatchQueue.main.asyncAfter(deadline: .now() + Constant.BLUETOOTH_CONTINUOUS_PACKET_DELAY) {
                    self.sendOutstandingPackets()
                }
            }
        } else {
            // Skipping round robin
            DDLogDebug("sendOutstandingPacket: Skipping round robin")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.sendOutstandingPackets()
            }
        }
    }
    
    private func sendContinuousZeroPacket(packet: ZeroPacket) {
        outstandingPacket = OutstandingPacket(packet: packet, type: OutstandingPacket.OutstandingPacketType.CONTINUOUS)
        sendData(packet.getFullPacket())
    }
    
    private func sendData(_ data: Data) {
        bytesSent += data.count
        DDLogDebug("writeUARTData")
        DDLogDebug(data.toHexString())
        DDLogDebug("totalBytesSent: \(bytesSent)")
        
        setResponseTimeout()
        receiveBuffer = Data()
        peripheral?.writeUARTData(data)
    }
    
    private func setResponseTimeout() {
        stopResponseTimeout()
        
        let disposable = Observable<Int>.timer(Constant.BLUETOOTH_DATA_TIMEOUT_SECONDS, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self else { return }
                DDLogDebug("RECEIVE TIMEOUT")
                strongSelf.onMalformedPacket()
            })
        _ = responseTimeoutDisposable.insert(disposable)
    }
    
    private func stopResponseTimeout() {
        responseTimeoutDisposable = DisposeBag()
    }
    
    private func startConnectionSequence() {
        self.connectionSequence = ConnectionSequence(zeroMotorcycleService: self, dataService: dataService)
        guard let connectionSequence = self.connectionSequence else { return }

        connectionSequence.start().subscribe(onNext: { (state) in
            // DO nothing
        }, onError: { (error) in
            DDLogDebug("Connection Sequence Error: \(error)")
        }, onCompleted: {
            self.sendOutstandingPackets()
        }) {
            // DO Nothing
        }.disposed(by: disposeBag)
    }
    
    // MARK: - ZeroMotorcycleService
    func connect(peripheral: TIOPeripheral) {
        peripheral.delegate = self
        peripheral.connect()
    }
    
    func getConnectionState() -> BehaviorRelay<Bool> {
        return connectionState
    }
    
    func isConnected() -> Bool {
        if let peripheral = self.peripheral {
            return peripheral.isConnected
        }
        return false
//        return connected
    }
    
    func isConnecting() -> Bool {
        if let peripheral = self.peripheral {
            return peripheral.isConnecting
        }
        return false
    }
    
    func getConnectedPeripheral() -> TIOPeripheral? {
        return peripheral
    }

    func sendSingleZeroPacket(packet: ZeroPacket, priority: Bool) -> Observable<ZeroPacket> {
        serialDispatchQueue.sync {
            if priority {
                queue.insert(packet, at: 0)
            } else {
                queue.append(packet)
            }
        }
        sendOutstandingPackets()
        return queueSubject
    }
    
    // Replace existing like packets in the queue before adding packet to queue
    // Used if this packet value will overwrite previous write packet
    func sendSingleZeroPacketReplacingSameType(packet: ZeroPacket, priority: Bool) -> Observable<ZeroPacket> {
        serialDispatchQueue.sync {
            queue = queue.filter({ (queuedPacket) -> Bool in
                object_getClassName(packet) != object_getClassName(queuedPacket)
            })
        }
        return sendSingleZeroPacket(packet:packet, priority: priority)
    }
    
    func queueSingleZeroPacket(packet: ZeroPacket) {
        // Remove previously inserted packet of same type
        offlineDispatchQueue.sync {
            offlineQueue = dataService.offlineQueue
            offlineQueue = offlineQueue.filter( {(queuedPacket) -> Bool in
                object_getClass(packet) != object_getClass(queuedPacket)
            })
            offlineQueue.append(packet)
            dataService.offlineQueue = offlineQueue
            offlineQueueEmpty.accept(dataService.offlineQueue.isEmpty)
        }
    }

    func offlineQueueIsEmpty() -> BehaviorRelay<Bool> {
        return offlineQueueEmpty
    }
    
    func clearOfflineQueue() {
        offlineDispatchQueue.sync {
            offlineQueue.removeAll()
            dataService.offlineQueue = offlineQueue
            offlineQueueEmpty.accept(dataService.offlineQueue.isEmpty)
        }
    }
    
    func sendOfflinePackets() {
        offlineDispatchQueue.sync {
            offlineQueue = dataService.offlineQueue
            for packet in offlineQueue {
                _ = sendSingleZeroPacket(packet: packet, priority: false)
            }
            offlineQueue.removeAll()
            dataService.offlineQueue = offlineQueue
            offlineQueueEmpty.accept(dataService.offlineQueue.isEmpty)
        }
    }
    
    func getResendSubject() -> Observable<ResendPacket> {
        return resendSubject
    }
    
    func getBikeInfo() -> Observable<BikeInfoPacket> {
        return bikeInfoSubject
    }
    
    func getDashGauges() -> Observable<DashGaugesPacket> {
        return dashGaugesSubject
    }
    
    func getChargeTarget() -> Observable<ChargeTargetPacket> {
        return chargeTargetSubject
    }
    
    func getChargeTargetResend() -> Observable<ResendPacket> {
        return chargeTargetResendSubject
    }
    
    func getScheduledChargingResend() -> Observable<ResendPacket> {
        return scheduledChargingResendSubject
    }
    
    public func getPacketByteOutput() -> PublishRelay<String> {
        return rawBytes
    }
    
    public func sendRawBytes(_ data: Data) -> PublishRelay<String> {
        sendData(data)
        return rawBytes
    }
    
    public func setSendRoundRobin(_ send: Bool) {
        sendRoundRobin = send
    }
}
