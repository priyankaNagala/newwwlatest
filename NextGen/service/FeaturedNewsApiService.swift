//
//  FeaturedNewsApiService.swift
//  nextgen
//
//  Created by Scott Wang on 3/13/18.
//  Copyright © 2018 Zero Motorcycles. All rights reserved.
//

import Foundation
import RxSwift
import Himotoki
import CocoaLumberjack

class FeaturedNewsApiService {
    
    private let network: Networking
    
    var disposeBag = DisposeBag()
    
    public init(network: Networking) {
        self.network = network
    }
    
    func api() -> Observable<FeaturedNews> {
        return Observable<FeaturedNews>.create({ (observer) -> Disposable in
            self.network.requestJSON(
                url: Constant.FEATURED_NEWS_API_BASE_URL,
                parameters: [String:AnyObject](),
                headers: ["Accept-Language": Locale.current.identifier])
            .debug()
            .subscribe(onNext: { (r, json) in
                if let featuredNews = (try? FeaturedNews.decodeValue(json)) as FeaturedNews? {
                    observer.onNext(featuredNews)
                }
                observer.onCompleted()
            }, onError: { (error) in
                DDLogError("On Error")
                DDLogError(error.localizedDescription)
            }, onCompleted: {
                DDLogDebug("On Completed")
            }, onDisposed: {
                DDLogDebug("On Disposed")
            })
        })
    }
}
