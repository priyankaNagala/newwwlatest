//
//  AppDelegate.swift
//  nextgen
//
//  Created by David Crawford on 10/26/17.
//  Copyright © 2017 Zero Motorcycles. All rights reserved.
//

import CocoaLumberjack
import Crashlytics
import Firebase
import GoogleMaps
import Swinject
import SwinjectStoryboard
import UIKit
import UserNotifications
import GoogleSignIn
import RxCocoa
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?
    var disposeBag = DisposeBag()
    var fileLogger: DDFileLogger = DDFileLogger()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Initialized Swinject Container
        _ = SwinjectUtil.sharedInstance
        
        // Initialize shared App instance
        _ = App.sharedInstance
        
        // Cocoa Lumberjack
        DDLog.add(DDASLLogger.sharedInstance)
        DDTTYLogger.sharedInstance.colorsEnabled = true
        
        fileLogger.doNotReuseLogFiles = true
        fileLogger.logFileManager.maximumNumberOfLogFiles = 5
        DDLog.add(fileLogger)

        // Google Maps
        GMSServices.provideAPIKey(Constant.GOOGLE_MAPS_API_KEY)
        
        // Initialize Google sign-in
        GIDSignIn.sharedInstance().clientID = "877803741246-lqdphc8m1gek5e0443ce2ae5j9n7c3f9.apps.googleusercontent.com"
        
        // Firebase
        let filePath: String!
        #if DEBUG
            filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
        #else
            filePath = Bundle.main.path(forResource: "GoogleServiceProd-Info", ofType: "plist")!
        #endif
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        Messaging.messaging().delegate = self
        //registerForPushNotifications()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.black
        window.makeKeyAndVisible()
        self.window = window

        if (!DataService.shared.hasSeenOnboarding) {
            UILauncher.launchWelcome()
        } else {
            UILauncher.launchRootTabBar()
        }

        UIAppearanceManager.establishAppAppearance()
        
        NotificationCenter.default.addObserver(self, selector: #selector(becomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resignActive), name: .UIApplicationWillResignActive, object: nil)

        // Fabric Needs to be the last lines in didFinishLaunching
        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    @objc func becomeActive() {
        if let connectionService = SwinjectUtil.sharedInstance.container.resolve(ConnectionService.self) {
            connectionService.start()
        }
    }
    
    @objc func resignActive() {
        if let connectionService = SwinjectUtil.sharedInstance.container.resolve(ConnectionService.self) {
            connectionService.stop()
        }
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                DDLogError("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                DDLogDebug("Remote instance ID token: \(result.token)")
                let starcomApiService = SwinjectUtil.sharedInstance.container.resolve(StarcomApiService.self)!
                let dataService = SwinjectUtil.sharedInstance.container.resolve(DataService.self)!
                if let sessionId = dataService.starcomSessionId {
                    var prefix = "zerop:"
                    #if DEBUG
                        prefix = "zerod:"
                    #endif
                    let regToken = prefix + result.token
                    dataService.registrationToken = regToken
                    Starcom.shared.registerForNotifications(starcomApiService: starcomApiService, sessionId: sessionId, registrationId: regToken).subscribe { event in
                        DDLogDebug("FCM Token Sent")
                    }.disposed(by: self.disposeBag)
                }
                // Subscribe to topics
                
                if let model = dataService.currentMotorcycle?.model {
                    Messaging.messaging().subscribe(toTopic: model) { error in
                        if error == nil {
                            DDLogDebug("Subscribed to \(model) topic")
                        } else {
                            DDLogError("Failed to subscribe to \(model) topic: \(error!)")
                        }
                    }
                }
                let locale = Locale.current.identifier
                Messaging.messaging().subscribe(toTopic: locale) { error in
                    if error == nil {
                        DDLogDebug("Subscribed to \(locale) topic")
                    } else {
                        DDLogError("Failed to subscribe to \(locale) topic: \(error!)")
                    }
                }
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        DDLogDebug("applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        DDLogDebug("applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        DDLogDebug("applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        DDLogDebug("applicationDidBecomeActive")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        DDLogDebug("applicationWillTerminate")
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        handleNotification(userInfo: userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        handleNotification(userInfo: userInfo)
    }
    
    func handleNotification(userInfo: [AnyHashable : Any]) {
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            DDLogDebug("Message ID: \(messageID)")
        }
        
        // Print full message.
        DDLogDebug("\(userInfo)")

        let title = AppDelegate.parseTitle(userInfo: userInfo)
        let body = AppDelegate.parsseBody(userInfo: userInfo)
        
        if (title.count > 0 || body.count > 0) {
            showNotificationAlert(title: title, body: body)
        }
    }
    
    func showNotificationAlert(title: String, body: String) {
        let alert = UIAlertController(
            title: title.count > 0 ? title : "push_notification".localized,
            message: body,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "global_ok".localized, style: .default, handler: nil))
        window?.rootViewController?.present(alert, animated: true)
    }
    
    class func parseTitle(userInfo: [AnyHashable : Any]) -> String {
        var title = ""
        if let aps = userInfo["aps"] as? [AnyHashable : Any], let alert = aps["alert"] as? [AnyHashable : Any] {
            if let titleKey = alert["title-loc-key"] as? String {
                title = titleKey.localized
                if let args = alert["title-loc-args"] as? [String] {
                    title = String.init(format: title, args)
                }
            } else {
                title = alert["title"] as? String ?? ""
            }
        }
        return title
    }
    
    class func parsseBody(userInfo: [AnyHashable : Any]) -> String {
        var body = ""
        if let aps = userInfo["aps"] as? [AnyHashable : Any], let alert = aps["alert"] as? [AnyHashable : Any] {
            if let bodyKey = alert["loc-key"] as? String {
                body = bodyKey.localized
                if let args = alert["loc-args"] as? [String] {
                    body = String.init(format: body, arguments: args)
                }
            } else {
                body = alert["body"] as? String ?? ""
            }
        }
        return body
    }
}

extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        DDLogDebug("Firebase registration token: \(fcmToken)")
        
        let starcomApiService = SwinjectUtil.sharedInstance.container.resolve(StarcomApiService.self)!
        let dataService = SwinjectUtil.sharedInstance.container.resolve(DataService.self)!
        if let sessionId = dataService.starcomSessionId {
            var prefix = "zerop:"
            #if DEBUG
            prefix = "zerod:"
            #endif
            let regToken = prefix + fcmToken
            dataService.registrationToken = regToken
            Starcom.shared.registerForNotifications(starcomApiService: starcomApiService, sessionId: sessionId, registrationId: regToken).subscribe { event in
                DDLogDebug("FCM Token Sent")
            }.disposed(by: self.disposeBag)
        }
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        DDLogDebug("Received data message: \(remoteMessage.appData)")
    }
}

